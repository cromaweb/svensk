<?php
class SYSTEM_Controllers_Front extends Zend_Controller_Action {
	
    public function init() {
    	$sessionNamespace = new Zend_Session_Namespace(SESSION_BASE);

    	if (isset($sessionNamespace->usuario_indicador)) {
    		$this->view->assign('sponsor', $sessionNamespace->usuario_indicador);
    	} else {
    		$this->view->assign('sponsor', "NONE");
    	}

    	$this->view->assign('controllerName', $controllerName = $this->_request->getControllerName());


        $currency = curl_init();
        curl_setopt($currency, CURLOPT_URL, "https://blockchain.info/en/ticker");
        curl_setopt($currency, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($currency, CURLOPT_HEADER, FALSE);
        $r = curl_exec($currency);

        curl_close($currency);

        if (!empty($r)) {
            $arr = json_decode($r);
            $this->view->assign('btc_usd',$arr->USD->last);
            $this->view->assign('btc_gbp',$arr->GBP->last);
            $this->view->assign('btc_eur',$arr->EUR->last); 
        } else {
            $this->view->assign('btc_usd','ERROR');
            $this->view->assign('btc_gbp','ERROR');
            $this->view->assign('btc_eur','ERROR');
        }
        
	}
}
?>