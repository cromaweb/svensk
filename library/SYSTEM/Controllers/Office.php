<?php

class SYSTEM_Controllers_Office extends Zend_Controller_Action  {
	
	public function init() {
		parent::init();

		$sessao = new Zend_Session_Namespace(SESSION_OFFICE);
		
		if (isset($sessao->coduser)) {
			$sessao->setExpirationSeconds(1200);
			$this->view->assign('User',$sessao->coduser);
			$codUser = $this->view->User;
			$this->view->packageNameTopMenu = $sessao->namePackage;
			$this->view->username = $sessao->username;
			$this->view->idTitulo = $sessao->idTitulo;
			$this->view->nomeTitulo = $sessao->nomeTitulo;
			$this->view->imagemTitulo = $sessao->imagemTitulo;
			$this->view->namePackage = $sessao->namePackage;
			$this->view->imagePackage = $sessao->imagePackage;
			$this->view->rede = $sessao->rede;
			$this->view->validate = $sessao->validate;
			$this->view->userType = $sessao->typeUser;
			$this->view->userLanguage = $sessao->userLanguage;
			$this->view->userCountry = $sessao->userCountry;
			$this->view->assign('primeiroLogin',$sessao->primeiroLogin);
			$this->view->assign('arrayLanguage',$sessao->arrayLanguage);

			$translate = $sessao->arrayLanguage;

			$translate = new Zend_Translate(array('adapter' => 'array', 'content' => $translate, 'locale' => "$sessao->userLanguage"));
			
			$this->view->tr = $translate;
		}

		$controllerName = $this->_request->getControllerName();
		$actionName = $this->_request->getActionName();
		$this->view->Controlador = $controllerName;

		if ($controllerName != 'signup' && $controllerName != 'ref' && $controllerName != 'confirmchange' && $controllerName != 'routines') {
			if (!isset($sessao->coduser) || empty($sessao->coduser)) {
				if ($controllerName != 'login') {
					$this->_redirect(LINK_OFFICE.'/login');
				}
			} else {
				if ($controllerName == 'login') {
					$this->_redirect(LINK_OFFICE.'/home');
				}
				if ($sessao->primeiroLogin == 'S'  && $controllerName != 'logout' && $controllerName != 'account' && $actionName != 'update' && $actionName != 'save') {
					$this->_redirect(LINK_OFFICE.'/account/update');
				}
			}
		}

	}
}

?>
