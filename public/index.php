<?php
// Define path to root directory
defined('ROOT_PATH')
        || define('ROOT_PATH', realpath(dirname(__FILE__) . '/../'));

// Define path to application directory
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../application'));

// Define application environment
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV',
              (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV')
                                         : 'production'));


//define('LINK_DEFAULT', 'https://'.$_SERVER['SERVER_NAME']);
define('LINK_DEFAULT', 'https://'.$_SERVER['SERVER_NAME']);
define('SITE_NAME', 'Svensk');
define('LINK_OFFICE', 'https://'.$_SERVER['SERVER_NAME']);
define('FOLDER_OFFICE','svensk');
define('SITE_DOMAIN','svensk.io');
define('SITE_SUBDOMAIN',$_SERVER['HTTP_HOST']);
define('EMAIL_SUPPORT','contact@svensk.io');
$server_prod = "backoffice.svensk.io";
define('RECAPTCHA_BACK','6Ld0T_8UAAAAANS0CCznLa-ZhK3QYLgMGL_d8rP1');
define('RECAPTCHA_FRONT','6Ld0T_8UAAAAAFCitORekWdB1FeV3sn5lo1ZhkzZ');
define('RECAPTCHA_ACTIVE','0');


define('USER_SENDGRID', 'ethernize@cromaweb.com.br');
define('PASS_SENDGRID', 'varVOZkDG13sd9pQ');
define('NOREPLY_SENDGRID', 'no-reply@svensk.io');
define('NAME_SENDGRID', 'Svensk');
define('SERVER_SENDGRID', 'smtp-relay.sendinblue.com');

define('VALIDATOR', 'documentation@svensk.io');
define('EMAIL_ADMIN_DEPOSIT', 'dev@ethernize.com');

defined('SESSION_BASE') || define('SESSION_BASE', 'SITE');
defined('SESSION_OFFICE') || define('SESSION_OFFICE', 'OFFICE');


// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH . '/../library')
   // get_include_path(),
)));

/** Zend_Application */
require_once 'Zend/Application.php';

//Se for igual ao servidor de produção, aplica as configurações de produção
if($_SERVER['HTTP_HOST']==$server_prod)
{
    // Create application, bootstrap, and run
    $application = new Zend_Application(
        APPLICATION_ENV,
        APPLICATION_PATH . '/configs/application.ini'
    );
    //Se não aplica as configurações de teste
}else{
    // Create application, bootstrap, and run
    $application = new Zend_Application(
        APPLICATION_ENV,
        APPLICATION_PATH . '/configs/application_dev.ini'
    ); 
}

$application->bootstrap()
            ->run();			