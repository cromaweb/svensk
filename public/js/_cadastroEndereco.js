$(document).ready(function()
{			
	$('#form_cadastro').submit(function()
	{
		var retorno;
		retorno = notnull($('#cad_cep').val(), $('#cad_cep').focus(), "CEP"); if (retorno == false) { return false; }
		retorno = validaCep($('#cad_cep').val(), $('#cad_cep').focus(), "CEP"); if (retorno == false) { return false; }
		retorno = notnull($('#cad_endereco').val(), $('#cad_endereco').focus(), "Endereço"); if (retorno == false) { return false; }
		retorno = notnull($('#cad_numero').val(), $('#cad_numero').focus(), "Número"); if (retorno == false) { return false; }
		retorno = noteletra($('#cad_numero').val(), $('#cad_numero').focus(), "Número"); if (retorno == false) { return false; }
		retorno = notnull($('#cad_bairro').val(), $('#cad_bairro').focus(), "Bairro"); if (retorno == false) { return false; }
		retorno = notnull($('#cad_cidade').val(), $('#cad_cidade').focus(), "Cidade"); if (retorno == false) { return false; }
		retorno = notnull($('#cad_estado').val(), $('#cad_estado').focus(), "Estado"); if (retorno == false) { return false; }

		var enderecoEntrega = 0;

		if($("#cad_entrega").attr("checked"))
		{
			enderecoEntrega = 1;
			retorno = notnull($('#cad_cep_entrega').val(), $('#cad_cep_entrega').focus(), "CEP"); if (retorno == false) { return false; }
			retorno = validaCep($('#cad_cep_entrega').val(), $('#cad_cep_entrega').focus(), "CEP"); if (retorno == false) { return false; }
			retorno = notnull($('#cad_endereco_entrega').val(), $('#cad_endereco_entrega').focus(), "Endereço"); if (retorno == false) { return false; }
			retorno = notnull($('#cad_numero_entrega').val(), $('#cad_numero_entrega').focus(), "Número"); if (retorno == false) { return false; }
			retorno = noteletra($('#cad_numero_entrega').val(), $('#cad_numero_entrega').focus(), "Número"); if (retorno == false) { return false; }
			retorno = notnull($('#cad_bairro_entrega').val(), $('#cad_bairro_entrega').focus(), "Bairro"); if (retorno == false) { return false; }
			retorno = notnull($('#cad_cidade_entrega').val(), $('#cad_cidade_entrega').focus(), "Cidade"); if (retorno == false) { return false; }
			retorno = notnull($('#cad_estado_entrega').val(), $('#cad_estado_entrega').focus(), "Estado"); if (retorno == false) { return false; }
		}

		if (confirm('Confirmar atualização de cadastro?'))
		{
			$(".cad_salvar").attr('disabled', 'disabled');
			$.post("libs/_salvarEndereco.php", { dados : $('#form_cadastro').serialize() }, function (data){
				if(data == "apelido")
				{
					alert('O Apelido digitado não está disponível.');
					$("#cad_apelido").focus();
					$(".cad_salvar").attr('disabled', '');
				}
				else if(data == "email")
				{
					alert('Este endereço de email não pode ser utilizado.');
					$("#cad_email").focus();
					$(".cad_salvar").attr('disabled', '');
				}
				else
				{
					alert(data);
					window.location = "./cadastro";
				}
			});
		}
	});
});