$(document).ready(function()
{
	$(".loading").hide();
	
	$("#bt_quitarFatura").click(function(e)
	{
		if ($("#quitarFatura").is(":visible")) 
		{
			$("#quitarFatura").hide();
			$("#quitarFatura2").hide();
			$("#saqueSaldo").hide();
			$("#saqueSaldo2").hide();
			$("#transferenciaInterna").hide();
			$("#transferenciaInterna2").hide();
		}else{
			$("#quitarFatura").show();
			$("#quitarFatura2").show();
			$("#saqueSaldo").hide();
			$("#saqueSaldo2").hide();
			$("#transferenciaInterna").hide();
			$("#transferenciaInterna2").hide();
		}
	});
	
	$("#bt_saque").click(function(e)
	{
		
		if ($("#saqueSaldo").is(":visible")) 
		{
			$("#quitarFatura").hide();
			$("#quitarFatura2").hide();
			$("#saqueSaldo").hide();
			$("#saqueSaldo2").hide();
			$("#transferenciaInterna").hide();
			$("#transferenciaInterna2").hide();
		}else{
			$("#quitarFatura").hide();
			$("#quitarFatura2").hide();
			$("#saqueSaldo").show();
			$("#saqueSaldo2").show();
			$("#transferenciaInterna").hide();
			$("#transferenciaInterna2").hide();
		}
	});
	
     $('#trans_value').priceFormat({
          prefix: '',
          centsSeparator: '.',
          thousandsSeparator: ',',
          limit: 8
     });
	 


});


function transferCredit()
{
	if(confirm('Confirms balance transfer?'))
	{
		if ($('#trans_username').val() != "" && $('#trans_value').val() != "")
		{
			$(".loading").show();
			//alert($("#valorRequisitado").val());
			$.post('libs/_transferCredit.php',
			{
				act : 1,
				username : $('#trans_username').val(),
				transferValue : $('#trans_value').val(),
				codeOfc : $("#codeOfc").val(),
				trans_secpass : $("#trans_secpass").val()
			},
			function(data)
			{
					//alert(data);
					if(data=="transferok"){
						alert("Transfer was successful!");
						location.reload();
					}else{
						$(".loading").hide();
						$("#viewInvoice").html(data);
						$("#viewInvoice").show();
					}

			});
		}
		else
		{
			alert("Empty field!");
		}
	}
}

function payInvoice()
{

	if ($('#payInv_invoice').val() != "" && $('#payInv_username').val() != "")
	{
		$("#viewInvoice").html('');
		$(".loading").show();
		$.post('libs/_payInvoice.php',
		{
			act : 1,
			invoice : $('#payInv_invoice').val(),
			username : $('#payInv_username').val(),
			codeOfc : $("#codeOfc").val()
		},
		function(data)
		{

			$(".loading").hide();
			$("#viewInvoice").html(data);
			$("#viewInvoice").show();
		});
	}
	else
	{
		alert("Empty field!");
	}
}



	
function withdrawBalance()
{
	if(confirm('Confirm the withdrawal request?'))
	{
		if ($('#amountWithdrawn').val() != "")
		{
			$("#inline1").html('');
			$(".loading").show();
			//alert($("#valorRequisitado").val());
			$.post('libs/_requestingWithdrawal.php',
			{
				op : 2,
				valorSaque : $('#amountWithdrawn').val(),
				codeOfc : $("#codeOfc").val()
			},
			function(data)
			{
					//alert(data);
					if(data=="withdrawn"){
						alert("Withdrawal request successfully!");
						location.reload();
					}else{
						$(".loading").hide();
						$("#inline1").html(data);
						$("#inline1").show();
					}

			});
		}
		else
		{
			alert("Empty field!");
		}
	}
}


var TipoNavegador = navigator.appName;

function search(evento)
{
	if (TipoNavegador=="Netscape") { if (evento.which == 13) { $(".bt_search").click(); } } else { if (evento.keyCode == 13) { $(".bt_search").click(); } }
}