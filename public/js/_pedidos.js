$(document).ready(function(e) {
	var pacote		= null;
	
	$(".green").css({opacity:'0.7'});
	$(".blue").css({opacity:'0.7'});
	$(".black").css({opacity:'0.7'});
	
	$(".green a").hover(function(e){
		$(".green").css({opacity:'1'});
		$(".green").css('box-shadow','4px 4px 7px #666');
	},function(e){
		if(pacote != 2) {
			$(".green").css({opacity:'0.7'});
			$(".green").css('box-shadow','0px 0px 0px');
		}
	});
	
	$(".blue a").hover(function(e){
		$(".blue").css({opacity:'1'});
		$(".blue").css('box-shadow','4px 4px 7px #666');
	},function(e){
		if(pacote != 3) {
			$(".blue").css({opacity:'0.7'});
			$(".blue").css('box-shadow','0px 0px 0px');
		}
	});
	
	$(".black a").hover(function(e){
		$(".black").css({opacity:'1'});
		$(".black").css('box-shadow','4px 4px 7px #666');
	},function(e){
		if(pacote != 4) {
			$(".black").css({opacity:'0.7'});
			$(".black").css('box-shadow','0px 0px 0px');
		}
	});
	
	$(".green a").click(function(e){
		$(".green").css({opacity:'1'});
		$(".blue").css({opacity:'0.7'});
		$(".black").css({opacity:'0.7'});
		
		$(".green").css('box-shadow','4px 4px 7px #666');
		$(".blue").css('box-shadow','0px 0px 0px');
		$(".black").css('box-shadow','0px 0px 0px');
		
		$(".avancar_compra").show();
		$(".avancar_upgrade").show();
		
		pacote 		= 2;
		$("#combo").val('2');
		$("#valor").val($("#valor_2").val());
	});
	
	$(".blue a").click(function(e){
		$(".green").css({opacity:'0.7'});
		$(".blue").css({opacity:'1'});
		$(".black").css({opacity:'0.7'});
		
		$(".green").css('box-shadow','0px 0px 0px');
		$(".blue").css('box-shadow','4px 4px 7px #666');
		$(".black").css('box-shadow','0px 0px 0px');
		
		$(".avancar_compra").show();
		$(".avancar_upgrade").show();
		
		pacote		= 3;
		$("#combo").val('3');
		$("#valor").val($("#valor_3").val());
	});
	
	$(".black a").click(function(e){
		$(".green").css({opacity:'0.7'});
		$(".blue").css({opacity:'0.7'});
		$(".black").css({opacity:'1'});
		
		$(".green").css('box-shadow','0px 0px 0px');
		$(".blue").css('box-shadow','0px 0px 0px');
		$(".black").css('box-shadow','4px 4px 7px #666');
		
		$(".avancar_compra").show();
		$(".avancar_upgrade").show();
		
		pacote		= 4;
		$("#combo").val('4');
		$("#valor").val($("#valor_4").val());
	});
	
	$(".avancar_compra").click(function(e){
		$("#pedidoCombo").submit();
	});
	
	$(".avancar_upgrade").click(function(e){
		$("#upgradeCombo").submit();
	});
});