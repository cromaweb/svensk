$(document).ready(function()
{
	$('.box').css('marginTop',($(document).height() / 2) - 250);
	
	if ($('#novasol').val() != "sim")
	{
		$('.frm_login').show();
		$('.frm_lembr').hide();
		$('.frm_lemid').hide();
		$('#lgn_user').focus();
		getCaptcha('lgn');
	}
	else
	{
		$('.frm_lembr').show();
		$('.frm_login').hide();
		$('.frm_lemid').hide();
		$('#lmb_cpf').focus();
		getCaptcha('lmb');
	}
});

$(window).resize(function(e) {
	$('.box').css('marginTop',($(document).height() / 2) - 250);
});

function acessar()
{
	$('.imprime_error').hide();
	$('.frm_lemid').fadeOut('slow');
	$('.frm_lembr').fadeOut('slow');
	$('.frm_login').fadeIn('slow');
	$('#lgn_user').val('');
	$('#lgn_pass').val('');
	$('#lgn_user').focus();
	getCaptcha('lgn');
}

function lembrete()
{
	$('.imprime_msg').hide();
	$('.imprime_error').hide();
	$('.frm_login').fadeOut('slow');
	$('.frm_lembr').fadeIn('slow');
	$('#lmb_cpf').val('');
	$('#lmb_email').val('');
	$('#lmb_cpf').focus();
	getCaptcha('lmb');
}

function meuid()
{
	$('.imprime_id').hide();
	$('.imprime_nm').hide();
	$('.imprime_error').hide();
	$('.frm_login').fadeOut('slow');
	$('.frm_lemid').fadeIn('slow');
	$('#lid_cpf').val('');
	$('#lid_email').val('');
	$('#lid_cpf').focus();
	getCaptcha('lid');
}

function validar(campo, teste, valor, foco)
{
	if (campo == teste)
	{
		if (valor == "US")
		{
			$('.imprime_error').html("Enter the Login.");
			$('.imprime_error').show();
		}
		if (valor == "PS")
		{
			$('.imprime_error').html("Enter the Password.");
			$('.imprime_error').show();
		}
		if (valor == "CP")
		{
			$('.imprime_error').html("Enter the code.");
			$('.imprime_error').show();
		}
		return false;
	}

	foco;
	return true;
}

function validarmail(vemail)
{
	if ((vemail.indexOf("@") == -1) || (vemail.indexOf(".") == -1)) { return false; } else { return true; }
}

function validacpf(cpf)
{
	var newcpf = ""
	newcpf = newcpf + cpf.substr(0,3);
	newcpf = newcpf + cpf.substr(3,3);
	newcpf = newcpf + cpf.substr(6,3);
	newcpf = newcpf + cpf.substr(9,2);
	var dv = cpf.substr(9,2);

	if ((newcpf=="") || 
		(newcpf=="00000000000") || 
		(newcpf=="11111111111") || 
		(newcpf=="22222222222") || 
		(newcpf=="33333333333") ||
		(newcpf=="44444444444") ||
		(newcpf=="55555555555") ||
		(newcpf=="66666666666") ||
		(newcpf=="77777777777") ||
		(newcpf=="88888888888") ||
		(newcpf=="99999999999"))
		{
			return false;
		} 
	var d1 = 0;
	for (i = 0; i < 9; i++)
	{
		d1 += newcpf.charAt(i)*(10-i);
	}
	if (d1 == 0)
	{
		return false;
	}
	d1 = 11 - (d1 % 11);
	if (d1 > 9) d1 = 0;
	if (dv.charAt(0) != d1)
	{
		return false;
	}
	d1 *= 2;
	for (i = 0; i < 9; i++)
	{
		d1 += newcpf.charAt(i)*(11-i);
	}
	d1 = 11 - (d1 % 11);
	if (d1 > 9) d1 = 0;
	if (dv.charAt(1) != d1)
	{
		return false;
	}

	return true;
}

function getCaptcha(campo)
{
	$('#'+campo+'_imagem').attr('src','libs/captcha.php');
	
	$.post('libs/_captcha.php', { op : 1 },function(data){
		$('#'+campo+'_imagem').attr('src','libs/captcha.php?captcha='+data+'');
		$('#'+campo+'_captcha').val(data);
	});
}

function conectar()
{
	var retorno;
	retorno = validar($('#lgn_user').val(), "", "US", $('#lgn_user').focus()); if (retorno == false) { return false; }
	retorno = validar($('#lgn_pass').val(), "", "PS", $('#lgn_pass').focus()); if (retorno == false) { return false; }
	retorno = validar($('#lgn_capt').val(), "", "CP", $('#lgn_capt').focus()); if (retorno == false) { return false; }

	$.post('libs/_conectar.php',
	{ 
		lgn_user	: $('#lgn_user').val(),
		lgn_pass	: $('#lgn_pass').val(),
		lgn_capt	: $('#lgn_capt').val(),
		lgn_captcha : $('#lgn_captcha').val()
	
	},
	function(data){
		if ($.trim(data) == "corret")
		{
			location = './home';
		}
		else if ($.trim(data) == "incorrectcode")
		{
			$('.imprime_error').html("Incorrect code.");
			$('.imprime_error').show();
			$('#lgn_capt').val('');
			$('#lgn_capt').focus();
			getCaptcha('lgn');
			
		}
		else
		{
			//alert(data);
			$('.imprime_error').html("Unauthorized access<br>Data Access incorrect.");
			$('.imprime_error').show();
			$('#lgn_user').val('');
			$('#lgn_pass').val('');
			$('#lgn_capt').val('');
			$('#lgn_user').focus();
			getCaptcha('lgn');
		}
	});
}

function recuperar_pass()
{
	$('.imprime_msg').hide();
	$('.imprime_error').hide();

	var retorno;
	if ($('#rec_username').val() == "")
	{
		$('.imprime_error').html("Enter the username.");
		$('.imprime_error').show();
		$('#rec_username').focus();
		return false;
	}
	else
	{
		retorno = validar($('#lmb_capt').val(), "", "CP", $('#lmb_capt').focus()); if (retorno == false) { return false; }
	}

	$.post('libs/_recuperar-pass.php',
	{
		rec_username: $('#rec_username').val(),
		lmb_capt 	: $('#lmb_capt').val(),
		lmb_captcha : $('#lmb_captcha').val()
	}, 
	function(data)
	{
		if ($.trim(data) == "incorreto")
		{
			$('.imprime_error').html("User not found.");
			$('.imprime_error').show();
			$('#rec_username').val('');
			$('#lmb_email').val('');
			$('#lmb_capt').val('');
			$('#rec_username').focus();
			getCaptcha('lmb');
		}
		else if ($.trim(data) == "naoconfere")
		{
			$('.imprime_error').html("Incorrect code.");
			$('.imprime_error').show();
			$('#lmb_capt').val('');
			$('#lmb_capt').focus();
			getCaptcha('lmb');
		}
		else if ($.trim(data) == "error")
		{
			$('.imprime_error').html("Error sending data access.");
			$('.imprime_error').show();
		}
		else
		{
			$('#rec_username').val('');
			$('#lmb_email').val('');
			$('#lmb_capt').val('');

			$('.imprime_msg').html("Access data sent by E-MAIL.");
			$('.imprime_msg').show();
		}
	});
}

function recuperar_id()
{
	$('.imprime_id').hide();
	$('.imprime_nm').hide();
	$('.imprime_error').hide();

	var retorno;
	if ($('#lid_cpf').val() != "")
	{
		retorno = validacpf($('#lid_cpf').val());
		if (retorno == false)
		{
			$('.imprime_error').html("O CPF informado está incorreto.");
			$('.imprime_error').show();
			$('#lid_cpf').focus();
			return false;
		}

		retorno = validar($('#lid_capt').val(), "", "CP", $('#lid_capt').focus()); if (retorno == false) { return false; }
	}
	else
	{
		$('.imprime_error').html("Informe o CPF.");
		$('.imprime_error').show();
		$('#lid_cpf').focus();
		return false;
	}

	$.post('libs/_recuperar-id.php',
	{
		lid_cpf		: $('#lid_cpf').val(),
		lid_capt	: $('#lid_capt').val(),
		lid_captcha : $('#lid_captcha').val()
	}, 
	function(data)
	{
		if ($.trim(data) == "incorreto")
		{
			$('.imprime_error').html("Consultor não localizado.");
			$('.imprime_error').show();
			$('#lid_cpf').val('');
			$('#lid_capt').val('');
			$('#lid_cpf').focus();
			getCaptcha('lid');
		}
		else if ($.trim(data) == "naoconfere")
		{
			$('.imprime_error').html("Código digitado incorretamente.");
			$('.imprime_error').show();
			$('#lid_capt').val('');
			$('#lid_capt').focus();
			getCaptcha('lid');
		}
		else
		{
			var retorno = data.split("|$|");
			$('#lid_cpf').val('');
			$('#lid_capt').val('');
			$('.imprime_id').html(retorno[0]);
			$('.imprime_nm').html(retorno[1]);
			$('.imprime_id').show();
			$('.imprime_nm').show();
		}
	});
}

var TipoNavegador = navigator.appName;

function conectar_click(evento)
{
	if (TipoNavegador=="Netscape") { if (evento.which == 13) { conectar(); } } else { if (evento.keyCode == 13) { conectar(); } }
}

function recuperar_pass_click(evento)
{
	if (TipoNavegador=="Netscape") { if (evento.which == 13) { recuperar_pass(); } } else { if (evento.keyCode == 13) { recuperar_pass(); } }
}

function recuperar_id_click(evento)
{
	if (TipoNavegador=="Netscape") { if (evento.which == 13) { recuperar_id(); } } else { if (evento.keyCode == 13) { recuperar_id(); } }
}

function txtBoxFormat(campo, sMask, evtKeyPress)
{
	var i, nCount, sValue, fldLen, mskLen,bolMask, sCod, nTecla;

	if(document.all)
	{
		nTecla = evtKeyPress.keyCode;
	}
	else if(document.layers)
	{
		nTecla = evtKeyPress.which;
	}
	else
	{
		nTecla = evtKeyPress.which;
		if (nTecla == 8)
		{
			return true;
		}
	}

	sValue = campo.value;
	sValue = sValue.toString().replace( ":", "" );
	sValue = sValue.toString().replace( "-", "" );
	sValue = sValue.toString().replace( "-", "" );
	sValue = sValue.toString().replace( ".", "" );
	sValue = sValue.toString().replace( ".", "" );
	sValue = sValue.toString().replace( "/", "" );
	sValue = sValue.toString().replace( "/", "" );
	sValue = sValue.toString().replace( "(", "" );
	sValue = sValue.toString().replace( "(", "" );
	sValue = sValue.toString().replace( ")", "" );
	sValue = sValue.toString().replace( ")", "" );
	sValue = sValue.toString().replace( " ", "" );
	sValue = sValue.toString().replace( " ", "" );
	fldLen = sValue.length;
	mskLen = sMask.length;

	i = 0;
	nCount = 0;
	sCod = "";
	mskLen = fldLen;

	while (i <= mskLen)
	{
		bolMask = ((sMask.charAt(i) == "-") || (sMask.charAt(i) == ":") || (sMask.charAt(i) == ".") || (sMask.charAt(i) == "/"))
		bolMask = bolMask || ((sMask.charAt(i) == "(") || (sMask.charAt(i) == ")") || (sMask.charAt(i) == " "))

		if (bolMask)
		{
			sCod += sMask.charAt(i);
			mskLen++;
		}
		else
		{
			sCod += sValue.charAt(nCount);
			nCount++;
		}
		i++;
	}

	campo.value = sCod;
	if (nTecla != 8)
	{
		if (sMask.charAt(i-1) == "9")
		{
			return ((nTecla > 47) && (nTecla < 58));
		}
		else
		{
			return true;
		}
	}
	else
	{
		return true;
	}
}

var isNN = (navigator.appName.indexOf("Netscape")!=-1);
function autoTab(input,len, e)
{
	var keyCode = (isNN) ? e.which : e.keyCode;
	var filter = (isNN) ? [0,8,9] : [0,8,9,16,17,18,37,38,39,40,46];
	if(input.value.length >= len && !containsElement(filter,keyCode))
	{
		input.value = input.value.slice(0, len);
		input.form[(getIndex(input)+1) % input.form.length].focus();
	}

	function containsElement(arr, ele)
	{
		var found = false, index = 0;
		while(!found && index < arr.length)
		if(arr[index] == ele)
		found = true;
		else
		index++;
		return found;
	}

	function getIndex(input)
	{
		var index = -1, i = 0, found = false;
		while (i < input.form.length && index == -1)
		if (input.form[i] == input)index = i;
		else i++;
		return index;
	}

	return true;
}