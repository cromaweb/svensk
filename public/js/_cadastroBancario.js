$(document).ready(function()
{			
	$('#form_cadastro').submit(function()
	{
		var retorno;
		retorno = notnull($('#cad_banco').val(), $('#cad_banco').focus(), "Banco"); if (retorno == false) { return false; }
		retorno = notnull($('#cad_agencia').val(), $('#cad_agencia').focus(), "Agencia"); if (retorno == false) { return false; }
		retorno = notnull($('#cad_conta').val(), $('#cad_conta').focus(), "Conta"); if (retorno == false) { return false; }
		retorno = notnull($('#cad_digconta').val(), $('#cad_digconta').focus(), "Digito Conta"); if (retorno == false) { return false; }
		retorno = notnull($('#cad_tipoconta').val(), $('#cad_tipoconta').focus(), "Tipo Conta"); if (retorno == false) { return false; }

		if (confirm('Confirmar atualização de cadastro?'))
		{
			$(".cad_salvar").attr('disabled', 'disabled');
			$.post("libs/_salvarBancario.php", { dados : $('#form_cadastro').serialize() }, function (data){
				if(data == "apelido")
				{
					alert('O Apelido digitado não está disponível.');
					$("#cad_apelido").focus();
					$(".cad_salvar").attr('disabled', '');
				}
				else if(data == "email")
				{
					alert('Este endereço de email não pode ser utilizado.');
					$("#cad_email").focus();
					$(".cad_salvar").attr('disabled', '');
				}
				else
				{
					alert(data);
					window.location = "./cadastro";
				}
			});
		}
	});
});