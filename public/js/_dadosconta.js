$(document).ready(function()
{	
	$("#uploadify_cpf").uploadify({
		'uploader'       : 'uploadify/uploadify.swf',
		'script'         : 'uploadify/uploadify.php',
		'cancelImg'      : 'uploadify/cancel.png',
		'folder'		 : 'arquivos/cpf',
		'fileDesc'		 : 'CPF',
		'onAllComplete'  : function() { window.location.reload(); },
		'buttonImg'      : 'uploadify/upload.png',
		'queueID'        : 'fileQueue',
		'fileExt'		 : '*.jpg;*.JPG;*.jpeg;*.JPEG;*.doc;*.docx;*.pdf;*.gif;*.GIF;*.png;*.PNG;*.bmp',
		'width'			 : '160',
		'fileSizeLimit'  : '3MB',
		'auto'           : true,
		'multi'          : false
	});

	$("#uploadify_rgfrente").uploadify({
		'uploader'       : 'uploadify/uploadify.swf',
		'script'         : 'uploadify/uploadify.php',
		'cancelImg'      : 'uploadify/cancel.png',
		'folder'		 : 'arquivos/rg_frente',
		'fileDesc'		 : 'Frente do RG',
		'onAllComplete'  : function() { window.location.reload(); },
		'buttonImg'      : 'uploadify/upload.png',
		'queueID'        : 'fileQueue',
		'fileExt'		 : '*.jpg;*.JPG;*.jpeg;*.JPEG;*.doc;*.docx;*.pdf;*.gif;*.GIF;*.png;*.PNG;*.bmp',
		'width'			 : '160',
		'fileSizeLimit'  : '3MB',
		'auto'           : true,
		'multi'          : false
	});

	$("#uploadify_rgverso").uploadify({
		'uploader'       : 'uploadify/uploadify.swf',
		'script'         : 'uploadify/uploadify.php',
		'cancelImg'      : 'uploadify/cancel.png',
		'folder'		 : 'arquivos/rg_verso',
		'fileDesc'		 : 'Verso do RG',
		'onAllComplete'  : function() { window.location.reload(); },
		'buttonImg'      : 'uploadify/upload.png',
		'queueID'        : 'fileQueue',
		'fileExt'		 : '*.jpg;*.JPG;*.jpeg;*.JPEG;*.doc;*.docx;*.pdf;*.gif;*.GIF;*.png;*.PNG;*.bmp',
		'width'			 : '160',
		'fileSizeLimit'  : '3MB',
		'auto'           : true,
		'multi'          : false
	});

	$("#uploadify_bancario").uploadify({
		'uploader'       : 'uploadify/uploadify.swf',
		'script'         : 'uploadify/uploadify.php',
		'cancelImg'      : 'uploadify/cancel.png',
		'folder'		 : 'arquivos/dadosbancarios',
		'fileDesc'		 : 'Comprovante de Conta Bancária',
		'onAllComplete'  : function() { window.location.reload(); },
		'buttonImg'      : 'uploadify/upload.png',
		'queueID'        : 'fileQueue',
		'fileExt'		 : '*.jpg;*.JPG;*.jpeg;*.JPEG;*.doc;*.docx;*.pdf;*.gif;*.GIF;*.png;*.PNG;*.bmp',
		'width'			 : '160',
		'fileSizeLimit'  : '3MB',
		'auto'           : true,
		'multi'          : false
	});

	$("#uploadify_enderecoentrega").uploadify({
		'uploader'       : 'uploadify/uploadify.swf',
		'script'         : 'uploadify/uploadify.php',
		'cancelImg'      : 'uploadify/cancel.png',
		'folder'		 : 'arquivos/enderecoentrega',
		'fileDesc'		 : 'Comprovante de Endereço',
		'onAllComplete'  : function() { window.location.reload(); },
		'buttonImg'      : 'uploadify/upload.png',
		'queueID'        : 'fileQueue',
		'fileExt'		 : '*.jpg;*.JPG;*.jpeg;*.JPEG;*.doc;*.docx;*.pdf;*.gif;*.GIF;*.png;*.PNG;*.bmp',
		'width'			 : '160',
		'fileSizeLimit'  : '3MB',
		'auto'           : true,
		'multi'          : false
	});

	$("#uploadify_aceite").uploadify({
		'uploader'       : 'uploadify/uploadify.swf',
		'script'         : 'uploadify/uploadify.php',
		'cancelImg'      : 'uploadify/cancel.png',
		'folder'		 : 'arquivos/aceitecontratual',
		'fileDesc'		 : 'Termo de aceite',
		'onAllComplete'  : function() { window.location.reload(); },
		'buttonImg'      : 'uploadify/upload.png',
		'queueID'        : 'fileQueue',
		'fileExt'		 : '*.jpg;*.JPG;*.jpeg;*.JPEG;*.doc;*.docx;*.pdf;*.gif;*.GIF;*.png;*.PNG;*.bmp',
		'width'			 : '160',
		'fileSizeLimit'  : '3MB',
		'auto'           : true,
		'multi'          : false
	});
	
	$("#uploadify_pispasep").uploadify({
		'uploader'       : 'uploadify/uploadify.swf',
		'script'         : 'uploadify/uploadify.php',
		'cancelImg'      : 'uploadify/cancel.png',
		'folder'		 : 'arquivos/pispasep',
		'fileDesc'		 : 'PISPASEP',
		'onAllComplete'  : function() { window.location.reload(); },
		'buttonImg'      : 'uploadify/upload.png',
		'queueID'        : 'fileQueue',
		'fileExt'		 : '*.jpg;*.JPG;*.jpeg;*.JPEG;*.doc;*.docx;*.pdf;*.gif;*.GIF;*.png;*.PNG;*.bmp',
		'width'			 : '160',
		'fileSizeLimit'  : '3MB',
		'auto'           : true,
		'multi'          : false
	});
});

function submeter(tipo)
{
	if (confirm('Deseja confirmar o envio da(s) imagem(ns) digitalizada(s) para verificação?'))
	{
		if (tipo == "titular")
		{
			$(".conftitular").hide();
			$(".cpfbotoes").hide();
			$(".frentergbotoes").hide();
			$(".versorgbotoes").hide();
		}

		if (tipo == "bancario")
		{
			$(".conftitular").hide();
			$(".bancariobotoes").hide();
		}

		if (tipo == "enderecoentrega")
		{
			$(".conftitular").hide();
			$(".enderecoentregabotoes").hide();
		}

		if (tipo == "aceitecontratual")
		{
			$(".conftitular").hide();
			$(".aceitecontratualbotoes").hide();
		}

		$.post('libs/_dadosconta.php',
		{
			codeOfc : $("#codeOfc").val(),
			acao : 'submeter',
			valor : tipo
		},
		function(data)
		{
			if (data)
			{
				alert("Sua documentação foi enviada para o nosso suporte!\n\nEm breve você receberá o resultado.");
			}

			window.location.reload();
		});
	}
}

function cancelar(tipo)
{
	if (confirm('Deseja excluir a imagem digitalizada que foi carregada e enviar outra imagem?'))
	{
		$(".conftitular").hide();

		if (tipo == "cpf")
		{
			$(".cpfbotoes").hide();
			$(".frentergbotoes").hide();
			$(".versorgbotoes").hide();
			$(".pispasepbotoes").hide();
		}

		if (tipo == "frenterg")
		{
			$(".cpfbotoes").hide();
			$(".frentergbotoes").hide();
			$(".versorgbotoes").hide();
			$(".pispasepbotoes").hide();
		}

		if (tipo == "versorg")
		{
			$(".cpfbotoes").hide();
			$(".frentergbotoes").hide();
			$(".versorgbotoes").hide();
			$(".pispasepbotoes").hide();
		}
		
		if (tipo == "pispasep")
		{
			$(".cpfbotoes").hide();
			$(".frentergbotoes").hide();
			$(".versorgbotoes").hide();
			$(".pispasepbotoes").hide();
		}

		if (tipo == "bancario")
		{
			$(".bancariobotoes").hide();
		}

		if (tipo == "enderecoentrega")
		{
			$(".enderecoentregabotoes").hide();
		}

		if (tipo == "aceitecontratual")
		{
			$(".aceitecontratualbotoes").hide();
		}

		$.post('libs/_dadosconta.php',
		{
			codeOfc : $("#codeOfc").val(),
			acao : 'cancelar',
			valor : tipo
		},
		function(data)
		{
			window.location.reload();
		});
	}
}

function mudacheck()
{
	if (($("#aceito").is(':checked')))
	{
		$("#termo_aceite").show();
	}
	else
	{
		$("#termo_aceite").hide();
	}
}