function cauculaConversao()
{
	if (parseInt($('#solicitado').val()) != "" && parseInt($('#solicitado').val()) > 0)
	{
		if (parseInt($('#disponivel').val()) >= parseInt($('#solicitado').val()))
		{
			$.post("libs/_converterbonus.php", { 
				op : 1,
				userconversao : $("#userconversao").val(),
				solicitado : $("#solicitado").val()
			
			}, function (data){
				retorno = data.split("|$|");
				if (retorno[0] == "solicitazero")
				{
					alert('A conversão somente poderá ser realizada para valores maiores que 0 (zero).');
					$('#solicitado').focus();
				}
				else if(retorno[0] == "insuficiente")
				{
					alert('Você não tem disponível os BBUs solicitados.');
					$('#solicitado').focus();
				}
				else
				{
					$(".confsolicitado").html(retorno[1]);
					$(".confcotacao").html(retorno[2]);
					$(".confconvertido").html(retorno[3]);
					$(".confdisponivel").html(retorno[4]);
					$(".viewsolicita").hide();
					$(".viewconfirma").show();
				}
			});
		}
		else
		{
			alert('Você não tem disponível os BBUs solicitados.');
			$('#solicitado').focus();
		}
	}
	else
	{
		alert('A conversão somente poderá ser realizada para valores maiores que 0 (zero).');
		$('#solicitado').focus();
	}
}
	
function confirmaConversao()
{
	if(confirm('Deseja confirmar a solicitação de conversão?'))
	{
		if (parseInt($('#solicitado').val()) != "" && parseInt($('#solicitado').val()) > 0)
		{
			$.post("libs/_converterbonus.php", { 
				op : 2,
				userconversao : $("#userconversao").val(),
				solicitado : $("#solicitado").val()
			
			}, function (data)
			{
				retorno = data.split("|$|");
				
				if (retorno[0] == "solicitazero")
				{
					alert('A conversão somente poderá ser realizada para valores maiores que 0 (zero).');
					cancelaConversao();
					$('#solicitado').focus();
				}
				else if (retorno[0] == "insuficiente")
				{
					alert('Você não tem disponível os BBUs solicitados.');
					cancelaConversao();
					$('#solicitado').focus();
				}
				else
				{
					alert('Conversão realizada com sucesso!');
					window.location.reload();
				}
			});
		}
	}
}

function cancelaConversao()
{
	$(".confsolicitado").html("");
	$(".confcotacao").html("");
	$(".confconvertido").html("");
	$(".confdisponivel").html("");
	$(".viewconfirma").hide();
	$(".viewsolicita").show();
}

var TipoNavegador = navigator.appName;
function processar(evento)
{
	if (TipoNavegador=="Netscape") { if (evento.which == 13) { cauculaConversao(); } } else { if (evento.keyCode == 13) { cauculaConversao(); } }
}