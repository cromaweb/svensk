$(document).ready(function()
{
	$('#form_pass').submit(function()
	{
		var retorno;
		retorno = notnull($('#pass_senhaatual').val(), $('#pass_senhaatual').focus(), "Current password"); if (retorno == false) { return false; }
		retorno = notnull($('#pass_novasenha').val(), $('#pass_novasenha').focus(), "New password"); if (retorno == false) { return false; }
		retorno = validaSenha($('#pass_novasenha').val(), $('#pass_novasenha').focus(), "New password"); if (retorno == false) { return false; }
		retorno = notnull($('#pass_confirmasenha').val(), $('#pass_confirmasenha').focus(), "Confirm password"); if (retorno == false) { return false; }
		if($('#pass_novasenha').val() != $('#pass_confirmasenha').val())
		{
			alert('The New Password field and Confirm Password should be equal.');
		}
		else
		{
			if (confirm('Confirm new password?'))
			{
				$(".pass_salvar").attr('disabled', 'disabled');
				$.post("libs/_password.php", { dados : $('#form_pass').serialize() }, function (data){
					if(data == "incorreto")
					{
						alert('Current password is incorrect, try again.');
						$('#pass_senhaatual').focus();
						$(".pass_salvar").attr('disabled', '');
					}
					else if(data == "igual")
					{
						alert('The new password can not be equal to the current.');
						$('#pass_novasenha').focus();
						$(".pass_salvar").attr('disabled', '');
					}
					else
					{
						alert('Password successfully updated.');
						$('#pass_senhaatual').val('');
						$('#pass_novasenha').val('');
						$('#pass_confirmasenha').val('');
						$('#pass_senhaatual').focus();
						$(".pass_salvar").attr('disabled', '');
					}
				});
			}
		}
	});
	
	$('#form_secpass').submit(function()
	{

			if (confirm('Confirm reset security password?'))
			{
				$(".passsec_save").attr('disabled', 'disabled');
				$.post("libs/_sec_password.php", { dados : $('#form_secpass').serialize() }, function (data){
					if(data == "correto")
					{
						alert('Message recovery password has been sent to your email!');
						$(".passsec_save").attr('disabled', '');
					}
					else
					{

						alert(data);
						$(".passsec_save").attr('disabled', '');
					}
				});
			}
	});
});