$(document).ready(function()
{
	$('.box').css('marginTop',($(document).height() / 2) - 250);
	$('#dig_senha').focus();
	getCaptcha('dig');
});
$(window).resize(function(e) {
	$('.box').css('marginTop',($(document).height() / 2) - 250);
});

function getCaptcha(campo)
{
	$('#'+campo+'_imagem').attr('src','libs/captcha.php');
	
	$.post('libs/_captcha.php', { op : 1 },function(data){
		$('#'+campo+'_imagem').attr('src','libs/captcha.php?captcha='+data+'');
		$('#'+campo+'_captcha').val(data);
	});
}

function validar(campo, teste, valor, foco)
{
	if (campo == teste)
	{
		if (valor == "DG")
		{
			$('.imprime_error').html("Digite a nova SENHA.");
			$('.imprime_error').show();
		}
		if (valor == "RD")
		{
			$('.imprime_error').html("Redigite a nova SENHA.");
			$('.imprime_error').show();
		}
		return false;
	}

	foco;
	return true;
}

function recuperar()
{
	$('.imprime_error').hide();
	$('.imprime_msg').hide();

	var retorno;
	retorno = validar($('#dig_senha').val(), "", "DG", $('#dig_senha').focus()); if (retorno == false) { return false; }
	retorno = validar($('#dig_redigite').val(), "", "RD", $('#dig_redigite').focus()); if (retorno == false) { return false; }
	if ($('#dig_senha').val() != $('#dig_redigite').val())
	{
		$('.imprime_error').html("As senhas devem ser iguais.");
		$('.imprime_error').show();
		$('#dig_senha').focus();
		return false;
	}

	$.post('libs/_pwreset.php',
	{
		dig_key		: $('#dig_key').val(),
		dig_senha 	: $('#dig_senha').val(),
		dig_capt	: $('#dig_capt').val(),
		dig_captcha : $('#dig_captcha').val()
	}, 
	function(data)
	{
		if ($.trim(data) == "naoconfere")
		{
			$('.imprime_error').html("Código digitado incorretamente.");
			$('.imprime_error').show();
			$('#dig_capt').val('');
			$('#dig_capt').focus();
			getCaptcha('dig');
		}
		else if($.trim(data) == "alterada")
		{
			$('.imprime_error').html("A senha já foi alterada.");
			$('.imprime_error').show();
			$('#dig_senha').val('');
			$('#dig_redigite').val('');
			$('#dig_capt').val('');
			$('#dig_senha').focus();
			getCaptcha('dig');
		}
		else
		{
			$('.imprime_msg').show();
			$('#dig_senha').val('');
			$('#dig_redigite').val('');
			$('#dig_capt').val('');
			$('#dig_senha').focus();
			getCaptcha('dig');
		}
	});
}

function novasol()
{
	location = './login.php?novasol=sim';
}

function acessar()
{
	location = './login.php';
}

var TipoNavegador = navigator.appName;

function recupera_click(evento)
{
	if (TipoNavegador=="Netscape") { if (evento.which == 13) { recuperar(); } } else { if (evento.keyCode == 13) { recuperar(); } }
}