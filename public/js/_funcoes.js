$(document).ready(function(e) {
    document.oncontextmenu = function() { return false; }
});

function validaSenha(campo, retorno, alerta)
{
	if (campo == "123456" || campo == "012345" || campo == "abcdef" || campo == "a1b2c3" || campo.length < 6)
	{
		alert(alerta + " digitada inválida, escolha uma nova senha!");
		retorno;
		return false;
	}
}

function validaCep(campo, retorno, alerta)
{
	if (campo == "99.999-999" || campo == "88.888-888" || campo == "77.777-777" || campo == "66.666-666" || campo == "55.555-555" || campo == "44.444-444" || campo == "33.333-333" || campo == "22.222-222" || campo == "11.111-111" || campo == "00.000-000")
	{
		alert("O campo " + alerta + " não é válido!");
		retorno;
		return false;
	}
}

function verificaApelido()
{
	$.post("/office/funcoes", { op: 1, id: $('#cad_id').val(), campo : $('#cad_apelido').val(), user : $('#cad_user').val() }, function (data)
	{
		if(data == 'apelido')
		{
			$('#ver_apelido').html('<font class="mensagem" style="color:#F00;"> &nbsp; (o apelido digitado não está disponível)</font>');
		}
		else
		{
			$('#ver_apelido').html('<font class="mensagem"> &nbsp; (somente números e letras sem acentuação)</font>');
		}
	});
}

function verificaEmail()
{
	$.post("/office/funcoes", { op: 2, id: $('#cad_id').val(), campo : $('#cad_email').val(), user : $('#cad_user').val() }, function (data)
	{
		if (data == 'email')
		{
			$('#ver_email').html('<font class="mensagem" style="color:#F00;"> &nbsp; (o email digitado não pode ser utilizado)</font>');
		}
		else
		{
			$('#ver_email').html('');
		}
	});
}

function notespecial(campo, retorno, alerta)
{
	var regex = '[^a-zA-Z0-9]+';

	if (campo.match(regex))
	{
		alert("O campo " + alerta + " só pode conter letras e números!");
		retorno;
		return false;
	}
}

function noteletra(campo, retorno, alerta)
{
	var regex = '[^0-9]+';

	if (campo.match(regex))
	{
		alert("O campo " + alerta + " só pode conter números!");
		retorno;
		return false;
	}
}

function notnull(campo, retorno, alerta)
{
	campo = $.trim(campo);
	if (campo == "")
	{
		alert("O campo " + alerta + " não pode ser vazio!");
		retorno;
		return false;
	}
}

function validaemail(vemail, retorno, alerta)
{
	if(vemail.indexOf("@") == -1)
	{
		alert(alerta + " incorreto!");
		retorno;
		return false;
	}
	if(vemail.indexOf(".") == -1)
	{
		alert(alerta + " incorreto!");
		retorno;
		return false;
	}
	if(vemail == "")
	{
		alert(alerta + " incorreto!");
		retorno;
		return false;
	}
	return true;
}

function notvazio(valor, campo, retorno, alerta1)
{
	if (campo == valor)
	{
		alert("O campo " + alerta1 + " não pode ser vazio!");
		retorno;
		return false;
	}
}
function txtBoxFormat(campo, sMask, evtKeyPress)
{
	var i, nCount, sValue, fldLen, mskLen,bolMask, sCod, nTecla;
	if(document.all)
	{
		nTecla = evtKeyPress.keyCode;
	}
	else if(document.layers)
	{
		nTecla = evtKeyPress.which;
	}
	else
	{
		nTecla = evtKeyPress.which;
		if (nTecla == 8)
		{
			return true;
		}
	}
	sValue = campo.value;
	sValue = sValue.toString().replace( ":", "" );
	sValue = sValue.toString().replace( "-", "" );
	sValue = sValue.toString().replace( "-", "" );
	sValue = sValue.toString().replace( ".", "" );
	sValue = sValue.toString().replace( ".", "" );
	sValue = sValue.toString().replace( ".", "" );
	sValue = sValue.toString().replace( ".", "" );
	sValue = sValue.toString().replace( "/", "" );
	sValue = sValue.toString().replace( "/", "" );
	sValue = sValue.toString().replace( "(", "" );
	sValue = sValue.toString().replace( "(", "" );
	sValue = sValue.toString().replace( ")", "" );
	sValue = sValue.toString().replace( ")", "" );
	sValue = sValue.toString().replace( " ", "" );
	sValue = sValue.toString().replace( " ", "" );
	fldLen = sValue.length;
	mskLen = sMask.length;
	i = 0;
	nCount = 0;
	sCod = "";
	mskLen = fldLen;
	while (i <= mskLen)
	{
		bolMask = ((sMask.charAt(i) == "-") || (sMask.charAt(i) == ":") || (sMask.charAt(i) == ".") || (sMask.charAt(i) == "/"))
		bolMask = bolMask || ((sMask.charAt(i) == "(") || (sMask.charAt(i) == ")") || (sMask.charAt(i) == " "))
		if (bolMask)
		{
			sCod += sMask.charAt(i);
			mskLen++;
		}
		else
		{
			sCod += sValue.charAt(nCount);
			nCount++;
		}
		i++;
	}
	campo.value = sCod;
	if (nTecla != 8)
	{
		if (sMask.charAt(i-1) == "9")
		{
			return ((nTecla > 47) && (nTecla < 58));
		}
		else
		{
			return true;
		}
	}
	else
	{
		return true;
	}
}

function getEndereco(campo, endereco, bairro, cidade, estado)
{
	$.getScript("http://cep.republicavirtual.com.br/web_cep.php?formato=javascript&cep="+$("#"+campo+"").val()+"", function()
	{
		if (resultadoCEP["resultado"])
		{
			$("#"+endereco+"").val(unescape(resultadoCEP["tipo_logradouro"])+" "+unescape(resultadoCEP["logradouro"]));
			$("#"+bairro+"").val(unescape(resultadoCEP["bairro"]));
			$("#"+cidade+"").val(unescape(resultadoCEP["cidade"]));
			$("#"+estado+"").val(unescape(resultadoCEP["uf"]));
		}
		else
		{
			alert("Endereço não encontrado.");
		}
	});				
}

function validaPIS(Numero,Digito)
{
	for (i=0; i<Numero.length; i++)
	{
		Numero = Numero.replace('.','');
		Numero = Numero.replace('-','');
	}
	
	var PASEP	= Numero;
	var peso1	= '3298765432';
	var soma1	= 0;
	var digito1 = 0;
	
	for (i = 1; i < 10 - Numero.length+1; i++) 
	{
		PASEP = eval("'" + 0 + PASEP + "'")
	}
	
	for (i = 1; i < PASEP.length+1; i++) 
	{
		soma1 += PASEP.substring(i, i-1) * peso1.substring(i, i-1);
	} 
	
	soma1 %= 11;
	
	if (soma1  < 2) 
	{
		digito1 = 0;
	} else {
		digito1 = 11 - soma1; 
	}
	
	if (eval("'" + digito1 +"'") != Digito)
	{
		return false;
	}else {
		return true;
	}
}