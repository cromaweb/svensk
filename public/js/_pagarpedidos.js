function calculaPedido()
{
	if($('#numeropedido').val() != "")
	{
		$.post("libs/_pagarpedido.php", { 
			op : 1,
			userpedido : $("#userpedido").val(),
			numeropedido : $("#numeropedido").val()
		
		}, function (data){
			retorno = data.split("|$|");
			
			if(retorno[0] == "naolocalizada")
			{
				alert('O pedido digitado não foi localizado.');
				$('#numeropedido').focus();
			}
			else if(retorno[0] == "paga")
			{
				alert('O pedido digitado já foi pago.');
				$('#numeropedido').focus();
			}
			else
			{
				$("#dataPedido").html(retorno[1]);
				$("#descricaoPedido").html(retorno[2]);
				$("#valorpedido").html("$ "+retorno[3]);
				$("#valorPedido").html("$ "+retorno[3]);
				$("#valorirrf").html("$ "+retorno[4]);
				$("#valoriss").html("$ "+retorno[5]);
				$("#valorinss").html("$ "+retorno[6]);
				$("#valodebitado").html("R$ "+retorno[7]);
				
				$("#avancarpedido").hide();
				$("#pedidoselecionado").show();
			}
		});
	}
	else
	{
		alert('Por favor digite o número do pedido desejado.');
		$('#numeropedido').focus();
	}
}

function pagarPedido() {
	if(confirm('Deseja mesmo solicitar o pagamento do pedido acima informado?'))
	{
		if($('#numeropedido').val() != "")
		{
			$.post("libs/_pagarpedido.php", { 
				op : 2,
				userpedido : $("#userpedido").val(),
				numeropedido : $("#numeropedido").val()
			
			}, function (data){
				retorno = data.split("|$|");
				
				if(retorno[0] == "naolocalizada")
				{
					alert('O pedido digitado não foi localizado.');
					cancelaPedido();
				}
				else if(retorno[0] == "paga")
				{
					alert('O pedido digitado já foi pago.');
					cancelaPedido();
				}
				else if(retorno[0] == "insuficiente")
				{
					alert('O seu saldo atual não é suficiente para o pagamento desse pedido.');
					cancelaPedido();
				}
				else
				{
					alert('Solicitação de pagamento de pedido realizada com sucesso.');
					cancelaPedido();
				}
			});
		}
		else
		{
			alert('Por favor digite o número do pedido desejado.');
			$('#numeropedido').focus();
		}
	}
}

function cancelaPedido() {
	$('#numeropedido').val("");
	$("#dataPedido").html("");
	$("#descricaoPedido").html("");
	$("#valorpedido").html("");
	$("#valorPedido").html("");
	$("#valorirrf").html("");
	$("#valoriss").html("");
	$("#valorinss").html("");
	$("#valodebitado").html("");
	
	$("#avancarpedido").show();
	$("#pedidoselecionado").hide();
}