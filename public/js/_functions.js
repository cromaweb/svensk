$(document).ready(function(e) {
    document.oncontextmenu = function() { return false; }
});

function validaSenha(campo, retorno, alerta)
{
	if (campo == "123456" || campo == "012345" || campo == "abcdef" || campo == "a1b2c3" || campo.length < 6)
	{
		alert(alerta + " | invalid password!");
		retorno;
		return false;
	}
}


function verificaApelido()
{
	$.post("libs/_verifica.php", { op: 1, id: $('#cad_id').val(), campo : $('#cad_apelido').val(), user : $('#cad_user').val() }, function (data)
	{
		if(data == 'apelido')
		{
			$('#ver_apelido').html('<font class="mensagem" style="color:#F00;"> &nbsp; (o apelido digitado não está disponível)</font>');
		}
		else
		{
			$('#ver_apelido').html('<font class="mensagem"> &nbsp; (somente números e letras sem acentuação)</font>');
		}
	});
}



function notespecial(campo, retorno, alerta)
{
	var regex = '[^a-zA-Z0-9]+';

	if (campo.match(regex))
	{
		alert("O campo " + alerta + " só pode conter letras e números!");
		retorno;
		return false;
	}
}

function noteletra(campo, retorno, alerta)
{
	var regex = '[^0-9]+';

	if (campo.match(regex))
	{
		alert("O campo " + alerta + " só pode conter números!");
		retorno;
		return false;
	}
}

function notnull(campo, retorno, alerta)
{
	campo = $.trim(campo);
	if (campo == "")
	{
		alert("O campo " + alerta + " não pode ser vazio!");
		retorno;
		return false;
	}
}

function validaemail(vemail, retorno, alerta)
{
	if(vemail.indexOf("@") == -1)
	{
		alert(alerta + " incorreto!");
		retorno;
		return false;
	}
	if(vemail.indexOf(".") == -1)
	{
		alert(alerta + " incorreto!");
		retorno;
		return false;
	}
	if(vemail == "")
	{
		alert(alerta + " incorreto!");
		retorno;
		return false;
	}
	return true;
}

function notvazio(valor, campo, retorno, alerta1)
{
	if (campo == valor)
	{
		alert("O campo " + alerta1 + " não pode ser vazio!");
		retorno;
		return false;
	}
}
function txtBoxFormat(campo, sMask, evtKeyPress)
{
	var i, nCount, sValue, fldLen, mskLen,bolMask, sCod, nTecla;
	if(document.all)
	{
		nTecla = evtKeyPress.keyCode;
	}
	else if(document.layers)
	{
		nTecla = evtKeyPress.which;
	}
	else
	{
		nTecla = evtKeyPress.which;
		if (nTecla == 8)
		{
			return true;
		}
	}
	sValue = campo.value;
	sValue = sValue.toString().replace( ":", "" );
	sValue = sValue.toString().replace( "-", "" );
	sValue = sValue.toString().replace( "-", "" );
	sValue = sValue.toString().replace( ".", "" );
	sValue = sValue.toString().replace( ".", "" );
	sValue = sValue.toString().replace( ".", "" );
	sValue = sValue.toString().replace( ".", "" );
	sValue = sValue.toString().replace( "/", "" );
	sValue = sValue.toString().replace( "/", "" );
	sValue = sValue.toString().replace( "(", "" );
	sValue = sValue.toString().replace( "(", "" );
	sValue = sValue.toString().replace( ")", "" );
	sValue = sValue.toString().replace( ")", "" );
	sValue = sValue.toString().replace( " ", "" );
	sValue = sValue.toString().replace( " ", "" );
	fldLen = sValue.length;
	mskLen = sMask.length;
	i = 0;
	nCount = 0;
	sCod = "";
	mskLen = fldLen;
	while (i <= mskLen)
	{
		bolMask = ((sMask.charAt(i) == "-") || (sMask.charAt(i) == ":") || (sMask.charAt(i) == ".") || (sMask.charAt(i) == "/"))
		bolMask = bolMask || ((sMask.charAt(i) == "(") || (sMask.charAt(i) == ")") || (sMask.charAt(i) == " "))
		if (bolMask)
		{
			sCod += sMask.charAt(i);
			mskLen++;
		}
		else
		{
			sCod += sValue.charAt(nCount);
			nCount++;
		}
		i++;
	}
	campo.value = sCod;
	if (nTecla != 8)
	{
		if (sMask.charAt(i-1) == "9")
		{
			return ((nTecla > 47) && (nTecla < 58));
		}
		else
		{
			return true;
		}
	}
	else
	{
		return true;
	}
}
