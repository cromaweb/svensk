function venderGiltPlus(id) {
	$("#disponivel_"+id+" .vender").show();
	$("#disponivel_"+id+" .transferir").hide();
	$("#disponivel_"+id).slideDown();
}

function transferirGiltPlus(id) {
	$("#disponivel_"+id+" .vender").hide();
	$("#disponivel_"+id+" .transferir").show();
	$("#disponivel_"+id).slideDown();
}

function visualizarGiltPlus(id) {
	$("#vendida_"+id+" .vendida").show();
	$("#vendida_"+id).slideToggle();
}

function visualizarTransferida(id) {
	$("#transferida_"+id+" .transferida").show();
	$("#transferida_"+id).slideToggle();
}

function cancelarGiltPlus(id) {
	$("#disponivel_"+id).slideUp();
	
	$("#disponivel_"+id+" .vender input").val('');
	$("#disponivel_"+id+" .transferir input").val('');
}

function vendeAssinatura(id) {
	var retorno;
	
	retorno = notnull($("#vender_"+id+" #nomeVende").val(), $("#vender_"+id+" #nomeVende").focus(), "Nome"); if (retorno == false) { return false; }
	retorno = notnull($("#vender_"+id+" #emailVende").val(), $("#vender_"+id+" #emailVende").focus(), "Email"); if (retorno == false) { return false; }
	retorno = validaemail($("#vender_"+id+" #emailVende").val(), $("#vender_"+id+" #emailVende").focus(), "Email"); if (retorno == false) { return false; }
	retorno = notnull($("#vender_"+id+" #cpfVende").val(), $("#vender_"+id+" #cpfVende").focus(), "CPF"); if (retorno == false) { return false; }
	
	if(confirm('Deseja realmente realizar a venda da assinatura para '+$("#vender_"+id+" #nomeVende").val()+'?')) {
		$.post("libs/_vendeAssinatura.php", { 
			op : 1,
			uservende : $("#vender_"+id+" #userVendeAssinatura").val(),
			nome : $("#vender_"+id+" #nomeVende").val(),
			email : $("#vender_"+id+" #emailVende").val(),
			cpf : $("#vender_"+id+" #cpfVende").val(),
			assinatura : id
		
		}, function (data){
			if (data == "naovalida")
			{
				alert('A assinatura selecionada para venda não é válida.');
				window.location = './giltpluspack';
			}
			else if(data == "emailexiste")
			{
				alert('O Email digitado já possui uma assinatura GiltPlus.');
				$("#vender_"+id+" #emailVende").focus();
			}
			else if(data == "cpfexiste")
			{
				alert('O CPF digitado já possui uma assinatura GiltPlus.');
				$("#vender_"+id+" #cpfVende").focus();
			}
			else
			{
				alert('Venda realizada com sucesso.');
				window.location = './giltpluspack';
			}
		});
	}
}

function tranfereConsultor(id) {
	$.post("libs/_tranfereAssinatura.php", { 
		op : 1,
		usertranfere : $("#transferir_"+id+" #userTranfereAssinatura").val(),
		idconsultor : $("#transferir_"+id+" #idTransfere").val()
	
	}, function (data){
		retorno = data.split("|$|");
		if (retorno[0] == "naolocalizado")
		{
			alert('Consultor não localizado.');
			$("#transferir_"+id+" #idTransfere").focus();
		}
		else if (retorno[0] == "igual")
		{
			alert('Você deve selecionar outro consultor para fazer a transferência.');
			$("#transferir_"+id+" #idTransfere").focus();
		}
		else
		{
			$("#transferir_"+id+" #nomeConsultor").html(retorno[1]);
			$("#transferir_"+id+" #cpfConsultor").html(retorno[2]);
		}
	});
}

function tranfereAssinatura(id) {
	if($("#transferir_"+id+" #idTransfere").val() != "") {
		if(confirm('Deseja realmente realizar a transferência da assinatura para o id '+$("#transferir_"+id+" #idTransfere").val()+'?')) {
			$.post("libs/_tranfereAssinatura.php", { 
				op : 2,
				usertranfere : $("#transferir_"+id+" #userTranfereAssinatura").val(),
				idconsultor : $("#transferir_"+id+" #idTransfere").val(),
				assinatura : id
			
			}, function (data){
				retorno = data.split("|$|");
				if (retorno[0] == "naolocalizado")
				{
					alert('Consultor não localizado.');
					$("#transferir_"+id+" #idTransfere").focus();
				}
				else if (retorno[0] == "igual")
				{
					alert('Você deve selecionar outro consultor para fazer a transferência.');
					$("#transferir_"+id+" #idTransfere").focus();
				}
				else if (retorno[0] == "naovalida")
				{
					alert('A assinatura selecionada para transferência não é válida.');
					window.location = './giltpluspack';
				}
				else
				{
					alert('Tranferencia realizada com sucesso.');
					window.location = './giltpluspack';
				}
			});
		}
	} else {
		alert('O ID do consultor não pode ser vazio.');	
		$("#transferir_"+id+" #idTransfere").focus();
	}
}