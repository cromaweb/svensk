$(document).ready(function()
{			
	$('#form').submit(function()
	{
		var retorno;
		retorno = notespecial($('#cad_apelido').val(), $('#cad_apelido').focus(), "Login"); if (retorno == false) { return false; }

		retorno = notnull($('#cad_email').val(), $('#cad_email').focus(), "E-mail"); if (retorno == false) { return false; }
		retorno = validaemail($('#cad_email').val(), $('#cad_email').focus(), "E-mail"); if (retorno == false) { return false; }

		if (confirm('Confirm update?'))
		{
			$(".bt_save").attr('disabled', 'disabled');
			$.post("/office/register/edit", { dados : $('#form').serialize() }, function (data){
				if(data == "apelido")
				{
					alert('Login already exists.');
					$("#cad_apelido").focus();
					$(".bt_save").attr('disabled', '');
				}
				else
				{
					alert(data);
					window.location = "/office/overview";
				}
			});
		}
	});
});