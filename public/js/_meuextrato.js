$(document).ready(function(e)
{
	$("#valorsaque").maskMoney({decimal:",", thousands:"."});
});

function avancaSolicitar()
{
	var valorsaque = ($('#valorsaque').val().replace(".","").replace(".","").replace(",",""));
	var valordispo = ($('#disponivel').val().replace(".","").replace(".","").replace(",",""));

	if(valorsaque != "" && valorsaque != "0,00")
	{
		if (parseFloat(valordispo) >= parseFloat(valorsaque))
		{
			$.post("libs/_solicitasaque.php", 
			{
				op : 1,
				usersaque : $("#usersaque").val(),
				valorsaque : $('#valorsaque').val()
			}, function (data){
				retorno = data.split("|$|");

				if (retorno[0] == "solicitazero")
				{
					alert('O saque somente poderá ser realizado para valores maiores que 0 (zero).');
					$('#valorsaque').focus();
				}
				else if (retorno[0] == "insuficiente")
				{
					alert('Saldo atual insuficiente para o valor solicitado.');
					$('#valorsaque').focus();
				}
				else
				{
					$(".confdisponivel").html(retorno[1]);
					$(".confrequisitado").html(retorno[2]);
					$(".confinss").html(retorno[3]);
					$(".confirrf").html(retorno[4]);
					$(".confiss").html(retorno[5]);
					$(".confliquido").html(retorno[6]);
					$(".viewconfirma").show();
					$(".viewsolicita").hide();
				}
			});
		}
		else
		{
			alert('Saldo atual insuficiente para o valor solicitado.');
			$('#valorsaque').focus();
		}
	}
	else
	{
		alert('O saque somente poderá ser realizado para valores maiores que 0 (zero).');
		$('#valorsaque').focus();
	}
}

function confirmaSaque()
{
	var valorsaque = ($('#valorsaque').val().replace(".","").replace(".","").replace(",","."));

	if(confirm('Deseja confirmar a solicitação de saque?'))
	{
		$.post("libs/_solicitasaque.php",
		{ 
			op : 2,
			usersaque : $("#usersaque").val(),
			valorsaque : parseFloat(valorsaque)
		}, function (data)
		{
			if (data == "solicitazero")
			{
				alert('O saque somente poderá ser realizado para valores maiores que 0 (zero).');
				cancelaSaque();
				$('#valorsaque').focus();
			}
			else if (data == "insuficiente")
			{
				alert('Saldo atual insuficiente para o valor solicitado.');
				cancelaSaque();
				$('#valorsaque').focus();
			}
			else
			{
				alert('Solicitação de saque realizada com sucesso.');
				window.location.reload();
			}
		});
	}
}

function cancelaSaque()
{
	$(".confdisponivel").html("");
	$(".confrequisitado").html("");
	$(".confinss").html("");
	$(".confirrf").html("");
	$(".confiss").html("");
	$(".confliquido").html("");
	$(".viewconfirma").hide();
	$(".viewsolicita").show();
}

var TipoNavegador = navigator.appName;
function solicitar(evento)
{
	if (TipoNavegador=="Netscape") { if (evento.which == 13) { avancaSolicitar(); } } else { if (evento.keyCode == 13) { avancaSolicitar(); } }
}