$(document).ready(function()
{
	$('#form_contato').submit(function()
	{
		var retorno;
		retorno = notnull($('#contato_area').val(), $('#contato_area').focus(), "Area"); if (retorno == false) { return false; }
		retorno = notnull($('#contato_assunto').val(), $('#contato_assunto').focus(), "Assunto"); if (retorno == false) { return false; }
		retorno = notnull($('#contato_mensagem').val(), $('#contato_mensagem').focus(), "Mensagem"); if (retorno == false) { return false; }
		
		$(".contato_enviar").attr('disabled', 'disabled');
		
		$.post("libs/_contato.php", { dados : $('#form_contato').serialize() }, function (data)
		{
			alert('Contato enviado com sucesso.');
			$(".contato_enviar").attr('disabled', '');
			$('#contato_area').val('');
			$('#contato_assunto').val('');
			$('#contato_mensagem').val('');
			$('#contato_area').focus();
		});
	});
});