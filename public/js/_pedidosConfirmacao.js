function concluiCompra(){
	if(confirm("Deseja adquirir o Combo de Serviços selecionado?\nApós o pedido confirmado, o cancelamento só poderá ser feito mediante solicitação ao suporte da Clickprime8.")){
		$(".concluircompra").hide();
		
		$.post("libs/_compras.php", { 
			op		: 2, 
			kit		: $("#combo").val(),
			id		: $("#user").val()
		}, function (data){
			alert('O pedido do combo foi realizado com sucesso.');
			window.location = "./pedidos";
		});	
	}
}

function concluiUpgrade(){
	if(confirm("Deseja fazer o upgrade para o Combo de Serviços selecionado?\nApós o pedido confirmado, o cancelamento só poderá ser feito mediante solicitação ao suporte da Clickprime8.")){
		$(".concluirupgrade").hide();
		
		$.post("libs/_compras.php", { 
			op		: 3, 
			kit		: $("#combo").val(),
			valor	: $("#valor").val(),
			id		: $("#user").val()
		}, function (data){
			alert('O pedido de upgrade foi realizado com sucesso.');
			window.location = "./pedidos";
		});	
	}
}

/*function cancelaCompra(fatura) {
	if (confirm('Confirmar o cancelamento do pedido do combo?'))
	{
		$.post("libs/_compras.php", { 
			op		: 1, 
			fatura	: fatura,
			id		: $("#user").val()
		}, function (data){
			if(data == "cancelada")
			{
				alert('O pedido do combo foi cancelado com sucesso.');
				window.location = "./pedidos";
			}
			else
			{
				alert('O pedido do combo não pode ser cancelado.');
				window.location = "./pedidos";
			}
		});
	}	
}*/