$('.title').hide();
var mq = window.matchMedia("(min-width: 768px)");

var resizeIt = function () {
  var navSize = $('.navbar').height();
  var squareHeight = $('.square, .square-fixed').height();

  if (!mq.matches) {
    $('body').css('padding-top', navSize + 'px');
  } else {
    $('body').css('padding-top', (navSize + squareHeight) + 'px');
  }

  //$('.square-fixed').height($('.square-fixed').width() + 'px');
}

resizeIt();

$(window).on('resize', resizeIt);

$('.square:not(.hidden-xs), .square-fixed:not(.hidden-xs)').on('mouseenter', function () {
  $(this).find('.title').show('slide', { direction: 'down' });
})

$('.square:not(.hidden-xs), .square-fixed:not(.hidden-xs)').on('mouseleave', function () {
  $(this).find('.title').hide('slide', { direction: 'down' });
})

$('.slide').slick({
  adaptiveHeight: true,
  autoplay: false,
  appendArrows: $('.slide'),
  prevArrow: '<a href="#" class="btn-prev"><img src="content/images/next.png"></img></a>',
  nextArrow: '<a href="#" class="btn-next"><img src="content/images/next.png"></img></a>',
  infinite: true,
  centerPadding: 0
});