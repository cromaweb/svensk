$('.title').hide();

var resizeIt = function () {
  var navSize = $('.navbar').height();
  $('body').css('padding-top', navSize + 'px');
}

resizeIt();

$(window).on('resize', resizeIt);

$('.square, .title').on('mouseenter', function () {
  $(this).find('.title').show('slide', { direction: 'down' });
})

$('.square').on('mouseleave', function () {
  $(this).find('.title').hide('slide', { direction: 'down' });
})

jQuery(document).ready(function () {
  $("#org").jOrgChart({
    chartElement: '#chart',
    dragAndDrop: false
  });
  $('.viewp').show();
  $('.view').show();
});

$('.slide').slick({
  adaptiveHeight: true,
  autoplay: false,
  appendArrows: $('.slide'),
  prevArrow: '<a href="#" class="btn-prev"><img src="/public/new_layout/content/images/next.png"></img></a>',
  nextArrow: '<a href="#" class="btn-next"><img src="/public/new_layout/content/images/next.png"></img></a>',
  infinite: true,
  centerPadding: 0,
  respondTo: 'slider'
});

