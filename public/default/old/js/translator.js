var frameContents;
var defaultLanguage = 'en';

function changeLang(languageCode) {

    var pt, en, element = "";

    if (languageCode == defaultLanguage) {
            element = jQuery(".goog-te-banner-frame:eq(0)").contents().find("button[id*='restore']");
    } else {
        switch (languageCode) {
            case 'pt':
            pt = "Portugu";
            en = "Portugu";
            break;
            case 'id':
            pt = "Indon";
            en = "Indon";
            break;
            case 'de':
            pt = "Alem";
            en = "Germ";
            break;
            case 'es':
            pt = "Espanhol";
            en = "Span";
            break;
            case 'fr':
            pt = "Fran";
            en = "Fren";
            break;
            case 'it':
            pt = "Italian";
            en = "Italian";
            break;
            case 'da':
            pt = 'Dinam';
            en = 'Dani';
            break;
            case 'hu':
            pt = 'Hung';
            en = 'Hung';
            break;
            case 'nl':
            pt = 'Holan';
            en = 'Dutch';
            break;
            case 'po':
            pt = 'Polon';
            en = 'Polish';
            break;
            case 'ro':
            pt = 'Romeno';
            en = 'Roman';
            break;
            case 'sl':
            pt = 'Eslovaco';
            en = 'Slovak';
            break;
            case 'sv':
            pt = 'Sueco';
            en = 'Swedish';
            break;
            case 'tr':
            pt = 'Turco';
            en = 'Turkish';
            break;
            case 'el':
            pt = 'Grego';
            en = 'Greek';
            break;
            case 'bg':
            pt = 'lgaro';
            en = 'Bulg';
            break;
            case 'ru':
            pt = 'Russ';
            en = 'Russ';
            break;
            case 'uk':
            pt = 'Ucrani';
            en = 'Ukra';
            break;
            case 'ar':
            pt = 'rabe';
            en = 'Arabic';
            break;
            case 'fa':
            pt = 'Pers';
            en = 'Pers';
            break;
            case 'hi':
            pt = 'Hindi';
            en = 'Hindi';
            break;
            case 'ko':
            pt = 'Coreano';
            en = 'Korean';
            break;
            case 'jp':
            pt = 'Jap';
            en = 'Jap';
            break;
            case 'zh-en':
            pt = 'Simpl';
            en = 'Simpl';
            break;
            case 'zh-cn':
            pt = 'Tradi';
            en = 'Tradi';
            break;
        }

        element = frameContents.find('.text:contains("'+pt+'"), .text:contains("'+en+'")');

    }
    if (element.length > 0) {
        element.click();
    }
}
function googleTranslateElementInit() {
    new google.translate.TranslateElement({
        pageLanguage: defaultLanguage,
        autoDisplay: false,
        layout: google.translate.TranslateElement.InlineLayout.SIMPLE
    },
    'google_translate_element');

    frameContents = $(".goog-te-menu-frame:eq(0)").contents();

    $('.change-lang').click(function () {
        languageCode = $(this).data('lang');
        applyUIChanges(languageCode);

    });
    $('.language-select').change(function () {
        var languageCode = $(this).val();
        changeLang(languageCode);
        $('#languages a').text($('.change-lang[data-lang="'+languageCode+'"]').text());
    });

    var googTransCookie = Cookies.get('googtrans');

    if(googTransCookie) {
        var languageCode = googTransCookie.split('/')[2].toLowerCase();
        applyUIChanges(languageCode);
    }

    function applyUIChanges (languageCode) {
        $('.language-select').val(languageCode).change();
        $('#languages a').text($('.change-lang[data-lang="'+languageCode+'"]').text());
    }

}
