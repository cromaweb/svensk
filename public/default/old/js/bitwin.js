$(function(){
    $('.goTop').hide();
            
    $(window).scroll(function(){
        if($(this).scrollTop()>700){
            $('.goTop').fadeIn();
        }else{
            $('.goTop').fadeOut();
        }
    })
})
function Click_Button(i)
{
    $(".section_slider #btn"+i+"").click();
}
function Click_right()
{
    $(".section_slider .slick-next").click();
}
function Click_left()
{
    $(".section_slider .slick-prev").click();
}

$(window).load(function () {
    var url = location.href;
    if(url.indexOf("our") > -1) {
        $(".btn_our").click();
    }
});

function pageScroll(to) {

    $('html, body').animate({
        scrollTop: $(to).offset().top
    }, 500);

}

$("#responcive").click(function () {
    $(".menuResponsive").toggleClass("activate");
    $(".bg_responsive").toggleClass("activate_menu");
});

$(".close_menu").click(function () {
    $(".menuResponsive").toggleClass("activate");
    $(".bg_responsive").toggleClass("activate_menu");
});

$(".ling_cur").click(function () {
    $(".lang_list").toggleClass("activate_lang");
    $(".languages_bg").toggleClass("activate_lang_bg");
});

$(".lang_list").click(function () {
    $(".lang_list").toggleClass("activate_lang");
    $(".languages_bg").toggleClass("activate_lang_bg");
});

$(".ex").click(function () {
    $(".bg_responsive").removeClass("activate_menu");
    $(".menuResponsive").removeClass("activate");
});

$(function () {
    // Trigger maximage
    $('#maximage').maximage({
        cycleOptions: {
            fx: bitwyn.config.bannerEffect,
            speed: 3000, // Has to match the speed for CSS transitions in jQuery.maximage.css (lines 30 - 33)
            timeout: 1,
            pager: '#cycle-nav ul',
            pagerAnchorBuilder: function (idx, slide) {
                return '<li><a href="#"></a></li>';
            }
        },
        onImagesLoaded:function () {
             $(".loading").fadeOut("slow");
        }
    });
});

$(document).on('ready', function () {
    $('.slide1').slick({
        dots: true,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        adaptiveHeight: true
    });
});


$(document).on('ready', function () {
    $('.slide3').slick({
        dots: true,
        infinite: false,
        speed: 300,
        slidesToShow: 3,
        slidesToScroll: 3,
        adaptiveHeight: true,
        responsive: [
        {
            breakpoint: 1001,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2,
                infinite: true,
                dots: true
            }
        },
        {
            breakpoint: 730,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }]
    });
});

$(document).on('ready', function () {
    $('.slide4').slick({
        dots: true,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        slidesToScroll: 1,
        adaptiveHeight: true
    });
});

var $selects = $(".lc-select");

$selects.each(function(){

    var $this = $(this),
    $text = $this.children(".lc-select__text"),
    $select = $this.children(".lc-select__select"),
    $opts = $select.children("option"),
    $optSelected = $opts.filter(":selected");

    $text.text( $optSelected.text() );

    $select.on( "change" , function() {
        $text.text( $opts.filter(":selected").text() );
    });

});


