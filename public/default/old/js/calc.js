$(function () {

    // config


    //var shareFactor = 20;

    var planFactor = 100;



    // elements

    var $revenue = $('.total-revenue');

    var $profitability = $('.profitability');



    var $indexVal = $('#index_value');

    var $noPlans = $('#no_plans');



    $('#no_plans, #index_value').change(updateValues);



    updateValues();



    function updateValues() {

        var numberOfPlans = $noPlans.val();

        if (numberOfPlans <= 9) {
            shareFactor = 20;
        } 

        if (numberOfPlans > 9 && numberOfPlans <= 24) {
            shareFactor = 25;
        } 

        if (numberOfPlans > 24 && numberOfPlans <= 49) {
            shareFactor = 30;
        } 

        if (numberOfPlans > 49) {
            shareFactor = 40;
        } 

        var share = numberOfPlans * shareFactor;

        var indexValue = $indexVal.val();



        $('#total_price').text(numberOfPlans * planFactor);

        $('#share').text(share);



        $.each($revenue, function(index, el) {

            var $this = $(el);

            var revenue = (share * indexValue) * $this.data('revenue');

            $(this).text(revenue.toFixed(2));

        });



        $.each($profitability, function(index, el) {

            var $this = $(el);

            var profitability = (share * indexValue * $this.data('profit') * 5) / (numberOfPlans * planFactor) * 100;

            $(this).text(profitability.toFixed(0));

        });

    }

});

