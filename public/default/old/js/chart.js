$(function () {
    function renderChart(data){
        Highcharts.setOptions({
            global: {
                useUTC: false
            }
        });

        $('#graph').highcharts('StockChart', {
            chart : {
                animation: false,
                marginBottom: 30,
                marginTop: 20,
                height: 300,
                backgroundColor: "transparent"
            },
            series : [{
                name : 'USD/BTC',
                tooltip: {
                    xDateFormat: "%Y-%m-%d %H:%M:%S",
                    shared: true,
                    followPointer: true,
                    followTouchMove: true,
                    shadow: false
                },
                data: data
            }],

            xAxis: {
                type: "datetime",
                margin: 0,
                lineColor: "rgba(255, 255, 255, 0.3)",
                labels: {
                    style: {
                        color: "#fff",
                    }
                }
            },
            yAxis: {
                title: {
                    text: ""
                },
                gridLineWidth: 1,
                gridLineColor: "rgba(255, 255, 255, 0.2)",
                opposite: false,
                labels: {
                    style: {
                        color: "#fff",
                        fontWeight: "bold"
                    }
                }
            },

            plotOptions: {
                series: {
                    color: '#fff'
                }
            },
            tooltip: {
                valueDecimals: 2,
                useHTML: true
            },
            credits: {
                enabled: false
            },
            scrollbar: {
                enabled: false
            },
            rangeSelector: {
                selected: 4,
                inputEnabled: false,
                buttonTheme: {
                    visibility: 'hidden'
                },
                labelStyle: {
                    visibility: 'hidden'
                }
            },
            navigator: {
                enabled: false
            }
        });
    }

    var priceSuffix = ' BTC/USD';
    var lastValueElement = $('#last_value #value');

    $.get("https://www.bitstamp.net/api/transactions/", function (data) {
        resultArray = [];

        $.each(data, function(index,element){
            resultArray.push({x:Number(element.date*1000),y:Number(element.price)})
        });

        resultArray.sort(compare);
        renderChart(resultArray);
        lastValueElement.text(getValueString(resultArray.pop().y));
        startWebSocket();
    });

    function compare(a, b) {
        if (a.x > b.x) {
            return 1;
        }
        if (a.x < b.x) {
            return -1;
        }
        return 0;
    }

    function getValueString(value){
        return '$' + value;
    }

    function startWebSocket(){

        var chart = $('#graph').highcharts();

        var series = chart.series[0];

        var pusher = new Pusher('de504dc5763aeef9ff52');
        var tradesChannel = pusher.subscribe('live_trades');

        tradesChannel.bind('trade', function (data) {
            lastValueElement.text(getValueString(data.price));
            series.addPoint([Number(data.timestamp*1000), Number(data.price)], true, true);
        });

    }

});
