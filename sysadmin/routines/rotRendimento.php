<?php
$val = $_GET["val"];

if($val=="x")
{
	require_once("../partner/scripts/spirit.php");
	require_once("../partner/libs/dbfunctions.php");
	require_once("../partner/libs/accountfunctions.php");

	echo ">> COMEÇO <<<br>";

	//Verifica se tem plano que já venceu e se venceu passa para o status de vencido
	executaSQL("UPDATE `tb_user_plans` SET `uplan_status`='2' 
					WHERE `uplan_id` IN 
					(SELECT cid FROM 
						(SELECT uplan_id as cid 
						FROM tb_user_plans 
						WHERE DATE_ADD(uplan_day_end, INTERVAL 1 DAY)<=DATE(now()) 
						AND uplan_status=1) AS c)");

	echo ">> Começa o cálculo do rendimento <<<br>";

	$sql = geraSQL("SELECT *,NOW() as today FROM tb_user_plans
			WHERE (EXTRACT(YEAR_MONTH FROM now())>EXTRACT(YEAR_MONTH FROM `uplan_data_rendimento`))
			AND (EXTRACT(YEAR_MONTH FROM now())>EXTRACT(YEAR_MONTH FROM `uplan_day_start`))
			AND uplan_status=1
			ORDER BY uplan_id ASC
			LIMIT 10");

	while($reg = fetch($sql))
	{
		//ID do user
		$id = $reg["uplan_id"];
		$userId = $reg["uplan_user_id"];
		$produto = $reg["uplan_prod_id"];
		$valorLocacao = $reg["uplan_value"];
		$dataRendimentoMensal = $reg["uplan_data_rendimento"];
		$dataInicial = $reg["uplan_day_start"];
		$dataAtual = $reg["today"];
		$mesAnoDataAtual = explode('-',$dataAtual)[0].explode('-',$dataAtual)[1];

		//Dias proporcionais de pagamento data inicial
		$proporcionalDataInicial = date('Ym', strtotime('+1 months', strtotime($dataInicial)));
		$proporcional = 0;
		//Se o mês ano for igual ao mes atual, então é mês seguinte da data inicial, deve fazer contabilidade dos dias proporcionais do mês passado da data inicial
		if($mesAnoDataAtual==$proporcionalDataInicial){
			$explodeMesAnoDataInicial = explode('-',$dataInicial);
			//Dia da data inicial do contrato
			$diaDataInicial = $explodeMesAnoDataInicial[2];
			$mesDataInicial = $explodeMesAnoDataInicial[1];
			$anoDataInicial = $explodeMesAnoDataInicial[0];
			echo "Data inicial | Dia: $diaDataInicial / Mês: $mesDataInicial / Ano: $anoDataInicial <br>";
			$ultimoDiaDoMesDataInicial = cal_days_in_month(CAL_GREGORIAN, $mesDataInicial, $anoDataInicial);
			echo "Último dia do mês da data inicial: $ultimoDiaDoMesDataInicial <br>";
			//Subtrai o dia de início do contrato com o último dia do mês da data inicial do contrato
			$diasProporcionais = $ultimoDiaDoMesDataInicial - $diaDataInicial;
			echo "Dias proporcionais: $diasProporcionais <br>";
			//Se a data proporcional for a última do mês, então adiciona 1 dia
			if($diasProporcionais==0){
				$diasProporcionais = 1;
			}
			$proporcional = 1;
		}else{
			$explodeMesAnoDataAtual = explode('-',date('Y-m', strtotime('-1 months', strtotime($dataAtual))));
			//Data mês passado do contrato
			$mesDataAtual = $explodeMesAnoDataAtual[1];
			$anoDataAtual = $explodeMesAnoDataAtual[0];
			echo "Data atual | Mês: $mesDataAtual / Ano: $anoDataAtual <br>";
			//Pega os dias proporcionais do mês passado
			$diasProporcionais = cal_days_in_month(CAL_GREGORIAN, $mesDataAtual, $anoDataAtual);
			echo "Dias proporcionais: $diasProporcionais <br>";
		}

		echo "ID: $id | User: $userId | Produto: $produto | Data rendimento: $dataRendimentoMensal | Data inicial: $dataInicial<br>";

		//Controle início do rendimento, setado para o número 3
		executaSQL("UPDATE tb_user_plans SET uplan_status='3' WHERE uplan_id='$id'");

		//Verifica os dados do produto
		list($porcentagemRendimento,$bonus) = abreSQL("SELECT prod_porcentagem,prod_bonus FROM `tb_product` WHERE `prod_id` = $produto");

		//Se não tiver dias proporcionais, calcula o valor cheio para o mês
		if($proporcional==0){
			$totalRendimento = $valorLocacao * $porcentagemRendimento / 100;
			echo "Sem proporcional: Valor locação: $valorLocacao  | Rendimento: $totalRendimento<br>";
			//Valor para divisão de lucros
			$valorDDL = $valorLocacao;
		//Caso haja dias proporcionais
		}else{
			$valorLocacaoProporcional = ($valorLocacao/$ultimoDiaDoMesDataInicial) * $diasProporcionais;
			$totalRendimento = $valorLocacaoProporcional * $porcentagemRendimento / 100;
			echo "Proporcional: Valor locação: $valorLocacao  | Rendimento: $totalRendimento<br>";
			//Valor para divisão de lucros
			$valorDDL = $valorLocacaoProporcional;
		}

		echo "Paga Rendimento<br>";

		//Extrato do pagamento
		setFinanceiroExtratoDB($userId,'0',"Rendimento [$id]",'r','c',$totalRendimento);
		
		//Insere total no saldo
		setFinanceiroSaldoRendimentoDB($userId,$totalRendimento,$total_btc);

		//Insere na tabela tb_bns o tipo bonus e seus pontos
		setBnsDB($userId,$userId,'n','1',$id,'',0,$totalRendimento);

		executaSQL("UPDATE tb_user_plans 
					SET uplan_data_rendimento=DATE(now())
					WHERE uplan_id='$id'");

		//Cria tabela bonus consolidado caso não exista
		bonusConsolidado($userId);
		//Seta no campo rendimento na tabela bonus consolidado
		setBonusConsolidado($userId,"bcons_valor_rendimento",$totalRendimento);
		//Seta valor para divisão de lucros
		divisaoDeLucro($userId,$id,$valorDDL);
		//Seta o bonus Shargin Bonus
		sharingBonus($userId,$id,$totalRendimento,$bonus);
		//Descrição do log
		$description = "ID: $id | User: $userId | Produto: $produto | Data inicial: $dataInicial | Valor locação: $valorLocacao";
		//Armazena o log da atividade
		setAlteraLogDB($userId,$description,$totalRendimento);
		//Final controle rendimento
		executaSQL("UPDATE tb_user_plans SET uplan_status='1' WHERE uplan_id='$id'");
		
	}

}

//Function Sharing Bonus
function sharingBonus($idUsuario,$idContrato,$rendimento,$bonus)
{
		
	//Pega ID do upline
	list($pai) = abreSQL("SELECT usr_invited_id FROM tb_user WHERE usr_id='$idUsuario'");

	echo "Usuário: $idUsuario | Patrocinador: $pai<br>";

	//Se pai existir e for diferente de 1 e 0
	if(!empty($pai) && $pai!=1 && $pai!=0)
	{
		//Calcula o bonus
		$comissao = $rendimento * $bonus / 100;

		echo "Porcentagem Bônus: $bonus | Rendimento: $rendimento | Sharing Bonus: $comissao<br>";

		//Extrato do pagamento
		setFinanceiroExtratoDB($pai,'0',"Sharing Bonus [$idContrato]",'c','c',$comissao);
		echo "User ID: ".$pai." | Comissão ".$comissao."<br>";
		//Insere total no saldo
		setFinanceiroSaldoComissoesDB($pai,$comissao);
		//Seta os valores
		setBnsDB($idUsuario,$pai,'n','2',$idContrato,'',0,$comissao);
		//Cria tabela bonus consolidado caso não exista
		bonusConsolidado($pai);
		//Seta no campo sharing bonus na tabela bonus consolidado
		setBonusConsolidado($pai,"bcons_valor_sharing_bonus",$comissao);

	}

	echo "Final Sharing BNS<br><br>";
	
}

//Function Sharing Bonus
function divisaoDeLucro($idUsuario,$idContrato,$valorLocacao)
{
	//Pega o país do usuário
	list($country) = abreSQL("SELECT usr_country FROM tb_user WHERE usr_id='$idUsuario'");

	echo "Usuário: $idUsuario | Country: $country<br>";

	$sql = geraSQL("SELECT usr_id,usr_login_id, usr_director_percentage,usr_director_percentage_international FROM tb_user WHERE usr_director=1");

	while($reg = fetch($sql))
	{
		//Id do diretor
		$diretor_id = $reg["usr_id"];
		$diretor_login = $reg["usr_login_id"];

		if($country=="BR")
		{
			//Porcentagem do bônus do diretor a ser paga
			$bonus = $reg["usr_director_percentage"];
			//Se a porcentagem foi maior que zero
			if($bonus>0)
			{
				//Calcula o bonus
				$comissao = $valorLocacao * $bonus / 100;

				echo "Diretor BR: $diretor_id | Login: $diretor_login | Porcentagem Bônus: $bonus | Valor Locação: $valorLocacao | Comissão: $comissao<br>";

				//Seta os valores
				setBnsDB($idUsuario,$diretor_id,'n','3',$idContrato,'',0,$comissao);
				//Cria tabela bonus consolidado caso não exista
				bonusConsolidado($diretor_id);
				//Seta no campo sharing bonus na tabela bonus consolidado
				setBonusConsolidado($diretor_id,"bcons_valor_team",$comissao);
			}

		}else{
			$bonus = $reg["usr_director_percentage_international"];
			//Se a porcentagem foi maior que zero
			if($bonus>0)
			{
				//Calcula o bonus
				$comissao = $valorLocacao * $bonus / 100;

				echo "Diretor INT: $diretor_id | Login: $diretor_login | Porcentagem Bônus: $bonus | Valor Locação: $valorLocacao | Comissão: $comissao<br>";

				//Seta os valores
				setBnsDB($idUsuario,$diretor_id,'n','3',$idContrato,'',0,$comissao);
				//Cria tabela bonus consolidado caso não exista
				bonusConsolidado($diretor_id);
				//Seta no campo sharing bonus na tabela bonus consolidado
				setBonusConsolidado($diretor_id,"bcons_valor_team",$comissao);
			}
		}

	}

	echo "Final Divisão de Lucros<br><br>";
	
}

function setBonusConsolidado($user,$campo,$valor){
		//Insere valor na tb_bonus_consolidado
		executaSQL("UPDATE tb_bns_consolidado 
				   SET $campo=$campo + $valor
				   WHERE bcons_idUsuario='$user'
				   AND DATE(bcons_data)=DATE(now())");
}


?>