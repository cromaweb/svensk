<?php
$val = $_GET["val"];

if($val=="x")
{
	require_once("../partner/scripts/spirit.php");
	require_once("../partner/libs/dbfunctions.php");
	require_once("../partner/libs/accountfunctions.php");

	echo ">> COMEÇO <<<br>";

	echo ">> Começa o pagamento da divisão de lucro <<<br>";

	list($contaRendimento) = abreSQL("SELECT count(*) FROM tb_user_plans
			WHERE (EXTRACT(YEAR_MONTH FROM now())=EXTRACT(YEAR_MONTH FROM `uplan_data_rendimento`))
			AND uplan_status=1");

	list($contaLocacoes) = abreSQL("SELECT count(*) FROM tb_user_plans 
			WHERE (EXTRACT(YEAR_MONTH FROM now())<>EXTRACT(YEAR_MONTH FROM `uplan_day_start`)) 
			AND uplan_status=1");

	if($contaRendimento==$contaLocacoes)
	{
		$sql = geraSQL("SELECT bcons_id,
							bcons_idUsuario,
							bcons_valor_team
				FROM tb_bns_consolidado
				WHERE bcons_divisao_de_lucro='N'
				ORDER BY bcons_id ASC");

		while($reg = fetch($sql))
		{
			$id = $reg["bcons_id"];
			$idUsuario = $reg["bcons_idUsuario"];
			$valor = $reg["bcons_valor_team"];

			//Inicia o processo de pagamento de DDL
			executaSQL("UPDATE tb_bns_consolidado SET bcons_divisao_de_lucro='P'
			WHERE bcons_idUsuario='$idUsuario'
			AND bcons_id='$id'");

			echo "DDL: $id | Usuário: $idUsuario | Valor: $valor<br>";

			if($valor>0){

				echo "Paga DDL<br>";

				//Extrato do pagamento
				setFinanceiroExtratoDB($idUsuario,'0',"Profit Sharing [$id]",'c','c',$valor);
				//Insere total no saldo
				setFinanceiroSaldoComissoesDB($idUsuario,$valor);
				//Insere na tabela tb_bns o tipo bonus e seus pontos
				setBnsDB($idUsuario,$idUsuario,'n','4',$id,'',0,$valor);
			}

			//Finaliza a contabilidade do DDL
			executaSQL("UPDATE tb_bns_consolidado SET bcons_divisao_de_lucro='S'
			WHERE bcons_idUsuario='$idUsuario'
			AND bcons_id='$id'");
			
		}
	}


}

?>