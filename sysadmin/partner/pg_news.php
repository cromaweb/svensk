<?php
//######### INICIO Paginação
  $numreg = 100; // Quantos registros por página vai ser mostrado
  $pg = $_GET['pg'];
  if (!isset($pg)) {
    $pg = 0;
  }
  $inicial = $pg * $numreg;

$sql = geraSQL("SELECT news_id, news_title, news_text, news_status, ncat_description, news_date
              FROM tb_news 
              INNER JOIN tb_news_category ON news_category = ncat_id
              ORDER BY news_id DESC 
              LIMIT $inicial, $numreg");
?>
<div class="content">
    <div class="content_resize">
      <div class="mainbar">
        <div class="article">
          <h2><span>News</span></h2>
          <div class="clr"></div>
          <p><input type="button" value="Add News" onclick="location.href='?p=news_new'" /> <input type="button" value="Add Category" onclick="location.href='?p=category'" /></p>
            <p>
            <?php
                include("pagination.php"); // Chama o arquivo que monta a paginação. ex: << anterior 1 2 3 4 5 próximo >>
            ?>
            </p>
            <table width="96%" border="0" cellspacing="0" cellpadding="0">
              <tr style="border:1px solid #CCC">
                <td bgcolor="#EEEEEE" width="6%">ID</td>
                <td bgcolor="#EEEEEE" width="20%">Title</td>
                <td bgcolor="#EEEEEE" width="20%">Status</td>
                <td bgcolor="#EEEEEE" width="20%">Category</td>
                <td bgcolor="#EEEEEE" width="20%">Date</td>
                <td bgcolor="#EEEEEE" width="14%">Action</td>
              </tr>
              <?php while($reg = mysqli_fetch_array($sql)){ ?>
              <tr style="border:1px solid #CCC">
                <td><?=$reg["news_id"]; ?></td>
                <td><?=$reg["news_title"]; ?></td>
                <td>
                  <?php switch ($reg["news_status"]) {
                    case 'A':
                      echo "Active";
                      break;

                    case 'I':
                      echo "Inactive";
                      break;
                    
                    default:
                      echo "Not Specified";
                      break;
                  }
                  ?>
                </td>
                <td><?=$reg["ncat_description"]; ?></td>
                <td><?=$reg["news_date"]; ?></td>
                <td><input name="edit" value="Editar" type="button" onclick="location.href='?p=news_edit&id=<?=$reg["news_id"]; ?>'" /></td>
              </tr>
              <?php } ?>
            </table>
            
			<p><?php include("pagination.php"); // Chama o arquivo que monta a paginação. ex: << anterior 1 2 3 4 5 próximo >> ?></p>
        </div>
      </div>
      <div class="clr"></div>
    </div>
  </div>