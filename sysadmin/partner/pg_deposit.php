<script language="Javascript">
function complete() {
	/*if (document.formSearch.search.value.length < 1) {
		alert("Insira algum valor no campo.");
		formSearch.search.focus();
		return false;
	}*/
	return true;
}

function cancel(id) {
	if (confirm("Confirmar o cancelamento do saque?")) {
		location.href='?p=withdrawal&cancel='+id;
	} 
}

</script>
<style>
 .ordenar {
 	text-decoration: none;
 	color:#003366;
 }
 .ordenar.marcado {
 	font-weight: bold;
 	color: #FF0000;
 }
</style>

<?php
require_once("./libs/accountfunctions.php");


if ($_POST["src"] == 1) {
	echo "<script>window.location = '?p=deposit&pg=0&search=".$_POST["search"]."&type=".$_POST["type"]."&status=".$_POST["status"]."';</script>";
}

$search = $_GET["search"];
$type	= $_GET["type"];
$status	= $_GET["status"];


if ((!empty($search) && !empty($type)) || !empty($status)) {
	$src = 1;
}

//######### INICIO Paginação
	$numreg = 100; // Quantos registros por página vai ser mostrado
	$pg = $_GET['pg'];
	if (!isset($pg)) {
		$pg = 0;
	}
	$inicial = $pg * $numreg;
	
//######### FIM dados Paginação
	switch ($_GET['order']) {
		case "valorasc":
			$order = "bco_bitcoin_amount ASC";
			break;

		case "valordesc" : 
			$order = "bco_bitcoin_amount DESC";
			break;

		case "valorusdasc":
			$order = "bco_dollar_amount ASC";
			break;

		case "valorusddesc" : 
			$order = "bco_dollar_amount DESC";
			break;

		case "dataasc":
			$order = "bco_date_insert ASC";
			break;

		case "datadesc" : 
			$order = "bco_date_insert DESC";
			break;

		default:
			$order = "bco_id DESC";
			break;
	}


	if($src==1)
	{
		// Faz o Select pegando o registro inicial até a quantidade de registros para página
		$sql = "SELECT 	bco_id,
						bco_bitcoin_amount,
						bco_dollar_amount,
						bco_account_to,
						bco_transaction_id,
						bco_status,
						bco_date_insert,
						usr_id,
						usr_login_id,
						usr_name
					FROM tb_bitcoin_operations INNER JOIN tb_user on usr_id = bco_user WHERE bco_type='D'";
		$sql2 = "SELECT count(*) FROM tb_bitcoin_operations INNER JOIN tb_user on usr_id = bco_user  WHERE bco_type='D'";

		if (!empty($search)) {
			if($type=="id")
			{
				$sql .= "AND usr_id='$search' ";
				$sql2 .= "AND usr_id='$search' ";
			}elseif($type=="deposit")
			{
				$sql .= "AND bco_id='$search' ";
				$sql2 .= "AND bco_id='$search' ";
			}elseif($type=="address")
			{
				$sql .= "AND bco_account_to='$search' ";
				$sql2 .= "AND bco_account_to='$search' ";
			}elseif($type=="transaction")
			{
				$sql .= "AND bco_transaction_id='$search' ";
				$sql2 .= "AND bco_transaction_id='$search' ";
			}elseif($type=="name")
			{
				$sql .= "AND usr_name LIKE '%$search%' ";
				$sql2 .= "AND usr_name LIKE '%$search%' ";
			}elseif($type=="username")
			{
				$sql .= "AND usr_login_id='$search' ";
				$sql2 .= "AND usr_login_id='$search' ";
			}
		}
		


		switch ($status) {
			case 'P':
				$sql .= "AND bco_status = 'P' ";
				$sql2 .= "AND bco_status = 'P' ";
				break;
			case 'C':
				$sql .= "AND bco_status = 'C' ";
				$sql2 .= "AND bco_status = 'C' ";
				break;
			case 'A':
				//mostrar todos
				//$sql .= "AND bco_status = 'C' ";
				//$sql2 .= "AND bco_status = 'C' ";
				break;
			case '':
				$sql .= "AND bco_status = 'U' ";
				$sql2 .= "AND bco_status = 'U' ";
				break;
			default:
				$sql .= "AND bco_status = 'U' ";
				$sql2 .= "AND bco_status = 'U' ";
				break;
		}
		
		$sql .= " ORDER BY ".$order."  
			  LIMIT $inicial, $numreg";
		
		$sql = geraSQL($sql);
		//Conta a quantidade de registros
		list($quantreg) = abreSQL($sql2);// Quantidade de registros pra paginação
	}else
	{
		// Faz o Select pegando o registro inicial até a quantidade de registros para página
		$sql = geraSQL("SELECT bco_id,
								bco_bitcoin_amount,
								bco_dollar_amount,
								bco_account_to,
								bco_transaction_id,
								bco_status,
								bco_date_insert,
								usr_id,
								usr_login_id,
								usr_name
							FROM tb_bitcoin_operations INNER JOIN tb_user on usr_id = bco_user
							WHERE bco_type='D'
							ORDER BY ".$order." 
							LIMIT $inicial, $numreg");
	
		// Serve para contar quantos registros você tem na seua tabela para fazer a paginação
		list($quantreg) = abreSQL("SELECT count(*) FROM tb_bitcoin_operations WHERE bco_type='D'");// Quantidade de registros pra paginação
		//echo "<script>alert('".$quantreg."');</script>";
	}
?>
  <div class="content">
    <div class="content_resize">
      <div class="mainbar">
        <div class="article">
          <h2><span>Depósitos</span></h2>
          <div class="clr"></div>
            <table width="600" border="0" cellspacing="0" cellpadding="0">
            <form name="formSearch" action="" method="post">
            <input type="hidden" name="src" value="1" />
              <tr>
                <td>Search&nbsp;</td>
                <td><label for="search"></label>
                <input name="search" type="text" id="search" size="40" value="<?=$_GET['search']; ?>" /></td>
                <td><label for="type"></label>
                  <select name="type" id="type">
					<option value="deposit" <?php if ($_GET['type'] == "deposit") { echo 'selected="selected"'; } ?>>ID do Depósito</option>
                    <option value="address" <?php if ($_GET['type'] == "address") { echo 'selected="selected"'; } ?>>Endereço BTC</option>
                    <option value="transaction" <?php if ($_GET['type'] == "transaction") { echo 'selected="selected"'; } ?>>Transaction</option>
                    <option value="username" <?php if ($_GET['type'] == "username") { echo 'selected="selected"'; } ?>>Username</option>
                    <option value="id" <?php if ($_GET['type'] == "id") { echo 'selected="selected"'; } ?>>ID do Usuário</option>
                    <option value="name" <?php if ($_GET['type'] == "name") { echo 'selected="selected"'; } ?>>Nome</option>
                </select></td>
                <td><label for="status"></label>
                  <select name="status" id="status">
                    <option value="A" <?php if ($_GET['status'] == "A") { echo 'selected="selected"'; } ?>>All</option>
                    <option value="C" <?php if ($_GET['status'] == "C") { echo 'selected="selected"'; } ?>>Canceled</option>
                    <option value="U"  <?php if ($_GET['status'] == "U") { echo 'selected="selected"'; } ?>>Processing</option>
                    <option value="P" <?php if ($_GET['status'] == "P") { echo 'selected="selected"'; } ?>>Complete</option>
                </select></td>
                <td><input type="submit" name="Submit" id="Submit" value="Enviar" /></td>
              </tr>
            </form>
            </table>
			<?php if($src==1){ ?>
            <table width="98%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td><a href="./?p=deposit" >Mostrar todos</a></td>
              </tr>
              <tr>
                <td>
                <?php
                    include("pagination.php"); // Chama o arquivo que monta a paginação. ex: << anterior 1 2 3 4 5 próximo >>
                ?>
                </td>
              </tr>
          </table>
            <?php }else{ ?>
            <p>
            <?php
                include("pagination.php"); // Chama o arquivo que monta a paginação. ex: << anterior 1 2 3 4 5 próximo >>
            ?>
            </p>
            <?php } ?>
            <table width="98%" border="0" cellspacing="0" cellpadding="0">
              <tr style="border:1px solid #CCC">
                <td bgcolor="#EEEEEE" width="6%">ID</td>
                <td bgcolor="#EEEEEE">Username</td>
                <td bgcolor="#EEEEEE">Address</td>
                <td bgcolor="#EEEEEE">
                	Valor USD 
                	<a class="ordenar <?php if ($_GET['order'] == "valorusdasc") { echo 'marcado'; } ?>" href="?p=deposit&pg=0&search=<?php echo $_GET['search']; ?>&type=<?php echo $_GET['type']; ?>&order=valorusdasc"><b>/\</b></a>
                	<a class="ordenar <?php if ($_GET['order'] == "valorusddesc") { echo 'marcado'; } ?>" href="?p=deposit&pg=0&search=<?php echo $_GET['search']; ?>&type=<?php echo $_GET['type']; ?>&order=valorusddesc"><b>\/</b></a>
                </td>
                <td bgcolor="#EEEEEE">
                	Valor BTC 
                	<a class="ordenar <?php if ($_GET['order'] == "valorasc") { echo 'marcado'; } ?>" href="?p=deposit&pg=0&search=<?php echo $_GET['search']; ?>&type=<?php echo $_GET['type']; ?>&order=valorasc"><b>/\</b></a>
                	<a class="ordenar <?php if ($_GET['order'] == "valordesc") { echo 'marcado'; } ?>" href="?p=deposit&pg=0&search=<?php echo $_GET['search']; ?>&type=<?php echo $_GET['type']; ?>&order=valordesc"><b>\/</b></a>
                </td>
                <td bgcolor="#EEEEEE">
                	Data
                	<a class="ordenar <?php if ($_GET['order'] == "dataasc") { echo 'marcado'; } ?>" href="?p=deposit&pg=0&search=<?php echo $_GET['search']; ?>&type=<?php echo $_GET['type']; ?>&order=dataasc"><b>/\</b></a>
                	<a class="ordenar <?php if ($_GET['order'] == "datadesc" || empty($_GET['order'])) { echo 'marcado'; } ?>" href="?p=withdrawal&pg=0&search=<?php echo $_GET['search']; ?>&type=<?php echo $_GET['type']; ?>&order=datadesc"><b>\/</b></a>
                </td>
                <td bgcolor="#EEEEEE">Status</td>
                <td bgcolor="#EEEEEE">Ação</td>
              </tr>
            <?php 
            $total_bitcoin = 0;
            $total_dollar = 0;
            while($reg = mysqli_fetch_array($sql)){ 
            	$total_bitcoin += $reg["bco_bitcoin_amount"];
            	$total_dollar += $reg['bco_dollar_amount'];
            ?>
              <tr style="border:1px solid #CCC">
                <td><?=$reg["bco_id"]; ?></td>
                <td><?=$reg["usr_login_id"]; ?></td>
                <td><?=$reg["bco_account_to"]; ?></td>
                <td><?=$reg["bco_dollar_amount"]; ?></td>
                <td><?=$reg["bco_bitcoin_amount"]; ?></td>
                <td><?=$reg["bco_date_insert"]; ?></td>
				<?php 
				switch ($reg['bco_status']) {
					case 'W':
						echo '<td style="color:#025E9B;">Pending</td>';
						break;
					case 'U':
						echo '<td style="color:#025E9B;">Processing</td>';
						break;
					case 'P':
						echo '<td style="color:#47a447;">Complete</td>';
						break;
					case 'C':
						echo '<td style="color:#FF0000;">Canceled</td>';
						break;
					default:
						echo '<td></td>';
						break;
				}
				?>
                <td>
                	<input name="view" value="View" type="button" onclick="location.href='?p=deposit_details&id=<?=$reg["bco_id"]; ?>'" />
                </td>
              </tr>
            <?php } ?>
              <tr>
              	<td colspan="3" align="right" style="font-weight:bold;"></td>
              	<td style="font-weight:bold;"><?php echo $total_dollar; ?></td>
              	<td style="font-weight:bold;"><?php echo $total_bitcoin; ?></td>
              	<td colspan="3"></td>
              </tr>
            </table>
			<p><?php include("pagination.php"); // Chama o arquivo que monta a paginação. ex: << anterior 1 2 3 4 5 próximo >> ?></p>
        </div>
      </div>
      <div class="clr"></div>
    </div>
  </div>