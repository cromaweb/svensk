<?php

require_once("./libs/dbfunctions.php"); 

  if($_POST){
    $username = addslashes($_POST["username"]);
    $wallet = addslashes($_POST["wallet"]);
    $type = addslashes($_POST["type"]);
    $value = str_replace(',','',addslashes($_POST["value"]));
    $description = addslashes($_POST["description"]);
    $erro = 0;

    //Armazena o user admin para log
    $admin_id = $usrID;

    if(empty($username)){
      $mensagem_erro .= "Username não pode ser vazio.<br>";
      $erro++;
    } else {
      list($usrID) = abreSQL("SELECT usr_id FROM tb_user WHERE usr_login_id='$username'");
      if(empty($usrID)){
        $mensagem_erro .= "Username não encontrado.<br>";
        $erro++;
      }
    }
    
    if(!in_array($wallet, array('l','r','c','b','d','a'))) {
      $mensagem_erro .= "Wallet inválida.<br>";
      $erro++;
    }

    if(!in_array($type, array('c','d'))) {
      $mensagem_erro .= "Tipo inválido.<br>";
      $erro++;
    }

    if($value < 0){
      $mensagem_erro .= "Valor deve ser maior que 0.<br>";
      $erro++;
    }

    if(empty($description)){
      $mensagem_erro .= "Campo de descrição não pode ser vazio.<br>";
      $erro++;
    }

    if($erro==0)
    {     

		if ($type == 'd') {
		$value = $value * (-1);
		}
		
		if ($wallet == "r") {
			list($saldoAnterior) = abreSQL("SELECT bsal_saldo_rendimento FROM tb_financeiro_saldo WHERE bsal_idUsuario='$usrID'");
		} elseif($wallet == "c") {
			list($saldoAnterior) = abreSQL("SELECT bsal_saldo_comissoes FROM tb_financeiro_saldo WHERE bsal_idUsuario='$usrID'");
		} elseif($wallet == "a") {
			list($saldoAnterior) = abreSQL("SELECT busr_rbma_trader FROM tb_bns_usuario WHERE busr_idUsuario ='$usrID'");
		} elseif($wallet == "d") {
			list($saldoAnterior) = abreSQL("SELECT bsal_saldo_deposit FROM tb_financeiro_saldo WHERE bsal_idUsuario='$usrID'");
		} else {
			list($saldoAnterior) = abreSQL("SELECT bsal_saldo_liberado FROM tb_financeiro_saldo WHERE bsal_idUsuario='$usrID'");
		}
	  
		$saldoAtual = $saldoAnterior + $value;
		
		if($wallet != "d" && $wallet != "a") {
			//Faz insert tabela tb_financeiro_extrato
			executaSQL("INSERT INTO tb_financeiro_extrato (extf_idUsuario,extf_idCompra, extf_descricao, extf_conta, extf_saldo_anterior,extf_saldo_atual, extf_tipo, extf_valor, extf_data) 
			VALUES ('$usrID','0','$description','$wallet','$saldoAnterior','$saldoAtual','$type','$value',now())");
		}
      
      if ($wallet == "r") {
        executaSQL("INSERT INTO tb_financeiro_saldo (bsal_idUsuario, bsal_saldo_rendimento) 
                      VALUES (".$usrID.",'".$saldoAtual."') 
                      ON DUPLICATE KEY 
                      UPDATE bsal_saldo_rendimento = (".$saldoAtual.")");
      } elseif($wallet == "c") {
        executaSQL("INSERT INTO tb_financeiro_saldo (bsal_idUsuario, bsal_saldo_comissoes) 
                      VALUES (".$usrID.",'".$saldoAtual."') 
                      ON DUPLICATE KEY 
                      UPDATE bsal_saldo_comissoes = (".$saldoAtual.")");
      } elseif($wallet == "a") {
        executaSQL("INSERT INTO tb_bns_usuario (busr_idUsuario, busr_rbma_trader) 
                      VALUES (".$usrID.",'".$saldoAtual."') 
                      ON DUPLICATE KEY 
                      UPDATE busr_rbma_trader = (".$saldoAtual.")");
      } elseif($wallet == "d") {
        executaSQL("INSERT INTO tb_financeiro_saldo (bsal_idUsuario, bsal_saldo_deposit) 
                      VALUES (".$usrID.",'".$saldoAtual."') 
                      ON DUPLICATE KEY 
                      UPDATE bsal_saldo_deposit = (".$saldoAtual.")");
      } else {
        executaSQL("INSERT INTO tb_financeiro_saldo (bsal_idUsuario, bsal_saldo_liberado) 
                      VALUES (".$usrID.",'".$saldoAtual."') 
                      ON DUPLICATE KEY 
                      UPDATE bsal_saldo_liberado = (".$saldoAtual.")");
      }

      $desc = "Wallet Balance | Admin: $admin_id | ID: $usrID | Username: $username | Valor: $value | Descrição: $description | Wallet: $wallet | Tipo: $type | Saldo Anterior: $saldoAnterior | Saldo Atual: $saldoAtual";
      //Armazena o log da atividade
      setAlteraLogDB($usrID,$desc,$saldoAtual);
	  
      print "
        <script type=\"text/javascript\">
        alert(\"Transação adicionada.\");
        window.location='?p=user_balance&id=$usrID';
        </script>";
    }else
    {
      $msgerro = '
      <table width="500" border="0" align="center" cellpadding="1" cellspacing="1" bgcolor="#999999">
        <tr>
          <td align="center">
            <table width="500" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#EEEEEE">
              <tr>
                <td class="stylexx" style="padding-left:10px; padding-right:10px; padding-top:10px;">
                  <b>Found the following errors:</b><br>
                  '.$mensagem_erro.'
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>';
    }
    
  }

	######################
	
	$user = $_GET["id"];

	list($username) 	= abreSQL("SELECT usr_login_id FROM tb_user WHERE usr_id = '$user'");
	
	//tb_financeiro_saldo, informações sobre o saldo do usuário
	list($w_bytc,$w_processing,$w_earnings,$w_comissions,$w_special,$w_deposit) = abreSQL("SELECT bsal_saldo_liberado,bsal_saldo_areceber,	bsal_saldo_rendimento,bsal_saldo_comissoes,bsal_saldo_bitwyn,bsal_saldo_deposit FROM tb_financeiro_saldo WHERE bsal_idUsuario = '$user'");
  list($rendimentoAcumulado) = abreSQL("SELECT busr_rbma_trader FROM tb_bns_usuario WHERE busr_idUsuario = '$user'");

?>

<div class="content">
  <div class="content_resize">
    <div class="mainbar">
      <div class="article">
        <h2><span>Saldo</span></h2>
        <div class="clr"></div>
        <table width="350" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><h3>Username: <a href="?p=user&pg=0&search=<?=$username ?>&type=username"> [<?=$username ?>]</a></h3></td>
            <td><button onclick="window.location.href = '?p=user_statement&pg=0&search=<?=$user ?>';">Extrato</button></td>
          </tr>
        </table>
        <table width="95%" border="1" cellspacing="0" cellpadding="0">
          <tr>
            <td align="center" bgcolor="#EEEEEE"><strong>Disponível</strong></td>
            <td align="center" bgcolor="#EEEEEE"><strong>Comissões</strong></td>
            <td align="center" bgcolor="#EEEEEE"><strong>Rendimentos</strong></td>
            <td align="center" bgcolor="#EEEEEE"><strong>Depósito BTC</strong></td>
            <td align="center" bgcolor="#EEEEEE"><strong>Processando</strong></td>
          </tr>
          <tr>
            <td align="center">&nbsp;<?php echo (!empty($w_bytc)) ? "$ $w_bytc" : "0.00"; ?></td>
            <td align="center">&nbsp;<?php echo (!empty($w_comissions)) ? "$ $w_comissions" : "0.00"; ?></td>
            <td align="center">&nbsp;<?php echo (!empty($w_earnings)) ? "$ $w_earnings" : "0.00"; ?></td>
            <td align="center">&nbsp;<?php echo (!empty($w_deposit)) ? "$ $w_deposit" : "0.00"; ?></td>
            <td align="center">&nbsp;<?php echo (!empty($w_processing)) ? "$ $w_processing" : "0.00"; ?></td>
          </tr>
        </table>
        <!-- ################ -->
        <?php if($usrType==1 || $usrType==3){?>
        <form name="form" id="form" action="" method="post">
        <input name="username" type="hidden" id="username" value="<?=$username; ?>" size="40" />
          <table width="100%" border="0" cellspacing="1" cellpadding="1">
            <tr>
              <td align="right">&nbsp;</td>
              <td></td>
            </tr>
            <tr>
              <td align="right">Wallet:</td>
              <td><select name="wallet" id="wallet">
                  <option value="">Selecione a wallet</option>
                  <option value="l">Disponível</option>
                  <option value="r">Rendimentos</option>
                  <option value="a">Rendimento Acumulado</option>
                  <option value="c">Comissões</option>
                  <option value="d">Depósito</option>
                </select></td>
            </tr>
            <tr>
              <td align="right">Tipo de transação:</td>
              <td><select name="type" id="type">
                  <option value="">Selecione o tipo</option>
                  <option value="c" selected="selected">Crédito</option>
                  <option value="d">Débito</option>
                </select></td>
            </tr>
            <tr>
              <td align="right">Valor:</td>
              <td><input name="value" type="text" id="value" value="<?=$value; ?>" size="40" /></td>
            </tr>
            <tr>
              <td align="right">Descrição:</td>
              <td><input name="description" type="text" id="description" value="" size="40"/></td>
            </tr>
            <tr>
              <td align="right">&nbsp;</td>
              <td>Legenda sugeridas: Crédito Saldo | Débito Saldo | Rendimento</td>
            </tr>
            <tr>
              <td align="right">&nbsp;</td>
              <td><input type="submit" name="Submit" id="Submit" value="Enviar" /></td>
            </tr>
          </table>
        </form>
        <?php } ?>
      </div>
    </div>
    <div class="clr"></div>
  </div>
</div>
<script type="text/javascript" src="/public/js/jquery.maskMoney.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
  $('#value').maskMoney();
});
</script>