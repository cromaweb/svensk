<link rel="stylesheet" href="http://code.jquery.com/ui/1.9.0/themes/base/jquery-ui.css" />
<script src="http://code.jquery.com/jquery-1.8.2.js"></script>
<script src="http://code.jquery.com/ui/1.9.0/jquery-ui.js"></script>
<?php

$search = $_GET["search"];
$type	= $_GET["type"];
$status	= $_GET["status"];
$pg	= $_GET["pg"];
$data_inicial = $_GET["data_inicial"];
$data_final = $_GET["data_final"];
$src	= $_GET["src"];
$act	= $_GET["act"];
$uplan_id	= $_GET["uplan_id"];

//######### INICIO Paginação
$numreg = 100; // Quantos registros por página vai ser mostrado
if (!isset($pg) || empty($pg)) {
	$pg = 0;
}
$inicial = $pg * $numreg;
	
//######### FIM dados Paginação

//Início Order
switch ($_GET['order']) {
	case "day_start_asc":
		$order = "b.uplan_day_start ASC";
		break;

	case "day_start_desc" : 
		$order = "b.uplan_day_start DESC";
		break;

	case "day_end_asc":
		$order = "b.uplan_day_end ASC";
		break;

	case "day_end_desc" : 
		$order = "b.uplan_day_end DESC";
		break;

	default:
		$order = "b.uplan_id DESC";
		break;
}

//Devolver desativa o plano do usuário
if($act==1){
	//Seta plano devolvido
	executaSQL("UPDATE tb_user_plans SET uplan_status = 3 WHERE uplan_id = $uplan_id;");
	//Insere log admin
	executaSQL("INSERT INTO tb_user_admin_log (logadm_user,logadm_description,logadm_username) VALUES ($usrID,'Estorno investimento [$uplan_id]','$usrLogin');");

}



// Faz o Select pegando o registro inicial até a quantidade de registros para página
$sql = "SELECT usr_id,
				usr_login_id,
				usr_name,
				usr_email,
				usr_contract
				uplan_id,
				uplan_user_id,
				uplan_prod_id,
				uplan_pedido_id,
				uplan_value,
                uplan_amount,
				uplan_day_start,
				uplan_day_end,
                uplan_data_rendimento,
                uplan_data_liberado,
                uplan_saque_mensal,
                uplan_saldo_acumulado,
                uplan_status,
                prod_valor,
                prod_titulo,
                prod_validade_dias 
		FROM tb_user a 
		INNER JOIN tb_user_plans b
        INNER JOIN tb_product c
        ON a.usr_id=b.uplan_user_id 
        AND b.uplan_prod_id=c.prod_id ";

$sql2 = "SELECT count(*),sum(uplan_amount*prod_valor)as sum
		FROM tb_user a 
		INNER JOIN tb_user_plans b
        INNER JOIN tb_product c
        ON a.usr_id=b.uplan_user_id 
        AND b.uplan_prod_id=c.prod_id ";

if($type=="plan")
{
	$sql .= "AND uplan_id='$search' ";
	$sql2 .= "AND uplan_id='$search' ";
}elseif($type=="name")
{
	$sql .= "AND usr_name LIKE '%$search%' ";
	$sql2 .= "AND usr_name LIKE '%$search%' ";
}elseif($type=="username")
{
	$sql .= "AND usr_login_id='$search' ";
	$sql2 .= "AND usr_login_id='$search' ";
}elseif($type=="userid")
{
	$sql .= "AND usr_id='$search' ";
	$sql2 .= "AND usr_id='$search' ";
}elseif($type=="product")
{
	$sql .= "AND uplan_prod_id='$search' ";
	$sql2 .= "AND uplan_prod_id='$search' ";
}elseif($type=="sponsor")
{
	list($search) = abreSQL("SELECT usr_id FROM tb_user WHERE usr_login_id='$search'");
	$sql .= "AND usr_invited_id='$search' ";
	$sql2 .= "AND usr_invited_id='$search' ";
}

if($status==1){
	$sql .= "AND uplan_status=1 ";
	$sql2 .= "AND uplan_status=1 ";
}elseif($status==2){
	$sql .= "AND uplan_status=2 ";
	$sql2 .= "AND uplan_status=2 ";
}

if(!empty($data_inicial)){
	$sql .= "AND DATE(uplan_day_start)>='$data_inicial' ";
	$sql2 .= "AND DATE(uplan_day_start)>='$data_inicial' ";
}

if(!empty($data_final)){
	$sql .= "AND DATE(uplan_day_start)<='$data_final' ";
	$sql2 .= "AND DATE(uplan_day_start)<='$data_final' ";
}

$sql .= " ORDER BY ".$order." LIMIT $inicial, $numreg";

$sql = geraSQL($sql);

list($quantreg,$soma) = @mysqli_fetch_array(geraSQL($sql2));// Quantidade de registros pra paginação

?>

  <div class="content">
    <div class="content_resize">
      <div class="mainbar">
        <div class="article">
          <h2><span>Contratos</span></h2>
          <div class="clr"></div>
			<table width="98%" border="0" cellspacing="0" cellpadding="0">
				<form name="formSearch" action="" method="get" >
				<input type="hidden" name="p" value="plans" />
				<input type="hidden" name="src" value="1" />
				<tr>
					<td>Search</td>
					<td>Type</td>
					<td>Status</td>
					<td>Data Inicial</td>
					<td>Data Final</td>
					<td></td>
				</tr>
				<tr>
					<td>
						<input name="search" type="text" id="search" size="20" />
					</td>
					<td>
						<select name="type" id="type">
							<option value="">-- Type --</option>
							<option value="plan" <?php if($type=="plan"){ echo "selected";} ?>>Número do contrato</option>
							<option value="name" <?php if($type=="name"){ echo "selected";} ?>>Nome</option>
							<option value="username" <?php if($type=="username"){ echo "selected";} ?>>Username</option>
							<option value="userid" <?php if($type=="userid"){ echo "selected";} ?>>User ID</option>
							<option value="sponsor" <?php if($type=="sponsor"){ echo "selected";} ?>>Patrocinador</option>
						</select>
					</td>
					<td>
						<label for="status"></label>
						<select name="status" id="status">
							<option value="">-- Status --</option>
							<option value="1" <?php if($status==1){ echo "selected";} ?>>Ativo</option>
							<option value="2" <?php if($status==2){ echo "selected";} ?>>Inativo</option>
						</select>
					</td>
					<td>
						<input name="data_inicial" id="data_inicial" type="text" id="data_inicial" size="20" value="<?php echo $data_inicial; ?>" />
					</td>
					<td>
						<input name="data_final" id="data_final" type="text" id="data_final" size="20" value="<?php echo $data_final; ?>" />
					</td>
					<td><input type="submit" name="Submit" d="Submit" value="Enviar" /></td>
				</tr>
			</form>
			</table>
			<table width="70%" border="0" cellspacing="0" cellpadding="0" style="margin-top:10px;">
				<tr bgcolor="#EEEEEE">
					<td>Quantidade</td>
					<td>Valor total</td>
				</tr>
				<tr>
					<td><strong><?php echo $quantreg ?></strong></td>
					<td><strong>$ <?=number_format($soma, 2, '.', ',') ; ?></strong></td>
				</tr>
			</table>
			<?php if($src==1){ ?>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td><a href="./?p=plans" >Mostrar todas</a></td>
			</tr>
			<tr>
				<td>
				<?php
					include("pagination.php"); // Chama o arquivo que monta a paginação. ex: << anterior 1 2 3 4 5 próximo >>
				?>
				</td>
			</tr>
			</table>
			<?php }else{ ?>
			<p>
			<?php
				include("pagination.php"); // Chama o arquivo que monta a paginação. ex: << anterior 1 2 3 4 5 próximo >>
			?>
			</p>
			<?php } ?>
			<table width="98%" border="0" cellspacing="0" cellpadding="0">
			<tr style="border:1px solid #CCC">
				<td bgcolor="#EEEEEE">Usr</td>
				<td bgcolor="#EEEEEE">Username</td>
				<td bgcolor="#EEEEEE">ID</td>
                <td bgcolor="#EEEEEE">Contrato</td>
				<td bgcolor="#EEEEEE">Valor</td>
                <td bgcolor="#EEEEEE">
                	Início 
                	<a class="ordenar <?php if ($_GET['order'] == "day_start_asc") { echo 'marcado'; } ?>" href="?p=plans&pg=0&search=<?php echo $_GET['search']; ?>&type=<?php echo $_GET['type']; ?>&order=day_start_asc"><b>/\</b></a>
                	<a class="ordenar <?php if ($_GET['order'] == "day_start_desc") { echo 'marcado'; } ?>" href="?p=plans&pg=0&search=<?php echo $_GET['search']; ?>&type=<?php echo $_GET['type']; ?>&order=day_start_desc"><b>\/</b></a>
                </td>
                <td bgcolor="#EEEEEE">
                	Final 
                	<a class="ordenar <?php if ($_GET['order'] == "day_end_asc") { echo 'marcado'; } ?>" href="?p=plans&pg=0&search=<?php echo $_GET['search']; ?>&type=<?php echo $_GET['type']; ?>&order=day_end_asc"><b>/\</b></a>
                	<a class="ordenar <?php if ($_GET['order'] == "day_end_desc") { echo 'marcado'; } ?>" href="?p=plans&pg=0&search=<?php echo $_GET['search']; ?>&type=<?php echo $_GET['type']; ?>&order=day_end_desc"><b>\/</b></a>
                </td>
				<td bgcolor="#EEEEEE">Status</td>
				<td bgcolor="#EEEEEE">Ação</td>
				<!--td bgcolor="#EEEEEE">Edit</td-->
			</tr>
			<?php while($reg = mysqli_fetch_array($sql)){ 
                $plan_name = explode(" ",$reg["prod_titulo"]);
                ?>
			<tr style="border:1px solid #CCC">
				<td><?=$reg["usr_id"]; ?></td>
				<td><?=$reg["usr_login_id"]; ?></td>
				<td><?=$reg["uplan_id"]; ?></td>
                <td><?php echo $plan_name[1]; echo " (".($reg["prod_validade_dias"]/30)."M)"; ?></td>
				<td>$ <?=number_format($reg["uplan_value"], 2, '.', ',') ; ?></td>
				<td style="padding:3px;"><?=$reg["uplan_day_start"]; ?></td>
				<td style="padding:3px;"><?=$reg["uplan_day_end"]; ?></td>
				<td>
				<?php 
				
				if($reg["uplan_status"]==2)
				{
					echo "<span style='color:#C00'>Em renovação</span>";
					/*echo "<span style='color:#C00'>
						<input name=\"payment\" value=\"Baixar\" type=\"button\" onclick=\"fatura(".$reg["fat_id"].");\" />
						</span>";*/
				}elseif(($reg["uplan_status"]==1)){
					echo "<span style='color:#090'>Ativo</span>";
				}elseif(($reg["uplan_status"]==3)){
					echo "<span style='color:#090'>Devolvido</span>";
				} ?>
				</td>
				<td>
				<?php if($reg["uplan_status"]!=3) { ?>
				<form name="form" action="" method="get" >
				<input type="hidden" name="p" value="plans" />
				<input type="hidden" name="act" value="1" />
				<input type="hidden" name="uplan_id" value="<?=$reg["uplan_id"]; ?>" />
				<input type="hidden" name="src" value="1" />
				<input type="hidden" name="search" value="<?php echo $_GET["search"]; ?>" />
				<input type="hidden" name="type" value="<?php echo $_GET["type"]; ?>" />
				<input type="hidden" name="status" value="<?php echo $_GET["status"]; ?>" />
				<input type="hidden" name="pg" value="<?php echo $_GET["pg"]; ?>" />
				<input type="hidden" name="data_inicial" value="<?php echo $_GET["data_inicial"]; ?>" />
				<input type="hidden" name="data_final" value="<?php echo $_GET["data_final"]; ?>" />
				<input name="action" value="Devolver" type="submit" >
				</form>
				<?php } ?>
				</td>
				<!--td>
				<?php if($reg["uplan_status"]!=3) { ?>
				<form name="form" action="" method="get" >
				<input type="hidden" name="p" value="plans_edit" />
				<input type="hidden" name="id" value="<?=$reg["uplan_id"]; ?>" />
				<input name="action" value="Editar" type="submit" >
				</form>
				<?php } ?>
				</td-->
			</tr>
			<?php } ?>
			</table>
            <a class="fancybox" href="#inline1" style="display:none" ></a>
            <div id="inline1" style="width:600px;display: none;">
            </div>
			<p><?php include("pagination.php"); // Chama o arquivo que monta a paginação. ex: << anterior 1 2 3 4 5 próximo >> ?></p>
        </div>
      </div>
      <div class="clr"></div>
    </div>
  </div>
<script>
$(function() {
    $( "#data_inicial" ).datepicker({ dateFormat: 'yy-mm-dd' });
	$( "#data_final" ).datepicker({ dateFormat: 'yy-mm-dd' });
});
</script>