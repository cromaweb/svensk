<?php
require_once("./libs/accountfunctions.php"); 

$deposit_id = $_GET["id"];

#################################################################

$rowCadastro 	= abreSQL("SELECT   bco_id,
									bco_bitcoin_amount,
									bco_bitcoin_amount_transfered,
									bco_dollar_amount,
									bco_dollar_amount_transfered,
									bco_dollar_fee,
									bco_date_insert,
									bco_status,
									bco_account_to,
									bco_transaction_id,
									bco_limit,
									bco_withdrawal_id,
									usr_id,
									usr_login_id,
									usr_name,
									usr_email
									FROM tb_bitcoin_operations INNER JOIN tb_user on usr_id = bco_user
									WHERE bco_type='D' AND bco_id = ".$deposit_id);

$bitcoin_amount			= $rowCadastro["bco_bitcoin_amount"];
$bitcoin_amount_transfered	= $rowCadastro["bco_bitcoin_amount_transfered"];
$dollar_amount 			= $rowCadastro["bco_dollar_amount"];
$dollar_amount_transfered 	= $rowCadastro["bco_dollar_amount_transfered"];
$dollar_fee 			= $rowCadastro["bco_dollar_fee"];
$data 			= $rowCadastro["bco_date_insert"];
$status 			= $rowCadastro["bco_status"];
$transaction		= $rowCadastro["bco_transaction_id"];
$addressBTC	= $rowCadastro["bco_account_to"];
$limitId	= $rowCadastro["bco_limit"];
$withdrawalId	= $rowCadastro["bco_withdrawal_id"];
$usr_id					= $rowCadastro["usr_id"];
$usr_login_id			= $rowCadastro["usr_login_id"];
$usr_name				= $rowCadastro["usr_name"];
$usr_email				= $rowCadastro["usr_email"];

?>
<div class="content">
	<div class="content_resize">
		<div class="mainbar">
			<div class="article">
				<h2><span>Detalhes do Depósito</span></h2>
				<div class="clr"></div>
				<p><a href="?p=deposit">Voltar</a></p>
				<?php 
				if(!empty($msgerro))
				{
					echo $msgerro;  
				}
				?>
				<table width="100%" border="0" cellspacing="1" cellpadding="1">

						<tr>
							<td align="right"><strong>Username:</strong></td>
							<td><?=$usr_login_id; ?></td>
						</tr>
						<tr>
							<td align="right"><strong>Name:</strong></td>
							<td><?=$usr_name; ?></td>
						</tr>
						<tr>
							<td align="right"><strong>E-mail:</strong></td>
							<td><?=$usr_email; ?></td>
						</tr>
						<tr>
						  <td align="right"><strong>ID Deposit:</strong></td>
						  <td><?=$deposit_id; ?></td>
					  </tr>
						<tr>
							<td align="right"><strong>Expected Value USD:</strong></td>
							<td><?=$dollar_amount; ?></td>
						</tr>
						<tr>
							<td align="right"><strong>Receveid Value USD:</strong></td>
							<td><?=$dollar_amount_transfered; ?></td>
						</tr>
				    <tr>
							<td align="right"><strong>Expected Value BTC:</strong></td>
							<td><?=$bitcoin_amount; ?></td>
						</tr>
						<tr>
							<td align="right"><strong>Expected Value BTC:</strong></td>
							<td><?=$bitcoin_amount_transfered; ?></td>
						</tr>
						<tr>
						  <td align="right"><strong>Fee USD:</strong></td>
						  <td><?=$dollar_fee; ?></td>
					  </tr>
						<tr>
						  <td align="right"><strong>Total USD:</strong></td>
						  <td><?=number_format(($dollar_amount-$dollar_fee), 2, '.', ','); ?></td>
						</tr>
						<tr>
							<td align="right"><strong>Address BTC:</strong></td>
							<td><?=$addressBTC; ?></td>
						</tr>
						<tr>
							<td align="right"><strong>Transaction:</strong></td>
							<td><?=$transaction; ?></td>
						</tr>
						<tr>
							<td align="right"><strong>Invoice Limit:</strong></td>
							<td><?php if($limitId != 0){ echo "Sim [ Invoice:$limitId ]"; }else{ echo "Não"; } ?></td>
						</tr>
						<tr>
							<td align="right"><strong>Fee Withdrawal:</strong></td>
							<td><?php if($withdrawalId != 0){ echo "Sim [ Deposit:$withdrawalId ]"; }else{ echo "Não"; } ?></td>
						</tr>
						<tr>
							<td align="right"><strong>Date :</strong></td>
							<td><?=$data; ?></td>
						</tr>
						<tr>
							<td align="right"><strong>Status :</strong></td>
							<?php 
							switch ($status) {
								case 'P':
									echo '<td style="color:#47a447;">Complete</td>';
									break;
								case 'U':
									echo '<td style="color:#025E9B;">Processing</td>';
									break;
								case 'W':
									echo '<td style="color:#025E9B;">Pending</td>';
									break;
								case 'C':
									echo '<td style="color:#FF0000;">Canceled</td>';
									break;
								default:
									echo '<td></td>';
									break;
							}
							?>
						</tr>
						<tr>
							<td align="right">&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
                        <?php if($status!="P"){ ?>
						<tr>
						  <td align="right">&nbsp;</td>
						  <td>
                          <input type="button" name="deposit" id="btnDeposit" value="Deposit" onclick="deposit('y')" />
                          <div id="confirmDeposit">
                          Confirm the deposit?
                                <input type="hidden" name="paymentId" id="paymentId" value="<?=$transaction ?>" />
                                <input type="hidden" name="orderId" id="orderId" value="<?=$deposit_id ?>" />
                                <input type="hidden" name="clientId" id="clientId" value="<?=$usr_id ?>" />
                                <input type="hidden" name="paymentAmountBTC" id="paymentAmountBTC" value="<?=$bitcoin_amount ?>" />
                                <input type="hidden" name="paymentAmountUSD" id="paymentAmountUSD" value="<?=$dollar_amount ?>" />
                                <input type="hidden" name="receivedAmountBTC" id="receivedAmountBTC" value="<?=$bitcoin_amount ?>" />
                                <input type="hidden" name="receivedAmountUSD" id="receivedAmountUSD" value="<?=$dollar_amount ?>" />
                                <input type="hidden" name="limitId" id="limitId" value="<?=$limitId ?>" />
                                <input type="hidden" name="withdrawalId" id="withdrawalId" value="<?=$withdrawalId ?>" />
                                <input type="hidden" name="status" id="status" value="complete" />
                                <input type="button" name="pay" value="Yes"  onclick="deposit('ok')" />
                                <input type="button" name="cancel" value="Cancel" onclick="deposit('n')" />                            
                            </div>
                          </td>
					  </tr>
                      <?php } ?>
				</table>
				<p>&nbsp;</p>
			</div>
		</div>
		<div class="clr"></div>
	</div>
</div>
<script src="jquery/jquery.js" type="text/javascript"></script>
<script>
$(document).ready(function() {					   
	$("#confirmDeposit").hide();
});

function deposit(yesno) {
	
	if(yesno == 'y'){
		$("#btnDeposit").hide();
		$("#confirmDeposit").show();
	}
	if(yesno == 'n'){
		$("#btnDeposit").show();
		$("#confirmDeposit").hide();
	}
	
	if(yesno == 'ok'){
			$("#confirmDeposit").hide();
			$("#btnDeposit").hide();
			$.post('https://bytc.io/updatetransaction/callbackmanually',
			{
				paymentId : $("#paymentId").val(),
				orderId : $("#orderId").val(),
				clientId : $("#clientId").val(),
				paymentAmountBTC : $("#paymentAmountBTC").val(),
				paymentAmountUSD : $("#paymentAmountUSD").val(),
				receivedAmountBTC : $("#receivedAmountBTC").val(),
				receivedAmountUSD : $("#receivedAmountUSD").val(),
				limitId : $("#limitId").val(),
				withdrawalId : $("#withdrawalId").val(),
				status : $("#status").val()
			},
			function(data)
			{
				if(data == "OK"){
					//$("#confirmDeposit").hide();
					//$("#btnDeposit").hide();
					alert("Deposit complete!");
					location.reload();

				}else{
					alert("Error deposit!");
					$("#btnDeposit").show();
					$("#confirmDeposit").show();
					//alert(data);
					//$("#message").html("<p style='color:red'>"+data+"</p>");
					//$("#message").show();
				}

			});
	}

	return false;
}
</script>