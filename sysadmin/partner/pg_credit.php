<script language="Javascript">
function complete() {
  if (document.formSearch.search.value.length < 1) {
    alert("Insira algum valor no campo.");
    formSearch.search.focus();
    return false;
  }
  return true;
}
function somente_numero(campo){  
  var digits="0123456789";  
  var campo_temp;   

  for (var i=0;i<campo.value.length;i++)
  {  
    campo_temp=campo.value.substring(i,i+1);   
    if (digits.indexOf(campo_temp)==-1)
    {  
      campo.value = campo.value.substring(0,i);  
    }  
  }  
}

</script>

<?php require_once("../libs/accountfunctions.php"); 
$cred = @$_POST["cred"];
$adm_user = @$_POST["adm_user"];
$cred_username = @$_POST["cred_username"];
$cred_value = @$_POST["cred_value"];
$description = @$_POST["description"];
echo $adm_user."<br>";


$pay = @$_POST["pay"];
$pay_userid = @$_POST["pay_userid"];
$pay_invoice = @$_POST["pay_invoice"];

if ($pay == '1') 
{
  $sqlVerify = "select * from tb_fatura, tb_user where usr_id = fat_idUsuario and fat_id = '$pay_invoice'";
  $conVerify = abreSQL($sqlVerify);
  if (!$conVerify)
  {
    echo '<script> alert("Invoice invalid");</script>';
  } else {
    payInvoiceAdmin($pay_invoice, $pay_userid);
    echo '<script> alert("Confirmation payment successfully executed");</script>';

  }
}

if ($cred == '1') 
{
  $usrid = getUserId($cred_username);
  addCredit($usrid, $adm_user, $cred_value, $description);
  echo "<script>window.location='?p=credit&op=creditsuccess';</script>";
}
if ($cred == '-1') 
{
  $usrid = getUserId($cred_username);
  addDebit($usrid, $adm_user, $cred_value, $description);
  echo "<script>window.location='?p=credit&op=debitsuccess';</script>";
}	
?>
<script type="text/javascript" src="../js/accountfunctions.js"></script>
<div class="content">
  <div class="content_resize">
    <div class="mainbar">
      <div class="article">
        <h2><span>Credit and Debit</span></h2>
        <?php if ($_GET['op'] == "creditsuccess") { ?>
        <h3 style="color:#197D19">Credit completed!</h3>
        <?php } ?>
        <div class="clr"></div>

        <table width="300" border="0" cellspacing="0" cellpadding="0">
          <form action="" id="frmCredit" name="frmCredit" method="post">
            <input type="hidden" id="cred" name="cred" value="1" />
            <input type="hidden" id="adm_user" name="adm_user" value="<?php echo $usrID; ?>" />
            <tr>
              <td><strong>Credit</strong></td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td>Description</td>
              <td>
                <select name="description">
                  <option value="Credit bonus">Credit bonus</option>
                  <option value="Credit Invalid Balance">Credit Invalid Balance</option>
                </select>
              </td>
            </tr>
            <tr>
              <td>Username</td>
              <td><input type="text" id="cred_username" name="cred_username" maxlength="45" value="" /></td>
            </tr>
            <tr>
              <td>Credit value</td>
              <td><input type="text" id="cred_value" name="cred_value" maxlength="15" value="" onkeyup="somente_numero(this);" /></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td><input type="submit" name="btnOK" value="OK" class="enviar" /></td>
            </tr>
          </form>  
        </table>
        <hr />
        <?php if ($_GET['op'] == "debitsuccess") { ?>
        <h3 style="color:#197D19">Debit completed!</h3>
        <?php } ?>
        <div class="clr"></div>
        <table width="300" border="0" cellspacing="0" cellpadding="0">
          <form action="" id="frmDebt" name="frmDebt" method="post">
            <input type="hidden" id="cred" name="cred" value="-1" />
            <input type="hidden" id="adm_user" name="adm_user" value="<?php echo $usrID ?>" />
            <tr>
              <td><strong>Debit</strong></td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td>Description</td>
              <td>
                <select name="description">
                  <option value="Bonus canceled">Debit bonus</option>
                  <option value="Debit Invalid Balance">Debit Invalid Balance</option>
                </select>
              </td>
            </tr>
            <tr>
              <td>Username</td>
              <td><input type="text" id="cred_username" name="cred_username" maxlength="45" value="" /></td>
            </tr>
            <tr>
              <td>Debit value</td>
              <td><input type="text" id="cred_value" name="cred_value" maxlength="15" value="" onkeyup="somente_numero(this);" /></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td><input type="submit" name="btnOK" value="OK" class="enviar" /></td>
            </tr>
          </form>
        </table>

      </div>
    </div>
    <div class="clr"></div>
  </div>
</div>