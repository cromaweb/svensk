<?php
  $id = $_GET["id"];
  if (empty($id)) {
    print " <script type=\"text/javascript\">
              window.location='?p=category';
            </script>";
    exit;
  }
  $rowCadastro  = abreSQL("select * from tb_news_category where ncat_id = " . $id);
  if (empty($rowCadastro)) {
    print " <script type=\"text/javascript\">
              window.location='?p=news';
            </script>";
    exit;
  }
  $description = $rowCadastro["ncat_description"];
  if($_POST){
    $description = addslashes($_POST["description"]);
    $erro = 0;

    if(empty($description)){
      $mensagem_erro .= "Description cannot be empty.<br>";
      $erro++;
    }

    if($erro==0)
    {     
      executaSQL("UPDATE tb_news_category SET ncat_description = '$description' WHERE ncat_id = " . $id);
        print "
        <script type=\"text/javascript\">
        alert(\"Category updated.\");
        window.location='?p=category';
        </script>";
    } else {
     $msgerro = '<table width="500" border="0" align="center" cellpadding="1" cellspacing="1" bgcolor="#999999">
      <tr>
        <td align="center">
          <table width="500" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#EEEEEE">
          <tr>
          <td class="stylexx" style="padding-left:10px; padding-right:10px; padding-top:10px;"><b>Found the following errors:</b><br>'.$mensagem_erro.'</td>
          </tr>
          </table>
        </td>
      </tr>
      </table>';
    }
    
  }

?>
<div class="content">
    <div class="content_resize">
      <div class="mainbar">
        <div class="article">
          <h2><span>New Category</span></h2>
          <div class="clr"></div>
          <p><a href="?p=category">Show all categories</a></p>
          <?php 
    			  if(!empty($msgerro))
    			  {
    				  echo $msgerro;  
    			  }
    			?>
          <div class="clr"></div>
          <table width="100%" border="0" cellspacing="1" cellpadding="1">
            <form name="form" id="form" action="" method="post">
              <tr>
                <td align="right">Description:</td>
                <td><input name="description" type="text" id="description" value="<?=$description; ?>" size="40" /></td>
              </tr>
              <tr>
                <td align="right">&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td align="right">&nbsp;</td>
                <td><input type="submit" name="Submit" id="Submit" value="Submit" /></td>
              </tr>
            </form>
          </table>

          <p>&nbsp;</p>
        </div>
      </div>
      <div class="clr"></div>
    </div>
  </div>