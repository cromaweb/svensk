<link rel="stylesheet" href="http://code.jquery.com/ui/1.9.0/themes/base/jquery-ui.css" />
<script src="http://code.jquery.com/jquery-1.8.2.js"></script>
<script src="http://code.jquery.com/ui/1.9.0/jquery-ui.js"></script>

<script language="Javascript">
  function complete() {
    if (document.formSearch.search.value.length < 1) {
      alert("Insira algum valor no campo.");
      formSearch.search.focus();
      return false;
    }
    return true;
  }
</script>
<?php
	
	########################
	$val = $_POST["val"];
	$planID = addslashes($_POST["planID"]);
  $start = addslashes($_POST["day_start"]);
  $end = addslashes($_POST["day_end"]);
  $rendimento = addslashes($_POST["dt_rendimento"]);
  $product = addslashes($_POST["product"]);
	
	if($val==1){
		
		$erro = 0;
		
		if(empty($planID)){
			$mensagem_erro .= "Empty plan ID!<br><br>";
			$erro++;
        }
        
		if(empty($start)){
			$mensagem_erro .= "Empty day start!<br><br>";
			$erro++;
    }
    
		if(empty($end)){
			$mensagem_erro .= "Empty day end!<br><br>";
			$erro++;
    }
    
		if(empty($rendimento)){
			$mensagem_erro .= "Empty data rendimento!<br><br>";
			$erro++;
    }
    
		if(empty($product)){
			$mensagem_erro .= "Empty product!<br><br>";
			$erro++;
		}
		
		#---------- Caso não tenha nenhum erro nos dados enviados ---------------------------
	
		if($erro==0)
		{
			
			executaSQL("UPDATE tb_user_plans SET uplan_day_start = '$start',uplan_day_end = '$end',uplan_data_rendimento='$rendimento',uplan_prod_id = '$product'
                      WHERE uplan_id = '$planID'");
      
      executaSQL("INSERT INTO tb_log_altera_dados (lad_descricao,lad_idUsuario)VALUES('Admin Plans: uplan_day_start =$start,uplan_day_end =$end,uplan_data_rendimento=$rendimento,uplan_prod_id = $product','Admin - $usrLogin')");
			
			  print "
			  <script type=\"text/javascript\">
			  alert(\"Dados atualizados!\");
			  </script>";
		}else
		{
	   $msgerro = '<table width="500" border="0" align="center" cellpadding="1" cellspacing="1" bgcolor="#999999">
			<tr>
				<td align="center">
					<table width="500" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#EEEEEE">
					<tr>
					<td class="stylexx" style="padding-left:10px; padding-right:10px; padding-top:10px;"><b>Found the following errors:</b><br><br>'.$mensagem_erro.'</td>
					</tr>
					</table>
				</td>
			</tr>
			</table>';
		}
		
	}
	
	####################
	
	$id = $_GET["id"];

	$rowCadastro 	= abreSQL("SELECT * FROM tb_user_plans WHERE uplan_id = '$id'");

    $userID		    		= $rowCadastro["uplan_user_id"];
    $prod 		    		= $rowCadastro["uplan_prod_id"];
	  $pedido	        	= $rowCadastro["uplan_pedido_id"];
    $amount 				  = $rowCadastro["uplan_amount"];
    $day_start 				= $rowCadastro["uplan_day_start"];
    $day_end 				  = $rowCadastro["uplan_day_end"];
    $data_rendimento 	= $rowCadastro["uplan_data_rendimento"];
    $data_liberado 		= $rowCadastro["uplan_data_liberado"];
    $status 				  = $rowCadastro["uplan_status"];

    list($username) = abreSQL("SELECT usr_login_id FROM tb_user WHERE usr_id='$userID'");
    list($nome_produto) = abreSQL("SELECT prod_titulo FROM tb_product WHERE prod_id='$prod'");
    $sql_prod 	= geraSQL("SELECT prod_id,prod_titulo,prod_descricao,prod_porcentagem FROM tb_product WHERE prod_status = 1");

?>
<!-- <script src="jquery/jquery.js" type="text/javascript"></script> -->
<script src="jquery/jquery.maskedinput.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){
	$("#day_start").mask("9999-99-99");
  $("#day_end").mask("9999-99-99");
	$("#dt_rendimento").mask("9999-99-99");
});
</script>
  <div class="content">
    <div class="content_resize">
      <div class="mainbar">
        <div class="article">
          <h2><span>Editar Contrato</span></h2>
          <div class="clr"></div>
          <p><a href="?p=plans">Voltar</a></p>
          <p>
          <?php 
            if(!empty($msgerro))
            {
              echo $msgerro;  
            }
          ?>
          </p>
        <table width="85%" border="0" cellspacing="1" cellpadding="1">
            <form name="form" id="form" action="" method="post">
            <input type="hidden" name="val" value="1" />
            <input type="hidden" name="planID" value="<?=$id ?>" />
              <tr>
                <td align="right"><strong>Plan ID:</strong></td>
                <td><?=$id; ?></td>
              </tr>
              <tr>
                <td align="right"><strong>Username:</strong></td>
                <td><?=$username; ?></td>
              </tr>
              <tr>
                <td align="right"><strong>Produto:</strong></td>
                <td>
                    <select name="product" id="product">
                      <?php  
                      while($reg = fetch($sql_prod)) 
                      { ?>
                      <option value="<?=$reg["prod_id"]; ?>" <?php if ($reg["prod_id"] == $prod){ echo "selected"; } ?>><?=$reg["prod_id"]; ?> - [<?=$reg["prod_descricao"]; ?> - <?=$reg["prod_porcentagem"]; ?>%]</option>
                      <?php } ?>
                    </select>
                </td>
              </tr>
              <tr>
                <td align="right"><strong>Pedido:</strong></td>
                <td><?=$pedido; ?></td>
              </tr>
              <tr>
                <td align="right"><strong>Quantidade :</strong></td>
                <td><?=$amount; ?></td>
              </tr>
              <tr>
                <td align="right"><strong>Início do plano :</strong></td>
                <td><input name="day_start" type="text" id="day_start" value="<?=$day_start; ?>" size="15" /></td>
              </tr>
              <tr>
                <td align="right"><strong>Final do plano :</strong></td>
                <td><input name="day_end" type="text" id="day_end" value="<?=$day_end; ?>" size="15" /></td>
              </tr>
              <tr>
                <td align="right"><strong>Data Rendimento :</strong></td>
                <td><input name="dt_rendimento" type="text" id="dt_rendimento" value="<?=$data_rendimento; ?>" size="15" /></td>
              </tr>
              <tr>
                <td align="right"><strong>Data Último Pagamento :</strong></td>
                <td><?=$data_liberado; ?></td>
              </tr>
              <tr>
                <td align="right">&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td align="right">&nbsp;</td>
                <td><input type="submit" name="Submit" id="Submit" value="Submit" /></td>
              </tr>
              </form>
          </table>

          <p>&nbsp;</p>
        </div>
      </div>
      <div class="clr"></div>
    </div>
  </div>
  <script>
    $(function() {
      $( "#day_start" ).datepicker({ dateFormat: 'yy-mm-dd' });
      $( "#day_end" ).datepicker({ dateFormat: 'yy-mm-dd' });
      $( "#dt_rendimento" ).datepicker({ dateFormat: 'yy-mm-dd' });
    });
  </script>