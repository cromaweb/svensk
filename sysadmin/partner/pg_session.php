<?php
	session_start();

	if (($_SESSION["adm_id"] == "") or ($_SESSION["adm_login"] == ""))
	{
		session_destroy();
		header("location:login.php");
	}
	else
	{
		$usrID = $_SESSION["adm_id"];
		$usrLogin = $_SESSION["adm_login"];
		$usrName = $_SESSION["adm_name"];
		$usrType = $_SESSION["adm_type"];
		$usrPages = $_SESSION["pages"];
	}
?>