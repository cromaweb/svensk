<?php
//FORMATA COMO TIMESTAMP
function dataToTimestamp($data){
   $ano = substr($data, 6,4);
   $mes = substr($data, 3,2);
   $dia = substr($data, 0,2);
return mktime(0, 0, 0, $mes, $dia, $ano);  
}

//USDBTC
function getTradeUSDBTC($value)
{
	
	//Conversão saldo disponível em BTC
	$currency = curl_init();
	curl_setopt($currency, CURLOPT_URL, "https://blockchain.info/tobtc?currency=USD&value=".$value);
	curl_setopt($currency, CURLOPT_RETURNTRANSFER, TRUE);
	curl_setopt($currency, CURLOPT_HEADER, FALSE);
	$r = curl_exec($currency);

	curl_close($currency);

	if (!empty($r)) {
		$retorno = $r;
	} else {
		$retorno = 'ERROR';
	}
	
	return $retorno;
   
}

//USDBTC
function getTradeBRLBTC($value)
{
	
	//Conversão saldo disponível em BTC
	$currency = curl_init();
	curl_setopt($currency, CURLOPT_URL, "https://blockchain.info/tobtc?currency=BRL&value=".$value);
	curl_setopt($currency, CURLOPT_RETURNTRANSFER, TRUE);
	curl_setopt($currency, CURLOPT_HEADER, FALSE);
	$r = curl_exec($currency);

	curl_close($currency);

	if (!empty($r)) {
		$retorno = $r;
	} else {
		$retorno = 'ERROR';
	}
	
	return $retorno;
   
}

//Current USDBTC
function getCurrentUSDBTC()
{
	//Cotação BTC/USD
	list($bitcoin) = abreSQL("SELECT rbma_valor_btc FROM tb_bns_rbma");
	
	return $bitcoin;
}

function getCurrentBRLBTC()
{
	//Cotação BTC/USD
	$currency = curl_init();
	curl_setopt($currency, CURLOPT_URL, "https://blockchain.info/en/ticker");
	curl_setopt($currency, CURLOPT_RETURNTRANSFER, TRUE);
	curl_setopt($currency, CURLOPT_HEADER, FALSE);
	$r = curl_exec($currency);

	curl_close($currency);

	if (!empty($r)) {
		$arr = json_decode($r);
		$retorno = $arr->BRL->buy;
	} else {
		$retorno = 'ERROR';
	}
	
	return $retorno;
}

function getBRLBTC()
{
	//Cotação BTC/USD
	$currency = curl_init();
	curl_setopt($currency, CURLOPT_URL, "https://www.mercadobitcoin.net/api/BTC/ticker");
	curl_setopt($currency, CURLOPT_RETURNTRANSFER, TRUE);
	curl_setopt($currency, CURLOPT_HEADER, FALSE);
	$r = curl_exec($currency);

	curl_close($currency);

	if (!empty($r)) {
		$arr = json_decode($r);
		$retorno = $arr->ticker->buy;
	} else {
		$retorno = 'ERROR';
	}
	
	return $retorno;
}

function getUSDBTC()
{
	//Cotação BTC/USD
	$currency = curl_init();
	curl_setopt($currency, CURLOPT_URL, "https://blockchain.info/en/ticker");
	curl_setopt($currency, CURLOPT_RETURNTRANSFER, TRUE);
	curl_setopt($currency, CURLOPT_HEADER, FALSE);
	$r = curl_exec($currency);

	curl_close($currency);

	if (!empty($r)) {
		$arr = json_decode($r);
		$retorno = $arr->USD->last;
	} else {
		$retorno = 'ERROR';
	}
	
	return $retorno;
}

function getconverteBRLBTC($valor)
{
	//Cotação BTC/USD
	$currency = curl_init();
	curl_setopt($currency, CURLOPT_URL, "https://www.mercadobitcoin.net/api/BTC/ticker");
	curl_setopt($currency, CURLOPT_RETURNTRANSFER, TRUE);
	curl_setopt($currency, CURLOPT_HEADER, FALSE);
	$r = curl_exec($currency);

	curl_close($currency);

	if (!empty($r)) {
		$arr = json_decode($r);
		$retorno = $arr->ticker->buy;
		$retorno = $valor / $retorno;
		$retorno = number_format($retorno, 8, '.', '');
	} else {
		$retorno = 'ERROR';
	}
	
	return $retorno;
}

function getBalanceAddress($address)
{

	$curl = curl_init();

	curl_setopt_array($curl, array(
		CURLOPT_URL => "https://blockchain.info/q/addressbalance/".trim($address),
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_TIMEOUT => 30000,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "GET",
		CURLOPT_HTTPHEADER => array(
			// Set Here Your Requesred Headers
			'Content-Type: application/json',
		),
	));

	$response = curl_exec($curl);
	curl_close($curl);

	if($response==404){
		$response = 0;
	}

	$bitcoin = ($response / 100000000);

	return $bitcoin;

}

//Função verifica se o usuário tem avanço de título
function avancoTitulo($idUsuario){
    
    $titulo = 0;
    
    $tituloUsr = getUserOneFieldDB("usr_titulo","$idUsuario");
    
    //Se for menor que o último título
    if($tituloUsr<6){
    
        $sql = geraSQL("SELECT tit_id,tit_vme FROM tb_bns_titulo");
    
        while($row = mysqli_fetch_array($sql)){
            
            $tituloID = $row["tit_id"];
            $vme = $row["tit_vme"];
            
            if($tituloUsr<$tituloID)
            {
                
                list($numRows) = abreSQL("SELECT count(*) FROM `tb_bns_titulo_vme` WHERE vme_sponsor='$idUsuario' AND vme_points>=$vme");
                echo "Titulo ID: $tituloID | Num: $numRows <br>";
                if($numRows>=5){
                    $titulo = $tituloID;
                    $tituloVME = $vme;
                    echo "Titulo alcançado: $titulo <br>";
                }
            }
        }
    }
    
    if($titulo!=0){
    	updateOneFieldDB("usr_titulo","tb_user",$titulo,"usr_id",$idUsuario);
    	setTitleHistoryDB($idUsuario,$titulo,$tituloVME);
    	echo "Título: $titulo";
    }
	
/*	$tituloUsr = getUserOneFieldDB("usr_titulo","$idUsuario");
	$tituloPtsUsr = getTitleOneFieldDB("tit_pontos","$tituloUsr");
	list($esqGP,$dirGP) = abreSQL("SELECT busr_pontos_esq_green,busr_pontos_dir_green FROM tb_bns_usuario WHERE busr_idUsuario='$idUsuario'");
	

	
	//Se o titulo do usuário for 0
	if($tituloUsr==0)
	{
		//Seleciona o titulo de menor pontuação
		list($tituloID,$tituloMinPts) = abreSQL("SELECT tit_id,MIN(tit_pontos) FROM tb_bns_titulo");
		
		if($esqGP >= $tituloMinPts && $dirGP >= $tituloMinPts)
		{
			updateOneFieldDB("usr_titulo","tb_user",$tituloID,"usr_id",$idUsuario);
			setTitleHistoryDB($idUsuario,$tituloID,$tituloMinPts,$esqGP,$dirGP);
			echo "Yes 1";
		}else{
			echo "No 1";
		}
		
	}else
	{
		//Seleciona o titulo de menor pontuação
		list($tituloID,$tituloMinPts) = abreSQL("SELECT tit_id,MIN(tit_pontos) FROM tb_bns_titulo WHERE tit_pontos>$tituloPtsUsr");
		echo $tituloUsr." | ".$tituloPtsUsr."<br>";
		echo $tituloID." | ".$tituloMinPts."<br>";
		echo $esqGP." | ".$dirGP."<br>";
		
		if(($esqGP >= $tituloMinPts) && ($dirGP >= $tituloMinPts))
		{
			updateOneFieldDB("usr_titulo","tb_user",$tituloID,"usr_id",$idUsuario);
			setTitleHistoryDB($idUsuario,$tituloID,$tituloMinPts,$esqGP,$dirGP);
			echo "Yes 2";
		}else{
			echo "No 2";	
		}
	}*/
	
}

//Function bonusConsolidado
function bonusConsolidado($idUsuario){
	
	//$now = date("Y-m-d");
	
	list($usuario) = abreSQL("SELECT bcons_idUsuario 
								   FROM tb_bns_consolidado 
								   WHERE bcons_idUsuario='$idUsuario'
								   AND DATE(bcons_data)=DATE(now())");
	
	//Se usuário/patrocinador não existir na tabela tb_bns_usuario, ele é inserido
	if($usuario==0){
		//Insere tabela na tb_bns_consolidado
		insertManyFieldsDB("tb_bns_consolidado","bcons_idUsuario","'$idUsuario'");
		
	}
	
}

//Function Bonus Indicação Indireta
function bonusIndicacaoIndireta($idUsuario,$idProduto,$idCompra,$qtdeItens){
    
	//Pega ID do upline do meu patrocinador para distribuir apartir do 2º nível
	list($patrocinador) = abreSQL("SELECT usr_invited_id FROM tb_user WHERE usr_id='$idUsuario'");
	
    //Verifica o avanço de título do patrocinador
    echo "Avanco Título: $patrocinador <br>";
    avancoTitulo($patrocinador);
	
	//ID inicial pega o ID do usuário comprador
	$id = $patrocinador;
	
	//Verifia se algum processo no controle de bonus de equipe já foi começado anteriormente
	list($controle) = abreSQL("SELECT count(*) FROM tb_bns_compra WHERE comp_bonus_equipe='N' AND comp_id='$idCompra'");
	echo "Usuário: $idUsuario | Patrocinador: $id<br>";
	//Se o processo não foi iniciado, dá prosseguimento
	if($controle==1)
	{
		//Insere o ínicio do processo de pontuação na tabela de controle com erro, se chegar no final atualiza para S, confirmando o processo
		updateManyFieldsDB("tb_bns_compra","comp_bonus_equipe='E'","comp_id",$idCompra);
		
		$contador = 0;
			
		while($finaliza==0)
		{
			
			list($pai) = abreSQL("SELECT usr_invited_id FROM tb_user WHERE usr_id='$id'");
			//list($perna) = abreSQL("SELECT contp_perna FROM tb_contperna_aws WHERE contp_idUsuario='$id' AND contp_idAntecessor='$pai'");
			
			//Se pai existir
			if(!empty($pai))
			{
			    
				//Se pai for diferente do ultimo patrocinador
				if($pai!=1)
				{
    			    //Inicia pontuação VME
    				//Seleciona bonus do plano ou conta comprado
    				list($bonus,$plano) = abreSQL("SELECT prod_valor,prod_especial FROM tb_product WHERE prod_id='$idProduto'");
    				$bonusVME = $bonus * $qtdeItens;
                    $pontos = explode('.', $bonusVME);
                    $pontos = $pontos[0];
                    echo "Produto: $idProduto | Bonus: $bonusVME | Pontos: $pontos<br>";
                    
                    //Seta o VME
                    setVMEDB($id,$pai,$pontos);
                    echo "VME - ID: $id | Pai: $pai<br>";
                    
                    //Seta os pontos VME
                    setBnsDB($idUsuario,$pai,'n','4',$idCompra,$perna,$pontos,0);
                    
                    avancoTitulo($pai);
				}
			    
			    //Zera a validação do titulo
			    $titValido = 0;
			    
			    //Pega o título do pai
			    list($titulo) = abreSQL("SELECT usr_titulo FROM tb_user WHERE usr_id='$pai'");
				echo "Pai: $pai | Título: $titulo<br>";
			    
			    if($contador==0 && $titulo>=2){
			        $titValido = 1;
			    }elseif($contador==1 && $titulo>=3){
			        $titValido = 1;
			    }elseif($contador==2 && $titulo>=4){
			        $titValido = 1;
			    }elseif($contador==3 && $titulo>=5){
			        $titValido = 1;
			    }elseif($contador==4 && $titulo==6){
			        $titValido = 1;
			    }
			    
			    if($titValido==1)
			    {
    				
    				//Verifica se usuario está negativado
    				$valido = getBonusLimit($pai);
    				
    				//Verifica se usuário pode receber bonus, se ele é valido
    				if($valido==0)
    				{
    					//Verifica se o campo da tb_bns_consolidado existe para o pai, caso não exista insere
    					bonusConsolidado($pai);
    					
    					//Se pai for diferente do ultimo patrocinador
    					if($pai!=1)
    					{	
    						echo $pai."<br>";
            				//Multiplica quantidade bonus com quantidade de itens
            				$bonus = $bonus * $qtdeItens;
    						echo "Bonus Total: $bonus<br>";
    						
    						//Insere na tabela tb_bns o tipo bonus e seus pontos
    						//executaSQL("INSERT INTO tb_bns (bon_idUsuario, bon_idUpline, bon_startfast, bon_tipo, bon_idCompra, bon_perna,bon_pontos, bon_data) VALUES ('$idUsuario','$pai','n','2','$idCompra','$perna','$bonus',now())");
    						
                    		//Calcula os 1% do bonus Indicação Indireta
                    		$valor = $bonus * 1 / 100;
                    		echo "Valor: $valor <br>";
                    		setBnsDB($idUsuario,$pai,'n','2',$idCompra,'',0,$valor);
                    		setFinanceiroExtratoDB($pai,$idCompra,"Bônus Indicação Indireta [$idCompra]",'c','c',$valor);
                    		setFinanceiroSaldoComissoesDB($pai,$valor,$btc);
                    		setUserLimitDB($pai,$valor,"Indicação Indireta");
                    		//setVMEDB($idUsuario,$patrocinador,$pontos[0]);
                    		
                    		list($usuario,$valorUsr) = getBnsNaoConsolidadoListDB("bcons_idUsuario,bcons_valor_team",$pai);
                    		
                    		$valorTotal = $valorUsr + $valor;
                    		echo "Valor Total: $valorTotal | $usuario<br>";
                    		
                    		executaSQL("UPDATE tb_bns_consolidado 
                    				   SET bcons_valor_team='$valorTotal'
                    				   WHERE bcons_idUsuario='$pai'
                    				   AND DATE(bcons_data)=DATE(now())");
                    		echo "Update<br><br>";
    					}
    				
    				}else{
    					setLogBonusLimitDB($pai,"Bônus perdido: Indicação Indireta $idCompra");
    				}
				
			    }else{
					echo "Sem qualificação para receber bônus!<br>";
					setAlteraLogDB($pai,"Bônus perdido título: Indicação Indireta $idCompra",$titulo);
				}
				
				$contador++;
				
				$processado = "S";
				
			}elseif(empty($pai)){
				//Se há erro no registro
				$processado = "E";
				$finaliza = 1;
			}
			
			$id = $pai;
			
			if($pai==1 || $contador==5){
				$finaliza = 1;
				$processado = "S";
			}
		}
		//Insere o final do processo de pontuação na tabela de controle
		updateManyFieldsDB("tb_bns_compra","comp_bonus_equipe='$processado'","comp_id",$idCompra);
		echo "Update BNS<br><br>";
	}
	
}


//Function STARTFAST BONUS
function bonusStartFast($idUsuario,$patrocinador,$idProduto,$tipoCompra,$idCompra,$qtdeItens){
	
	//Verifica se usuario está negativado
	$valido = getBonusLimit($patrocinador);
	
	//Se for plano
	if($tipoCompra==1 && $valido==0){
		echo "###### Bonus Start Fast #######<br>";
		echo "ID Usuário: $idUsuario<br>";
		echo "Patrocinador: $patrocinador<br>";
		echo "-----------------------------------------<br><br>";
		
		//Verifica se o campo da tb_bns_consolidado existe para o pai, caso não exista insere
		bonusConsolidado($patrocinador);
		
		list($bonus,$plano) = selectListManyFieldsDB("prod_valor,prod_especial","tb_product","prod_id",$idProduto);
		echo "Bonus $bonus | $plano <br>";
		
		//Multiplica quantidade bonus com quantidade de itens
		$bonus = $bonus * $qtdeItens;
		echo "Bonus Total: $bonus<br>";
		
		//Pontuação para o Sharing Bonus
		$pontos = explode('.', $bonus);
		$pontos = $pontos[0];

		$perna = getLegSponsorDB($idUsuario,$patrocinador);
		echo "Perna $perna <br>";
			
		//Calcula os 2.5% do bonus Start Fast
		$valor = $bonus * 2.5 / 100;
		echo "Valor: $valor <br>";
		setBnsDB($idUsuario,$patrocinador,'s','1',$idCompra,'',0,$valor);
		setFinanceiroExtratoDB($patrocinador,$idCompra,"Bônus Indicação Direta [$idCompra]",'c','c',$valor);
		setFinanceiroSaldoComissoesDB($patrocinador,$valor,$btc);
		setUserLimitDB($patrocinador,$valor,"Indicação Direta");
		setVMEDB($idUsuario,$patrocinador,$pontos);
        //Seta os pontos VME
        setBnsDB($idUsuario,$patrocinador,'n','4',$idCompra,'',$pontos,0);
		
		list($usuario,$valorUsr) = getBnsNaoConsolidadoListDB("bcons_idUsuario,bcons_valor_startfast",$patrocinador);
		
		$valorTotal = $valorUsr + $valor;
		echo "Valor Total: $valorTotal | $usuario<br>";
		
		executaSQL("UPDATE tb_bns_consolidado 
				   SET bcons_valor_startfast='$valorTotal'
				   WHERE bcons_idUsuario='$patrocinador'  
				   AND DATE(bcons_data)=DATE(now())");
		echo "Update Start Fast<br>";
	}else{
		setLogBonusLimitDB($patrocinador,"Bônus perdido: Indicação Direta $idCompra");
	}
		
}

function bonusTeam($idUsuario,$patrocinador,$idProduto,$tipoCompra,$idCompra,$qtdeItens)
{
	//echo "<br>###### Bonus TEAM #######<br>";
	//Bonus Blue Point
	echo "<br>###### Bonus Indicação Indireta #######<br>";
	bonusIndicacaoIndireta($idUsuario,$idProduto,$idCompra,$qtdeItens);

}

//Funçao function getBonusLimit($idUser)
/*
$idUser = ID do usuário
*/
function getBonusLimit($idUser){
	
	//Verifica se o user tem direito a bonus verificando se tem a
	list($limit) = abreSQL("SELECT count(*) as amount FROM tb_bns_usuario WHERE ((busr_investimento_total = 0 OR busr_investimento_total IS NULL) OR (busr_bonus_limite = 0 OR busr_bonus_limite IS NULL)) AND busr_idUsuario = $idUser");
	
	//Verifica se o user tem direito a bonus verificando se tem a
	//list($fee) = abreSQL("SELECT usr_fee FROM tb_user WHERE usr_id = $idUser");
	
	//$amount = $limit + $fee;
	$amount = $limit;
	
	return $amount;
	
}


//Function Bonus Indicação Indireta
function bonusIndicacaoIndiretaX($idUsuario,$idProduto,$idCompra,$qtdeItens){
    
	//Pega ID do upline do meu patrocinador para distribuir apartir do 2º nível
	list($patrocinador) = abreSQL("SELECT usr_invited_id FROM tb_user WHERE usr_id='$idUsuario'");
	
	//ID inicial pega o ID do usuário comprador
	$id = $patrocinador;

	echo "Usuário: $idUsuario | Patrocinador: $id<br>";
	//Se o processo não foi iniciado, dá prosseguimento
	if($controle==0)
	{
		
		$contador = 0;
			
		while($finaliza==0)
		{
			
			list($pai) = abreSQL("SELECT usr_invited_id FROM tb_user WHERE usr_id='$id'");
			//list($perna) = abreSQL("SELECT contp_perna FROM tb_contperna_aws WHERE contp_idUsuario='$id' AND contp_idAntecessor='$pai'");
			
			//Se pai existir
			if(!empty($pai))
			{
			    
				//Se pai for diferente do ultimo patrocinador
				if($pai!=1)
				{
    			    //Inicia pontuação VME
    				//Seleciona bonus do plano ou conta comprado
    				list($bonus,$plano) = abreSQL("SELECT prod_valor,prod_especial FROM tb_product WHERE prod_id='$idProduto'");
    				$bonusVME = $bonus * $qtdeItens;
                    $pontos = explode('.', $bonusVME);
                    $pontos = $pontos[0];
                    echo "Produto: $idProduto | Bonus: $bonusVME | Pontos: $pontos<br>";
                    
                    //Seta o VME
                    setVMEDB($id,$pai,$pontos);
                    echo "VME - ID: $id | Pai: $pai<br>";
                    
                    //Seta os pontos VME
                    setBnsDB($idUsuario,$pai,'n','4',$idCompra,$perna,$pontos,0);
                    
                    avancoTitulo($pai);
				}
			    
			    //Zera a validação do titulo
			    $titValido = 0;
			    
			    //Pega o título do pai
			    list($titulo) = abreSQL("SELECT usr_titulo FROM tb_user WHERE usr_id='$pai'");
				echo "Pai: $pai | Título: $titulo<br>";
				
				$contador++;
				
				$processado = "S";
				
			}else{
				//Se há erro no registro
				$processado = "E";
				$finaliza = 1;
			}
			
			$id = $pai;
			
			if($pai==1 || $contador==5){
				$finaliza = 1;
				$processado = "S";
			}
		}

		echo "Update BNS<br><br>";
	}
	
}


//Function STARTFAST BONUS
function bonusStartFastX($idUsuario,$patrocinador,$idProduto,$tipoCompra,$idCompra,$qtdeItens){
	
	//Se for plano
	if($tipoCompra==1){
		echo "###### Bonus Start Fast #######<br>";
		echo "ID Usuário: $idUsuario<br>";
		echo "Patrocinador: $patrocinador<br>";
		echo "-----------------------------------------<br><br>";
		
		list($bonus,$plano) = selectListManyFieldsDB("prod_valor,prod_especial","tb_product","prod_id",$idProduto);
		echo "Bonus $bonus | $plano <br>";
		
		//Multiplica quantidade bonus com quantidade de itens
		$bonus = $bonus * $qtdeItens;
		echo "Bonus Total: $bonus<br>";
		
		//Pontuação para o Sharing Bonus
		$pontos = explode('.', $bonus);
		$pontos = $pontos[0];

		setVMEDB($idUsuario,$patrocinador,$pontos);

	}
		
}

?>
