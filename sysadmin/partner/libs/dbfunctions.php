<?php
//Funçao selectOneField
/*
$field = campo a ser retornado
$table = nome da tabela
$fieldCondiction = campo de condição
$valueSearch = valor procurado no campo de condição
$value = valor retornado
*/
function selectOneFieldDB($field,$table,$fieldCondiction,$valueCondiction){
	//Faz o select de acordo com as informações
	list($value) = abreSQL("SELECT $field FROM $table WHERE $fieldCondiction='$valueCondiction'");
	return $value; 
}


//Funçao updateOneField
/*
$field = campo a ser retornado
$table = nome da tabela
$fieldCondiction = campo de condição
$valueSearch = valor procurado no campo de condição
$value = valor retornado
*/
function updateOneFieldDB($field,$table,$value,$fieldCondiction,$valueCondiction){
	//Faz o select de acordo com as informações
	executaSQL("UPDATE $table SET $field='$value' WHERE $fieldCondiction='$valueCondiction'");
}

//Funçao selectListManyFieldsDB($fields,$table,$fieldCondiction,$valueCondiction)
/*
$fields = campos a ser retornado
$table = nome da tabela
$fieldCondiction = campo de condição
$valueSearch = valor procurado no campo de condição
$value = valor retornado
*/
function selectListManyFieldsDB($fields,$table,$fieldCondiction,$valueCondiction){
	//Faz o select de acordo com as informações
	$value = abreSQL("SELECT $fields FROM $table WHERE $fieldCondiction='$valueCondiction'");
	return $value; 
}

//Funçao insertManyFieldsDB($table,$fields,$values)
/*
$fields = campos a ser retornado
$table = nome da tabela
$fieldCondiction = campo de condição
$valueSearch = valor procurado no campo de condição
$value = valor retornado
*/
function insertManyFieldsDB($table,$fields,$values){
	//Faz o insert em uma tabela com vários campos
	executaSQL("INSERT INTO $table ($fields) VALUES ($values)");
}

//Funçao updateManyFieldsDB
/*
$fields = campos a ser retornado
$table = nome da tabela
$fieldCondiction = campo de condição
$valueSearch = valor procurado no campo de condição
$value = valor retornado
*/
function updateManyFieldsDB($table,$fields,$fieldCondiction,$valueCondiction){
	//Faz o update em uma tabela com vários campos
	executaSQL("UPDATE $table SET $fields WHERE $fieldCondiction='$valueCondiction'");
}

//Funçao userSelectOneField
/*
$field = campo a ser retornado
$idUser = ID do usuário
*/
function getUserOneFieldDB($field,$idUser){
	//Faz o select de acordo com as informações
	list($value) = abreSQL("SELECT $field FROM tb_user WHERE usr_id='$idUser'");
	return $value; 
}

//Funçao titleSelectOneField
/*
$field = campo a ser retornado
$idUser = ID do usuário
*/
function getTitleOneFieldDB($field,$idTitle){
	//Faz o select de acordo com as informações
	list($value) = abreSQL("SELECT $field FROM tb_bns_titulo WHERE tit_id='$idTitle'");
	return $value; 
}

//Funçao TitleHistoryInsert
/*
$idUser = ID do usuário
$idTitle = ID do título
$points = pontos do título
*/
function setTitleHistoryDB($idUser,$idTitle,$points){
	//Faz insert tabela tb_bns_titulo_historico
	executaSQL("INSERT INTO tb_bns_titulo_historico (`tithist_idUsuario`, `tithist_titulo`, `tithist_pontos`) VALUES ('$idUser', '$idTitle', '$points')");
}

//Funçao setBnsDB($idUser,$sponsor,$startfast,$type,$purchase,$leg,$points,$value)
/*
$idUser = ID do usuário
$idTitle = ID do título
$points = pontos do título
*/
function setBnsDB($idUser,$sponsor,$startfast,$type,$purchase,$leg,$points,$value){
	//Faz insert tabela tb_bns
	executaSQL("INSERT INTO tb_bns (bon_idUsuario, bon_idUpline, bon_startfast, bon_tipo, bon_idCompra, bon_perna, bon_pontos, bon_valor, bon_data) VALUES ('$idUser','$sponsor','$startfast','$type','$purchase','$leg','$points','$value',now())");
}

//Funçao setFinanceiroSaldoLiberadoDB($idUser,$value)
/*
$idUser = ID do usuário
$idTitle = ID do título
$points = pontos do título
*/
function setFinanceiroSaldoLiberadoDB($idUser,$value){
	//Verifica se usuario tem saldo e existe na tabela
	$getFinanceiroSaldo = getFinanceiroSaldoDB($idUser);
	$numRows = mysqli_num_rows($getFinanceiroSaldo);
	
	if($numRows==0){
		//Faz insert tabela tb_financeiro_saldo
		executaSQL("INSERT INTO tb_financeiro_saldo (bsal_idUsuario,bsal_saldo_liberado) VALUES ('$idUser','$value')");
	}else{
		$reg = fetch($getFinanceiroSaldo);
		$value = $reg["bsal_saldo_liberado"] + $value;
		//Faz update tabela tb_financeiro_extrato_saldo
		executaSQL("UPDATE tb_financeiro_saldo SET bsal_saldo_liberado='$value' WHERE bsal_idUsuario='$idUser'");	
	}

}


//Funçao setFinanceiroSaldoRendimentoDB($idUser,$value)
/*
$idUser = ID do usuário
$idTitle = ID do título
$points = pontos do título
*/
function setFinanceiroSaldoRendimentoDB($idUser,$value,$btc = 0){
	//Verifica se usuario tem saldo e existe na tabela
	$getFinanceiroSaldo = getFinanceiroSaldoDB($idUser);
	$numRows = mysqli_num_rows($getFinanceiroSaldo);
	
	if($numRows==0){
		//Faz insert tabela tb_financeiro_saldo
		executaSQL("INSERT INTO tb_financeiro_saldo (bsal_idUsuario,bsal_saldo_rendimento,bsal_saldo_rendimento_btc) VALUES ('$idUser','$value','$btc')");
	}else{
		$reg = fetch($getFinanceiroSaldo);
		$value = $reg["bsal_saldo_rendimento"] + $value;
		$value_btc = $reg["bsal_saldo_rendimento_btc"] + $btc;
		//Faz update tabela tb_financeiro_extrato_saldo
		executaSQL("UPDATE tb_financeiro_saldo SET bsal_saldo_rendimento='$value',bsal_saldo_rendimento_btc='$value_btc' WHERE bsal_idUsuario='$idUser'");	
	}

}

//Funçao setFinanceiroSaldoRendimentoDB($idUser,$value)
/*
$idUser = ID do usuário
$idTitle = ID do título
$points = pontos do título
*/
function setFinanceiroSaldoComissoesDB($idUser,$value,$btc = 0){
	//Verifica se usuario tem saldo e existe na tabela
	$getFinanceiroSaldo = getFinanceiroSaldoDB($idUser);
	$numRows = mysqli_num_rows($getFinanceiroSaldo);
	
	if($numRows==0){
		//Faz insert tabela tb_financeiro_saldo
		executaSQL("INSERT INTO tb_financeiro_saldo (bsal_idUsuario,bsal_saldo_comissoes,bsal_saldo_comissoes_btc) VALUES ('$idUser','$value','$btc')");
	}else{
		$reg = fetch($getFinanceiroSaldo);
		$value = $reg["bsal_saldo_comissoes"] + $value;
		$value_btc = $reg["bsal_saldo_comissoes_btc"] + $btc;
		//Faz update tabela tb_financeiro_extrato_saldo
		executaSQL("UPDATE tb_financeiro_saldo SET bsal_saldo_comissoes='$value',bsal_saldo_comissoes_btc='$value_btc' WHERE bsal_idUsuario='$idUser'");	
	}

}

//Funçao function getFinanceiroSaldoDB($idUser,$value)
/*
$idUser = ID do usuário
$idTitle = ID do título
$points = pontos do título
*/
function getFinanceiroSaldoDB($idUser){
	
	//Select tabela tb_financeiro_extrato_saldo
	$value = geraSQL("SELECT * FROM tb_financeiro_saldo WHERE bsal_idUsuario='$idUser'");
	return $value;
}

//Funçao setFinanceiroExtratoDB($idUser,$purchase,$description,$type,$value)
/*
$idUser = ID do usuário
$idTitle = ID do título
$points = pontos do título
*/
function setFinanceiroExtratoDB($idUser,$purchase,$description,$conta = 'l',$type,$value){
	
	list($saldoLiberadoAnterior,$saldoRendimentoAnterior,$saldoComissoesAnterior) = abreSQL("SELECT bsal_saldo_liberado,bsal_saldo_rendimento,bsal_saldo_comissoes FROM tb_financeiro_saldo WHERE bsal_idUsuario='$idUser'");
	
	if($conta=='l')
	{
		$saldoAnterior = $saldoLiberadoAnterior;
		$saldoAtual = $saldoLiberadoAnterior + $value;
	}
	elseif($conta=='r')
	{
		$saldoAnterior = $saldoRendimentoAnterior;
		$saldoAtual = $saldoRendimentoAnterior + $value;
	}
	elseif($conta=='c')
	{
		$saldoAnterior = $saldoComissoesAnterior;
		$saldoAtual = $saldoComissoesAnterior + $value;
	}
	
	//Faz insert tabela tb_financeiro_extrato
	executaSQL("INSERT INTO tb_financeiro_extrato (extf_idUsuario,extf_idCompra, extf_descricao, extf_conta, extf_saldo_anterior, extf_saldo_atual, extf_tipo, extf_valor, extf_data) VALUES ('$idUser','$purchase','$description','$conta','$saldoAnterior','$saldoAtual','$type','$value',now())");
}

//Funçao function getLimitDB($idUser,$value)
/*
$idUser = ID do usuário
*/
function getUserLimitDB($idUser){
	
	//Select tabela tb_bns_usuario
	list($value) = abreSQL("SELECT busr_bonus_limite FROM tb_bns_usuario WHERE busr_idUsuario='$idUser'");
	return $value;
}

//Funçao function setUserLimitDB($idUser,$value)
/*
$idUser = ID do usuário
$value = valor do bonus
*/
function setUserLimitDB($idUser,$value,$description = "No"){
	
	//Pega o limite do usuário
	$limit = getUserLimitDB($idUser);
	
	$new_limit = $limit - $value;
	
	setUserLimitExtratoDB($idUser,$value,$new_limit,$limit,$description);
	
	if($new_limit<=0){
		
		//Zera o limite do usuário
		executaSQL("UPDATE tb_bns_usuario SET busr_bonus_limite='0' WHERE busr_idUsuario='$idUser'");
		
	}else{
		//Atualiza o limite do usuário
		executaSQL("UPDATE tb_bns_usuario SET busr_bonus_limite='$new_limit' WHERE busr_idUsuario='$idUser'");
	}
}

//Funçao function setUserLimitDB($idUser,$value)
/*
$idUser = ID do usuário
$value = valor do bonus
*/
function setUserLimitExtratoDB($user,$value,$currentValue,$previousValue,$description = "No"){
		
	//Zera o limite do usuário
	executaSQL("INSERT INTO tb_log_bns_limit_extrato (lble_user, lble_valor, lble_valor_anterior,lble_valor_atual, lble_descricao) VALUES ('$user', '$value','$previousValue','$currentValue','$description')");

}

//Funçao setVMEDB($idUser,$value)
/*
$idUser = ID do usuário
$idTitle = ID do título
$points = pontos do título
*/
function setVMEDB($user,$sponsor,$value){
    
    $sql = "INSERT INTO tb_bns_titulo_vme (vme_user,vme_sponsor,vme_points) 
			 			VALUES ('$user','$sponsor','$value')
						ON DUPLICATE KEY 
						UPDATE vme_points = vme_points + ".$value."";
	//Verifica se usuario tem saldo e existe na tabela
    executaSQL($sql);

    executaSQL("INSERT INTO tb_bns_log_vme (vmelog_user,vmelog_sponsor,vmelog_points) VALUES ('$user','$sponsor','$value')");

}

//Funçao setVMEunilevelDB($idUser,$value)
/*
$idUser = ID do usuário
$idTitle = ID do título
$points = pontos do título
*/
function setVMEunilevelDB($user,$sponsor,$value){
    
	list($points) = abreSQL("SELECT unilevel_vme FROM tb_unilevel WHERE unilevel_user='$user' AND  unilevel_antecessor='$sponsor'");
	$points = $points + $value;
	
	executaSQL("UPDATE tb_unilevel SET unilevel_vme='$points' WHERE unilevel_user='$user' AND  unilevel_antecessor='$sponsor'");

}

//Funçao setBnsUsuarioMinerDB($idUser,$value)
/*
$idUser = ID do usuário
$idTitle = ID do título
$points = pontos do título
*/
function setBnsUsuarioMinerDB($idUser,$value){
	//Verifica se usuario tem saldo e existe na tabela
	list($numRows,$rbma_miner) = abreSQL("SELECT count(busr_idUsuario),busr_rbma_miner FROM tb_bns_usuario WHERE busr_idUsuario='$idUser'");
	
	if($numRows==0){
		//Faz insert tabela tb_financeiro_saldo
		executaSQL("INSERT INTO tb_bns_usuario (busr_idUsuario,busr_rbma_miner) VALUES ('$idUser','$value')");
	}else{
		
		$value = $rbma_miner + $value;
		//Faz update tabela tb_financeiro_extrato_saldo
		executaSQL("UPDATE tb_bns_usuario SET busr_rbma_miner='$value' WHERE busr_idUsuario='$idUser'");	
	}

}

//Funçao setBnsUsuarioMinerDB($idUser,$value)
/*
$idUser = ID do usuário
$idTitle = ID do título
$points = pontos do título
*/
function setBnsUsuarioTraderDB($idUser,$value){
	//Verifica se usuario tem saldo e existe na tabela
	list($numRows,$rbma_trader) = abreSQL("SELECT count(busr_idUsuario),busr_rbma_trader FROM tb_bns_usuario WHERE busr_idUsuario='$idUser'");
	
	if($numRows==0){
		//Faz insert tabela tb_financeiro_saldo
		executaSQL("INSERT INTO tb_bns_usuario (busr_idUsuario,busr_rbma_trader) VALUES ('$idUser','$value')");
	}else{
		
		$value = $rbma_trader + $value;
		//Faz update tabela tb_financeiro_extrato_saldo
		executaSQL("UPDATE tb_bns_usuario SET busr_rbma_trader='$value' WHERE busr_idUsuario='$idUser'");	
	}

}

//Funçao getBnsNaoConsolidadoListDB($field,$idUser)
/*
$field = campo a ser retornado
$idUser = ID do usuário
*/
function getBnsNaoConsolidadoListDB($field,$idUser){
	//Faz o select de acordo com as informações
	$value = abreSQL("SELECT $field FROM tb_bns_consolidado 
					 WHERE bcons_idUsuario='$idUser' 
					 AND DATE(bcons_data)=DATE(now())");
	return $value; 
}

//Funçao getLegSponsorDB($idUser,$sponsor)
/*
$field = campo a ser retornado
$idUser = ID do usuário
*/
function getLegSponsorDB($idUser,$sponsor){
	//Faz o select de acordo com as informações
	list($value) = abreSQL("SELECT contp_perna FROM tb_contperna_aws 
									WHERE contp_idUsuario='$idUser' 
									AND contp_idAntecessor='$sponsor'");
	return $value; 
}

//Funçao getSponsorDB($idUser)
/*
$field = campo a ser retornado
$idUser = ID do usuário
*/
function getSponsorDB($idUser){
	//Faz o select de acordo com as informações
	list($value) = abreSQL("SELECT usr_invited_id FROM tb_user WHERE usr_id='$idUser'");
	return $value; 
}

/**
	Function: getInvoiceValue($invoice)
	Parameters: $invoice: Número da fatura
	Action: Retorna o valor da fatura
	Return: fat_valor
	Table: tb_fatura
*/
function getInvoiceValue($invoice)
{
		list($invoiceValue) = abreSQL("SELECT fat_valor FROM tb_fatura WHERE fat_id = '$invoice'");
		return $invoiceValue;
}

/**
	Function: getFormOfPaymentDB($id)
	Parameters: $invoice: Número da fatura
	Action: Retorna o valor da fatura
	Return: fat_valor
	Table: tb_fatura
*/
function getFormOfPaymentDB($id)
{
		list($value) = abreSQL("SELECT mdp_nome FROM tb_meiodepagamento WHERE mdp_id='$id'");
		return $value;
}

/**
	Function: getInvoiceOrderDB($idInvoice,$numRows = 0)
	Parameters: $invoice: Número da fatura
	Action: Retorna o valor da fatura
	Return: fat_valor
	Table: tb_fatura
*/
function getInvoiceOrderDB($idInvoice,$numRows = 0)
{
	$value = geraSQL("SELECT prod_especial,prod_id,prod_max,prod_autoenvio,prod_valor,prod_ordem,ped_id FROM tb_product INNER JOIN tb_item INNER JOIN tb_pedido INNER JOIN tb_fatura ON prod_id=item_produto AND item_pedido=ped_id AND ped_id=fat_pedido AND fat_id='$idInvoice'");
	
	if($numRows==1){
		$num = mysqli_num_rows($value);
		return $num;
	}else{
		return $value;
	}
}

/**
	Function: confirmPaymentInvoiceDB($idInvoice,$descPayment,$formPayment)
	Parameters: $invoice: Número da fatura
	Action: Retorna o valor da fatura
	Return: fat_valor
	Table: tb_fatura
*/
function setPaymentInvoiceDB($idInvoice,$descPayment,$formPayment)
{
	$sql = executaSQL("UPDATE tb_fatura SET fat_status='2',fat_pagseguro='$descPayment',fat_formaPagamento='$formPayment',fat_dataPagamento=now() 
					  WHERE fat_id='$idInvoice'");
	return $sql;
}

/**
	Function: confirmPaymentInvoiceDB($idInvoice,$descPayment,$formPayment)
	Parameters: $invoice: Número da fatura
	Action: Retorna o valor da fatura
	Return: fat_valor
	Table: tb_fatura
*/
function setOrderDB($idOrder)
{
	$sql = executaSQL("UPDATE tb_pedido SET ped_pago='1',ped_dataPagamento=now() 
					  WHERE ped_id='$idOrder'");
	return $sql;
}

/**
	Function: setNewPlanUserDB($idUser,$product,$order,$max,$dateStart,$dateEnd)
	Parameters: $invoice: Número da fatura
	Action: Retorna o valor da fatura
	Return: fat_valor
	Table: tb_fatura
*/
function setNewPlanUserDB($idUser,$product,$order,$max,$dateStart,$dateEnd)
{
	$sql = executaSQL("INSERT INTO `tb_user_plans`(`uplan_user_id`, `uplan_prod_id`,`uplan_pedido_id`, `uplan_max`, `uplan_day_start`, `uplan_day_end`) VALUES ('$idUser','$product','$order','$max','$dateStart','$dateEnd')");
	return $sql;
}

/**
	Function: setUpdatePackageUserDB($idUser,$order,$product)
	Parameters: $invoice: Número da fatura
	Action: Retorna o valor da fatura
	Return: fat_valor
	Table: tb_alteraLog
*/
function setUpdatePackageUserDB($idUser,$order,$product)
{
	executaSQL("UPDATE tb_pedido SET ped_upgrade='1' WHERE ped_id='$order'");
	$sql = executaSQL("UPDATE tb_user SET usr_start='1',usr_package='$product' WHERE usr_id='$idUser'");	
	return $sql;
}

/**
	Function: setBonusPurchase($idUser,$sponsor,$invoice,$product,$usrPackage,$typePurchase,$usrActive)
	Parameters: $invoice: Número da fatura
	Action: Retorna o valor da fatura
	Return: fat_valor
	Table: tb_bns_compra
*/
function setBonusPurchase($idUser,$sponsor,$invoice,$product,$usrPackage,$typePurchase,$usrActive)
{
	executaSQL("INSERT INTO tb_bns_compra (comp_idUsuario,comp_patrocinador,comp_idFatura, comp_idProduto,comp_idProdutoAnterior, comp_tipoCompra, comp_data,comp_ativo) VALUES ('$idUser','$sponsor','$invoice','$product','$usrPackage','$typePurchase',now(),'$usrActive')");
}

/**
	Function: setAlteraLogDB($idUser,$description,$usrPackage)
	Parameters: $invoice: Número da fatura
	Action: Retorna o valor da fatura
	Return: fat_valor
	Table: tb_alteraLog
*/
function setLogBonusLimitDB($idUser,$description)
{
	executaSQL("INSERT INTO tb_log_bns_limit (lbl_user,lbl_descricao) VALUES($idUser,'$description')");
}

/**
	Function: setAlteraLogDB($idUser,$description,$usrPackage)
	Parameters: $invoice: Número da fatura
	Action: Retorna o valor da fatura
	Return: fat_valor
	Table: tb_alteraLog
*/
function setAlteraLogDB($idUser,$description,$usrPackage)
{
	executaSQL("INSERT INTO tb_alteraLog (logalt_tipo,logalt_valor,logalt_usuario) VALUES('$description','$usrPackage',$idUser)");
}

/**
	Function: getNumLegsUplinesDB($idUser)
	Parameters: $invoice: Número da fatura
	Action: Retorna o valor da fatura
	Return: fat_valor
	Table: tb_fatura
*/
function getNumLegsUplinesDB($idUser)
{
	list($value) = abreSQL("SELECT count(*) FROM tb_contperna_aws WHERE contp_idUsuario='$idUser'");
	return $value;
}

//Funçao function setFinanceiroSaldoDB($idUser,$value)
/*
$idUser = ID do usuário
$idTitle = ID do título
$points = pontos do título
*/
function getGreenPoints($idUser){
	
	//Select tabela tb_financeiro_extrato_saldo
	$value = abreSQL("SELECT busr_idUsuario,busr_pontos_esq_green,busr_pontos_dir_green FROM tb_bns_usuario WHERE busr_idUsuario='$idUser'");
	return $value;
}

?>
