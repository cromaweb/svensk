<!-- Add mousewheel plugin (this is optional) -->
<script type="text/javascript" src="../fancybox/lib/jquery.mousewheel-3.0.6.pack.js"></script>

<!-- Add fancyBox main JS and CSS files -->
<script type="text/javascript" src="../fancybox/source/jquery.fancybox.js?v=2.1.5"></script>
<link rel="stylesheet" type="text/css" href="../fancybox/source/jquery.fancybox.css?v=2.1.5" media="screen" />

<!-- Add Button helper (this is optional) -->
<link rel="stylesheet" type="text/css" href="../fancybox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" />
<script type="text/javascript" src="../fancybox/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>

<!-- Add Thumbnail helper (this is optional) -->
<link rel="stylesheet" type="text/css" href="../fancybox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" />
<script type="text/javascript" src="../fancybox/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>

<!-- Add Media helper (this is optional) -->
<script type="text/javascript" src="../fancybox/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>

	<script type="text/javascript">
		$(document).ready(function() {
			/*
			 *  Simple image gallery. Uses default settings
			 */

			$('.fancybox').fancybox();

			/*
			 *  Different effects
			 */

			// Change title type, overlay closing speed
			$(".fancybox-effects-a").fancybox({
				helpers: {
					title : {
						type : 'outside'
					},
					overlay : {
						speedOut : 0
					}
				}
			});

			// Disable opening and closing animations, change title type
			$(".fancybox-effects-b").fancybox({
				openEffect  : 'none',
				closeEffect	: 'none',

				helpers : {
					title : {
						type : 'over'
					}
				}
			});

			// Set custom style, close if clicked, change title type and overlay color
			$(".fancybox-effects-c").fancybox({
				wrapCSS    : 'fancybox-custom',
				closeClick : true,

				openEffect : 'none',

				helpers : {
					title : {
						type : 'inside'
					},
					overlay : {
						css : {
							'background' : 'rgba(238,238,238,0.85)'
						}
					}
				}
			});

			// Remove padding, set opening and closing animations, close if clicked and disable overlay
			$(".fancybox-effects-d").fancybox({
				padding: 0,

				openEffect : 'elastic',
				openSpeed  : 150,

				closeEffect : 'elastic',
				closeSpeed  : 150,

				closeClick : true,

				helpers : {
					overlay : null
				}
			});

			/*
			 *  Button helper. Disable animations, hide close button, change title type and content
			 */

			$('.fancybox-buttons').fancybox({
				openEffect  : 'none',
				closeEffect : 'none',

				prevEffect : 'none',
				nextEffect : 'none',

				closeBtn  : false,

				helpers : {
					title : {
						type : 'inside'
					},
					buttons	: {}
				},

				afterLoad : function() {
					this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '');
				}
			});


			/*
			 *  Thumbnail helper. Disable animations, hide close button, arrows and slide to next gallery item if clicked
			 */

			$('.fancybox-thumbs').fancybox({
				prevEffect : 'none',
				nextEffect : 'none',

				closeBtn  : false,
				arrows    : false,
				nextClick : true,

				helpers : {
					thumbs : {
						width  : 50,
						height : 50
					}
				}
			});

			/*
			 *  Media helper. Group items, disable animations, hide arrows, enable media and button helpers.
			*/
			$('.fancybox-media')
				.attr('rel', 'media-gallery')
				.fancybox({
					openEffect : 'none',
					closeEffect : 'none',
					prevEffect : 'none',
					nextEffect : 'none',

					arrows : false,
					helpers : {
						media : {},
						buttons : {}
					}
				});

			/*
			 *  Open manually
			 */

			$("#fancybox-manual-a").click(function() {
				$.fancybox.open('1_b.jpg');
			});

			$("#fancybox-manual-b").click(function() {
				$.fancybox.open({
					href : 'iframe.html',
					type : 'iframe',
					padding : 5
				});
			});

			$("#fancybox-manual-c").click(function() {
				$.fancybox.open([
					{
						href : '1_b.jpg',
						title : 'My title'
					}, {
						href : '2_b.jpg',
						title : '2nd title'
					}, {
						href : '3_b.jpg'
					}
				], {
					helpers : {
						thumbs : {
							width: 75,
							height: 50
						}
					}
				});
			});


		});
	</script>
<script language="Javascript">

function complete() {
	if (document.formSearch.search.value.length < 1) {
		alert("Insira algum valor no campo.");
		formSearch.search.focus();
		return false;
	}
	return true;
}
</script>
  <div class="content">
    <div class="content_resize">
      <div class="mainbar">
        <div class="article">
          <h2><span>Logística</span></h2>
          <div class="clr"></div>
<?php

include("../config.php");

$src = $_POST["src"];
$search = $_POST["search"];
$type	= $_POST["type"];
$pg	= $_GET["pg"];

//######### INICIO Paginação
	$numreg = 100; // Quantos registros por página vai ser mostrado
	if (!isset($pg)) {
		$pg = 0;
	}
	$inicial = $pg * $numreg;
	
//######### FIM dados Paginação
	
	if($src==1)
	{
		if($type=="nossonumero"){
			// Faz o Select pegando o registro inicial até a quantidade de registros para página
			$sql = "SELECT usr_id,
						  usr_login_id,
						  usr_name,
						  usr_email,
						  fat_id,
						  fat_descricao,
						  fat_valor,
						  ped_entregue 
				  FROM tb_user a 
				  INNER JOIN tb_fatura b 
				  INNER JOIN tb_boleto c
				  INNER JOIN tb_pedido d
				  ON a.usr_id=b.fat_idUsuario 
				  AND b.fat_id=c.bol_fatura
				  AND b.fat_pedido=d.ped_id
				  AND d.ped_promocao='0'
				  AND c.bol_id='$search' 
				  AND b.fat_status='2'";
				  
		}elseif($type=="todos"){
			
		$sql = "SELECT usr_id,
						usr_login_id,
						usr_name,
						usr_email,
						fat_id,
						fat_descricao,
						fat_valor,
						ped_entregue 
				FROM tb_user a 
				INNER JOIN tb_fatura b 
				INNER JOIN tb_pedido c
				ON a.usr_id=b.fat_idUsuario
				AND b.fat_pedido=c.ped_id
				AND c.ped_promocao='0'
				AND b.fat_status='2' ";
			
		}else{
			// Faz o Select pegando o registro inicial até a quantidade de registros para página
			$sql = "SELECT usr_id,
						  usr_login_id,
						  usr_name,
						  usr_email,
						  fat_id,
						  fat_descricao,
						  fat_valor,
						  ped_entregue 
				  FROM tb_user a 
				  INNER JOIN tb_fatura b
				  INNER JOIN tb_pedido c
				  ON a.usr_id=b.fat_idUsuario 
				  AND b.fat_pedido=c.ped_id
				  AND c.ped_promocao='0'
				  AND b.fat_status='2'";
		}

		if($type=="invoice")
		{
			$sql .= "AND fat_id='$search' ";
		}elseif($type=="name")
		{
			$sql .= "AND usr_name LIKE '%$search%' ";
		}elseif($type=="username")
		{
			$sql .= "AND usr_login_id='$search' ";
		}elseif($type=="userid")
		{
			$sql .= "AND usr_id='$search' ";
		}
		conexao();
		//Conta a quantidade de registros
		$sql_conta = mysql_query($sql);
		// Quantidade de registros pra paginação
		$quantreg = mysqli_num_rows($sql_conta); 
		fecha();
		
		$sql .= " ORDER BY fat_id ASC 
			  LIMIT $inicial, $numreg";
		
		conexao();
		$sql = mysql_query($sql)or die(mysql_error());
		fecha();
		
		
	}else
	{
		conexao();	
		// Faz o Select pegando o registro inicial até a quantidade de registros para página
		$sql = mysql_query("SELECT usr_id,
									usr_login_id,
									usr_name,
									usr_email,
									fat_id,
									fat_descricao,
									fat_valor,
									ped_entregue 
							FROM tb_user a 
							INNER JOIN tb_fatura b 
							INNER JOIN tb_pedido c
							ON a.usr_id=b.fat_idUsuario
							AND b.fat_pedido=c.ped_id
							AND c.ped_promocao='0'
							AND c.ped_entregue<>'2'
							AND c.ped_entregue<>'3'
							AND c.ped_entregue<>'4'
							AND b.fat_status='2'
							ORDER BY a.usr_id ASC 
							LIMIT $inicial, $numreg");
	
		// Serve para contar quantos registros você tem na seua tabela para fazer a paginação
		$sql_conta = mysql_query("SELECT usr_id,
									  usr_login_id,
									  usr_name,
									  usr_email,
									  fat_id,
									  fat_descricao,
									  fat_valor,
									  ped_entregue 
								  FROM tb_user a 
								  INNER JOIN tb_fatura b
								  INNER JOIN tb_pedido c
								  ON a.usr_id=b.fat_idUsuario 
								  AND b.fat_pedido=c.ped_id
								  AND c.ped_promocao='0'
									AND c.ped_entregue<>'2'
									AND c.ped_entregue<>'3'
									AND c.ped_entregue<>'4'
								  AND b.fat_status='2' ");
		
		fecha();
		$quantreg = mysqli_num_rows($sql_conta); // Quantidade de registros pra paginação
	}
?>
<table width="550" border="0" cellspacing="0" cellpadding="0">
<form name="formSearch" action="" method="post" onSubmit="return complete();">
<input type="hidden" name="src" value="1" />
  <tr>
    <td>Search</td>
    <td><label for="search"></label>
    <input name="search" type="text" id="search" size="40" /></td>
    <td><label for="type"></label>
      <select name="type" id="type">
        <option value="invoice">Número da Fatura</option>
        <option value="name">Nome</option>
        <option value="username">Username</option>
        <option value="userid">ID</option>
        <option value="nossonumero">Nosso Número</option>
        <option value="todos">Todos</option>
    </select></td>
    <td><input type="submit" name="Submit" d="Submit" value="Enviar" /></td>
  </tr>
</form>
</table>
<?php if($src==1){ ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><a href="./?p=logistica" >Mostrar todas</a></td>
  </tr>
  <tr>
    <td>
	<?php
        include("pagination.php"); // Chama o arquivo que monta a paginação. ex: << anterior 1 2 3 4 5 próximo >>
    ?>
    </td>
  </tr>
</table>
<?php }else{ ?>
<p>
<?php
	include("pagination.php"); // Chama o arquivo que monta a paginação. ex: << anterior 1 2 3 4 5 próximo >>
?>
</p>
<?php } ?>
<table width="98%" border="0" cellspacing="0" cellpadding="0">
  <tr style="border:1px solid #CCC">
    <td bgcolor="#EEEEEE" width="6%">ID User</td>
    <td bgcolor="#EEEEEE" width="10%">Username</td>
    <td bgcolor="#EEEEEE" width="25%">Name</td>
    <td bgcolor="#EEEEEE">ID Fatura</td>
    <td bgcolor="#EEEEEE">Descrição</td>
    <td bgcolor="#EEEEEE">Valor</td>
    <td bgcolor="#EEEEEE">Detalhes</td>
    <td bgcolor="#EEEEEE">Status</td>
  </tr>
<?php while($reg = mysqli_fetch_array($sql)){ ?>
  <tr style="border:1px solid #CCC">
    <td><?=$reg["usr_id"]; ?></td>
    <td><?=$reg["usr_login_id"]; ?></td>
    <td><?=$reg["usr_name"]; ?></td>
    <td><?=$reg["fat_id"]; ?></td>
    <td><?=$reg["fat_descricao"]; ?></td>
    <td>$ <?=number_format($reg["fat_valor"], 2, '.', ',') ; ?></td>
    <td><input name="payment" value="Detalhes" type="button" onclick="detalhes(<?=$reg["fat_id"]?>);" /></td>
    <td>
	<?php 
	
	if($reg["ped_entregue"]==1)
	{ 
		echo "<span style='color:#9C0'>Enviado</span>";
	}elseif(($reg["ped_entregue"]==2)){
		echo "<span style='color:#090'>Entregue</span>";
	}elseif(($reg["ped_entregue"]==3)){
		echo "<span style='color:#F63'>Devolvido</span>";
	}elseif(($reg["ped_entregue"]==0)){
		echo "<span style='color:#F63'>Aguardando Envio</span>";
	}; ?>
    </td>
  </tr>
<?php } ?>
</table>
            <a class="fancybox" href="#inline1" style="display:none" ></a>
            <div id="inline1" style="width:600px;display: none;">
			
            </div>
<p><?php include("pagination.php"); // Chama o arquivo que monta a paginação. ex: << anterior 1 2 3 4 5 próximo >> ?></p>
        </div>
      </div>
      <div class="clr"></div>
    </div>
  </div>