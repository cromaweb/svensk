  <div class="content">
      <div class="content_resize">
          <div class="mainbar">
              <div class="article">
                  <h2><span>Relatórios</span></h2>
                  <div class="clr"></div>
                  <?php
			
			list($valor_bns_total) = abreSQL("SELECT SUM(bcons_valor_team+bcons_valor_rendimento+bcons_valor_sharing_bonus) 
      FROM `tb_bns_consolidado`");
      list($valor_bcons_valor_startfast,$valor_bcons_valor_team,$valor_bcons_valor_rendimento,$valor_bcons_valor_sharing_bonus) = abreSQL("SELECT SUM(bcons_valor_startfast),SUM(bcons_valor_team),SUM(bcons_valor_rendimento),SUM(bcons_valor_sharing_bonus) FROM `tb_bns_consolidado`");
      
      ?>
                  <table width="95%" border="0" cellspacing="0" cellpadding="0">
                      <tbody>
                          <th colspan="5" style="background-color:#CCC">Comissões/Rendimento</th>
                          <tr>
                              <td><strong>Divisão de lucro</strong></td>
                              <td><strong>Rendimento</strong></td>
                              <td><strong>Bônus Agente</strong></td>
                              <td><strong>Total</strong></td>

                          </tr>
                          <tr>
                              <td>$ <?=number_format($valor_bcons_valor_team, 2, '.', ','); ?></td>
                              <td>$ <?=number_format($valor_bcons_valor_rendimento, 2, '.', ','); ?></td>
                              <td>$ <?=number_format($valor_bcons_valor_sharing_bonus, 2, '.', ','); ?></td>
                              <td>$ <?=number_format($valor_bns_total, 2, '.', ','); ?></td>
                          </tr>
                          <tr>
                              <td colspan="5">&nbsp;</td>
                          </tr>
                      </tbody>
                  </table>
                  <div class="clr"></div>


                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                          <td>
                              <?php

list($qtde_locacao,$valor_locacao) = abreSQL("SELECT SUM(uplan_amount) AS plan, SUM(uplan_value) AS value FROM tb_user_plans WHERE uplan_status=1");

?>
                              <table width="90%" border="0" cellspacing="0" cellpadding="0">
                                  <tbody>
                                      <th colspan="7" style="background-color:#CCC">
                                          Locações
                                      </th>
                                      <tr>
                                          <td><strong>Quantidade</strong></td>
                                          <td><strong>Valor total</strong></td>
                                      </tr>
                                      <tr>
                                          <td><?php echo $qtde_locacao; ?></td>
                                          <td>$ <?=number_format($valor_locacao); ?>,00</td>
                                      </tr>
                                  </tbody>
                              </table>
                          </td>
                          <td>
                              <?php
			
			$sql = geraSQL("SELECT DISTINCT bco_status, COUNT(bco_status) As count, SUM(bco_dollar_amount) As amount, SUM(bco_bitcoin_amount) As btc FROM `tb_bitcoin_operations` WHERE `bco_type` = 'W' GROUP BY bco_status");
			
			$conta_total = 0;
			
			while($reg = mysqli_fetch_array($sql))
			{
				if($reg["bco_status"]=="P")
				{
					$pay_count = $reg["count"];
					$pay_amount = $reg["amount"];
					$pay_btc = $reg["btc"];
				}
				elseif($reg["bco_status"]=="U")
				{
					$processing_count = $reg["count"];
					$processing_amount = $reg["amount"];
				}
				elseif($reg["bco_status"]=="C")
				{
					$canceled_count = $reg["count"];
					$canceled_amount = $reg["amount"];
				}
				
			}
			
            ?>
                              <table width="90%" border="0" cellspacing="0" cellpadding="0">
                                  <tbody>
                                      <th colspan="7" style="background-color:#CCC">
                                          Quantidade de Saques
                                      </th>
                                      <tr>
                                          <td><strong>Pago</strong></td>
                                          <td><strong>Processando</strong></td>
                                          <td><strong>Cancelado</strong></td>
                                      </tr>
                                      <tr>
                                          <td>$ <?=number_format($pay_amount, 2, '.', ','); ?></td>
                                          <td>$ <?=number_format($processing_amount, 2, '.', ',');  ?></td>
                                          <td>$ <?=number_format($canceled_amount, 2, '.', ',');  ?></td>
                                      </tr>
                                  </tbody>
                              </table>

                          </td>
                      </tr>
                      <tr>
                          <td>&nbsp;</td>
                          <td>&nbsp;</td>
                      </tr>
                  </table>

                  <?php

                  list($areceber,$rendimento,$comissoes) = abreSQL("SELECT SUM(bsal_saldo_areceber) As areceber, SUM(bsal_saldo_rendimento) As rendimento,SUM(bsal_saldo_comissoes) As comissoes FROM tb_financeiro_saldo");

                  list($credit) = abreSQL("SELECT SUM(bsal_saldo_liberado) FROM tb_financeiro_saldo WHERE bsal_saldo_liberado>=0");
                  list($debit) = abreSQL("SELECT SUM(bsal_saldo_liberado) FROM tb_financeiro_saldo WHERE bsal_saldo_liberado<0");

                  ?>
                  <table width="95%" border="0" cellspacing="0" cellpadding="0">
                      <tbody>
                          <th colspan="7" style="background-color:#CCC">Saldo Wallet's</th>
                          <tr>
                              <td><strong>Disponível</strong></td>
                              <td><strong>Processando</strong></td>
                              <td><strong>Rendimentos</strong></td>
                              <td><strong>Comissões</strong></td>

                          </tr>
                          <tr>
                              <td>$ <?=number_format($credit, 2, '.', ','); ?></td>
                              <td>$ <?=number_format($areceber, 2, '.', ','); ?></td>
                              <td>$ <?=number_format($rendimento, 2, '.', ','); ?></td>
                              <td>$ <?=number_format($comissoes, 2, '.', ','); ?></td>
                          </tr>
                      </tbody>
                  </table>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tbody>
                          <tr>
                            <td>&nbsp;</td>
                          </tr>
                          <tr>
                            <td><button onclick="location.href='?p=profits'">Financeiro Detalhado</button></td>
                          </tr>
                      </tbody>
                  </table>
                  
              </div>
          </div>
          <div class="clr"></div>
      </div>
  </div>