function confirmacao(id,idUsr){
	var valor = document.getElementById("mdp").value;
	var resposta = confirm("Confirmar pagamento da fatura "+id+" ?");
	if(resposta == true){
		window.location.href = "faturaConfirma.php?id="+id+"&idUsr="+idUsr+"&tipo="+valor+"&val=1";
	}
}

function entrega(id,idUsr){
	var logStatus = document.getElementById("logStatus").value;
	var logDescricao = document.getElementById("logDescricao").value;
	var codRastreamento = document.getElementById("codRastreamento").value;
	var resposta = confirm("Confirmar atualização do pedido "+id+" ?");
	if(resposta == true){
		window.location.href = "atualizaEntrega.php?id="+id+"&idUsr="+idUsr+"&status="+logStatus+"&descricao="+logDescricao+"&rastreamento="+codRastreamento+"&val=1";
	}
}

function complete() {
	if (document.formSearch.search.value.length < 1) {
		alert("Insira algum valor no campo.");
		formSearch.search.focus();
		return false;
	}
	return true;
}

function fatura(id)
{

	if (id != "")
	{
		$("#inline1").html('');
		$.post('libs/_fatura.php',
		{
			idfatura : id
		},
		function(data)
		{					
			$('.fancybox').click();
			$("#inline1").html(data);
		});
	}
	else
	{
		alert("Baixa em branco!");
	}
};

function detalhes(id)
{

	if (id != "")
	{

		$("#inline1").html('');
		$.post('libs/_detalhes.php',
		{
			idfatura : id
		},
		function(data)
		{					
			$('.fancybox').click();
			$("#inline1").html(data);
		});
	}
	else
	{
		alert("Baixa em branco!");
	}
};