$(document).ready(function()
{
	$(".loading").show();
	$("#mask").show();
	$.post('libs/_downline.php',
	{
		code : '-',
		codeOfc : $("#codeOfc").val()
	},
	function(data)
	{
		if (data != "cancelar")
		{
			publica(data);
		}
		else
		{
			$(".loading").hide();
			$("#mask").hide();
		}
	});

	$(".bt_top").click(function(e)
	{
		$(".loading").show();
		$("#mask").show();
		$.post('libs/_downline.php',
		{
			code : 'top',
			codeOfc : $("#codeOfc").val()
		},
		function(data)
		{
			if (data != "cancelar")
			{
				publica(data);
			}
			else
			{
				$(".loading").hide();
				$("#mask").hide();
			}
		});
	});

	$(".bt_up_level").click(function(e)
	{
		if ($("#sobenivel").val() == "sim")
		{
			$(".loading").show();
			$("#mask").show();
			$.post('libs/_downline.php',
			{
				code : $('#pai').val(),
				codeOfc : $("#codeOfc").val()
			},
			function(data)
			{
				if (data != "cancelar")
				{
					publica(data);
				}
				else
				{
					$(".loading").hide();
					$("#mask").hide();
				}
			});
		}
	});

	$(".bt_bottom_left").click(function(e)
	{
		$(".loading").show();
		$("#mask").show();
		$.post('libs/_downline.php',
		{
			code : 'esq',
			soueu : $('#soueu').val(),
			codeOfc : $("#codeOfc").val()
		},
		function(data)
		{
			if (data != "cancelar")
			{
				publica(data);
			}
			else
			{
				$(".loading").hide();
				$("#mask").hide();
			}
		});
	});

	$(".bt_bottom_right").click(function(e)
	{
		$(".loading").show();
		$("#mask").show();
		$.post('libs/_downline.php',
		{
			code : 'dir',
			soueu : $('#soueu').val(),
			codeOfc : $("#codeOfc").val()
		},
		function(data)
		{
			if (data != "cancelar")
			{
				publica(data);
			}
			else
			{
				$(".loading").hide();
				$("#mask").hide();
			}
		});
	});

});

function publica(data)
{
	var retorno = data.split("|$|");
	$("#content_center").html(retorno[0]);
	$("#content_listag").html(retorno[1]);
	$("#information").html(retorno[2]);
	$(".loading").hide();
	$("#resniv2").hide();
	$("#resniv1").show();
	$('.pesqs').hide();
	$("#mask").hide();
}

function downline(codigo, e)
{
	if(e.button == 2)
	{
		$.post('libs/_informacoes.php',
		{
			code : codigo,
			codeOfc : $("#codeOfc").val()
		},
		function(data)
		{
			$("#informacoes").html(data);
		});
	}
	else
	{
		$(".loading").show();
		$("#mask").show();
		$.post('libs/_downline.php',
		{
			code : codigo,
			codeOfc : $("#codeOfc").val()
		},
		function(data)
		{
			if (data != "cancelar")
			{
				publica(data);
			}
			else
			{
				$(".loading").hide();
				$("#mask").hide();
			}
		});
	}
}

function downline_busca(codigo)
{
	$(".loading").show();
	$("#mask").show();
	$.post('libs/_downline.php',
	{
		code : codigo,
		codeOfc : $("#codeOfc").val()
	},
	function(data)
	{
		if (data != "cancelar")
		{
			publica(data);
		}
		else
		{
			$(".loading").hide();
			$("#mask").hide();
		}
	});
}

function fnConfirmPlacement(codePai, valor, posicao, nomePai)
{
	var sPosicao = (posicao == "E") ? "Esquerda" : "Direita";
	var sTranslade = "Clique em OK para colocar o novo membro na expansão [\item\] ";

	if (confirm(sTranslade.replace("[\item\]", sPosicao) + " " + codePai + " (" + nomePai + ")?"))
	{
		$("#frmPlacementID").val(codePai);
		$("#frmPlacementBID").val(valor);
		$("#frmPlacementPosition").val(posicao);
		$("#formPlacement").submit();
	}
}

function verifyCount(codigo)
{
	$("#count01").hide();
	$("#count02").hide();
	$("#count03").hide();
	$("#count04").hide();
	$("#loading").show();
	$.post('libs/_countLegs.php',
	{
		code : codigo,
		codeOfc : $("#codeOfc").val()
	},
	function(data)
	{
		var retorno = data.split("|$|");
		$("#loading").hide();
		$(".compExt").hide();
		$("#count01").show();
		$("#count02").html(retorno[0]);
		$("#count02").show();
		$("#count03").show();
		$("#count04").html(retorno[1]);
		$("#count04").show();
		if (codigo == $("#codeOfc").val())
		{
			$(".informacao_es").html(retorno[0]);
			$(".informacao_di").html(retorno[1]);
		}
	});
}

var TipoNavegador = navigator.appName;

function search(evento)
{
	if (TipoNavegador=="Netscape") { if (evento.which == 13) { $(".bt_search").click(); } } else { if (evento.keyCode == 13) { $(".bt_search").click(); } }
}