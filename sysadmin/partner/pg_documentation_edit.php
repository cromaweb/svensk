<script language="Javascript">
function complete() {
	if (document.formSearch.search.value.length < 1) {
		alert("Insira algum valor no campo.");
		formSearch.search.focus();
		return false;
	}
	return true;
}
</script>
<?php
$user_id = $_GET["id"];

function sendEmail($user,$url,$reason= ""){
	$currency = curl_init();
	curl_setopt($currency, CURLOPT_URL, "https://".$_SERVER['HTTP_HOST']."/routines/".$url."?user=".$user."&reason=".urlencode($reason));
	curl_setopt($currency, CURLOPT_RETURNTRANSFER, TRUE);
	curl_setopt($currency, CURLOPT_HEADER, FALSE);
	$r = curl_exec($currency);

	curl_close($currency);

	return true;
}

if($_POST){
	executaSQL("UPDATE tb_documentacao SET 
		doc_pessoal_status = '".$_POST['docp_status']."', doc_pessoal_detalhes = '".$_POST['e_pessoal_detalhes']."',
		doc_pessoal_verso_status = '".$_POST['docv_status']."', doc_pessoal_verso_detalhes = '".$_POST['e_pessoal_verso_detalhes']."', 
		doc_selfie_status = '".$_POST['docs_status']."', doc_selfie_detalhes = '".$_POST['e_selfie_detalhes']."',
		doc_endereco_status = '".$_POST['doce_status']."', doc_endereco_detalhes = '".$_POST['e_endereco_detalhes']."',  
		doc_company_status = '".$_POST['docc_status']."', doc_company_detalhes = '".$_POST['e_company_detalhes']."'
		WHERE doc_idUsuario = '$user_id'");

	$rowUser	= abreSQL("SELECT * FROM tb_user WHERE usr_id = " . $user_id);
	$usr_type	= $rowUser["usr_type"];

	//Se o usuário for PF
	if($rowUser["usr_type"]==0){
		//se os documentos foram verificados
		if($_POST['docp_status']=='V' 
			&& $_POST['docv_status']=='V' 
			&& $_POST['docs_status']=='V' 
			&& $_POST['doce_status']=='V')
		{
			//Atualiza o usuário como validado
			executaSQL("UPDATE tb_user SET usr_documentation=1 WHERE usr_id = " . $user_id);

			//Dispara email
			$url = "approveddocument";
			sendEmail($user_id,$url);

		}elseif($rowUser["usr_documentation"]==1){

			//Atualiza o usuário como validado
			executaSQL("UPDATE tb_user SET usr_documentation=0 WHERE usr_id = " . $user_id);

		}

	}else{ //Se o usuário for PJ
		//se os documentos foram verificados
		if($_POST['docp_status']=='V' 
			&& $_POST['docv_status']=='V' 
			&& $_POST['docs_status']=='V' 
			&& $_POST['doce_status']=='V' 
			&& $_POST['docc_status']=='V')
		{
			//Atualiza o usuário como validado
			executaSQL("UPDATE tb_user SET usr_documentation=2 WHERE usr_id = " . $user_id);

			//Dispara email
			$url = "approveddocument";
			sendEmail($user_id,$url);

		}elseif($rowUser["usr_documentation"]==2){

			//Atualiza o usuário como validado
			executaSQL("UPDATE tb_user SET usr_documentation=0 WHERE usr_id = " . $user_id);

		}
	}

	//Inicia variável de documento inválido como false
	$inval = false;

	if($_POST['docp_status']=='I'){
		$inval = true;
		$title = " - Documento de identificação:<br>";
		$details = $_POST['e_pessoal_detalhes']."<br>";

	}

	if($_POST['docv_status']=='I'){
		$inval = true;
		$title .= " - Verso documento de identificação:<br>";
		$details .= $_POST['e_pessoal_verso_detalhes']."<br>";
	} 
	
	if($_POST['docs_status']=='I'){
		$inval = true;
		$title .= " - Selfie:<br>";
		$details .= $_POST['e_selfie_detalhes']."<br>";
	}
	
	if($_POST['doce_status']=='I'){
		$inval = true;
		$title .= " - Comprovante de endereço:<br>";
		$details .= $_POST['e_endereco_detalhes']."<br>";
	}

	if($_POST['docc_status']=='I'){
		$inval = true;
		$title .= " - Contrato social:<br>";
		$details .= $_POST['e_company_detalhes']."<br>";
	}

	//Se tiver algum documento inválido, envia email
	if($inval)
	{
		//Dispara email
		$url = "invaliddocument";
		sendEmail($user_id,$url,$title.$details);
	}

	print "
	<script type=\"text/javascript\">
	alert(\"Dados atualizados!\");
	window.location = 'index.php?p=documentation_edit&id=".$user_id."';
	</script>";
}

####################

$rowCadastro 	= abreSQL("SELECT * FROM tb_documentacao 
							INNER JOIN tb_user on usr_id = doc_idUsuario
							WHERE usr_id = " . $user_id);

$usr_id					= $rowCadastro["usr_id"];
$usr_login_id			= $rowCadastro["usr_login_id"];
$usr_name				= $rowCadastro["usr_name"];
$usr_email				= $rowCadastro["usr_email"];
$usr_address			= $rowCadastro["usr_address"];
$usr_address2			= $rowCadastro["usr_address2"];
$usr_number				= $rowCadastro["usr_number"];
$usr_city				= $rowCadastro["usr_city"];
$usr_state				= $rowCadastro["usr_state"];
$usr_zip_code			= $rowCadastro["usr_zip_code"];
$usr_country			= $rowCadastro["usr_country"];
$usr_cpf				= $rowCadastro["usr_cpf"];
$usr_cnpj				= $rowCadastro["usr_cnpj"];
$usr_company_name		= $rowCadastro["usr_company_name"];
$usr_type				= $rowCadastro["usr_type"];
$usr_rg					= $rowCadastro["usr_rg"];
$usr_birth				= $rowCadastro["usr_birth"];
$usr_rede				= $rowCadastro["usr_rede"];

$pessoal_status			= $rowCadastro["doc_pessoal_status"];
$pessoal_detalhes		= $rowCadastro["doc_pessoal_detalhes"];
$pessoal_arquivo		= $rowCadastro["doc_pessoal_arquivo"];

$backdocument_status	= $rowCadastro["doc_pessoal_verso_status"];
$backdocument_detalhes	= $rowCadastro["doc_pessoal_verso_detalhes"];
$backdocument_arquivo	= $rowCadastro["doc_pessoal_verso_arquivo"];

$selfie_status			= $rowCadastro["doc_selfie_status"];
$selfie_detalhes		= $rowCadastro["doc_selfie_detalhes"];
$selfie_arquivo			= $rowCadastro["doc_selfie_arquivo"];

$company_status			= $rowCadastro["doc_company_status"];
$company_detalhes		= $rowCadastro["doc_company_detalhes"];
$company_arquivo		= $rowCadastro["doc_company_arquivo"];

$endereco_status		= $rowCadastro["doc_endereco_status"];
$endereco_detalhes		= $rowCadastro["doc_endereco_detalhes"];
$endereco_arquivo		= $rowCadastro["doc_endereco_arquivo"];



?>
<script src="jquery/jquery.js" type="text/javascript"></script>

<div class="content">
	<div class="content_resize">
		<div class="mainbar">
			<div class="article">
				<h2><span>Documentação</span></h2>
				<div class="clr"></div>
				<p><a href="?p=documentation">Voltar</a></p>
				<?php 
				if(!empty($msgerro))
				{
					echo $msgerro;  
				}
				?>
				<form name="form" id="form" action="index.php?p=documentation_edit&id=<?php echo $user_id; ?>" method="post">
					<table width="100%" border="0" cellspacing="1" cellpadding="1">
						<tr>
							<td align="" width="100" class="strong">Tipo:</td>
							<td><?php if($usr_rede==1){echo "<strong style='color:green'>AGENTE</strong>";}else{echo "<strong style='color:orange'>CLIENTE</strong>";} ?></td>
							<td rowspan="3"></td>
						</tr>
						<tr>
							<td align="" width="100" class="strong">ID:</td>
							<td><?=$usr_id; ?></td>
							<td rowspan="3"></td>
						</tr>
						<tr>
							<td align="" class="strong">Username:</td>
							<td><?=$usr_login_id; ?></td>
						</tr>
						<tr>
							<td align="" class="strong">E-mail:</td>
							<td><?=$usr_email; ?></td>
						</tr>
						<tr>
							<td align="">&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td colspan="2" style="font-size:20px;">
								Documento de identificação
							</td>
							<td rowspan="7">
								<?php if(!empty($pessoal_arquivo)){ ?>
								<iframe name="iframe_pessoal" id="iframe_pessoal" frameborder="0" width="95%" height="300" src="https://documentation.<?php echo $website ; ?>/<?php echo $pessoal_arquivo;?>"></iframe>
								<?php } ?>
							</td>
						</tr>
						<tr>
							<td align="" class="strong">Name:</td>
							<td><?=$usr_name; ?></td>
						</tr>
						<tr>
							<td align="" class="strong">CPF:</td>
							<td><?=$usr_cpf; ?></td>
						</tr>
						<tr>
							<td align="" class="strong">RG:</td>
							<td><?=$usr_rg; ?></td>
						</tr>
						<tr>
							<td align="" class="strong">Data nascimento:</td>
							<td><?=implode('/', array_reverse(explode('-', $usr_birth)));; ?></td>
						</tr>
						<tr>
							<td align="" class="strong">Doc Status:</td>
							<td>
								<select name="docp_status">
									<option value="P" <?php if ($pessoal_status == "P") { echo 'selected="selected"'; } ?>>Processando</option>
									<option value="U" <?php if ($pessoal_status == "U") { echo 'selected="selected"'; } ?>>Arquivo carregado</option>
									<option value="V" <?php if ($pessoal_status == "V") { echo 'selected="selected"'; } ?>>Verificado</option>
									<option value="I" <?php if ($pessoal_status == "I") { echo 'selected="selected"'; } ?>>Inválido</option>
									<option value="" <?php if ($pessoal_status != "P" && $pessoal_status != "U" && $pessoal_status != "V" && $pessoal_status != "I") { echo 'selected="selected"'; } ?>>Not Sent</option>
								</select>
							</td>
						</tr>
						<tr>
							<td align="" class="strong">Doc Detalhes:</td>
							<td><input type="text" value="" name="e_pessoal_detalhes" /></td>
						</tr>
						<tr>
							<td align="" class="strong">Doc Arquivo:</td>
							<td><a href="https://documentation.<?php echo $website ; ?>/<?php echo $pessoal_arquivo;?>" target="_blank">Abrir em nova Janela</a></td>
						</tr>
						<tr>
							<td align="">&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td colspan="2" style="font-size:20px;">
								Verso documento de identificação
							</td>
							<td rowspan="6">
							<?php if(!empty($backdocument_arquivo)){ ?>
							<iframe name="iframe_backdocument" id="iframe_backdocument" frameborder="0" width="95%" height="300" src="https://documentation.<?php echo $website ; ?>/<?php echo $backdocument_arquivo;?>"></iframe></td>
							<?php } ?>
						</tr>
						<tr>
							<td align="" class="strong">Doc Status:</td>
							<td>
								<select name="docv_status">
									<option value="P" <?php if ($backdocument_status == "P") { echo 'selected="selected"'; } ?>>Processando</option>
									<option value="U" <?php if ($backdocument_status == "U") { echo 'selected="selected"'; } ?>>Arquivo carregado</option>
									<option value="V" <?php if ($backdocument_status == "V") { echo 'selected="selected"'; } ?>>Verificado</option>
									<option value="I" <?php if ($backdocument_status == "I") { echo 'selected="selected"'; } ?>>Inválido</option>
									<option value="" <?php if ($backdocument_status != "P" && $backdocument_status != "U" && $backdocument_status != "V" && $backdocument_status != "I") { echo 'selected="selected"'; } ?>>Não enviado</option>
								</select>
							</td>
						</tr>
						<tr>
							<td align="" class="strong">Doc Detalhes:</td>
							<td><input type="text" value="" name="e_pessoal_verso_detalhes" /></td>
						</tr>
						<tr>
							<td align="" class="strong">Doc Arquivo:</td>
							<td><a href="https://documentation.<?php echo $website ; ?>/<?php echo $backdocument_arquivo;?>" target="_blank">Abrir em nova Janela</a></td>
						</tr>
						<tr>
							<td align="">&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td align="">&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td align="">&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td align="">&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>

						<tr>
							<td colspan="2" style="font-size:20px;">
								Selfie
							</td>
							<td rowspan="6">
							<?php if(!empty($selfie_arquivo)){ ?>
							<iframe name="iframe_selfie" id="iframe_selfie" frameborder="0" width="95%" height="300" src="https://documentation.<?php echo $website ; ?>/<?php echo $selfie_arquivo;?>"></iframe></td>
							<?php } ?>
						</tr>
						<tr>
							<td align="" class="strong">Doc Status:</td>
							<td>
								<select name="docs_status">
									<option value="P" <?php if ($selfie_status == "P") { echo 'selected="selected"'; } ?>>Processando</option>
									<option value="U" <?php if ($selfie_status == "U") { echo 'selected="selected"'; } ?>>Arquivo carregado</option>
									<option value="V" <?php if ($selfie_status == "V") { echo 'selected="selected"'; } ?>>Verificado</option>
									<option value="I" <?php if ($selfie_status == "I") { echo 'selected="selected"'; } ?>>Inválido</option>
									<option value="" <?php if ($selfie_status != "P" && $selfie_status != "U" && $selfie_status != "V" && $selfie_status != "I") { echo 'selected="selected"'; } ?>>Not Sent</option>
								</select>
							</td>
						</tr>
						<tr>
							<td align="" class="strong">Doc Detalhes:</td>
							<td><input type="text" value="" name="e_selfie_detalhes" /></td>
						</tr>
						<tr>
							<td align="" class="strong">Doc Arquivo:</td>
							<td><a href="https://documentation.<?php echo $website ; ?>/<?php echo $selfie_arquivo;?>" target="_blank">Abrir em nova Janela</a></td>
						</tr>
						<tr>
							<td align="">&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td align="">&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td align="">&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td align="">&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>

						<tr>
							<td colspan="2" style="font-size:20px;">
								Contrato social
							</td>
							<td rowspan="6">
							<?php if(!empty($company_arquivo)){ ?>
							<iframe name="iframe_company" id="iframe_company" frameborder="0" width="95%" height="300" src="https://documentation.<?php echo $website ; ?>/<?php echo $company_arquivo;?>"></iframe></td>
							<?php } ?>
						</tr>
						<tr>
							<td align="" class="strong">Razão social:</td>
							<td><?=$usr_company_name; ?></td>
						</tr>
						<tr>
							<td align="" class="strong">CNPJ:</td>
							<td><?=$usr_cnpj; ?></td>
						</tr>
						<tr>
							<td align="" class="strong">Doc Status:</td>
							<td>
								<select name="docc_status">
									<option value="P" <?php if ($company_status == "P") { echo 'selected="selected"'; } ?>>Processando</option>
									<option value="U" <?php if ($company_status == "U") { echo 'selected="selected"'; } ?>>Arquivo carregado</option>
									<option value="V" <?php if ($company_status == "V") { echo 'selected="selected"'; } ?>>Verificado</option>
									<option value="I" <?php if ($company_status == "I") { echo 'selected="selected"'; } ?>>Inválido</option>
									<option value="" <?php if ($company_status != "P" && $company_status != "U" && $company_status != "V" && $company_status != "I") { echo 'selected="selected"'; } ?>>Não enviado</option>
								</select>
							</td>
						</tr>
						<tr>
							<td align="" class="strong">Doc Detalhes:</td>
							<td><input type="text" value="<?php echo $company_detalhes;?>" name="e_company_detalhes" /></td>
						</tr>
						<tr>
							<td align="" class="strong">Doc Arquivo:</td>
							<td><a href="https://documentation.<?php echo $website ; ?>/<?php echo $company_arquivo;?>" target="_blank">Abrir em nova Janela</a></td>
						</tr>
						<tr>
							<td align="">&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td align="">&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>

						<tr>
							<td colspan="2" style="font-size:20px;">
								Comprovante de endereço
							</td>
							<td rowspan="12">
							<?php if(!empty($endereco_arquivo)){ ?>
								<iframe name="iframe_endereco" id="iframe_endereco" frameborder="0" width="95%" height="300" src="https://documentation.<?php echo $website ; ?>/<?php echo $endereco_arquivo;?>"></iframe>
							<?php } ?>
							</td>
						</tr>
						<tr>
							<td align="" class="strong">CEP:</td>
							<td><?=$usr_zip_code; ?></td>
						</tr>
						<tr>
							<td align="" class="strong">Endereço:</td>
							<td><?=$usr_address; ?></td>
							
						</tr>
						<tr>
							<td align="" class="strong">Número:</td>
							<td><?=$usr_number; ?></td>
						</tr>
						<tr>
							<td align="" class="strong">Complemento:</td>
							<td><?=$usr_address2; ?></td>
						</tr>
						<tr>
							<td align="" class="strong">Cidade:</td>
							<td><?=$usr_city; ?></td>
						</tr>
						<tr>
							<td align="" class="strong">Estado:</td>
							<td><?=$usr_state; ?></td>
						</tr>
						<tr>
							<td align="" class="strong">País:</td>
							<td><?=$usr_country; ?></td>
						</tr>
						<tr>
							<td align="" class="strong">Doc Status:</td>
							<td>
								<select name="doce_status">
									<option value="P" <?php if ($endereco_status == "P") { echo 'selected="selected"'; } ?>>Processando</option>
									<option value="U" <?php if ($endereco_status == "U") { echo 'selected="selected"'; } ?>>Arquivo carregado</option>
									<option value="V" <?php if ($endereco_status == "V") { echo 'selected="selected"'; } ?>>Verificado</option>
									<option value="I" <?php if ($endereco_status == "I") { echo 'selected="selected"'; } ?>>Inválido</option>
									<option value="" <?php if ($endereco_status != "P" && $endereco_status != "U" && $endereco_status != "V" && $endereco_status != "I") { echo 'selected="selected"'; } ?>>Não enviado</option>
								</select>
							</td>
						</tr>
						<tr>
							<td align="" class="strong">Doc Detalhes:</td>
							<td><input type="text" value="<?php echo $endereco_detalhes;?>" name="e_endereco_detalhes" /></td>
						</tr>
						<tr>
							<td align="" class="strong">Doc Arquivo:</td>
							<td><a href="https://documentation.<?php echo $website ; ?>/<?php echo $endereco_arquivo;?>" target="_blank">Abrir em nova Janela</a></td>
						</tr>
						<tr>
							<td align="" height="20"></td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td align="">&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td align="" height="100"><input type="submit" name="Submit" id="Submit" value="Salvar" /></td>
							<td>&nbsp;</td>
						</tr>
					</table>
				</form>
				<p>&nbsp;</p>
			</div>
		</div>
		<div class="clr"></div>
	</div>
</div>