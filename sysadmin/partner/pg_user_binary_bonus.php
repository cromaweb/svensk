<?php

	$user = $_GET["id"];
	$cancel = $_GET["cancel"];
	
	list($username)	= abreSQL("SELECT usr_login_id FROM tb_user WHERE usr_id = '$user'");
	
	//tb_bns_consolidado
	list($ptsAcumuladosEsq,$ptsAcumuladosDir) = abreSQL("SELECT busr_pontos_acumulados_esquerda,busr_pontos_acumulados_direita FROM tb_bns_usuario WHERE busr_idUsuario = '$user'");
	
	//tb_bns_consolidado
	list($ptsEsq,$ptsDir) = abreSQL("SELECT SUM(bcons_pontos_esq_red+bcons_pontos_esq_blue),SUM(bcons_pontos_dir_red+bcons_pontos_dir_blue) FROM tb_bns_consolidado WHERE bcons_idUsuario = '$user'");

	//Binary Lost	
	if(!empty($user))
	{
	
		$sql = geraSQL("SELECT * FROM tb_log_bns_limit WHERE lbl_user='$user' AND lbl_status=0");
		
		$valorStartFast = 0;
		$valorBluePointEsq = 0;
		$valorBluePointDir = 0;
		$valorRedPointEsq = 0;
		$valorRedPointDir = 0;
		$ptsBluePointEsq = 0;
		$ptsBluePointDir = 0;
		$ptsRedPointEsq = 0;
		$ptsRedPointDir = 0;
		$cancelOK = 0;
		
		while($reg = mysqli_fetch_array($sql))
		{
			$idLog = $reg["lbl_id"];
			$bonus = $reg["lbl_descricao"];
			$quebra = explode("Bonus Lost: ",$bonus);
			//echo $quebra[1]."<br>";
			
			//Start Fast
			$quebraStartFast = explode("Start Fast ",$quebra[1]);
			$startFast = $quebraStartFast[1];
			
			if(!empty($startFast)){
				list($compra) = abreSQL("SELECT comp_idFatura FROM tb_bns_compra WHERE comp_id='$startFast'");
				list($valor) = abreSQL("SELECT fat_valor FROM tb_fatura WHERE fat_id='$compra'");
				$valorStartFast += $valor*0.25;
			}
			
			//Blue Point
			$quebraBluePoint = explode("Blue Point ",$quebra[1]);
			$bluePoint = $quebraBluePoint[1];
			
			if(!empty($bluePoint)){
				list($idUser,$compra2) = abreSQL("SELECT comp_idUsuario,comp_idFatura FROM tb_bns_compra WHERE comp_id='$bluePoint'");
				list($perna) = abreSQL("SELECT contp_perna FROM tb_contperna_aws WHERE contp_idUsuario='$idUser' AND contp_idAntecessor='$user'");
				list($valor2) = abreSQL("SELECT fat_valor FROM tb_fatura WHERE fat_id='$compra2'");
				//echo "ID: $idUser | Perna: $perna | Valor: $valor2<br>";
				if($perna=="E"){
					$valorBluePointEsq += $valor2/2;
					if($cancel=="left"){
						executaSQL("UPDATE tb_log_bns_limit SET lbl_status='1' WHERE lbl_id='$idLog'");
						$cancelOK = 1;
					}
				}else{
					$valorBluePointDir += $valor2/2;
					if($cancel=="right"){
						executaSQL("UPDATE tb_log_bns_limit SET lbl_status='1' WHERE lbl_id='$idLog'");
						$cancelOK = 1;
					}
				}
			}
			
			//Red Point
			$quebraRedPoint = explode("Red Point ",$quebra[1]);
			$redPoint = $quebraRedPoint[1];
			
			if(!empty($redPoint)){
				list($idUser,$compra3) = abreSQL("SELECT comp_idUsuario,comp_idFatura FROM tb_bns_compra WHERE comp_id='$redPoint'");
				list($perna) = abreSQL("SELECT contp_perna FROM tb_contperna_aws WHERE contp_idUsuario='$idUser' AND contp_idAntecessor='$user'");
				list($valor3) = abreSQL("SELECT fat_valor FROM tb_fatura WHERE fat_id='$compra3'");
				//echo "ID: $idUser | Perna: $perna | Valor: $valor3<br>";
				
				if($perna=="E"){
					$valorRedPointEsq += $valor3/2;
					if($cancel=="left"){
						executaSQL("UPDATE tb_log_bns_limit SET lbl_status='1' WHERE lbl_id='$idLog'");
						$cancelOK = 1;
					}
				}else{
					$valorRedPointDir += $valor3/2;
					if($cancel=="right"){
						executaSQL("UPDATE tb_log_bns_limit SET lbl_status='1' WHERE lbl_id='$idLog'");
						$cancelOK = 1;
					}
				}
			}
								
		}
		
		//echo "Start Fast: $valorStartFast<br>";
		
		if($valorBluePointEsq==$valorRedPointEsq){
			$ptsPerdidosEsq = $valorBluePointEsq;	
		}else{
			$ptsPerdidosEsq = $valorBluePointEsq + $valorRedPointEsq;
		}
		
		if($valorBluePointDir==$valorRedPointDir){
			$ptsPerdidosDir = $valorBluePointDir;	
		}else{
			$ptsPerdidosDir = $valorBluePointDir + $valorRedPointDir;
		}
		
		if($cancelOK==1){
			print "
			<script type=\"text/javascript\">
			alert(\"Pontos excluídos.\");
			window.location='?p=user_binary_bonus&pg=0&id=$user';
			</script>";
		}

	
	}


	//Post ############################################################################
	if($_POST){
		
		require_once("./libs/dbfunctions.php");
		require_once("./libs/accountfunctions.php");
		
		$leg = addslashes($_POST["leg"]);
		$type = addslashes($_POST["type"]);
		$value = $_POST["value"];
		$erro = 0;
		
		if(!in_array($leg, array('l','r'))) {
		  $mensagem_erro .= "Invalid leg.<br>";
		  $erro++;
		}
		
		if(!in_array($type, array('c','d'))) {
		  $mensagem_erro .= "Invalid type.<br>";
		  $erro++;
		}
		
		if($value < 0){
		  $mensagem_erro .= "Value must be greater than 0.<br>";
		  $erro++;
		}
		
		if($erro==0)
		{     
		
			if ($type == 'd') {
			$value = $value * (-1);
			}
			
			bonusConsolidado($user);
			
			if ($leg == "l") {
				list($saldoAnterior) = abreSQL("SELECT bcons_pontos_esq_blue FROM tb_bns_consolidado WHERE bcons_idUsuario='$user' AND bcons_binario='N' AND DATE(bcons_data)=DATE(now())");
			} elseif($leg == "r") {
				list($saldoAnterior) = abreSQL("SELECT bcons_pontos_dir_blue FROM tb_bns_consolidado WHERE bcons_idUsuario='$user' AND bcons_binario='N' AND DATE(bcons_data)=DATE(now())");
			}
		  
			$saldoAtual = $saldoAnterior + $value;
		  
		  if ($leg == "l") {
			//Insere na tabela tb_bns o tipo bonus e seus pontos
			executaSQL("INSERT INTO tb_bns (bon_idUsuario, bon_idUpline, bon_tipo, bon_perna,bon_pontos, bon_data) VALUES ('$user','$user','10','E','$value',now())");
			executaSQL("UPDATE tb_bns_consolidado 
					   SET bcons_pontos_esq_blue = '$saldoAtual' 
					   WHERE bcons_idUsuario='$user' 
					   AND bcons_binario='N' 
					   AND DATE(bcons_data)=DATE(now())");
			
		  } elseif($leg == "r") {
			//Insere na tabela tb_bns o tipo bonus e seus pontos
			executaSQL("INSERT INTO tb_bns (bon_idUsuario, bon_idUpline, bon_tipo, bon_perna,bon_pontos, bon_data) VALUES ('$user','$user','10','D','$value',now())");
			executaSQL("UPDATE tb_bns_consolidado 
					   SET bcons_pontos_dir_blue = '$saldoAtual' 
					   WHERE bcons_idUsuario='$user' 
					   AND bcons_binario='N' 
					   AND DATE(bcons_data)=DATE(now())");
		  }
		  
		  print "
			<script type=\"text/javascript\">
			alert(\"Transaction added.\");
			window.location='?p=user_binary_bonus&pg=0&id=$user';
			</script>";
			
		} else {
		 $msgerro = '<table width="500" border="0" align="center" cellpadding="1" cellspacing="1" bgcolor="#999999">
		  <tr>
			<td align="center">
			  <table width="500" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#EEEEEE">
			  <tr>
			  <td class="stylexx" style="padding-left:10px; padding-right:10px; padding-top:10px;"><b>Found the following errors:</b><br>'.$mensagem_erro.'</td>
			  </tr>
			  </table>
			</td>
		  </tr>
		  </table>';
		}
		
	}
	
?> 
<div class="content">
  <div class="content_resize">
    <div class="mainbar">
      <div class="article">
        <h2><span>Bonus</span> Binário</h2>
        <div class="clr"></div>
        <table width="200" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td colspan="3"><h3>Username: <a href="?p=user&pg=0&search=<?=$username ?>&type=username"> [<?=$username ?>]</a></h3></td>
          </tr>
        </table>
        <table width="95%" border="1" cellspacing="0" cellpadding="0">
          <tr>
            <td align="center" bgcolor="#EEEEEE"><strong>Pts Acumulados Esquerda</strong></td>
            <td align="center" bgcolor="#EEEEEE"><strong>Pts Acumulados Direita</strong></td>
            <td align="center" bgcolor="#EEEEEE"><strong>Total Pontos Esquerda</strong></td>
            <td align="center" bgcolor="#EEEEEE"><strong>Total Pontos Direita</strong></td>
            <td align="center" bgcolor="#EEEEEE"><strong>Pontos Perdidos Esquerda</strong></td>
            <td align="center" bgcolor="#EEEEEE"><strong>Pontos Perdidos Direita</strong></td>
          </tr>
          <tr>
            <td align="center">&nbsp;<?php echo (!empty($ptsAcumuladosEsq)) ? "$ptsAcumuladosEsq" : "0"; ?></td>
            <td align="center">&nbsp;<?php echo (!empty($ptsAcumuladosDir)) ? "$ptsAcumuladosDir" : "0"; ?></td>
            <td align="center">&nbsp;<?php echo (!empty($ptsEsq)) ? "$ptsEsq" : "0"; ?></td>
            <td align="center">&nbsp;<?php echo (!empty($ptsDir)) ? "$ptsDir" : "0"; ?></td>
            <td align="center">&nbsp;<?php echo (!empty($ptsPerdidosEsq)) ? "$ptsPerdidosEsq" : "0"; ?> | <a style="cursor:pointer;" onclick="deletePts('left')">[Excluir]</a></td>
            <td align="center">&nbsp;<?php echo (!empty($ptsPerdidosDir)) ? "$ptsPerdidosDir" : "0"; ?> | <a style="cursor:pointer;" onclick="deletePts('right')">[Excluir]</a></td>
          </tr>
        </table>
        <!-- ################ -->
        <?php if($usrType==1 || $usrType==3){?>
		<?php 
          if(!empty($msgerro))
          {
              echo $msgerro;  
          }
        ?>
        <form name="form" id="form" action="" method="post">
          <table width="100%" border="0" cellspacing="1" cellpadding="1">
            <tr>
              <td align="right">&nbsp;</td>
              <td></td>
            </tr>
            <tr>
              <td align="right">Perna:</td>
              <td><select name="leg" id="leg">
                  <option value="">Selecione a perna</option>
                  <option value="l">Esquerda</option>
                  <option value="r">Direita</option>
                </select></td>
            </tr>
            <tr>
              <td align="right">Tipo:</td>
              <td><select name="type" id="type">
                  <option value="">Select type</option>
                  <option value="c">Credit</option>
                  <option value="d">Debit</option>
                </select></td>
            </tr>
            <tr>
              <td align="right">Pontos:</td>
              <td><input name="value" type="text" id="value" value="<?=$value; ?>" size="20" /></td>
            </tr>
            <tr>
              <td align="right">&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td align="right">&nbsp;</td>
              <td><input type="submit" name="Submit" id="Submit" value="Submit" /></td>
            </tr>
          </table>
        </form>
        <?php } ?>
      </div>
    </div>
    <div class="clr"></div>
  </div>
</div>
<script>
function deletePts(leg) {
	
	if (confirm("Excluir pontos perdidos ["+leg+"] ?")) {
		location.href='?p=user_binary_bonus&pg=0&id=<?php echo $user ?>&cancel='+leg;
	} 
}
</script>
