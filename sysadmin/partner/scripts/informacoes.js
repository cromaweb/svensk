$(document).ready(function(){


	// ###### Função form
	
	$("#form").validate({
		// Define as regras
		rules:{
			nome:{
				required: true, minlength: 2
			},
			cpf:{
				required: true
			},
			rg:{
				required: true
			},
			dia_nascimento:{
				required: true
			},
			mes_nascimento:{
				required: true
			},
			ano_nascimento:{
				required: true
			},
			email:{
				required: true, email: true
			},
			dddResidencia:{
				required: true
			},
			telefoneResidencia:{
				required: true
			}
			

		},
		// Define as mensagens de erro para cada regra
		messages:{
			nome:{
				required: "Digite o seu nome",
				minLength: "O seu nome deve conter, no mínimo, 2 caracteres"
			},
			cpf:{
				required: "Digite o seu CPF"
			},
			rg:{
				required: "Digite seu RG"
			},
			dia_nascimento:{
				required: "Digite selecione o dia do seu nascimento"
			},
			mes_nascimento:{
				required: "Digite selecione o mês do seu nascimento"
			},
			ano_nascimento:{
				required: "Digite selecione o ano do seu nascimento"
			},
			ano_nascimento:{
				required: "Digite selecione o ano do seu nascimento"
			},
			email:{
				required: "Digite seu e-mail.",
				email: "E-mail incorreto!"
			},
			dddResidencia:{
				required: "Digite DDD do telefone residencial."
			},
			telefoneResidencia:{
				required: "Digite o telefone residencial."
			}
		}
	});
	
});