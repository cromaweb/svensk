<link rel="stylesheet" href="http://code.jquery.com/ui/1.9.0/themes/base/jquery-ui.css" />
<script src="http://code.jquery.com/jquery-1.8.2.js"></script>
<script src="http://code.jquery.com/ui/1.9.0/jquery-ui.js"></script>

<script language="Javascript">
function complete() {
	/*if (document.formSearch.search.value.length < 1) {
		alert("Insira algum valor no campo.");
		formSearch.search.focus();
		return false;
	}*/
	return true;
}

function cancel(id) {
	if (confirm("Confirmar o cancelamento do saque?")) {
		location.href='?p=withdrawal&cancel='+id;
	} 
}
</script>
<style>
 .ordenar {
 	text-decoration: none;
 	color:#003366;
 }
 .ordenar.marcado {
 	font-weight: bold;
 	color: #FF0000;
 }
</style>

<?php
require_once("./libs/accountfunctions.php");

$cancel = $_GET['cancel'];

if (!empty($cancel)) {
	list($idUser, $valorSaqueUSD, $accountFrom) = abreSQL("select bco_user, bco_dollar_amount, bco_account_from from tb_bitcoin_operations where bco_type='W' and bco_status = 'U' and bco_id = " . $cancel);
	if (!empty($idUser)) {
		executaSQL("UPDATE tb_bitcoin_operations SET bco_status = 'C' WHERE bco_id = '$cancel'");

		if ($accountFrom == "r") {
			list($saldoAnterior) = abreSQL("SELECT bsal_saldo_rendimento FROM tb_financeiro_saldo WHERE bsal_idUsuario='$idUser'");
		} elseif($accountFrom == "c") {
			list($saldoAnterior) = abreSQL("SELECT bsal_saldo_comissoes FROM tb_financeiro_saldo WHERE bsal_idUsuario='$idUser'");
		} else {
			$accountFrom = "l";
			list($saldoAnterior) = abreSQL("SELECT bsal_saldo_liberado FROM tb_financeiro_saldo WHERE bsal_idUsuario='$idUser'");
		}
		
		$saldoAtual = $saldoAnterior + $valorSaqueUSD;
		//Faz insert tabela tb_financeiro_extrato
		executaSQL("INSERT INTO tb_financeiro_extrato (extf_idUsuario,extf_idCompra, extf_descricao, extf_conta, extf_saldo_anterior,extf_saldo_atual, extf_tipo, extf_valor, extf_data) 
			VALUES ('$idUser','0','Withdrawal Cancelation','$accountFrom','$saldoAnterior','$saldoAtual','c','$valorSaqueUSD',now())");

		executaSQL("UPDATE tb_financeiro_saldo SET bsal_saldo_areceber = (bsal_saldo_areceber - ".$valorSaqueUSD.") WHERE bsal_idUsuario = $idUser;");
		
		if ($accountFrom == "r") {
			executaSQL("UPDATE tb_financeiro_saldo SET bsal_saldo_rendimento = (bsal_saldo_rendimento + ".$valorSaqueUSD.") WHERE bsal_idUsuario = $idUser;");
		} elseif($accountFrom == "c") {
			executaSQL("UPDATE tb_financeiro_saldo SET bsal_saldo_comissoes = (bsal_saldo_comissoes + ".$valorSaqueUSD.") WHERE bsal_idUsuario = $idUser;");
		} else {
			executaSQL("UPDATE tb_financeiro_saldo SET bsal_saldo_liberado = (bsal_saldo_liberado + ".$valorSaqueUSD.") WHERE bsal_idUsuario = $idUser;");
		}
		
	}
	
	print "
		<script type=\"text/javascript\">
		alert(\"Saque cancelado com sucesso!\");
		window.location='?p=withdrawal';
		</script>";
}


if ($_POST["src"] == 1) {
	echo "<script>window.location = '?p=withdrawal&pg=0&src=".$_POST["src"]."&search=".$_POST["search"]."&type=".$_POST["type"]."&status=".$_POST["status"]."&data_inicial=".$_POST["data_inicial"]."&data_final=".$_POST["data_final"]."';</script>";
}

$search = $_GET["search"];
$type	= $_GET["type"];
$status	= $_GET["status"];
$data_inicial	= $_GET["data_inicial"];
$data_final	= $_GET["data_final"];
$src	= $_GET["src"];

//######### INICIO Paginação
	$numreg = 100; // Quantos registros por página vai ser mostrado
	$pg = $_GET['pg'];
	if (!isset($pg)) {
		$pg = 0;
	}
	$inicial = $pg * $numreg;
	
//######### FIM dados Paginação
	switch ($_GET['order']) {
		case "valorasc":
			$order = "bco_bitcoin_amount ASC";
			break;

		case "valordesc" : 
			$order = "bco_bitcoin_amount DESC";
			break;

		case "valorusdasc":
			$order = "bco_dollar_amount ASC";
			break;

		case "valorusddesc" : 
			$order = "bco_dollar_amount DESC";
			break;

		case "dataasc":
			$order = "bco_date_insert ASC";
			break;

		case "datadesc" : 
			$order = "bco_date_insert DESC";
			break;

		default:
			$order = "bco_date_insert DESC";
			break;
	}


	if($src==1)
	{
		// Faz o Select pegando o registro inicial até a quantidade de registros para página
		$sql = "SELECT 	bco_id,
						bco_bitcoin_amount,
						bco_dollar_amount,
						bco_status,
						bco_date_insert,
						bco_transaction_id,
						usr_id,
						usr_login_id,
						usr_name
					FROM tb_bitcoin_operations INNER JOIN tb_user on usr_id = bco_user WHERE bco_type='W'";
		$sql2 = "SELECT count(*) FROM tb_bitcoin_operations INNER JOIN tb_user on usr_id = bco_user  WHERE bco_type='W'";

		if (!empty($search)) {
			if($type=="id")
			{
				$sql .= "AND usr_id='$search' ";
				$sql2 .= "AND usr_id='$search' ";
			}elseif($type=="name")
			{
				$sql .= "AND usr_name LIKE '%$search%' ";
				$sql2 .= "AND usr_name LIKE '%$search%' ";
			}elseif($type=="username")
			{
				$sql .= "AND usr_login_id='$search' ";
				$sql2 .= "AND usr_login_id='$search' ";
			}
		}

		switch ($status) {
			case 'P':
				//Pagos
				$sql .= "AND bco_status = 'P' ";
				$sql2 .= "AND bco_status = 'P' ";
				break;
			case 'C':
				//Cancelados
				$sql .= "AND bco_status = 'C' ";
				$sql2 .= "AND bco_status = 'C' ";
				break;
			case 'U':
				//Processando
				$sql .= "AND bco_status = 'U' ";
				$sql2 .= "AND bco_status = 'U' ";
				break;
			default:
				//mostrar todos
				break;
		}

		//Search Date
		if(!empty($data_inicial)){
			
			$sql .= "AND DATE(bco_date_insert)>='$data_inicial' ";
			$sql2 .= "AND DATE(bco_date_insert)>='$data_inicial' ";
		}
		
		if(!empty($data_final)){
			$sql .= "AND DATE(bco_date_insert)<='$data_final' ";
			$sql2 .= "AND DATE(bco_date_insert)<='$data_final' ";
		}
		
		$sql .= " ORDER BY ".$order."  
			  LIMIT $inicial, $numreg";
		
		$sql = geraSQL($sql);
		//Conta a quantidade de registros
		list($quantreg) = abreSQL($sql2);// Quantidade de registros pra paginação
	}else
	{
		// Faz o Select pegando o registro inicial até a quantidade de registros para página
		$sql = geraSQL("SELECT bco_id,
								bco_bitcoin_amount,
								bco_dollar_amount,
								bco_status,
								bco_date_insert,
								bco_transaction_id,
								usr_id,
								usr_login_id,
								usr_name
							FROM tb_bitcoin_operations INNER JOIN tb_user on usr_id = bco_user
							WHERE bco_type='W' AND bco_status = 'U'
							ORDER BY ".$order." 
							LIMIT $inicial, $numreg");
	
		// Serve para contar quantos registros você tem na seua tabela para fazer a paginação
		list($quantreg) = abreSQL("SELECT count(*) FROM tb_bitcoin_operations WHERE bco_type='W' AND bco_status = 'U'");// Quantidade de registros pra paginação
		//echo "<script>alert('".$quantreg."');</script>";
	}
?>
  <div class="content">
    <div class="content_resize">
      <div class="mainbar">
        <div class="article">
          <h2><span>Saques</span></h2>
          <div class="clr"></div>
            <table width="92%" border="0" cellspacing="0" cellpadding="0">
            <form name="formSearch" action="" method="post">
            <input type="hidden" name="src" value="1" />
				<tr>
					<td>Search</td>
					<td>Type</td>
					<td>Status</td>
					<td>Data Inicial</td>
					<td>Data Final</td>
					<td></td>
				</tr>
              <tr>
                <td><input name="search" type="text" id="search" size="20" value="<?=$_GET['search']; ?>" /></td>
                <td>
					<select name="type" id="type">
						<option value="username" <?php if ($_GET['type'] == "username") { echo 'selected="selected"'; } ?>>Username</option>
						<option value="id" <?php if ($_GET['type'] == "id") { echo 'selected="selected"'; } ?>>ID do Usuário</option>
						<option value="name" <?php if ($_GET['type'] == "name") { echo 'selected="selected"'; } ?>>Nome</option>
					</select>
				</td>
                <td>
					<select name="status" id="status">
						<option value="" <?php if ($_GET['status'] == "") { echo 'selected="selected"'; } ?>>Todos</option>
						<option value="C" <?php if ($_GET['status'] == "C") { echo 'selected="selected"'; } ?>>Cancelados</option>
						<option value="U"  <?php if ($_GET['status'] == "U") { echo 'selected="selected"'; } ?>>Processando</option>
						<option value="P" <?php if ($_GET['status'] == "P") { echo 'selected="selected"'; } ?>>Pago</option>
					</select>
				</td>
				<td>
					<input name="data_inicial" id="data_inicial" type="text" id="data_inicial" size="20" value="<?php echo $data_inicial; ?>" />
				</td>
				<td>
					<input name="data_final" id="data_final" type="text" id="data_final" size="20" value="<?php echo $data_final; ?>" />
				</td>
                <td>
					<input type="submit" name="Submit" id="Submit" value="Enviar" />
				</td>
              </tr>
            </form>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
                <td><strong>&nbsp;</strong></td>
				<td><strong>&nbsp;</strong></td>
				<td><strong>&nbsp;</strong></td>
				
              </tr>
              <tr>
                <td>Blockchain: <strong><?="$ ".number_format(getUSDBTC(), 2, '.', ','); ?></strong></td>
				<td>Endereço carregamento: <strong>1D6M4Noz3FXtGkk1v1aBFSgj1eGrF95385</strong></td>
				<td>Saldo: <strong><?=getBalanceAddress('1D6M4Noz3FXtGkk1v1aBFSgj1eGrF95385'); ?> BTC</strong></td>
				
              </tr>
			</table>
			<?php if($src==1){ ?>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td><a href="./?p=withdrawal" >Mostrar todos</a></td>
              </tr>
              <tr>
                <td>
                <?php
                    include("pagination.php"); // Chama o arquivo que monta a paginação. ex: << anterior 1 2 3 4 5 próximo >>
                ?>
                </td>
              </tr>
            </table>
            <?php }else{ ?>
            <p>
            <?php
                include("pagination.php"); // Chama o arquivo que monta a paginação. ex: << anterior 1 2 3 4 5 próximo >>
            ?>
            </p>
            <?php } ?>
            <table width="96%" border="0" cellspacing="0" cellpadding="0">
              <tr style="border:1px solid #CCC">
                <td bgcolor="#EEEEEE" width="6%">ID User</td>
                <td bgcolor="#EEEEEE">Username</td>
                <td bgcolor="#EEEEEE">Nome</td>
                <td bgcolor="#EEEEEE">
                	Valor USDT 
                	<a class="ordenar <?php if ($_GET['order'] == "valorusdasc") { echo 'marcado'; } ?>" href="?p=withdrawal&pg=0&search=<?php echo $_GET['search']; ?>&type=<?php echo $_GET['type']; ?>&order=valorusdasc"><b>/\</b></a>
                	<a class="ordenar <?php if ($_GET['order'] == "valorusddesc") { echo 'marcado'; } ?>" href="?p=withdrawal&pg=0&search=<?php echo $_GET['search']; ?>&type=<?php echo $_GET['type']; ?>&order=valorusddesc"><b>\/</b></a>
                </td>
                <td bgcolor="#EEEEEE">
                	Valor BTC 
                	<a class="ordenar <?php if ($_GET['order'] == "valorasc") { echo 'marcado'; } ?>" href="?p=withdrawal&pg=0&search=<?php echo $_GET['search']; ?>&type=<?php echo $_GET['type']; ?>&order=valorasc"><b>/\</b></a>
                	<a class="ordenar <?php if ($_GET['order'] == "valordesc") { echo 'marcado'; } ?>" href="?p=withdrawal&pg=0&search=<?php echo $_GET['search']; ?>&type=<?php echo $_GET['type']; ?>&order=valordesc"><b>\/</b></a>
                </td>
                <td bgcolor="#EEEEEE">
                	Data Saque
                	<a class="ordenar <?php if ($_GET['order'] == "dataasc") { echo 'marcado'; } ?>" href="?p=withdrawal&pg=0&search=<?php echo $_GET['search']; ?>&type=<?php echo $_GET['type']; ?>&order=dataasc"><b>/\</b></a>
                	<a class="ordenar <?php if ($_GET['order'] == "datadesc" || empty($_GET['order'])) { echo 'marcado'; } ?>" href="?p=withdrawal&pg=0&search=<?php echo $_GET['search']; ?>&type=<?php echo $_GET['type']; ?>&order=datadesc"><b>\/</b></a>
				</td>
				<td bgcolor="#EEEEEE">Hash</td>
                <td bgcolor="#EEEEEE">Status</td>
				<?php if($usrType==1 || $usrType==3){?>
                <td bgcolor="#EEEEEE">Ação</td>
				<?php } ?>
              </tr>
            <?php 
            $total_bitcoin = 0;
            $total_dollar = 0;
            while($reg = mysqli_fetch_array($sql)){ 
            	//$total_bitcoin += getTradeBRLBTC($reg["bco_dollar_amount"]);
            	$total_dollar += $reg['bco_dollar_amount'];
            ?>
              <tr style="border:1px solid #CCC">
                <td><?=$reg["usr_id"]; ?></td>
                <td><?=$reg["usr_login_id"]; ?></td>
                <td><?=$reg["usr_name"]; ?></td>
                <td><?="$ ".number_format($reg["bco_dollar_amount"], 2, '.', ','); ?></td>
                <td><?=getTradeUSDBTC($reg["bco_dollar_amount"]); ?></td>
				<td><?=$reg["bco_date_insert"]; ?></td>
				<td><?=$reg["bco_transaction_id"]; ?></td>
				<?php 
				switch ($reg['bco_status']) {
					case 'U':
						echo '<td style="color:#025E9B;">Processando</td>';
						break;
					case 'P':
						echo '<td style="color:#47a447;">Pago</td>';
						break;
					case 'C':
						echo '<td style="color:#FF0000;">Cancelado</td>';
						break;
					default:
						echo '<td></td>';
						break;
				}
				?>
				<?php if($usrType==1 || $usrType==3){?>
                <td>
                	<?php if ($reg['bco_status'] == 'U') { ?>
                	<input name="edit" value="Depositar" type="button" onclick="location.href='?p=withdrawal_edit&id=<?=$reg["bco_id"]; ?>'" />
                	<input name="cancel" value="Cancelar" type="button" onclick="cancel('<?=$reg["bco_id"]; ?>');" />
                	<?php } ?>

                </td>
				<?php } ?>
              </tr>
            <?php } ?>
              <tr>
              	<td colspan="3" align="right" style="font-weight:bold;"></td>
              	<td style="font-weight:bold;"><?="$ ".number_format($total_dollar, 2, '.', ','); ?></td>
              	<td style="font-weight:bold;"><?=getTradeUSDBTC($total_dollar); ?></td>
              	<td colspan="3"></td>
              </tr>
            </table>
			<p><?php include("pagination.php"); // Chama o arquivo que monta a paginação. ex: << anterior 1 2 3 4 5 próximo >> ?></p>
        </div>
      </div>
      <div class="clr"></div>
    </div>
  </div>
<script>
$(function() {
    $( "#data_inicial" ).datepicker({ dateFormat: 'yy-mm-dd' });
	$( "#data_final" ).datepicker({ dateFormat: 'yy-mm-dd' });
});
</script>