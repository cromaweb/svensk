<?php


$act = $_POST["act"];

if($act==1)
{

	$password_current = $_POST["password_current"];
	$password_new = $_POST["password_new"];
	$r_password_new = $_POST["r_password_new"];

	$erro=0;

	if(empty($password_current))
	{

	  print "
	  <script type=\"text/javascript\">
	  alert(\"Digite a senha atual!\");
	  </script>";
	  $erro++;

	}

	if(empty($password_new))
	{

	  print "
	  <script type=\"text/javascript\">
	  alert(\"Digite a nova senha!\");
	  </script>";
	  $erro++;

	}

	if(strlen($password_new) < 6)
	{

	  print "
	  <script type=\"text/javascript\">
	  alert(\"A nova senha deve possuir mais que 5 caracteres!\");
	  </script>";
	  $erro++;

	}

	if(empty($r_password_new))
	{

	  print "
	  <script type=\"text/javascript\">
	  alert(\"Favor repetir a nova senha!\");
	  </script>";
	  $erro++;

	}

	if($password_new != $r_password_new)
	{

	  print "
	  <script type=\"text/javascript\">
	  alert(\"A senha digitada não confere!\");
	  </script>";
	  $erro++;

	}

	$val_sql = geraSQL("SELECT * FROM tb_user_admin 
						   WHERE adm_id='$usrID' 
						   AND adm_password='".crypt($password_current,"b12")."'");

	$val_num = mysqli_num_rows($val_sql);

	if($val_num==0)
	{

	  print "
	  <script type=\"text/javascript\">
	  alert(\"Senha atual inválida.\");
	  </script>";
	  $erro++;

	}

	if($erro==0)
	{

		$sql = executaSQL("UPDATE tb_user_admin SET adm_password='".crypt($password_new,"b12")."' WHERE adm_id='$usrID'");

		if($sql)
		{
		
			print " <meta http-equiv=\"Refresh\" content=\"0; URL=logout.php\"/>
			<script type=\"text/javascript\">
			alert(\"A senha foi alterada com sucesso!\");
			</script>";
		
		}else
		{
		
			print "
			<script type=\"text/javascript\">
			alert(\"Problema ao atualizar a senha.\");
			</script>";
		
		}

	}

}

?>
  <div class="content">
    <div class="content_resize">
      <div class="mainbar">
        <div class="article">
          <h2><span>Senha</span></h2>
          <div class="clr"></div>
            <table width="420" border="0" cellspacing="0" cellpadding="0">
            <form name="formSearch" action="" method="post" onSubmit="return complete();">
            <input type="hidden" name="act" value="1" />
              <tr>
                <td>Senha Atual</td>
                <td><label for="password_current"></label>
                <input name="password_current" type="password" id="password_current" size="20" /></td>
              </tr>
              <tr>
                <td>Nova Senha</td>
                <td><input name="password_new" type="password" id="password_new" size="20" /></td>
              </tr>
              <tr>
                <td>Repita Nova Senha</td>
                <td><input name="r_password_new" type="password" id="r_password_new" size="20" /></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td><input type="submit" name="Submit" id="Submit" value="Enviar" /></td>
              </tr>
            </form>
            </table>
        </div>
      </div>
      <div class="clr"></div>
    </div>
  </div>