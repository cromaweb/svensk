<style type="text/css">
<!--
.pgoff {font-family: Verdana, Arial, Helvetica; font-size: 13px; color: #FF0000; text-decoration: none}
a.pg {font-family: Verdana, Arial, Helvetica; font-size: 13px; color: #003366; text-decoration: none}
a:hover.pg {font-family: Verdana, Arial, Helvetica; font-size: 13px; color: #0066cc; text-decoration:underline}
-->
</style>
<?php 
$quant_pg = ceil($quantreg/$numreg);
$quant_pg++;

$prox = $pg + 1;
$ant = $pg - 1;
$ultima_pag = ceil($quantreg / $numreg);
$penultima = $ultima_pag - 1;  
$adjacentes = 8;

$pgorder = "";
if (!empty($_GET['order'])) {
	$pgorder = "&order=".$_GET['order'];
}

$src = "";
if (!empty($_GET['src'])) {
	$src = "&src=".$_GET['src'];
}

$status = "";
if (!empty($_GET['status'])) {
	$status = "&status=".$_GET['status'];
}

$data_inicial = "";
if (!empty($_GET['data_inicial'])) {
	$data_inicial = "&data_inicial=".$_GET['data_inicial'];
}

$data_final = "";
if (!empty($_GET['data_final'])) {
	$data_final = "&data_final=".$_GET['data_final'];
}

if ($ultima_pag > (1 + (2 * $adjacentes)))
{
	$paginacao = '';
	// Verifica se esta na primeira página, se nao estiver ele libera o link para anterior
	if ($pg > 0) { 
		$paginacao .= "<a href='?p=".$p."&pg=".($pg-1)."&search=".$search."&type=".$type.$pgorder.$src.$status.$data_inicial.$data_final."' class=pg><b>&laquo; anterior</b></a>";
	} else { 
		$paginacao .= "<font color=#CCCCCC>&laquo; anterior</font>";
	}
	if (($pg+1) < (1 + $adjacentes))
	{
		for ($i=1; $i < 2 + (2 * $adjacentes); $i++)
		{
			if ($i == ($pg+1) )
			{
				$paginacao .= '&nbsp;<span class="pgoff">['.$i.']</span>&nbsp;';  
			} else {
				$paginacao .= '&nbsp;<a href="?p='.$p.'&pg='.($i-1).'&search='.$search.'&type='.$type.$pgorder.$src.$status.$data_inicial.$data_final.'" class="pg"><b>'.$i.'</b></a>&nbsp;';        
			}
		}
		$paginacao .= '&nbsp;...&nbsp;';
		$paginacao .= '&nbsp;<a href="?p='.$p.'&pg='.($penultima-1).'&search='.$search.'&type='.$type.$pgorder.$src.$status.$data_inicial.$data_final.'" class="pg"><b>'.$penultima.'</b></a>&nbsp;';
		$paginacao .= '&nbsp;<a href="?p='.$p.'&pg='.($ultima_pag-1).'&search='.$search.'&type='.$type.$pgorder.$src.$status.$data_inicial.$data_final.'" class="pg"><b>'.$ultima_pag.'</b></a>&nbsp;';
	}

	elseif(($pg+1) > $adjacentes && ($pg+1) < $ultima_pag - 3)
	{
		$paginacao .= '&nbsp;<a href="?p='.$p.'&pg=0&search='.$search.'&type='.$type.$pgorder.$src.$status.$data_inicial.$data_final.'" class="pg"><b>1</b></a>&nbsp;';
		$paginacao .= '&nbsp;<a href="?p='.$p.'&pg=1&search='.$search.'&type='.$type.$pgorder.$src.$status.$data_inicial.$data_final.'" class="pg"><b>2</b></a>&nbsp;'; 
		$paginacao .= '&nbsp;...&nbsp;';
		for ($i = ($pg+1) -$adjacentes; $i<= ($pg+1) + $adjacentes; $i++)
		{
			if ($i == ($pg+1) )
			{
				$paginacao .= '&nbsp;<span class=pgoff>['.$i.']</span>&nbsp;'; 
			} else {
				$paginacao .= '&nbsp;<a href="?p='.$p.'&pg='.($i-1).'&search='.$search.'&type='.$type.$pgorder.$src.$status.$data_inicial.$data_final.'" class="pg"><b>'.$i.'</b></a>&nbsp;';         
			}
		}
		$paginacao .= '...';
		$paginacao .= '&nbsp;...&nbsp;';
		$paginacao .= '&nbsp;<a href="?p='.$p.'&pg='.($penultima-1).'&search='.$search.'&type='.$type.$pgorder.$src.$status.$data_inicial.$data_final.'" class="pg"><b>'.$penultima.'</b></a>&nbsp;';
		$paginacao .= '&nbsp;<a href="?p='.$p.'&pg='.($ultima_pag-1).'&search='.$search.'&type='.$type.$pgorder.$src.$status.$data_inicial.$data_final.'" class="pg"><b>'.$ultima_pag.'</b></a>&nbsp;';
	}
	else {
		$paginacao .= '&nbsp;<a href="?p='.$p.'&pg=0&search='.$search.'&type='.$type.$pgorder.$src.$status.$data_inicial.$data_final.'" class="pg"><b>1</b></a>&nbsp;';
		$paginacao .= '&nbsp;<a href="?p='.$p.'&pg=1&search='.$search.'&type='.$type.$pgorder.$src.$status.$data_inicial.$data_final.'" class="pg"><b>2</b></a>&nbsp;'; 
		$paginacao .= '&nbsp;...&nbsp;';
		for ($i = $ultima_pag - (2 * $adjacentes); $i <= $ultima_pag; $i++)
		{
			if ($i == ($pg+1) )
			{
				$paginacao .= '&nbsp;<span class=pgoff>['.$i.']</span>&nbsp;';       
			} else {
				$paginacao .= '&nbsp;<a href="?p='.$p.'&pg='.($i-1).'&search='.$search.'&type='.$type.$pgorder.$src.$status.$data_inicial.$data_final.'" class="pg"><b>'.$i.'</b></a>&nbsp;';           
			}
		}
	}
	// Verifica se esta na ultima página, se nao estiver ele libera o link para próxima
	if (($pg+1) != $ultima_pag) { 
		$paginacao .= "<a href='?p=".$p."&pg=".($pg+1)."&search=".$search."&type=".$type.$pgorder.$src.$status.$data_inicial.$data_final."' class=pg><b>próximo &raquo;</b></a>";
	} else { 
		$paginacao .= "<font color=#CCCCCC>próximo &raquo;</font>";
	}
	echo $paginacao;
} else {
	// Verifica se esta na primeira página, se nao estiver ele libera o link para anterior
	if ( $pg > 0) { 
		echo "<a href='?p=".$p."&pg=".($pg-1)."&search=".$search."&type=".$type.$pgorder.$src.$status.$data_inicial.$data_final."' class=pg><b>&laquo; anterior</b></a>";
	} else { 
		echo "<font color=#CCCCCC>&laquo; anterior</font>";
	}

	// Faz aparecer os numeros das página entre o ANTERIOR e PROXIMO
	for($i_pg=1;$i_pg<$quant_pg;$i_pg++) { 
	// Verifica se a página que o navegante esta e retira o link do número para identificar visualmente
		if ($pg == ($i_pg-1)) { 
			echo "&nbsp;<span class=pgoff>[$i_pg]</span>&nbsp;";
		} else {
			$i_pg2 = $i_pg-1;
			echo "&nbsp;<a href='?p=".$p."&pg=".$i_pg2."&search=".$search."&type=".$type.$pgorder.$src.$status.$data_inicial.$data_final."' class=pg><b>".$i_pg."</b></a>&nbsp;";
		}
	}

	// Verifica se esta na ultima página, se nao estiver ele libera o link para próxima
	if (($pg+2) < $quant_pg) { 
		echo "<a href='?p=".$p."&pg=".($pg+1)."&search=".$search."&type=".$type.$pgorder.$src.$status.$data_inicial.$data_final."' class=pg><b>próximo &raquo;</b></a>";
	} else { 
		echo "<font color=#CCCCCC>próximo &raquo;</font>";
	}
}
?>