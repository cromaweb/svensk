<style>
 .ordenar {
 	text-decoration: none;
 	color:#003366;
 }
 .ordenar.marcado {
 	font-weight: bold;
 	color: #FF0000;
 }
</style>

<?php

if ($_POST["src"] == 1) {
	echo "<script>window.location = '?p=documentation&pg=0&search=".$_POST["search"]."&type=".$_POST["type"]."&status=".$_POST["status"]."';</script>";
}

$search = $_GET["search"];
$type	= $_GET["type"];
$status	= $_GET["status"];

if (!empty($search) && !empty($type)) {
	$src = 1;
}

if (empty($status)) {
	$status = 3;
}

//######### INICIO Paginação
	$numreg = 100; // Quantos registros por página vai ser mostrado
	$pg = $_GET['pg'];
	if (!isset($pg)) {
		$pg = 0;
	}
	$inicial = $pg * $numreg;
	
//######### FIM dados Paginação
	$order = "doc_idUsuario";

	if($src==1)
	{
		// Faz o Select pegando o registro inicial até a quantidade de registros para página
		$sql = "SELECT usr_id, usr_login_id, usr_name, doc_pessoal_status, doc_pessoal_verso_status, doc_endereco_status, doc_selfie_status,doc_company_status 
				FROM tb_documentacao INNER JOIN tb_user on usr_id = doc_idUsuario ";

		//Adiciona o sql para paginação
		$sql2 = "SELECT count(*) FROM tb_documentacao INNER JOIN tb_user on usr_id = doc_idUsuario ";
		
		if($status == 1)
		{
			$sql .= "WHERE (doc_pessoal_status = 'V') AND (doc_pessoal_verso_status = 'V') AND (doc_endereco_status = 'V') AND (doc_selfie_status = 'V') AND (doc_company_status = 'V') ";
			$sql2 .= "WHERE (doc_pessoal_status = 'V') AND (doc_pessoal_verso_status = 'V') AND (doc_endereco_status = 'V') AND (doc_selfie_status = 'V') AND (doc_company_status = 'V') ";
		}elseif($status == 2)
		{
			$sql .= "WHERE (doc_pessoal_status = 'I') OR (doc_pessoal_verso_status = 'I') OR (doc_endereco_status = 'I') OR (doc_selfie_status = 'I') OR (doc_company_status = 'I') ";
			$sql2 .= "WHERE (doc_pessoal_status = 'I') OR (doc_pessoal_verso_status = 'I') OR (doc_endereco_status = 'I') OR (doc_selfie_status = 'I') OR (doc_company_status = 'I') ";
		}elseif($status == 3)
		{
			$sql .= "WHERE (doc_pessoal_status = 'P') OR (doc_pessoal_verso_status = 'P') OR (doc_endereco_status = 'P') OR (doc_selfie_status = 'P') OR (doc_company_status = 'P') ";
			$sql2 .= "WHERE (doc_pessoal_status = 'P') OR (doc_pessoal_verso_status = 'P') OR (doc_endereco_status = 'P') OR (doc_selfie_status = 'P') OR (doc_company_status = 'P') ";
		}

		if(!empty($search)){

			if($type=="id")
			{
				$sql .= " AND usr_id='$search' ";
				$sql2 .= " AND usr_id='$search' ";
			}elseif($type=="name")
			{
				$sql .= " AND usr_name LIKE '%$search%' ";
				$sql2 .= " AND usr_name LIKE '%$search%' ";
			}elseif($type=="username")
			{
				$sql .= " AND usr_login_id='$search' ";
				$sql2 .= " AND usr_login_id='$search' ";
			}
		}
		
		$sql .= " ORDER BY ".$order."  
			  LIMIT $inicial, $numreg";

		$sql = geraSQL($sql);
		//Conta a quantidade de registros
		list($quantreg) = abreSQL($sql2);// Quantidade de registros pra paginação

	}else
	{

		$sql = "SELECT usr_id, usr_login_id, usr_name, doc_pessoal_status, doc_pessoal_verso_status, doc_endereco_status, doc_selfie_status,doc_company_status 
		FROM tb_documentacao INNER JOIN tb_user on usr_id = doc_idUsuario ";

		//Adiciona o sql para paginação
		$sql2 = "SELECT count(*) FROM tb_documentacao INNER JOIN tb_user on usr_id = doc_idUsuario ";

		if($status == 1){
			$sql .= "WHERE (doc_pessoal_status = 'V' AND doc_pessoal_verso_status = 'V' AND doc_endereco_status = 'V' AND doc_selfie_status = 'V' AND doc_company_status = 'V') ";
			$sql2 .= "WHERE (doc_pessoal_status = 'V' AND doc_pessoal_verso_status = 'V' AND doc_endereco_status = 'V' AND doc_selfie_status = 'V' AND doc_company_status = 'V') ";
		}elseif($status == 2){
			$sql .= "WHERE (doc_pessoal_status = 'I' OR doc_pessoal_verso_status = 'I' OR doc_endereco_status = 'I' OR doc_selfie_status = 'I' OR doc_company_status = 'I') ";
			$sql2 .= "WHERE (doc_pessoal_status = 'I' OR doc_pessoal_verso_status = 'I' OR doc_endereco_status = 'I' OR doc_selfie_status = 'I' OR doc_company_status = 'I') ";
		}elseif($status == 3){
			$sql .= "WHERE (doc_pessoal_status = 'P' OR doc_pessoal_verso_status = 'P' OR doc_endereco_status = 'P' OR doc_selfie_status = 'P' OR doc_company_status = 'P') ";
			$sql2 .= "WHERE (doc_pessoal_status = 'P' OR doc_pessoal_verso_status = 'P' OR doc_endereco_status = 'P' OR doc_selfie_status = 'P' OR doc_company_status = 'P') ";
		}

		$sql .= "ORDER BY $order LIMIT $inicial, $numreg";

		// Faz o Select pegando o registro inicial até a quantidade de registros para página
		$sql = geraSQL($sql);
	
		// Serve para contar quantos registros você tem na seua tabela para fazer a paginação
		list($quantreg) = abreSQL($sql2);// Quantidade de registros pra paginação

	}
?>
  <div class="content">
    <div class="content_resize">
      <div class="mainbar">
        <div class="article">
          <h2><span>Documentação</span></h2>
          <div class="clr"></div>
            <table width="700" border="0" cellspacing="0" cellpadding="0">
            <form name="formSearch" action="" method="post" onSubmit="return complete();">
            <input type="hidden" name="src" value="1" />
              <tr>
                <td>Search</td>
                <td><label for="search"></label>
                <input name="search" type="text" id="search" size="40" value="<?=$_GET['search']; ?>" /></td>
                <td><label for="type"></label>
                  <select name="type" id="type">
                    <option value="username" <?php if ($_GET['type'] == "username") { echo 'selected="selected"'; } ?>>Username</option>
                    <option value="id" <?php if ($_GET['type'] == "id") { echo 'selected="selected"'; } ?>>ID do Usuário</option>
                    <option value="name" <?php if ($_GET['type'] == "name") { echo 'selected="selected"'; } ?>>Nome</option>
				</select></td>
				<td><select name="status" id="status" >
                    <option value="1" <?php if ($status == "1") { echo 'selected="selected"'; } ?>>Aprovados</option>
                    <option value="2" <?php if ($status == "2") { echo 'selected="selected"'; } ?>>Inválidos</option>
					<option value="3"  <?php if ($status == "3") { echo 'selected="selected"'; } ?>>Aguardando</option>
					<option value="4"  <?php if ($status == "4") { echo 'selected="selected"'; } ?>>Todos</option>
                </select></td>
                <td><input type="submit" name="Submit" id="Submit" value="Enviar" /></td>
              </tr>
            </form>
			</table>
			<?php if($src==1){ ?>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td><a href="./?p=documentation" >Mostrar todos</a></td>
              </tr>
              <tr>
                <td>
                <?php
                    include("pagination.php"); // Chama o arquivo que monta a paginação. ex: << anterior 1 2 3 4 5 próximo >>
                ?>
                </td>
              </tr>
            </table>
            <?php }else{ ?>
            <p>
            <?php
                include("pagination.php"); // Chama o arquivo que monta a paginação. ex: << anterior 1 2 3 4 5 próximo >>
            ?>
            </p>
            <?php } ?>
            <table width="96%" border="0" cellspacing="0" cellpadding="0">
              <tr style="border:1px solid #CCC">
                <td bgcolor="#EEEEEE" width="6%">ID User</td>
                <td bgcolor="#EEEEEE">Username</td>
                <td bgcolor="#EEEEEE">Name</td>
				<td bgcolor="#EEEEEE">Doc Pessoal</td>
				<td bgcolor="#EEEEEE">Doc Verso</td>
                <td bgcolor="#EEEEEE">Doc Endereço</td>
				<td bgcolor="#EEEEEE">Doc Selfie</td>
				<td bgcolor="#EEEEEE">Doc Company</td>
                <td bgcolor="#EEEEEE">Ação</td>
              </tr>
            <?php while($reg = mysqli_fetch_array($sql)){ ?>
              <tr style="border:1px solid #CCC">
                <td><?=$reg["usr_id"]; ?></td>
                <td><?=$reg["usr_login_id"]; ?></td>
                <td><?=$reg["usr_name"]; ?></td>
                <td>
                	<?php 
					switch ($reg["doc_pessoal_status"]) { 
	                  case 'P' :
	                    echo '<font id="fontpersonal" style="color:#025E9B;">Processando</font>';
	                    break;
	                  case 'U' :
	                    echo '<font id="fontpersonal" style="color:#025E9B;">Arquivo Carregado</font>';
	                    break;
	                  case 'V' :
	                    echo '<font id="fontpersonal" style="color:#47a447;">Verificado</font>';
	                    break;
	                  case 'I' :
	                    echo '<font id="fontpersonal" style="color:#FF0000;">Invalido</font>';
	                    break;
	                  default :
	                    echo '<font id="fontpersonal" style="color:#FF0000;">Não Enviado</font>';
	                    break;
	                }
					?>
				</td>
                <td>
                	<?php 
					switch ($reg["doc_pessoal_verso_status"]) { 
	                  case 'P' :
	                    echo '<font id="fontpersonal" style="color:#025E9B;">Processando</font>';
	                    break;
	                  case 'U' :
	                    echo '<font id="fontpersonal" style="color:#025E9B;">Arquivo Carregado</font>';
	                    break;
	                  case 'V' :
	                    echo '<font id="fontpersonal" style="color:#47a447;">Verificado</font>';
	                    break;
	                  case 'I' :
	                    echo '<font id="fontpersonal" style="color:#FF0000;">Invalido</font>';
	                    break;
	                  default :
	                    echo '<font id="fontpersonal" style="color:#FF0000;">Não Enviado</font>';
	                    break;
	                }
					?>
                </td>
                <td>
                	<?php 
					switch ($reg["doc_endereco_status"]) { 
	                  case 'P' :
	                    echo '<font id="fontpersonal" style="color:#025E9B;">Processando</font>';
	                    break;
	                  case 'U' :
	                    echo '<font id="fontpersonal" style="color:#025E9B;">Arquivo Carregado</font>';
	                    break;
	                  case 'V' :
	                    echo '<font id="fontpersonal" style="color:#47a447;">Verificado</font>';
	                    break;
	                  case 'I' :
	                    echo '<font id="fontpersonal" style="color:#FF0000;">Invalido</font>';
	                    break;
	                  default :
	                    echo '<font id="fontpersonal" style="color:#FF0000;">Não Enviado</font>';
	                    break;
	                }
					?>
                </td>
				<td>
					<?php 
					switch ($reg["doc_selfie_status"]) { 
	                  case 'P' :
	                    echo '<font id="fontpersonal" style="color:#025E9B;">Processando</font>';
	                    break;
	                  case 'U' :
	                    echo '<font id="fontpersonal" style="color:#025E9B;">Arquivo Carregado</font>';
	                    break;
	                  case 'V' :
	                    echo '<font id="fontpersonal" style="color:#47a447;">Verificado</font>';
	                    break;
	                  case 'I' :
	                    echo '<font id="fontpersonal" style="color:#FF0000;">Invalido</font>';
	                    break;
	                  default :
	                    echo '<font id="fontpersonal" style="color:#FF0000;">Não Enviado</font>';
	                    break;
	                }
					?>
				</td>
                <td>
                	<?php 
					switch ($reg["doc_company_status"]) { 
	                  case 'P' :
	                    echo '<font id="fontpersonal" style="color:#025E9B;">Processando</font>';
	                    break;
	                  case 'U' :
	                    echo '<font id="fontpersonal" style="color:#025E9B;">Arquivo Carregado</font>';
	                    break;
	                  case 'V' :
	                    echo '<font id="fontpersonal" style="color:#47a447;">Verificado</font>';
	                    break;
	                  case 'I' :
	                    echo '<font id="fontpersonal" style="color:#FF0000;">Invalido</font>';
	                    break;
	                  default :
	                    echo '<font id="fontpersonal" style="color:#FF0000;">Não Enviado</font>';
	                    break;
	                }
					?>
                </td>
                <td><input name="edit" value="Verificar" type="button" onclick="location.href='?p=documentation_edit&id=<?=$reg["usr_id"]; ?>'" /></td>
              </tr>
            <?php } ?>
            </table>
            
			<p><?php include("pagination.php"); // Chama o arquivo que monta a paginação. ex: << anterior 1 2 3 4 5 próximo >> ?></p>
        </div>
      </div>
      <div class="clr"></div>
    </div>
  </div>