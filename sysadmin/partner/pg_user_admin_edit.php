<?php
	
	########################
	$val = $_POST["val"];
	$act = $_POST["act"];
	$userID = $_POST["userID"];
	$pageID = $_POST["page"];
	
	if($act==1 && !empty($userID) && !empty($pageID))
	{
		//Insere novo usuário administrador
		executaSQL("INSERT INTO tb_user_admin_acesso (uac_user,uac_page) VALUES ('".$userID."','".$pageID."')");

		print "
		<script type=\"text/javascript\">
		alert(\"Página cadastrada!\");
		</script>";

	}
		
	if($act==2 && !empty($_POST["uac_id"]))
	{
		executaSQL("UPDATE tb_user_admin_acesso SET uac_view=".$_POST["view"].",uac_edit=".$_POST["edit"].",uac_delete=".$_POST["delete"]." WHERE uac_id=".$_POST["uac_id"]);

		print "
		<script type=\"text/javascript\">
		alert(\"Alteração feita com sucesso!\");
		</script>";

	}
	
	####################
	
	$user = $_GET["id"];

	$rowCadastro 	= abreSQL("SELECT * FROM tb_user_admin WHERE adm_id = '$user'");

	$name 				= $rowCadastro["adm_name"];
	$email 				= $rowCadastro["adm_email"];
	$login 				= $rowCadastro["adm_login"];
	$type				= $rowCadastro["adm_type"];
	$status				= $rowCadastro["adm_status"];

	// Faz o Select pegando o registro inicial até a quantidade de registros para página
	$sql = geraSQL("SELECT uac_id,
							upag_name,
							uac_view,
							uac_edit,
							uac_delete
						FROM tb_user_admin_acesso
						INNER JOIN tb_user_admin_pages
						ON upag_id=uac_page
						AND uac_user=$user
						ORDER BY uac_id ");

	// Serve para contar quantos registros você tem na seua tabela para fazer a paginação
	list($quantreg) = abreSQL("SELECT count(*) FROM tb_user_admin_acesso WHERE uac_user=$user");// Quantidade de registros pra paginação

	// Faz o Select pegando o registro inicial até a quantidade de registros para página
	$sql_page = geraSQL("SELECT upag_id,
								upag_name,
								upag_language
						FROM tb_user_admin_pages
						WHERE upag_status=1
						AND upag_id NOT IN (SELECT uac_page FROM tb_user_admin_acesso WHERE uac_user=$user)");

?>

  <div class="content">
    <div class="content_resize">
      <div class="mainbar">
        <div class="article">
          <h2><span>Editar permissão de acesso</span></h2>
          <div class="clr"></div>
		  <p><a href="?p=user_admin">Voltar</a></p>
		  <table width="96%" border="0" cellspacing="0" cellpadding="0" id="newuser" style="margin:20px 0">
              <tr style="border:1px solid #CCC;" bgcolor="#EEEEEE">
				<td><strong>Usuário</strong></td>
				<td>&nbsp;</td>
              </tr>
              <tr>
			  	<td width="25%">Login</td>
				<td><?php echo $login; ?></td>
			  </tr>
              <tr>
			  	<td width="25%">Nome</td>
				<td><?php echo $name; ?></td>
              </tr>
              <tr>
			  	<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
            </table>
		  <table width="96%" border="0" cellspacing="0" cellpadding="0" id="newuser" style="margin:20px 0">
			<form name="form" action="" method="post">
			  <input type="hidden" name="act" value="1" />
			  <input type="hidden" name="userID" value="<?php echo $user; ?>" />
              <tr style="border:1px solid #CCC;" bgcolor="#EEEEEE">
				<td><strong>Incluir página de acesso</strong></td>
				<td>&nbsp;</td>
              </tr>
              <tr>
			  	<td width="25%">Página</td>
				<td>
					<select name="page">
					<?php while($reg_page = mysqli_fetch_array($sql_page)) { ?>
					<option value="<?php echo $reg_page["upag_id"]; ?>"><?php echo $reg_page["upag_name"]; ?></option>
					<?php } ?>
					</select>
					<input type="submit" name="send" value="Incluir">
				</td>
              </tr>
              <tr>
			  	<td>&nbsp;</td>
				<td>&nbsp;</td>
			  </tr>
			</form>
            </table>
		  <table width="96%" border="0" cellspacing="1" cellpadding="2">
			<tr style="border:1px solid #CCC">
				<td bgcolor="#EEEEEE"><strong>Página</strong></td>
				<td colspan="5" style="text-align:center" bgcolor="#EEEEEE"><strong>Ação</strong></td>
			</tr>
              <tr style="border:1px solid #CCC">
                <td bgcolor="#EEEEEE">Página</td>
                <td bgcolor="#EEEEEE">Visualizar</td>
                <td bgcolor="#EEEEEE">Editar</td>
				<td bgcolor="#EEEEEE">Deletar</td>
				<td bgcolor="#EEEEEE">Ação</td>
				<td bgcolor="#EEEEEE">Ação</td>
              </tr>
				<?php 
					while($reg = mysqli_fetch_array($sql))
					{ 
				?>
			  <tr style="border:1px solid #CCC; font-weight:bold" >
			  <form name="form" action="" method="post">
				<input type="hidden" name="uac_id" value="<?=$reg["uac_id"]; ?>" />
				<input type="hidden" name="act" value="2" />
                <td><?=$reg["upag_name"]; ?></td>
                <td>
					<select name="view">
						<option value="0" <?php if($reg["uac_view"]==0){ echo "selected"; } ?>>Não</option>
						<option value="1" <?php if($reg["uac_view"]==1){ echo "selected"; } ?>>Sim</option>
					</select>
				</td>
                <td>
					<select name="edit">
						<option value="0" <?php if($reg["uac_edit"]==0){ echo "selected"; } ?>>Não</option>
						<option value="1" <?php if($reg["uac_edit"]==1){ echo "selected"; } ?>>Sim</option>
					</select>
				</td>
                <td>
					<select name="delete">
						<option value="0" <?php if($reg["uac_delete"]==0){ echo "selected"; } ?>>Não</option>
						<option value="1" <?php if($reg["uac_delete"]==1){ echo "selected"; } ?>>Sim</option>
					</select>
				</td>
				<td><input name="alterar" value="Alterar" type="submit" /></td>
				</form>
				<td>
				<form name="form" action="" method="post">
				<input type="hidden" name="act" value="3" />
				<input type="hidden" name="uac_id" value="<?=$reg["uac_id"]; ?>" />
				<input name="remove" value="Remover" type="submit" />
				</form>
				</td>
              </tr>
            <?php } ?>
            </table>

          <p>&nbsp;</p>
        </div>
      </div>
      <div class="clr"></div>
    </div>
  </div>