<?php
require_once("./libs/accountfunctions.php"); 
########################
$saque_id = $_GET["id"];

if($_POST){

	list($status) = abreSQL("SELECT bco_status,bco_transaction_id FROM tb_bitcoin_operations WHERE bco_type='W' AND bco_id = ".$saque_id);

	$erro = 0;

	if ($status != "U") {
		$mensagem_erro .= "Você não pode transferir dinheiro de um saque com status diferente de Processando!<br>";
		$erro++;
	}

#---------- Caso não tenha nenhum erro nos dados enviados ---------------------------

	if($erro==0)
	{
		list($idUser, $valorSaque, $account_to) = abreSQL("SELECT bco_user, bco_dollar_amount, bco_account_to FROM tb_bitcoin_operations WHERE bco_type='W' AND bco_status = 'U' AND bco_id = " . $saque_id);
		
		$valorSaqueBTC = getTradeUSDBTC($valorSaque);

		executaSQL("UPDATE tb_financeiro_saldo SET bsal_saldo_areceber = (bsal_saldo_areceber - $valorSaque) WHERE bsal_idUsuario = $idUser;");
		
		executaSQL("UPDATE tb_bitcoin_operations SET bco_status = 'P',bco_bitcoin_amount='$valorSaqueBTC' WHERE bco_id = '$saque_id'");

		print "
		<script type=\"text/javascript\">
		alert(\"Valor transferido com sucesso!\");
		window.location='?p=withdrawal';
		</script>";

		
	}else
	{
		$msgerro = '
		<table width="500" border="0" align="center" cellpadding="1" cellspacing="1" bgcolor="#999999">
			<tr>
				<td align="center">
					<table width="500" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#EEEEEE">
						<tr>
							<td class="stylexx" style="padding-left:10px; padding-right:10px; padding-top:10px;">
								<b>Encontrou os seguintes erros:</b><br><br>
								'.$mensagem_erro.'
							</td>
						</tr>
					</table>
				</td>
			</tr>

			<tr>
				<td align="center">
					<table width="500" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#EEEEEE">
						<tr>
							<td align="center">
								<a href="javascript:history.go(-1)"> << Return >></a>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>';
	}

}

####################

$rowCadastro 	= abreSQL("SELECT   bco_id,
									bco_bitcoin_amount,
									bco_dollar_amount,
									bco_date_insert,
									bco_status,
									bco_account_to,
									usr_id,
									usr_login_id,
									usr_name,
									usr_email
									FROM tb_bitcoin_operations INNER JOIN tb_user on usr_id = bco_user
									WHERE bco_type='W' AND bco_id = ".$saque_id);

$bitcoin_amount			= $rowCadastro["bco_bitcoin_amount"];
$dollar_amount 			= $rowCadastro["bco_dollar_amount"];
$saque_data 			= $rowCadastro["bco_date_insert"];
$saque_status 			= $rowCadastro["bco_status"];
$transaction_id 		= $rowCadastro["bco_transaction_id"];
$saque_obs 				= "...";
$saque_dados_bancarios	= $rowCadastro["bco_account_to"];
$usr_id					= $rowCadastro["usr_id"];
$usr_login_id			= $rowCadastro["usr_login_id"];
$usr_name				= $rowCadastro["usr_name"];
$usr_email				= $rowCadastro["usr_email"];

?>
<script src="jquery/jquery.js" type="text/javascript"></script>

<div class="content">
	<div class="content_resize">
		<div class="mainbar">
			<div class="article">
				<h2><span>Editar Saque</span></h2>
				<div class="clr"></div>
				<p><a href="?p=withdrawal">Voltar</a></p>
				<?php 
				if(!empty($msgerro))
				{
					echo $msgerro;  
				}
				?>
				<table width="100%" border="0" cellspacing="1" cellpadding="1">
					<form name="form" id="form" action="" method="post">
						<input type="hidden" name="val" value="1" />
						<input type="hidden" name="wdID" value="<?=$saque_id ?>" />
						<tr>
							<td align="right">ID:</td>
							<td><?=$usr_id; ?></td>
						</tr>
						<tr>
							<td align="right">Username:</td>
							<td><?=$usr_login_id; ?></td>
						</tr>
						<tr>
							<td align="right">Nome:</td>
							<td><?=$usr_name; ?></td>
						</tr>
						<tr>
							<td align="right">E-mail:</td>
							<td><?=$usr_email; ?></td>
						</tr>
						<tr>
							<td align="right">Valor:</td>
							<td><?="$ ".number_format($dollar_amount, 2, '.', ','); ?></td>
						</tr>
						<tr>
							<td align="right">BTC:</td>
							<td><?=getTradeUSDBTC($dollar_amount); ?></td>
						</tr>
						<tr>
							<td align="right">Hash:</td>
							<td><?=$transaction_id; ?></td>
						</tr>
						<tr>
							<td align="right">Data:</td>
							<td><?=$saque_data; ?></td>
						</tr>
						<tr>
							<td align="right">Endereço BTC:</td>
							<td><?=$saque_dados_bancarios; ?></textarea></td>
						</tr>
						<tr>
							<td align="right">Status:</td>
							<?php 
							switch ($saque_status) {
								case 'P':
									echo '<td style="color:#47a447;">Pago</td>';
									break;
								case 'U':
									echo '<td style="color:#47a447;">Processando</td>';
									break;
								case 'C':
									echo '<td style="color:#FF0000;">Cancelado</td>';
									break;
								default:
									echo '<td></td>';
									break;
							}
							?>
						</tr>
						<tr>
							<td align="right">&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td align="right">&nbsp;</td>
							<td>
							<?php if ($saque_status == 'U') { ?>
								<input type="submit" name="bt_transfer" id="bt_transfer" value="Confirmar transferência" />
							<?php } ?>
							</td>
						</tr>
					</form>
				</table>
				<p>&nbsp;</p>
			</div>
		</div>
		<div class="clr"></div>
	</div>
</div>