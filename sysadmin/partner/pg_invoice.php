<link rel="stylesheet" href="http://code.jquery.com/ui/1.9.0/themes/base/jquery-ui.css" />
<script src="http://code.jquery.com/jquery-1.8.2.js"></script>
<script src="http://code.jquery.com/ui/1.9.0/jquery-ui.js"></script>
<?php

$search = $_GET["search"];
$type	= $_GET["type"];
$status	= $_GET["status"];
$pg	= $_GET["pg"];
$data_inicial = $_GET["data_inicial"];
$data_final = $_GET["data_final"];
$src	= $_GET["src"];

//######### INICIO Paginação
$numreg = 100; // Quantos registros por página vai ser mostrado
if (!isset($pg)) {
	$pg = 0;
}
$inicial = $pg * $numreg;
	
//######### FIM dados Paginação

// Faz o Select pegando o registro inicial até a quantidade de registros para página
$sql = "SELECT usr_id,
				usr_login_id,
				usr_name,
				usr_email,
				fat_id,
				fat_descricao,
				fat_valor,
				fat_dataVencimento,
				fat_dataEnvio,
				fat_limite,
				fat_status 
		FROM tb_user a 
		INNER JOIN tb_fatura b
		ON a.usr_id=b.fat_idUsuario ";

$sql2 = "SELECT count(*),sum(fat_valor)as sum
		FROM tb_user a 
		INNER JOIN tb_fatura b
		ON a.usr_id=b.fat_idUsuario ";

if($type=="invoice")
{
	$sql .= "AND fat_id='$search' ";
	$sql2 .= "AND fat_id='$search' ";
}elseif($type=="name")
{
	$sql .= "AND usr_name LIKE '%$search%' ";
	$sql2 .= "AND usr_name LIKE '%$search%' ";
}elseif($type=="username")
{
	$sql .= "AND usr_login_id='$search' ";
	$sql2 .= "AND usr_login_id LIKE '%$search%' ";
}elseif($type=="userid")
{
	$sql .= "AND usr_id='$search' ";
	$sql2 .= "AND usr_id LIKE '%$search%' ";
}

if($status=="aberto"){
	$sql .= "AND fat_status=1 ";
	$sql2 .= "AND fat_status=1 ";
}elseif($status=="pago"){
	$sql .= "AND fat_status=2 ";
	$sql2 .= "AND fat_status=2 ";
}elseif($status=="cancelado"){
	$sql .= "AND fat_status=0 ";
	$sql2 .= "AND fat_status=0 ";
}

if(!empty($data_inicial)){
	$sql .= "AND DATE(fat_dataVencimento)>='$data_inicial' ";
	$sql2 .= "AND DATE(fat_dataVencimento)>='$data_inicial' ";
}

if(!empty($data_final)){
	$sql .= "AND DATE(fat_dataVencimento)<='$data_final' ";
	$sql2 .= "AND DATE(fat_dataVencimento)<='$data_final' ";
}

$sql .= " ORDER BY fat_id DESC 
		LIMIT $inicial, $numreg";


$sql = geraSQL($sql);

list($quantreg,$soma) = @mysqli_fetch_array(geraSQL($sql2));// Quantidade de registros pra paginação

?>

  <div class="content">
    <div class="content_resize">
      <div class="mainbar">
        <div class="article">
          <h2><span>Faturas</span></h2>
          <div class="clr"></div>
			<table width="98%" border="0" cellspacing="0" cellpadding="0">
				<form name="formSearch" action="" method="get" >
				<input type="hidden" name="p" value="invoice" />
				<input type="hidden" name="src" value="1" />
				<tr>
					<td>Search</td>
					<td>Type</td>
					<td>Status</td>
					<td>Data Inicial</td>
					<td>Data Final</td>
					<td></td>
				</tr>
				<tr>
					<td>
						<input name="search" type="text" id="search" size="20" />
					</td>
					<td>
						<label for="type"></label>
						<select name="type" id="type">
							<option value="">-- Type --</option>
							<option value="invoice">Número da Fatura</option>
							<option value="name">Nome</option>
							<option value="username">Username</option>
							<option value="userid">ID</option>
							<option value="nossonumero">Nosso Número</option>
						</select>
					</td>
					<td>
						<label for="status"></label>
						<select name="status" id="status">
							<option value="">-- Status --</option>
							<option value="aberto" <?php if($status=="aberto"){ echo "selected";} ?>>Aberto</option>
							<option value="pago" <?php if($status=="pago"){ echo "selected";} ?>>Pago</option>
							<option value="cancelado" <?php if($status=="cancelado"){ echo "selected";} ?>>Cancelado</option>
						</select>
					</td>
					<td>
						<input name="data_inicial" id="data_inicial" type="text" id="data_inicial" size="20" value="<?php echo $data_inicial; ?>" />
					</td>
					<td>
						<input name="data_final" id="data_final" type="text" id="data_final" size="20" value="<?php echo $data_final; ?>" />
					</td>
					<td><input type="submit" name="Submit" d="Submit" value="Enviar" /></td>
				</tr>
			</form>
			</table>
			<table width="70%" border="0" cellspacing="0" cellpadding="0" style="margin-top:10px;">
				<tr bgcolor="#EEEEEE">
					<td>Quantidade</td>
					<td>Valor total</td>
				</tr>
				<tr>
					<td><strong><?php echo $quantreg ?></strong></td>
					<td><strong>$ <?=number_format($soma, 2, '.', ',') ; ?></strong></td>
				</tr>
			</table>
			<?php if($src==1){ ?>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td><a href="./?p=invoice" >Mostrar todas</a></td>
			</tr>
			<tr>
				<td>
				<?php
					include("pagination.php"); // Chama o arquivo que monta a paginação. ex: << anterior 1 2 3 4 5 próximo >>
				?>
				</td>
			</tr>
			</table>
			<?php }else{ ?>
			<p>
			<?php
				include("pagination.php"); // Chama o arquivo que monta a paginação. ex: << anterior 1 2 3 4 5 próximo >>
			?>
			</p>
			<?php } ?>
			<table width="98%" border="0" cellspacing="0" cellpadding="0">
			<tr style="border:1px solid #CCC">
				<td bgcolor="#EEEEEE">ID User</td>
				<td bgcolor="#EEEEEE">Username</td>
				<td bgcolor="#EEEEEE">ID Fatura</td>
				<td bgcolor="#EEEEEE">Descrição</td>
				<td bgcolor="#EEEEEE">Valor</td>
				<td bgcolor="#EEEEEE">Vencimento</td>
				<td bgcolor="#EEEEEE">Cadastro</td>
				<td bgcolor="#EEEEEE">Status</td>
			</tr>
			<?php while($reg = mysqli_fetch_array($sql)){ ?>
			<tr style="border:1px solid #CCC">
				<td><?=$reg["usr_id"]; ?></td>
				<td><?=$reg["usr_login_id"]; ?></td>
				<td><?=$reg["fat_id"]; ?></td>
				<td><?=$reg["fat_descricao"]; ?></td>
				<td>$ <?=number_format($reg["fat_valor"], 2, '.', ',') ; ?></td>
				<td><?=$reg["fat_dataVencimento"]; ?></td>
				<td><?=$reg["fat_dataEnvio"]; ?></td>
				<td>
				<?php 
				
				if($reg["fat_status"]==1)
				{
					echo "<span style='color:#C00'>Aberto</span>";
					/*echo "<span style='color:#C00'>
						<input name=\"payment\" value=\"Baixar\" type=\"button\" onclick=\"fatura(".$reg["fat_id"].");\" />
						</span>";*/
				}elseif(($reg["fat_status"]==2)){
					echo "<span style='color:#090'>Pago</span>";
				}elseif(($reg["fat_status"]==3) || ($reg["fat_status"]==0)){
					echo "<span style='color:#F63'>Cancelado</span>";
				}; ?>
				</td>
			</tr>
			<?php } ?>
			</table>
            <a class="fancybox" href="#inline1" style="display:none" ></a>
            <div id="inline1" style="width:600px;display: none;">
            </div>
			<p><?php include("pagination.php"); // Chama o arquivo que monta a paginação. ex: << anterior 1 2 3 4 5 próximo >> ?></p>
        </div>
      </div>
      <div class="clr"></div>
    </div>
  </div>
<script>
$(function() {
    $( "#data_inicial" ).datepicker({ dateFormat: 'yy-mm-dd' });
	$( "#data_final" ).datepicker({ dateFormat: 'yy-mm-dd' });
});
</script>