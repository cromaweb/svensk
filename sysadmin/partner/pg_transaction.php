<?php
  
  if($_POST){
    $username = addslashes($_POST["username"]);
    $wallet = addslashes($_POST["wallet"]);
    $type = addslashes($_POST["type"]);
    $value = str_replace(',','',addslashes($_POST["value"]));
    $description = addslashes($_POST["description"]);
    $erro = 0;

    if(empty($username)){
      $mensagem_erro .= "Username cannot be empty.<br>";
      $erro++;
    } else {
      list($usrID) = abreSQL("SELECT usr_id FROM tb_user WHERE usr_login_id='$username'");
      if(empty($usrID)){
        $mensagem_erro .= "Username not found.<br>";
        $erro++;
      }
    }
    
    if(!in_array($wallet, array('l','r','c','b','d'))) {
      $mensagem_erro .= "Invalid wallet.<br>";
      $erro++;
    }

    if(!in_array($type, array('c','d'))) {
      $mensagem_erro .= "Invalid type.<br>";
      $erro++;
    }

    if($value < 0){
      $mensagem_erro .= "Value must be greater than 0.<br>";
      $erro++;
    }

    if(empty($description)){
      $mensagem_erro .= "Description cannot be empty.<br>";
      $erro++;
    }

    if($erro==0)
    {     

		if ($type == 'd') {
		$value = $value * (-1);
		}
		
		if ($wallet == "r") {
			list($saldoAnterior) = abreSQL("SELECT bsal_saldo_rendimento FROM tb_financeiro_saldo WHERE bsal_idUsuario='$usrID'");
		} elseif($wallet == "c") {
			list($saldoAnterior) = abreSQL("SELECT bsal_saldo_comissoes FROM tb_financeiro_saldo WHERE bsal_idUsuario='$usrID'");
		} elseif($wallet == "b") {
			list($saldoAnterior) = abreSQL("SELECT bsal_saldo_bitwyn FROM tb_financeiro_saldo WHERE bsal_idUsuario='$usrID'");
		} elseif($wallet == "d") {
			list($saldoAnterior) = abreSQL("SELECT bsal_saldo_deposit FROM tb_financeiro_saldo WHERE bsal_idUsuario='$usrID'");
		} else {
			list($saldoAnterior) = abreSQL("SELECT bsal_saldo_liberado FROM tb_financeiro_saldo WHERE bsal_idUsuario='$usrID'");
		}
	  
		$saldoAtual = $saldoAnterior + $value;
		
		if($wallet != "d") {
			//Faz insert tabela tb_financeiro_extrato
			executaSQL("INSERT INTO tb_financeiro_extrato (extf_idUsuario,extf_idCompra, extf_descricao, extf_conta, extf_saldo_anterior,extf_saldo_atual, extf_tipo, extf_valor, extf_data) 
			VALUES ('$usrID','0','$description','$wallet','$saldoAnterior','$saldoAtual','$type','$value',now())");
		}
      
      if ($wallet == "r") {
        executaSQL("INSERT INTO tb_financeiro_saldo (bsal_idUsuario, bsal_saldo_rendimento) 
                      VALUES (".$usrID.",'".$saldoAtual."') 
                      ON DUPLICATE KEY 
                      UPDATE bsal_saldo_rendimento = (".$saldoAtual.")");
      } elseif($wallet == "c") {
        executaSQL("INSERT INTO tb_financeiro_saldo (bsal_idUsuario, bsal_saldo_comissoes) 
                      VALUES (".$usrID.",'".$saldoAtual."') 
                      ON DUPLICATE KEY 
                      UPDATE bsal_saldo_comissoes = (".$saldoAtual.")");
      } elseif($wallet == "b") {
        executaSQL("INSERT INTO tb_financeiro_saldo (bsal_idUsuario, bsal_saldo_bitwyn) 
                      VALUES (".$usrID.",'".$saldoAtual."') 
                      ON DUPLICATE KEY 
                      UPDATE bsal_saldo_bitwyn = (".$saldoAtual.")");
      } elseif($wallet == "d") {
        executaSQL("INSERT INTO tb_financeiro_saldo (bsal_idUsuario, bsal_saldo_deposit) 
                      VALUES (".$usrID.",'".$saldoAtual."') 
                      ON DUPLICATE KEY 
                      UPDATE bsal_saldo_deposit = (".$saldoAtual.")");
      } else {
        executaSQL("INSERT INTO tb_financeiro_saldo (bsal_idUsuario, bsal_saldo_liberado) 
                      VALUES (".$usrID.",'".$saldoAtual."') 
                      ON DUPLICATE KEY 
                      UPDATE bsal_saldo_liberado = (".$saldoAtual.")");
      }
	  
      print "
        <script type=\"text/javascript\">
        alert(\"Transaction added.\");
        window.location='?p=transaction';
        </script>";
    } else {
     $msgerro = '<table width="500" border="0" align="center" cellpadding="1" cellspacing="1" bgcolor="#999999">
      <tr>
        <td align="center">
          <table width="500" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#EEEEEE">
          <tr>
          <td class="stylexx" style="padding-left:10px; padding-right:10px; padding-top:10px;"><b>Found the following errors:</b><br>'.$mensagem_erro.'</td>
          </tr>
          </table>
        </td>
      </tr>
      </table>';
    }
    
  }

?>
<div class="content">
    <div class="content_resize">
      <div class="mainbar">
        <div class="article">
          <h2><span>Transaction</span></h2>
          <div class="clr"></div>
          <?php 
    			  if(!empty($msgerro))
    			  {
    				  echo $msgerro;  
    			  }
    			?>
          <div class="clr"></div>
          <form name="form" id="form" action="" method="post">
            <table width="100%" border="0" cellspacing="1" cellpadding="1">
            
              <tr>
                <td align="right">Username:</td>
                <td><input name="username" type="text" id="username" value="<?=$username; ?>" size="40" /></td>
              </tr>
              <tr>
                <td align="right">Wallet:</td>
                <td>
                  <select name="wallet" id="wallet">
                    <option value="">Select the wallet</option>
                    <option value="l">BYTC</option>
                    <option value="r">Earnings</option>
                    <option value="c">Comissions</option>
                    <option value="b">Bitwyn</option>
                    <option value="d">Deposit</option>
                  </select>
                </td>
              </tr>
              <tr>
                <td align="right">Type of transaction:</td>
                <td>
                  <select name="type" id="type">
                    <option value="">Select type</option>
                    <option value="c" selected="selected">Credit</option>
                    <option value="d">Debit</option>
                  </select>
                </td>
              </tr>
              <tr>
                <td align="right">Value:</td>
                <td><input name="value" type="text" id="value" value="<?=$value; ?>" size="40" /></td>
              </tr>
              <tr>
                <td align="right">Description:</td>
                <td><input name="description" type="text" id="description" value="<?php echo $description; ?>" size="40" /></td>
              </tr>
              <tr>
                <td align="right">&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td align="right">&nbsp;</td>
                <td><input type="submit" name="Submit" id="Submit" value="Submit" /></td>
              </tr>
            </table>
          </form>
          <p>&nbsp;</p>
        </div>
      </div>
      <div class="clr"></div>
    </div>
  </div>
<script type="text/javascript" src="/public/js/jquery.maskMoney.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
  $('#value').maskMoney();
});
</script>