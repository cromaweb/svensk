<link rel="stylesheet" href="http://code.jquery.com/ui/1.9.0/themes/base/jquery-ui.css" />
<script src="http://code.jquery.com/jquery-1.8.2.js"></script>
<script src="http://code.jquery.com/ui/1.9.0/jquery-ui.js"></script>
<?php


$pg	= $_GET["pg"];
$mes = $_GET["mes"];
$src	= $_GET["src"];

//######### INICIO Paginação
$numreg = 100; // Quantos registros por página vai ser mostrado
if (!isset($pg)) {
	$pg = 0;
}
$inicial = $pg * $numreg;
	
//######### FIM dados Paginação

// Faz o Select pegando o registro inicial até a quantidade de registros para página
$sql_value = "SELECT sum(uplan_value)as value FROM tb_user_plans WHERE uplan_status=1 ";

if(!empty($mes)){
	$sql_value .= "AND MONTH(uplan_day_start) = $mes";
}

list($valor_total) = abreSQL($sql_value);

// Faz o Select pegando o registro inicial até a quantidade de registros para página
$sql_consolidado = "SELECT sum(bcons_valor_team),sum(bcons_valor_rendimento),sum(bcons_valor_sharing_bonus) FROM `tb_bns_consolidado` ";

if(!empty($mes)){
	$sql_consolidado .= "WHERE MONTH(bcons_data) = $mes";
}

list($divisao,$rendimento,$sharing_bonus) = abreSQL($sql_consolidado);

?>

  <div class="content">
    <div class="content_resize">
      <div class="mainbar">
        <div class="article">
          <h2><span>Relatório Financeiro Mensal</span></h2>
          <div class="clr"></div>
			<table width="98%" border="0" cellspacing="0" cellpadding="0">
				<form name="formSearch" action="" method="get" >
				<input type="hidden" name="p" value="profits" />
				<input type="hidden" name="src" value="1" />
				<tr>
					<td width="12%">Mês</td>
					<td></td>
				</tr>
				<tr>
					<td>
						<select name="mes" id="mes">
							<option value="">-- Mês --</option>
							<option value="8" <?php if($mes=="8"){ echo "selected";} ?>>Agosto/2020</option>
							<option value="7" <?php if($mes=="7"){ echo "selected";} ?>>Julho/2020</option>
							<option value="6" <?php if($mes=="6"){ echo "selected";} ?>>Junho/2020</option>
						</select>
					</td>
					<td><input type="submit" name="Submit" d="Submit" value="Enviar" /></td>
				</tr>
			</form>
			</table>
			<table width="70%" border="0" cellspacing="0" cellpadding="0" style="margin-top:10px;">
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
			</table>
			<table width="98%" border="0" cellspacing="0" cellpadding="0">
			<tr style="border:1px solid #CCC">
				<td bgcolor="#EEEEEE"><strong>Valor total (entradas)</strong></td>
				<td bgcolor="#EEEEEE"><strong><a href="?p=profits_agente&&src=1&mes=<?=$mes?>">Agente - Comissões</a></strong></td>
				<td bgcolor="#EEEEEE"><strong><a href="?p=profits_rendimento&&src=1&mes=<?=$mes?>">Rendimentos</a></strong></td>
				<td bgcolor="#EEEEEE"><strong><a href="?p=profits_ddl&&src=1&mes=<?=$mes?>">Divisão de Lucros</a></strong></td>
			</tr>
			<tr style="border:1px solid #CCC; font-weight:bold">
				<td>$ <?=number_format($valor_total, 2, '.', ','); ?></td>
                <td>$ <?=number_format($sharing_bonus, 2, '.', ','); ?></td>
                <td>$ <?=number_format($rendimento, 2, '.', ','); ?></td>
                <td>$ <?=number_format($divisao, 2, '.', ','); ?></td>
			</tr>
			</table>
            <a class="fancybox" href="#inline1" style="display:none" ></a>
            <div id="inline1" style="width:600px;display: none;">
            </div>
			<p><?php include("pagination.php"); // Chama o arquivo que monta a paginação. ex: << anterior 1 2 3 4 5 próximo >> ?></p>
        </div>
      </div>
      <div class="clr"></div>
    </div>
  </div>