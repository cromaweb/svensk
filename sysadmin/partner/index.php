<?php	
	require_once("pg_session.php");
	//@require_once("../config.php");
	require_once("scripts/spirit.php");
	
	$p = $_GET["p"];

	$request = explode("/",strtolower(substr($_SERVER['REQUEST_URI'],1)));

	$request0 = $request[0];
	$request1 = $request[1];
	$request2 = $request[2];
	$request3 = $request[3];

	$website = "worldtradecorporation.io";

	@require_once("pg_header.php");

			switch ($p)
			{
				case "home"					: require("pg_main.php"); break;
				case "user"					: require("pg_user.php"); break;
				case "user_edit"			: require("pg_user_edit.php"); break;
				case "user_statement"		: require("pg_user_statement.php"); break;
				case "user_balance"			: require("pg_user_balance.php"); break;
				case "user_bonus"			: require("pg_user_bonus.php"); break;
				case "user_binary_bonus"	: require("pg_user_binary_bonus.php"); break;
				case "user_admin"			: require("pg_user_admin.php"); break;
				case "user_admin_edit"		: require("pg_user_admin_edit.php"); break;
				case "documentation"		: include("pg_documentation.php"); break;
				case "documentation_edit"	: include("pg_documentation_edit.php"); break;
				case "invoice"				: include("pg_invoice.php"); break;
				case "configuracoes"		: include("pg_configuracoes.php"); break;
				case "credit"				: require("pg_credit.php"); break;
				case "bonus" 				: require("pg_bonus.php"); break;
				case "plans" 				: require("pg_plans.php"); break;
				case "plans_edit" 			: require("pg_plans_edit.php"); break;
				case "profits" 				: require("pg_profits.php"); break;
				case "profits_agente" 		: require("pg_profits_agente.php"); break;
				case "profits_rendimento"	: require("pg_profits_rendimento.php"); break;
				case "profits_ddl"			: require("pg_profits_ddl.php"); break;
				case "reports"				: require("pg_reports.php"); break;
				case "password"				: require("pg_user_password.php"); break;
				case "deposit"				: require("pg_deposit.php"); break;
				case "deposit_details"		: require("pg_deposit_details.php"); break;
				case "withdrawal"			: require("pg_withdrawal.php"); break;
				case "withdrawal_edit"		: require("pg_withdrawal_edit.php"); break;
				case "category"				: require("pg_category.php"); break;
				case "category_new"			: require("pg_category_new.php"); break;
				case "category_edit"		: require("pg_category_edit.php"); break;
				case "news"					: require("pg_news.php"); break;
				case "news_new"				: require("pg_news_new.php"); break;
				case "news_edit"			: require("pg_news_edit.php"); break;
				case "user_promotion"		: require("pg_user_promotion.php"); break;
				default						: require("pg_main.php"); break;
			}
			

	@require_once("pg_footer.php");
?>