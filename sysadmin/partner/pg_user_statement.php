<style>
 .ordenar {
 	text-decoration: none;
 	color:#003366;
 }
 .ordenar.marcado {
 	font-weight: bold;
 	color: #FF0000;
 }
</style>
  <div class="content">
    <div class="content_resize">
      <div class="mainbar">
        <div class="article">
          <h2><span>Extrato Financeiro</span></h2>
          <div class="clr"></div>
<?php

if ($_POST["src"] == 1) {
	echo "<script>window.location = '?p=user_statement&pg=0&search=".$_POST["search"]."&type=".$_POST["type"]."';</script>";
}

$search = $_GET["search"];
$type = $_GET["type"];
$pg	= $_GET["pg"];

//######### INICIO Paginação
$numreg = 50; // Quantos registros por página vai ser mostrado
if (!isset($pg)) {
	$pg = 0;
}
$inicial = $pg * $numreg;
	
//######### FIM dados Paginação

// Faz o Select pegando o registro inicial até a quantidade de registros para página		
list($user,$username) = abreSQL("SELECT usr_id,usr_login_id FROM tb_user WHERE usr_id='$search'");

if(!empty($user))
{
	switch ($_GET['order']) {
		case "contaasc":
			$order = "extf_conta ASC";
			break;

		case "contadesc" : 
			$order = "extf_conta DESC";
			break;

		case "dataasc":
			$order = "extf_data ASC";
			break;

		case "datadesc" : 
			$order = "extf_data DESC";
			break;

		default:
			$order = "extf_id DESC";
			break;
	}
	
	$sql = "SELECT * FROM tb_financeiro_extrato 
			WHERE extf_idUsuario='$user' ";
	$sql2 = "SELECT count(*) FROM tb_financeiro_extrato 
			WHERE extf_idUsuario='$user' ";
	if(!empty($type)){
		$sql .= "AND extf_conta='$type' ";
		$sql2 .= "AND extf_conta='$type' ";
	}
	$sql .= "ORDER BY ".$order."
			LIMIT $inicial, $numreg";
			
	$sql = geraSQL($sql);
	
	list($quantreg) = abreSQL($sql2);

}
?>

<table width="200" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="3"><h3>Username: <a href="?p=user&pg=0&search=<?=$username ?>&type=username"> [<?=$username ?>]</a></h3></td>
    </tr>
<form name="formSearch" action="" method="post">
<input type="hidden" name="src" value="1" />
<input type="hidden" name="search" value="<?=$search ?>" />
  <tr>
    <td>Filter</td>
    <td><label for="type"></label>
      <select name="type" id="type">
		<option value="">Todos</option>
        <option value="l" <?php if($type=="l"){echo "selected";} ?>>Disponível</option>
        <option value="r" <?php if($type=="r"){echo "selected";} ?>>Rendimentos</option>
        <option value="c" <?php if($type=="c"){echo "selected";} ?>>Comissões</option>
    </select></td>
    <td><input type="submit" name="Submit" d="Submit" value="Enviar" /></td>
  </tr>
</form>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
	<?php
        include("pagination.php"); // Chama o arquivo que monta a paginação. ex: << anterior 1 2 3 4 5 próximo >>
    ?>
    </td>
  </tr>
</table>

<table width="98%" border="0" cellspacing="0" cellpadding="0">
  <tr style="border:1px solid #CCC">
	<td bgcolor="#EEEEEE">ID</td>
    <td bgcolor="#EEEEEE">ID Compra</td>
    <td bgcolor="#EEEEEE">Descrição</td>
    <td bgcolor="#EEEEEE">Conta</td>
    <td bgcolor="#EEEEEE">Saldo Anterior</td>
    <td bgcolor="#EEEEEE">Saldo Atual</td>
    <td bgcolor="#EEEEEE">Tipo</td>
    <td bgcolor="#EEEEEE">Valor</td>
    <td bgcolor="#EEEEEE">Data
<a class="ordenar <?php if ($_GET['order'] == "dataasc") { echo 'marcado'; } ?>" href="?p=user_statement&pg=0&search=<?php echo $_GET['search']; ?>&type=<?php echo $_GET['type']; ?>&order=dataasc"><b>/\</b></a>
                	<a class="ordenar <?php if ($_GET['order'] == "datadesc" || empty($_GET['order'])) { echo 'marcado'; } ?>" href="?p=user_statement&pg=0&search=<?php echo $_GET['search']; ?>&type=<?php echo $_GET['type']; ?>&order=datadesc"><b>\/</b></a>
    </td>
  </tr>
<?php while($reg = mysqli_fetch_array($sql)){ ?>
  <tr style="border:1px solid #CCC">
    <td><?=$reg["extf_id"]; ?></td>
    <td><?=$reg["extf_idCompra"]; ?></td>
    <td><?=$reg["extf_descricao"]; ?></td>
    <td>
	<?php 
	switch ($reg["extf_conta"])
	{
		case 'l':
			$account = "Disponível";
			break;
		case 'r':
			$account= "Rendimento";
			break;
		case 'c':
			$account = "Comissões";
			break;
		default:
			$account = "";
			break;
	}
	
	echo $account;
	
	?>
    </td>
    <td>$ <?=number_format($reg["extf_saldo_anterior"], 2, '.', ',') ; ?></td>
    <td>$ <?=number_format($reg["extf_saldo_atual"], 2, '.', ',') ; ?></td>
    <td><?php if($reg["extf_tipo"]=='c'){ echo "Credito"; }else{ echo "Debito"; } ?></td>
    <td>$ <?=number_format($reg["extf_valor"], 2, '.', ',') ; ?></td>
    <td><?=$reg["extf_data"]; ?></td>
  </tr>
<?php } ?>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>
	<?php
        include("pagination.php"); // Chama o arquivo que monta a paginação. ex: << anterior 1 2 3 4 5 próximo >>
    ?>
    </td>
  </tr>
</table>		
</div>
        </div>
      </div>
      <div class="clr"></div>
    </div>