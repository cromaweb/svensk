<link rel="stylesheet" href="http://code.jquery.com/ui/1.9.0/themes/base/jquery-ui.css" />
<script src="http://code.jquery.com/jquery-1.8.2.js"></script>
<script src="http://code.jquery.com/ui/1.9.0/jquery-ui.js"></script>
<?php

$search = $_GET["search"];
$type	= $_GET["type"];
$status	= $_GET["status"];
$pg	= $_GET["pg"];
$data_inicial = $_GET["data_inicial"];
$data_final = $_GET["data_final"];
$src	= $_GET["src"];
$act	= $_GET["act"];
$mes = $_GET["mes"];

//######### INICIO Paginação
$numreg = 100; // Quantos registros por página vai ser mostrado
if (!isset($pg) || empty($pg)) {
	$pg = 0;
}
$inicial = $pg * $numreg;

// Faz o Select pegando o registro inicial até a quantidade de registros para página
$sql = "SELECT usr_id,
				usr_login_id,
				usr_name,
				bcons_valor_rendimento,
				MONTH(bcons_data)as mes,
				YEAR(bcons_data)as ano
		FROM tb_user a 
		INNER JOIN tb_bns_consolidado b
		ON a.usr_id=b.bcons_idUsuario 
		AND bcons_valor_rendimento>0 ";
		
		if(!empty($mes)){
			$sql .= "AND MONTH(bcons_data) = $mes";
		}

$sql = geraSQL($sql);

?>

  <div class="content">
    <div class="content_resize">
      <div class="mainbar">
        <div class="article">
          <h2><span>Relatório Rendimentos</span></h2>
		  <div class="clr"></div>
          <p><a href="?p=profits&mes=<?=$mes?>">Voltar</a></p>
			<table width="98%" border="0" cellspacing="0" cellpadding="0">
				<form name="formSearch" action="" method="get" >
				<input type="hidden" name="p" value="profits_rendimento" />
				<input type="hidden" name="src" value="1" />
				<tr>
					<td width="12%">Mês</td>
					<td></td>
				</tr>
				<tr>
					<td>
						<select name="mes" id="mes">
							<option value="">-- Mês --</option>
							<option value="8" <?php if($mes=="8"){ echo "selected";} ?>>Agosto/2020</option>
							<option value="7" <?php if($mes=="7"){ echo "selected";} ?>>Julho/2020</option>
							<option value="6" <?php if($mes=="6"){ echo "selected";} ?>>Junho/2020</option>
						</select>
					</td>
					<td><input type="submit" name="Submit" d="Submit" value="Enviar" /></td>
				</tr>
				</form>
			</table>
			<table width="70%" border="0" cellspacing="0" cellpadding="0" style="margin-top:10px;">
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
			</table>
			<table width="98%" border="0" cellspacing="0" cellpadding="0">
			<tr style="border:1px solid #CCC">
				<td bgcolor="#EEEEEE">Mês</td>
				<td bgcolor="#EEEEEE">Agente ID</td>
				<td bgcolor="#EEEEEE">Agente Name</td>
				<td bgcolor="#EEEEEE">Valor comissão</td>

			</tr>
			<?php while($reg = mysqli_fetch_array($sql)){ ?>
			<tr style="border:1px solid #CCC">
				<td><?=$reg["mes"]; ?>/<?=$reg["ano"]; ?></td>
				<td><?=$reg["usr_login_id"]; ?></td>
				<td><?=$reg["usr_name"]; ?></td>
				<td>$ <?=number_format($reg["bcons_valor_rendimento"], 2, '.', ',') ; ?></td>

			</tr>
			<?php } ?>
			</table>
            <a class="fancybox" href="#inline1" style="display:none" ></a>
            <div id="inline1" style="width:600px;display: none;">
            </div>
			<p><?php include("pagination.php"); // Chama o arquivo que monta a paginação. ex: << anterior 1 2 3 4 5 próximo >> ?></p>
        </div>
      </div>
      <div class="clr"></div>
    </div>
  </div>