<?php

  $id = $_GET["id"];
  if (empty($id)) {
    print " <script type=\"text/javascript\">
              window.location='?p=news';
            </script>";
    exit;
  }
  $rowCadastro  = abreSQL("select * from tb_news where news_id = " . $id);
  if (empty($rowCadastro)) {
    print " <script type=\"text/javascript\">
              window.location='?p=news';
            </script>";
    exit;
  }
  $title = $rowCadastro['news_title'];
  $text = $rowCadastro['news_text'];
  $category = $rowCadastro['news_category'];
  $status = $rowCadastro['news_status'];

  $sqlCategory = geraSQL("SELECT ncat_id, ncat_description
                          FROM tb_news_category 
                          ORDER BY ncat_description ASC");
  if($_POST){
    $title = addslashes($_POST["title"]);
    $text = $_REQUEST["text"];
    $category = addslashes($_POST["category"]);
    $status = addslashes($_POST["status"]);
    $erro = 0;

    if(empty($title)){
      $mensagem_erro .= "Title cannot be empty.<br>";
      $erro++;
    }

    if(empty($text)){
      $mensagem_erro .= "Text cannot be empty.<br>";
      $erro++;
    }

    if(empty($category)){
      $mensagem_erro .= "Category cannot be empty.<br>";
      $erro++;
    }

    if(empty($status)){
      $mensagem_erro .= "Status cannot be empty.<br>";
      $erro++;
    } elseif($status != 'A' && $status != 'I'){
      $mensagem_erro .= "Status should be either active or inactive.<br>";
      $erro++;
    }

    

    if($erro==0)
    {     
        executaSQL("UPDATE tb_news SET news_title = '".$title."', news_text = '".$text."', news_category = '".$category."', news_status = '".$status."'");
        print "
        <script type=\"text/javascript\">
        alert(\"News updated.\");
        window.location='?p=news';
        </script>";
    } else {
     $msgerro = '<table width="500" border="0" align="center" cellpadding="1" cellspacing="1" bgcolor="#999999">
      <tr>
        <td align="center">
          <table width="500" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#EEEEEE">
          <tr>
          <td class="stylexx" style="padding-left:10px; padding-right:10px; padding-top:10px;"><b>Found the following errors:</b><br>'.$mensagem_erro.'</td>
          </tr>
          </table>
        </td>
      </tr>
      </table>';
    }
    
  }

?>
<div class="content">
    <div class="content_resize">
      <div class="mainbar">
        <div class="article">
          <h2><span>New News</span></h2>
          <div class="clr"></div>
          <p><a href="?p=news">Show all news</a></p>
          <?php 
            if(!empty($msgerro))
            {
              echo $msgerro;  
            }
          ?>
          <div class="clr"></div>
          <table width="100%" border="0" cellspacing="1" cellpadding="1">
            <form name="form" id="form" action="" method="post">
              <tr>
                <td align="right">Title:</td>
                <td><input name="title" type="text" id="title" value="<?=$title; ?>" size="40" /></td>
              </tr>
              <tr>
                <td align="right">Text:</td>
                <td><textarea name="text" id="text"><?=$text; ?></textarea></td>
              </tr>
              <tr>
                <td align="right">Category:</td>
                <td>
                  <select name="category" id="category">
                    <option value="">Select</option>
                    <?php while($reg = mysqli_fetch_array($sqlCategory)){ ?>
                    <option value="<?php echo $reg['ncat_id']; ?>" <?php if ($category == $reg['ncat_id']) { echo 'selected'; }?>><?php echo $reg['ncat_description']; ?></option>
                    <?php } ?>
                  </select>
                </td>
              </tr>
              <tr>
                <td align="right">Status:</td>
                <td>
                  <select name="status" id="status">
                    <option value="">Select</option>
                    <option value="A" <?php if ($status == "A") { echo 'selected'; }?>>Active</option>
                    <option value="I" <?php if ($status == "I") { echo 'selected'; }?>>Inactive</option>
                  </select>
                </td>
              </tr>
              <tr>
                <td align="right">&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td align="right">&nbsp;</td>
                <td><input type="submit" name="Submit" id="Submit" value="Submit" /></td>
              </tr>
            </form>
          </table>

          <p>&nbsp;</p>
        </div>
      </div>
      <div class="clr"></div>
    </div>
  </div>

  <script src="jquery/ckeditor/ckeditor.js"></script>
  <script>
      // Replace the <textarea id="editor1"> with a CKEditor
      // instance, using default configuration.
      CKEDITOR.replace( 'text' );
  </script>