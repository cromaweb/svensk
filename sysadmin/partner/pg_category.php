<?php
//######### INICIO Paginação
  $numreg = 100; // Quantos registros por página vai ser mostrado
  $pg = $_GET['pg'];
  if (!isset($pg)) {
    $pg = 0;
  }
  $inicial = $pg * $numreg;

$sql = geraSQL("SELECT ncat_id, ncat_description
              FROM tb_news_category 
              ORDER BY ncat_id DESC 
              LIMIT $inicial, $numreg");
?>
<div class="content">
    <div class="content_resize">
      <div class="mainbar">
        <div class="article">
          <h2><span>News Categories</span></h2>
          <div class="clr"></div>
          <p><a href="?p=category_new">Add new category</a></p>
            <p>
            <?php
                include("pagination.php"); // Chama o arquivo que monta a paginação. ex: << anterior 1 2 3 4 5 próximo >>
            ?>
            </p>
            <table width="96%" border="0" cellspacing="0" cellpadding="0">
              <tr style="border:1px solid #CCC">
                <td bgcolor="#EEEEEE" width="6%">ID</td>
                <td bgcolor="#EEEEEE" width="80%">Description</td>
                <td bgcolor="#EEEEEE" width="14%">Action</td>
              </tr>
              <?php while($reg = mysqli_fetch_array($sql)){ 
        			?>
              <tr style="border:1px solid #CCC">
                <td><?=$reg["ncat_id"]; ?></td>
                <td><?=$reg["ncat_description"]; ?></td>
                <td><input name="edit" value="Editar" type="button" onclick="location.href='?p=category_edit&id=<?=$reg["ncat_id"]; ?>'" /></td>
              </tr>
            <?php } ?>
            </table>
            
			<p><?php include("pagination.php"); // Chama o arquivo que monta a paginação. ex: << anterior 1 2 3 4 5 próximo >> ?></p>
        </div>
      </div>
      <div class="clr"></div>
    </div>
  </div>