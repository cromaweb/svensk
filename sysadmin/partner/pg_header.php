<?php date_default_timezone_set('America/Sao_Paulo'); ?>
<?php 
  // if(array_search('invoice', array_column($usrPages, 'upag_name'))){
  //   echo "Ok";
  // }else{
  //   echo "Falso";
  // }
  //var_dump($found_key);
  //echo $usrPages[$found_key]['upag_name']." | View: ".$usrPages[$found_key]['uac_view']." | Edit: ".$usrPages[$found_key]['uac_edit'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Administrator</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<link href="style.css" rel="stylesheet" type="text/css" />

<!-- Add jQuery library -->
<script type="text/javascript" src="../fancybox/lib/jquery-1.10.1.min.js"></script>
<script> $.getScript("./js/_funcoes.js", function(){}); </script>
</head>
<body>
<div class="main">
  <div class="header">
    <div class="header_resize">
      <div class="logo">
        <h1><a href="#"><img src="/public/assets/img/logo2.png" width="175"  /></a></h1>
      </div>
      <div style="width:650px; float:right; color:#FFF;">
		<p style="font-weight:bold; font-size:13px">Olá, <?=$usrName ?> (<?=$usrLogin ?>)<br />
        <a href="?p=password">Alterar Senha</a> | <a href="logout.php" style="color:#F00">Logout</a>
        <br><?=date("d/m/Y"); ?> | Horário: <?=date("H:i:s"); ?>
        </p>
      </div>
      <div class="menu_nav">
        <ul>
          <li><a href="./"><span><span>Home</span></span></a></li>
          <li><a href="?p=user"><span><span>Usuários</span></span></a></li>
          <?php if($usrType==1 || (array_search('invoice', array_column($usrPages, 'upag_name')))){?>
          <li><a href="?p=invoice"><span><span>Faturas</span></span></a></li>
          <?php } ?>
          <?php if($usrType==1 || (array_search('deposit', array_column($usrPages, 'upag_name')))){?>
          <li><a href="?p=deposit"><span><span>Depósitos</span></span></a></li>
          <?php } ?>
          <?php if($usrType==1 || (array_search('plans', array_column($usrPages, 'upag_name')))){?>
          <li><a href="?p=plans"><span><span>Contratos</span></span></a></li>
          <?php } ?>
          <?php if($usrType==1 || (array_search('withdrawal', array_column($usrPages, 'upag_name')))){?>
          <li><a href="?p=withdrawal"><span><span>Saques</span></span></a></li>
          <?php } ?>
          <?php if($usrType==1 || (array_search('documentation', array_column($usrPages, 'upag_name')))){?>
          <li><a href="?p=documentation"><span><span>Documentação</span></span></a></li>
          <?php } ?>
          <?php if($usrType==1 || (array_search('reports', array_column($usrPages, 'upag_name')))){?>
          <li><a href="?p=reports"><span><span>Relatórios</span></span></a></li>
          <?php } ?>
          
        </ul>
      </div>
      <div class="clr"></div>
    </div>
  </div>