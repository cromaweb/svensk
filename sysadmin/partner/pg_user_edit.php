<script language="Javascript">
function complete() {
	if (document.formSearch.search.value.length < 1) {
		alert("Insira algum valor no campo.");
		formSearch.search.focus();
		return false;
	}
	return true;
}
        function c(id){

                var palavras=document.getElementById(id).value;

                palavras=palavras.split("");

                var tmp="";

                for(i=0;i<palavras.length;i++){

                        if(palavras[i-1]){

                                if(palavras[i-1]==" "){palavras[i]=palavras[i].replace(palavras[i],palavras[i].toUpperCase());}

                                else{palavras[i]=palavras[i].replace(palavras[i],palavras[i].toLowerCase());}

                        }

                        else{palavras[i]=palavras[i].replace(palavras[i],palavras[i].toUpperCase());}

                        tmp+=palavras[i];

                }

                document.getElementById(id).value=tmp;

        }
</script>
<?php
	
	########################
	$val = $_POST["val"];
	$userID = $_POST["userID"];
	$userSponsorCurrent = $_POST["userSponsorCurrent"];
	$userCurrent = $_POST["userCurrent"];
	$emailCurrent = $_POST["emailCurrent"];
	
	if($val==1){
		$usrName = addslashes($_POST["name"]);
		$usrUsername = addslashes($_POST["username"]);
		$usrEmail = addslashes($_POST["email"]);
		$usrSponsor = addslashes($_POST["sponsor"]);
		$leg = addslashes($_POST["leg_preference"]);
		$usrTitulo = addslashes($_POST["titulo"]);
		$usrRG = addslashes($_POST["rg"]);
		$usrCPF = addslashes($_POST["cpf"]);
		$usrCNPJ = addslashes($_POST["cnpj"]);
		$usrPhone = addslashes($_POST["phone"]);
		$usrDateOfBirth = addslashes($_POST["birth"]);
		$usrAddress = addslashes($_POST["address"]);
		$usrAddress2 = addslashes($_POST["address2"]);
		$usrNumber = addslashes($_POST["number"]);
		$usrCity = addslashes($_POST["city"]);
		$usrState = addslashes($_POST["state"]);
		$usrZipCode = addslashes($_POST["zip_code"]);
		$usrRede = addslashes($_POST["rede"]);
		$usrCompanyName = addslashes($_POST["company_name"]);
		$usrOpeningDate = addslashes($_POST["opening_date"]);
		$password = addslashes($_POST["password"]);
		
		$erro = 0;

		if(!preg_match('/@/i', $usrEmail) ){
			$mensagem_erro .= "E-mail incorreto!<br><br>";
			$erro++; 
		}
		
		/*if(empty($usrName)){
			$mensagem_erro .= "Empty name!<br><br>";
			$erro++;
		}*/
		
		if(empty($usrUsername)){
			$mensagem_erro .= "Campo username vazio!<br><br>";
			$erro++;
		}
		
		if($userCurrent!=$usrUsername){
			list($usrLogin) = abreSQL("SELECT usr_login_id FROM tb_user WHERE usr_login_id='$usrUsername'");
			list($usrLoginBlocked) = abreSQL("SELECT login FROM tb_user_blocked WHERE login='$usrUsername'");
			list($usrNewLogin) = abreSQL("SELECT new_login FROM tb_user_login_change WHERE new_login='$usrUsername' AND confirmed='N'");
			if(!empty($usrLogin) || !empty($usrLoginBlocked) || !empty($usrNewLogin)){
				$mensagem_erro .= "Username já existe.<br><br>";
				$erro++;
			}
		}
		
		if($emailCurrent!=$usrEmail){
			list($usrEmailOld) = abreSQL("SELECT usr_email FROM tb_user WHERE usr_email='$usrEmail'");
			list($usrEmailNew) = abreSQL("SELECT new_email FROM tb_user_email_change WHERE new_email='$usrEmail' AND (confirmed_old='N' OR confirmed_new='N')");
			if(!empty($usrEmailOld) || !empty($usrEmailNew)){
				$mensagem_erro .= "E-mail já existe.<br><br>";
				$erro++;
			}
		}

		if(empty($usrDateOfBirth) || $usrDateOfBirth=="0000-00-00"){
			$usrDateOfBirth = NULL;
		}
		
		if(empty($usrOpeningDate) || $usrOpeningDate=="0000-00-00"){
			$usrOpeningDate = NULL;
		}

		#---------- Caso não tenha nenhum erro nos dados enviados ---------------------------
	
		if($erro==0)
		{
			$usrName = ucwords(strtolower($usrName));

			$sqlUsr = "UPDATE tb_user SET usr_name = '$usrName',
			usr_login_id = '$usrUsername',
			usr_invited_id = '$usrSponsor',
			usr_email = '$usrEmail',
			usr_titulo = '$usrTitulo',
			usr_rg = '$usrRG',
			usr_cnpj = '$usrCNPJ',
			usr_cpf = '$usrCPF',
			usr_phone = '$usrPhone',
			usr_birth = '$usrDateOfBirth',
			usr_address = '$usrAddress',
			usr_address2 = '$usrAddress2',
			usr_number = '$usrNumber',
			usr_city = '$usrCity',
			usr_state = '$usrState',
			usr_zip_code = '$usrZipCode',
			usr_company_name = '$usrCompanyName',
			usr_opening_date = '$usrOpeningDate'
			WHERE usr_id = '$userID'";
			
			executaSQL($sqlUsr);
			
			if(!empty($password)){

				//Altera a senha do usuário
				executaSQL("UPDATE tb_user SET usr_password = '".sha1("x16$password")."' WHERE usr_id = '$userID'");

				//Insere logo de modificação
				executaSQL("INSERT tb_log_altera_dados (lad_descricao,lad_idUsuario,lad_ip_acesso,lad_data) VALUES('Update Password: Admin: $usrID','$userID','$remote_addr',NOW())");
				
			}

			if($userSponsorCurrent!=$usrSponsor)
			{
				//Pega o IP do usuário
				$remote_addr = $_SERVER['REMOTE_ADDR'];
				//Insere logo de modificação do patrocinador
				executaSQL("INSERT tb_log_altera_dados (lad_descricao,lad_idUsuario,lad_ip_acesso,lad_data) VALUES('Update Sponsor Previous: $userSponsorCurrent | New: $usrSponsor | Admin: $usrID','$userID','$remote_addr',NOW())");
				//Modifica o patrocinador na tb_organizacao
				executaSQL("UPDATE tb_organizacao SET org_patrocinador='$usrSponsor'
												WHERE org_idUsuario = '$userID'");
			}

			list($rowRede,$typeRede,$partnerRede,$countryRede) = abreSQL("SELECT usr_rede,usr_type,usr_partner,usr_country FROM tb_user WHERE usr_id='$userID'");

			if($countryRede=="BR") // Inicio BR
			{

				//Se for setado para fazer rede
				if($usrRede==1 || $usrRede==2){
					//Pega a informação para verificar se está habilitado para fazer rede
					
					//Se não tiver habilitado para a rede
					if($rowRede==0 && $typeRede==0){
						//Habilita para rede e altera para primeiro login para refazer o cadastro e validar os documentos de CNPJ
						executaSQL("UPDATE tb_user SET usr_rede = 1,usr_type = 1,usr_primeiro_login = 'S',usr_documentation = 0 WHERE usr_id='$userID'");
						//Se for Parceiro
						if($usrRede==2){
							executaSQL("UPDATE tb_user SET usr_partner = 1 WHERE usr_id='$userID'");
						}

					}elseif($rowRede==0 && $typeRede==1){
						//Habilita para rede 
						executaSQL("UPDATE tb_user SET usr_rede = 1 WHERE usr_id='$userID'");
						//Se for Parceiro
						if($usrRede==2){
							executaSQL("UPDATE tb_user SET usr_partner = 1 WHERE usr_id='$userID'");
						}

					}elseif($rowRede==1 && $typeRede==1 && $usrRede==2){

						executaSQL("UPDATE tb_user SET usr_partner = 1 WHERE usr_id='$userID'");

					}elseif($rowRede==1 && $partnerRede==1 && $usrRede==1){
						//Habilita para rede e altera para primeiro login para refazer o cadastro e validar os documentos de CNPJ
						executaSQL("UPDATE tb_user SET usr_partner = 0 WHERE usr_id='$userID'");
					}
				}

				//Se for setado para não fazer rede
				if($usrRede==0){
					//Pega a informação para verificar se está habilitado para fazer rede
					list($rowRede) = abreSQL("SELECT usr_rede FROM tb_user WHERE usr_id='$userID'");
					//Se tiver habilitado para a rede
					if($rowRede==1){
						//Habilita para rede e altera para primeiro login para refazer o cadastro e validar os documentos de CNPJ
						executaSQL("UPDATE tb_user SET usr_rede = 0, usr_partner = 0 WHERE usr_id='$userID'");
					}
				}

			}// Final BR
			else
			{

				//Se for setado para fazer rede
				if($usrRede==1 || $usrRede==2){
					//Pega a informação para verificar se está habilitado para fazer rede
					
					//Se não tiver habilitado para a rede
					if($rowRede==0){
						//Habilita para rede e altera para primeiro login para refazer o cadastro e validar os documentos de CNPJ
						executaSQL("UPDATE tb_user SET usr_rede = 1 WHERE usr_id='$userID'");
						//Se for Parceiro
						if($usrRede==2){
							executaSQL("UPDATE tb_user SET usr_partner = 1 WHERE usr_id='$userID'");
						}

					}elseif($rowRede==1 && $partnerRede==0 && $usrRede==2){

						executaSQL("UPDATE tb_user SET usr_partner = 1 WHERE usr_id='$userID'");

					}elseif($rowRede==1 && $partnerRede==1 && $usrRede==1){
						//Habilita para rede e altera para primeiro login para refazer o cadastro e validar os documentos de CNPJ
						executaSQL("UPDATE tb_user SET usr_partner = 0 WHERE usr_id='$userID'");
					}
				}

				//Se for setado para não fazer rede
				if($usrRede==0){
					//Pega a informação para verificar se está habilitado para fazer rede
					list($rowRede) = abreSQL("SELECT usr_rede FROM tb_user WHERE usr_id='$userID'");
					//Se não tiver habilitado para a rede
					if($rowRede==1){
						//Habilita para rede e altera para primeiro login para refazer o cadastro e validar os documentos de CNPJ
						executaSQL("UPDATE tb_user SET usr_rede = 0, usr_type = 0,usr_partner = 0 WHERE usr_id='$userID'");
					}

				}
			}

			//Insere logo de modificação
			executaSQL("INSERT tb_log_altera_dados (lad_descricao,lad_idUsuario,lad_ip_acesso,lad_data) VALUES('Update User: Admin: $usrID | Rede/Partner: $usrRede | Query: ".addslashes($sqlUsr)."','$userID','$remote_addr',NOW())");

			print "
			<script type=\"text/javascript\">
			alert(\"Dados atualizados!\");
			</script>";

		}else
		{
			$msgerro = '<table width="500" border="0" align="center" cellpadding="1" cellspacing="1" bgcolor="#999999">
					<tr>
						<td align="center">
							<table width="500" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#EEEEEE">
							<tr>
							<td class="stylexx" style="padding-left:10px; padding-right:10px; padding-top:10px;"><b>Found the following errors:</b><br><br>'.$mensagem_erro.'</td>
							</tr>
							</table>
						</td>
					</tr>
					</table>';
		}
		
	}
	
	if($val==2){

		executaSQL("UPDATE tb_user SET usr_password = '".sha1("x16@ether2020")."' WHERE usr_id = '$userID'");

		//Insere logo de modificação
		executaSQL("INSERT tb_log_altera_dados (lad_descricao,lad_idUsuario,lad_ip_acesso,lad_data) VALUES('Reset Password: Admin: $usrID','$userID','$remote_addr',NOW())");

		print "
		<script type=\"text/javascript\">
		alert(\"Senha resetada!\");
		</script>";

	}
	
	if($val==3){

		$qry = "UPDATE tb_user SET usr_ewallet_active = '".$_POST['ewallet_active']."',usr_active = '".$_POST['status']."',usr_transfer = '".$_POST['transfer']."',usr_withdrawal = '".$_POST['withdrawal']."' WHERE usr_id = '$userID'";
		executaSQL($qry);

		//Insere logo de modificação
		executaSQL("INSERT tb_log_altera_dados (lad_descricao,lad_idUsuario,lad_ip_acesso,lad_data) VALUES('User Edit Labels: Admin: $usrID | Query: ".addslashes($qry)."','$userID','$remote_addr',NOW())");
		
		print "
		<script type=\"text/javascript\">
		alert(\"Updated!\");
		</script>";
	}

	if($val==4){

		executaSQL("UPDATE tb_user SET usr_google_auth_code = NULL WHERE usr_id = '$userID'");

		//Insere logo de modificação
		executaSQL("INSERT tb_log_altera_dados (lad_descricao,lad_idUsuario,lad_ip_acesso,lad_data) VALUES('Desable 2FA: Admin: $usrID ','$userID','$remote_addr',NOW())");
		
		print "
		<script type=\"text/javascript\">
		alert(\"2FA desabilitado!\");
		</script>";
	}
	
	####################
	
	$user = $_GET["id"];

	$rowCadastro 	= abreSQL("select * from tb_user where usr_id = '$user'");

	$name 				= $rowCadastro["usr_name"];
	$email 				= $rowCadastro["usr_email"];
	$login 				= $rowCadastro["usr_login_id"];
	$titulo				= $rowCadastro["usr_titulo"];
	$phone 				= $rowCadastro["usr_phone"];
	$rg 				= $rowCadastro["usr_rg"];
	$cpf 				= $rowCadastro["usr_cpf"];
	$cnpj 				= $rowCadastro["usr_cnpj"];
	$birth 				= $rowCadastro["usr_birth"];
	$address			= $rowCadastro["usr_address"];
	$address2			= $rowCadastro["usr_address2"];
	$number				= $rowCadastro["usr_number"];
	$city				= $rowCadastro["usr_city"];
	$state				= $rowCadastro["usr_state"];
	$zip_code			= $rowCadastro["usr_zip_code"];
	$idsponsor			= $rowCadastro["usr_invited_id"];
	$status				= $rowCadastro["usr_active"];
	$ewallet_active 	= $rowCadastro["usr_ewallet_active"];
	$transfer			= $rowCadastro["usr_transfer"];
	$withdrawal			= $rowCadastro["usr_withdrawal"];
	$rede				= $rowCadastro["usr_rede"];
	$fa2				= $rowCadastro["usr_google_auth_code"];
	$partner				= $rowCadastro["usr_partner"];
	$company_name		= $rowCadastro["usr_company_name"];
	$opening_date		= $rowCadastro["usr_opening_date"];
	$type				= $rowCadastro["usr_type"];
	
	
	list($sponsor) 	= abreSQL("select usr_login_id from tb_user where usr_id = '$idsponsor'");
	$sql_sponsor 	= geraSQL("SELECT usr_id,usr_login_id,contp_idAntecessor 
							  FROM tb_user INNER JOIN tb_contperna_aws 
							  WHERE usr_id=contp_idAntecessor
							  AND contp_idUsuario = '$user'");

?>
<script src="jquery/jquery.js" type="text/javascript"></script>
<script src="jquery/jquery.maskedinput.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){
	$("#birth").mask("9999-99-99");
	$("#opening_date").mask("9999-99-99");
	$('#country').val('<?=$countryx ?>');
});
</script>
  <div class="content">
    <div class="content_resize">
      <div class="mainbar">
        <div class="article">
          <h2><span>Editar Usuário</span></h2>
          <div class="clr"></div>
          <p><a href="?p=user">Voltar</a></p>
            <table width="98%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="center" >
                  Resetar senha (@ether2020)                
				</td>
                <td align="center" >
                  Desativar 2FA                
                </td>
                <td rowspan="3" align="center"><table width="100%" border="1" cellspacing="1" cellpadding="1" style="border-style: solid;border-color:#CCC;">
                  <form id="form" name="form" method="post" action="">
                    <input type="hidden" name="val" value="3" />
                    <input type="hidden" name="userID" value="<?=$user ?>" />
                    <tr>
                      <td align="center"> Status </td>
                      <td align="center"> Wallet</td>
                      <td align="center"> Transfer</td>
                      <td align="center">Withdrawal</td>
                    </tr>
                    <tr>
                      <td align="center"><select name="status">
                        <option value="y" <?php echo ($status == "y") ? "selected" : ""; ?>>Confirmado</option>
                        <option value="n" <?php echo ($status == "n") ? "selected" : ""; ?>>Não confirmado</option>
                        <option value="d" <?php echo ($status == "d") ? "selected" : ""; ?>>Bloqueado</option>
                      </select></td>
                      <td align="center"><select name="ewallet_active">
                        <option value="s" <?php echo ($ewallet_active == "s") ? "selected" : ""; ?>>Ativo</option>
                        <option value="n" <?php echo ($ewallet_active == "n") ? "selected" : ""; ?>>Bloqueado</option>
                      </select></td>
                      <td align="center"><select name="transfer">
                        <option value="s" <?php echo ($transfer == "s") ? "selected" : ""; ?>>Ativo</option>
                        <option value="n" <?php echo ($transfer == "n") ? "selected" : ""; ?>>Bloqueado</option>
                      </select></td>
                      <td align="center"><select name="withdrawal">
                        <option value="s" <?php echo ($withdrawal == "s") ? "selected" : ""; ?>>Ativo</option>
                        <option value="n" <?php echo ($withdrawal == "n") ? "selected" : ""; ?>>Bloqueado</option>
                      </select></td>
                    </tr>
                    <tr>
                      <td colspan="4" align="center"><input type="submit" name="button" id="button" value="Salvar" /></td>
                    </tr>
                  </form>
                </table></td>
              </tr>
              <tr>
                <td align="center">
	                <form id="form1" name="form1" method="post" action="">
		                <input type="hidden" name="val" value="2" />
		                <input type="hidden" name="userID" value="<?=$user ?>" />
		                <input type="submit" name="reset_password" id="reset_password" value="Resetar" />
	                </form>
                </td>                
                <td align="center">
					<?php if(!empty($fa2)){ ?>
	                <form id="form1" name="form1" method="post" action="">
		                <input type="hidden" name="val" value="4" />
		                <input type="hidden" name="userID" value="<?=$user ?>" />
		                <input type="submit" name="reset_2fa" id="reset_2fa" value="Desativar" />
					</form>
					<?php }else{ ?>
						<strong>Inativo</strong>
					<?php } ?>
				</td>
              </tr>
              <tr>
                <td align="center">&nbsp;</td>
              </tr>
            </table>
<p>
          <?php 
			  if(!empty($msgerro))
			  {
				echo $msgerro;  
			  }
			?>
          </p>
          <table width="100%" border="0" cellspacing="1" cellpadding="1">
                <form name="form" id="form" action="" method="post">
                <input type="hidden" name="val" value="1" />
                <input type="hidden" name="userID" value="<?=$user ?>" />
                <input type="hidden" name="userSponsorCurrent" value="<?=$idsponsor ?>" />
                <input type="hidden" name="userCurrent" value="<?=$login ?>" />
                <input type="hidden" name="emailCurrent" value="<?=$email ?>" />
			  <tr>
              	<td align="right">Tipo:</td>
                <td>
                
                <select name="rede" id="rede">
                	<option value="0" <?php if ($rede == "0"){ echo "selected"; } ?>>Cliente</option>
                    <option value="1" <?php if ($rede == "1"){ echo "selected"; } ?>>Agente</option>
					<option value="2" <?php if ($partner == "1"){ echo "selected"; } ?>>Parceiro</option>
                </select>
                </td>
			  </tr>
			  <tr>
              	<td>&nbsp;</td>
                <td>&nbsp;</td>
			  </tr>
			  <tr>
				<td align="right">Patrocinador:</td>
				<td><?php //$sponsor; ?>
				<select name="sponsor" id="sponsor">
					<?php  
						while($reg = fetch($sql_sponsor)) 
						{ 
					?>
					<option value="<?=$reg["usr_id"]; ?>" <?php if ($reg["usr_id"] == "$idsponsor"){ echo "selected"; } ?>><?=$reg["usr_login_id"]; ?> - [<?=$reg["usr_id"]; ?>]</option>
					<?php } ?>
				</select>
				</td>
			  </tr>
              <tr>
                <td align="right">Nome Completo:</td>
                <td><input name="name" type="text" id="name" value="<?=$name; ?>" size="40" onkeyup="c('name')" /></td>
              </tr>
              <tr>
                <td align="right">Username:</td>
                <td><input name="username" type="text" id="username" value="<?=$login; ?>" size="20" /></td>
              </tr>
              <tr>
                <td align="right">E-mail:</td>
                <td><input name="email" type="text" id="email" value="<?=$email; ?>" size="40" /></td>
              </tr>
              <tr>
                <td align="right">RG:</td>
                <td><input name="rg" type="text" id="rg" value="<?=$rg; ?>" size="20" /></td>
              </tr>
              <tr>
                <td align="right">CPF:</td>
                <td><input name="cpf" type="text" id="cpf" value="<?=$cpf; ?>" size="20" /></td>
			  </tr>
              <tr>
                <td align="right">Telefone:</td>
                <td><input name="phone" type="text" id="phone" value="<?=$phone; ?>" size="15" /></td>
              </tr>
              <tr>
                <td align="right">Data de Nascimento:</td>
                <td><input name="birth" type="text" id="birth" value="<?=$birth; ?>" size="15" /></td>
			  </tr>
			  <?php if($type==1){ ?>
              <tr>
                <td align="right">CNPJ:</td>
                <td><input name="cnpj" type="text" id="cnpj" value="<?=$cnpj; ?>" size="20" /></td>
			  </tr>
              <tr>
                <td align="right">Razão social:</td>
                <td><input name="company_name" type="text" id="company_name" value="<?=$company_name; ?>" size="40" /></td>
			  </tr>
              <tr>
                <td align="right">Data de abertura:</td>
                <td><input name="opening_date" type="text" id="opening_date" value="<?=$opening_date; ?>" size="15" /></td>
			  </tr>
			  <?php } ?>
              <tr>
                <td align="right">Endereco:</td>
                <td><input name="address" type="text" id="address" value="<?=$address; ?>" size="40" /></td>
              </tr>
              <tr>
                <td align="right">Complemento:</td>
                <td>
                  <input name="address2" type="text" id="address2" value="<?=$address2; ?>" size="40" /></td>
              </tr>
              <tr>
                <td align="right">Número:</td>
                <td>
                  <input name="number" type="text" id="number" value="<?=$number; ?>" size="20" maxlength="15" /></td>
              </tr>
              <tr>
                <td align="right">Cidade:</td>
                <td><input name="city" type="text" id="city" value="<?=$city; ?>" size="25" /></td>
              </tr>
              <tr>
                <td align="right">Estado:</td>
                <td><input name="state" type="text" id="state" value="<?=$state; ?>" size="20" /></td>
              </tr>
              <tr>
                <td align="right">CEP:</td>
                <td><input name="zip_code" type="text" id="zip_code" value="<?=$zip_code; ?>" size="20" maxlength="15" /></td>
              </tr>
              <tr>
                <td align="right">Senha:</td>
                <td><input name="password" type="text" id="password" value="" size="15" /></td>
			  </tr>
              <tr>
                <td align="right">&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td align="right">&nbsp;</td>
                <td><input type="submit" name="Submit" id="Submit" value="Submit" /></td>
              </tr>
              </form>
          </table>

          <p>&nbsp;</p>
        </div>
      </div>
      <div class="clr"></div>
    </div>
  </div>