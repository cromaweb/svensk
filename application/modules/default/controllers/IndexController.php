<?php
class Default_IndexController extends SYSTEM_Controllers_Office
{
    public function init() {
        parent::init();
		if($this->view->User == ''){ $this->_redirect(LINK_OFFICE . '/logout'); }
    }
	
    public function indexAction()
	{
		$this->_redirect(LINK_OFFICE . "/home");
	}
	
 
}