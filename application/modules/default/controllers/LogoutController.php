<?php
class Default_LogoutController extends SYSTEM_Controllers_Office
{
    public function init() {
        parent::init();
    }
	
    public function indexAction()
    {  
		Zend_Session::destroy();
		$this->_redirect(LINK_OFFICE . '/login');
    }
}

?>
