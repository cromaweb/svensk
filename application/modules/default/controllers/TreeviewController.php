<?php
class Default_TreeviewController extends SYSTEM_Controllers_Office
{
    public function init() {
        parent::init();
		$this->modelAccount = new Application_Model_Account();
		$this->modelIndex = new Application_Model_Index();
		$this->modelTreeview = new Application_Model_Treeview();
		$this->modelSignup = new Application_Model_Signup();
		$this->view->page_main = 'Downline';
		$this->view->page_link = LINK_OFFICE."/treeview/";
		$this->view->subpages = $this->modelIndex->getSubPages("downline");
    }
	
	//#################################################################################################
    public function indexAction()
    {
		
		$user = $this->view->User;		
		$this->view->user = $user;			
	
	}
	
	//#################################################################################################
	public function searchAction(){
		$this->_helper->layout->disableLayout();	
	}
	
	//#################################################################################################
	public function downlinegAction()
	{
		$max = 4;
		
		if (!empty($_POST['max'])) {
			$max = $_POST['max'];
		}

		$user = $this->view->User;
		$userSession = $this->view->User;
		$children = 0;

		if (!empty($_POST['user']) && $_POST['user'] != $user) {
			$user = $_POST['user'];
			$children = $this->modelTreeview->getCheckLeg($user,$userSession);
		}
		//$user = '100117';
		$dad = $this->modelTreeview->getDadOrg($user);

		if($children==0) {
			$user = $userSession;
			$dad = '';
		}

		$lastIdLeg = $this->modelSignup->getLastIdLeg($user);
		$ultimoIdPernaEsquerda = $lastIdLeg[0];
		$ultimoIdPernaDireita = $lastIdLeg[1];
		$qtdPernaEsquerda = $lastIdLeg[2];
		$qtdPernaDireita = $lastIdLeg[3];

		//builds the tree
		$arr = array();
		$i = 0;
		$users[] = $user;
		$nivel[] = 0;
		
		$username = $this->modelAccount->getUserLogin($user);
		$packageId = $this->modelAccount->getUserPackage($user);
		$packageImage = $this->modelTreeview->getPackageImage($packageId);

		if(empty($packageImage)){
			$packageImage = "account_free.png";
		}

		$arr[] = array(array("v" => $user, "f" => '<img style="border:none;" src="/public/images/account/'.$packageImage.'"><br>'.$username.''),'',$user);
		//[{v:$user, f:'<img style="border:none;" src="/public/images/usr_open.png">'},'', '']
		while ($i < sizeof($users) && $nivel[$i] < $max) {
			$esqUser = $this->modelTreeview->getChildOrg($users[$i],'E');
			$esqUsername = $this->modelAccount->getUserLogin($esqUser);
			$esqPackageId = $this->modelAccount->getUserPackage($esqUser);
			$esqPackageImage = $this->modelTreeview->getPackageImage($esqPackageId);
			if(empty($esqPackageImage)){
				$esqPackageImage = "account_free.png";
			}
			/*$esq = abreSQL("SELECT org_idUsuario FROM tb_organizacao WHERE org_pai = ".$users[$i]." AND org_perna = 'E'");
			$esqUser = $esq["org_idUsuario"];*/
			if (!empty($esqUser)) {
				$arr[] = array(array("v" => $esqUser, "f" => '<a href="javascript:tree(\''.$esqUser.'\');"><img style="border:none;" src="/public/images/account/'.$esqPackageImage.'"><br>'.$esqUsername.'</a>'),$users[$i],$esqUsername);
				$users[] = $esqUser;
				$nivel[] = $nivel[$i] + 1;
			} else {
				$arr[] = array(array("v" => $users[$i].'e', "f" => "<img style=\"border:none;\" src=\"/public/images/account/account_open_position.png\">"),$users[$i],'');
			}

			$dirUser = $this->modelTreeview->getChildOrg($users[$i],'D');
			$dirUsername = $this->modelAccount->getUserLogin($dirUser);
			$dirPackageId = $this->modelAccount->getUserPackage($dirUser);
			$dirPackageImage = $this->modelTreeview->getPackageImage($dirPackageId);
			if(empty($dirPackageImage)){
				$dirPackageImage = "account_free.png";
			}
			/*$dir = abreSQL("SELECT org_idUsuario FROM tb_organizacao WHERE org_pai = ".$users[$i]." AND org_perna = 'D'");
			$dirUser = $dir["org_idUsuario"];*/
			if (!empty($dirUser)) {
				$arr[] = array(array("v" => $dirUser, "f" => '<a href="javascript:tree(\''.$dirUser.'\');"><img style="border:none;" src="/public/images/account/'.$dirPackageImage.'"><br>'.$dirUsername.'</a>'),$users[$i],$dirUsername);
				$users[] = $dirUser;
				$nivel[] = $nivel[$i] + 1;
			} else {
				$arr[] = array(array("v" => $users[$i].'d', "f" => "<img style=\"border:none;\" src=\"/public/images/account/account_open_position.png\">"),$users[$i],'');
			}
			$i++;
		}

		$return['arr'] = $arr;
		$return['dad'] = $dad;
		$return['lastIdLegLeft'] = $ultimoIdPernaEsquerda;
		$return['lastIdLegRight'] = $ultimoIdPernaDireita;

		echo json_encode($return);
		exit;
	}

	//#################################################################################################
	public function downlinemAction()
	{
		@require_once(APPLICATION_PATH."/libs/spirit.php");
		@require_once(APPLICATION_PATH."/libs/cripto.php");


		$user = $this->view->User;

		$arr = array();
		$users[] = $user;
		$i = 0;
		$arr[] = array(array("v" => $user, "f" => '<img style="border:none;" src="/public/images/usr_registered.png">'),'',$user);
		//[{v:$user, f:'<img style="border:none;" src="/public/images/usr_open.png">'},'', '']
		while ($i < sizeof($users)) {
			$esq = abreSQL("SELECT * FROM tb_organizacao WHERE org_pai = ".$users[$i]." AND org_perna = 'E'");
			if (!empty($esq)) {
				$arr[] = array(array("v" => $esq['org_idUsuario'], "f" => '<img style="border:none;" src="/public/images/usr_black.png">'),$users[$i],$esq['org_idUsuario']);
				if ($esq['org_pai'] == $user) {
					$users[] = $esq['org_idUsuario'];
				}
				
			} else {
				$arr[] = array(array("v" => $users[$i].'e', "f" => "<a href=\"javascript:fnConfirmPlacement('".$users[$i]."', '1', 'E', '".$users[$i]."');\"  title=\"Open Position\"><img style=\"border:none;\" src=\"/public/images/usr_open.png\"></a>"),$users[$i],'');
			}

			$dir = abreSQL("SELECT * FROM tb_organizacao WHERE org_pai = ".$users[$i]." AND org_perna = 'D'");
			if (!empty($dir)) {
				$arr[] = array(array("v" => $dir['org_idUsuario'], "f" => '<img style="border:none;" src="/public/images/usr_black.png">'),$users[$i],$dir['org_idUsuario']);
				if ($dir['org_pai'] == $user) {
					$users[] = $dir['org_idUsuario'];
				}
			} else {
				$arr[] = array(array("v" => $users[$i].'d', "f" => "<a href=\"javascript:fnConfirmPlacement('".$users[$i]."', '1', 'D', '".$users[$i]."');\"  title=\"Open Position\"><img style=\"border:none;\" src=\"/public/images/usr_open.png\"></a>"),$users[$i],'');
			}
			$i++;
		}

		echo json_encode($arr);
		exit;
	}
	
	//#################################################################################################
	public function downlineAction(){
		
		require_once(APPLICATION_PATH."/libs/_downline.php");	
		
		exit();
	}
	
	//#################################################################################################
	//User Information Tree View
	public function userdataAction(){
	
		$user = $this->view->User;		
		$codeOfc = $user;
		
		if (!empty($_POST["code"])) {
			$code = $_POST["code"];
		} else {
			$code = $user;
		}

		//Informa��es do usu�rio
		$informationUser = $this->modelAccount->getUserBasic($code);
		
		$information["id"]	= $informationUser["usr_id"];
		$information["username"]	= $informationUser["usr_login_id"];
		$information["name"]	= $informationUser["usr_name"];
		$information["last_name"]	= $informationUser["usr_last_name"];
		$gender	= $informationUser["usr_gender"];
		if(!empty($gender)){
			$information["gender"]	=  ($gender == "m") ? "Male" : "Female";
		}else{
			$information["gender"]	= "";
		}
		$active	= $informationUser["usr_active"];
		$information["active"]	=  ($active == "y") ? "Yes" : "No";
		$information["id_sponsor"]	= $informationUser["usr_invited_id"];
		$information["cell_phone"]	= $informationUser["usr_phone"];
		$information["email"]	= $informationUser["usr_email"];
		$information["country"] = $this->modelIndex->getNameCountry($informationUser["usr_country"]);
		$registered		= $informationUser["usr_reg_date"];
		$registered		= explode(" ", $registered);
		$registered		= explode("-", $registered[0]);
		$information["registered"]	= $registered[2]."/".$registered[1]."/".$registered[0];
		//Pega a Conta [Package] do Usu�rio
		if(empty($informationUser["usr_package"])){
			$information["package"]	= "Free";
		}else{
			$pkg = $this->modelTreeview->getPackageName($informationUser["usr_package"]);
			$information["package"]	= $pkg["prod_titulo"];
		}
		
		if(empty($informationUser["usr_titulo"])){
			$information["title"]	= "Default";
		}else{
			$information["title"]	= $this->modelTreeview->getTitleName($informationUser["usr_titulo"]);
		}
		
		//Informa��es do patrocinador
		$informationSponsor = $this->modelAccount->getUserBasic($informationUser["usr_invited_id"]);
		
		$information["username_sponsor"]= $informationSponsor["usr_login_id"];
		$information["name_sponsor"]= $informationSponsor["usr_name"];
		$information["last_name_sponsor"]= $informationSponsor["usr_last_name"];
		
		foreach($information as $chave => $valor){ //Percorre o array information[]
			if(empty($valor)){ //Verifica se tem algum valor vazio 
				// fazer qualquer coisa no caso de n�o ser vazio
				$information["$chave"] = "&nbsp;"; // Se tiver valor vazio, insere espa�o
			}
		}
		
		$this->_helper->json->sendJson($information);
		//echo $code;
		
		exit();

	}
	
	//#################################################################################################
	//Count Legs
	public function countlegsAction(){
		
		if($_POST)
		{		
			$user = $this->view->User;
			$code = $_POST["code"];
		
			$pernaEsquerda = $this->modelTreeview->getCountLegsLeft($code);
			$pernaDireita = $this->modelTreeview->getCountLegsRight($code);
		
			echo $pernaEsquerda."|$|".$pernaDireita;
			
			exit();
		}
		
	}
	
	//#################################################################################################
	public function ajaxsearchAction(){
	
		$this->_helper->layout->disableLayout();
		
		$user = $this->view->User;	

		$action = $_GET["action"];
		
		//Faz a valida��o se a chamada � atrav�s do a��o 'autocomplete'
		if($action=='autocomplete')
		{
			$value = addslashes(trim(@$_GET["src"]));
			$value = str_replace("%", "", $value);
			
			//Buscar por ID ($user cont�m o ID)
			$reg = $this->modelTreeview->getSearch($value,$user);
			//Se tiver resultado, faz o retorno dos dados
			if (!empty($reg))
			{
				//Retorno em Json
				$json = json_encode($reg[0]);
				echo $json;
			}
			else
			{
				//Buscar por nome ou sobrenome
				$reg = $this->modelTreeview->getSearch(null,$user,$value);
					//Se tiver resultado, faz o retorno dos dados
					if(!empty($reg))
					{
						//Retorno em Json
						$json = json_encode($reg);
						echo $json;
					}
					else
					{
						//Busca por Username/Login
						$reg = $this->modelTreeview->getSearch(null,$user,null,$value);
						//Se tiver resultado, faz o retorno dos dados
						if (!empty($reg))
						{
							//Retorno em Json
							$json = json_encode($reg);
							echo $json;
						}
					}
			}
		}
		exit();
	}	
		
}

?>