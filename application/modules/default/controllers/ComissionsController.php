<?php
class Default_ComissionsController extends SYSTEM_Controllers_Office
{
    public function init() {
        parent::init();
		$this->modelIndex = new Application_Model_Index();
		$this->model = new Application_Model_Bonus();
		$this->modelAccount = new Application_Model_Account();
    }
  
    public function indexAction()
    {
		$user = $this->view->User;	
		$this->view->user = $user;
		
		$this->view->bonus = $this->model->getBonusAmount($user);

		$this->view->contract = $this->modelAccount->getUserContractSign($user);

		$registered = $this->modelAccount->getUserRegistered($user);
		$registered	= explode(" ", $registered);
		$this->view->registered = $registered[0];
		

		$date = $_POST['e_date'];
		
		if (!empty($date)) {
			$search = $date;
		} else {
			$search = 1;
		}
		
		$this->view->search = $search;
		
		if($search == 1)
		{
			$monthYear = date("m-Y");	
		}else{
			$monthYear = $date;
		}

		if (!empty($_POST['e_order']) && $_POST['e_order'] >= 1 && $_POST['e_order'] <= 2) {
			$this->view->order = $_POST['e_order'];
		} else {
			$this->view->order = 1;
		}
		
		if ($this->view->order == 1) {
			$order = 'DESC';
		} else {
			$order = 'ASC';
		}

		//$this->view->extrato = geraSQL("select * from tb_financeiro_extrato  where extf_idUsuario = " . $user . $where . " order by extf_data ". $list);
		$this->view->statement = $this->model->getBonusConsolidated($user,$monthYear,$order);
		

	}
	
    public function extratoAction()
    {
		$user = $this->view->User;	
		$this->view->user = $user;
		
		$this->view->bonus = $this->model->getBonusAmount($user);

		$registered = $this->modelAccount->getUserRegistered($user);
		$registered	= explode(" ", $registered);
		$this->view->registered = $registered[0];
		
		$initialDate = $_POST['e_initialDate'];
		$finalDate = $_POST['e_finalDate'];
		$type = $_POST['type'];
		
		$this->view->type = $type;
		
		if($initialDate>$finalDate){
		    $this->view->error = "Data inválida";
		}
		else
		{

    		$this->view->initialDate = $initialDate;
    		$this->view->finalDate = $finalDate;
    		
    		if (!empty($initialDate)) {
    			$search = $initialDate;
    		} else {
    			$search = "s";
    		}

    		$this->view->search = $search;
    		
    		if(empty($initialDate))
    		{
    			$initialDate = date("d/m/Y");
			}

    		if(empty($finalDate))
    		{
    			$finalDate = date("d/m/Y");
			}

    		//ORDER
    		if (!empty($_POST['e_order']) && $_POST['e_order'] >= 1 && $_POST['e_order'] <= 2) {
    			$this->view->order = $_POST['e_order'];
    		} else {
    			$this->view->order = 1;
    		}
    		
    		if ($this->view->order == 1) {
    			$order = 'DESC';
    		} else {
    			$order = 'ASC';
    		}
    		
    		$this->view->statement = $this->model->getBonus($user,$initialDate,$finalDate,$order,$type);
		}

	}
}

?>