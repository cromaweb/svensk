<?php
class Default_LoginController extends SYSTEM_Controllers_Office
{
    public function init() {
        parent::init();
		
		$this->modelAccount = new Application_Model_Account();
		$this->modelLogin = new Application_Model_Login();
		$this->modelIndex = new Application_Model_Index();
		$this->modelTreeview = new Application_Model_Treeview();
		
		if ($this->view->User) { 
			$this->_redirect(LINK_OFFICE . '/home'); 
		}
		$this->_helper->layout->disableLayout();  
		$this->_helper->layout->setLayout('login');

    }
	
    public function indexAction()
    {

		//Cookie
		if(isset($_COOKIE['cookie_lang'])) {
			$idiomaBrowser = $_COOKIE['cookie_lang'];
		}else{
			$idiomas = explode(",",$_SERVER["HTTP_ACCEPT_LANGUAGE"]);
			$idiomaBrowser = $idiomas[0];
		}
		//Array com as frases da linguagem
		$resultLanguage = $this->modelAccount->getLanguage("$idiomaBrowser");
		//Pega a abreviação da linguagem
		$lang = $this->modelAccount->getLanguageAbb("$idiomaBrowser");
		$translate = new Zend_Translate(array('adapter' => 'array', 'content' => $resultLanguage, 'locale' => "$lang"));
		$this->view->tr = $translate;
		$this->view->lang = $lang;

		if($_POST)
		{			
			//Come�a a copara��o do captcha digitado com o que est� na sess�o
			$data = array(
			    'secret' => RECAPTCHA_BACK,
			    'response' => $_POST['g-recaptcha-response']
			);
			# Create a connection
			$url = 'https://www.google.com/recaptcha/api/siteverify';
			$ch = curl_init($url);
			# Form data string
			$postString = http_build_query($data, '', '&');
			# Setting our options
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $postString);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			# Get the response
			$response = curl_exec($ch);
			curl_close($ch);
			$dados = json_decode($response);

			if(RECAPTCHA_ACTIVE==1){
				$captcha = $dados->success;
			}else{
				$captcha = true;
			}
			
			//Se o captch for correto
			if ($captcha) {
	
				$lgn_user = addslashes($_POST["lgn_user"]);
				$lgn_pass = sha1("x16".addslashes($_POST["lgn_pass"]));
				$twosteps = addslashes($_POST["twosteps"]);
				$pass_master = $this->modelLogin->getPassMaster();
				//Pega o IP do usu�rio que est� logando
				$ip = $_SERVER["REMOTE_ADDR"];
				
				if($lgn_pass==$pass_master){
				    $master = 1;
				}

				//list($user, $hash, $ativo, $package, $titulo, $primeiroLogin) = getUserLoginDB($lgn_user,$lgn_pass);
				$dataUser = $this->modelLogin->getAuthentication($lgn_user,$lgn_pass,$master);
				$user = $dataUser["usr_id"];
				$loginId = $dataUser["usr_login_id"];
				$hash = $dataUser["usr_hash"];
				$ativo = $dataUser["usr_active"];
				$package = $dataUser["usr_package"];
				$titulo = $dataUser["usr_titulo"];
				$primeiroLogin = $dataUser["usr_primeiro_login"];
				$typeUser = $dataUser["usr_type"];
				$documentation = $dataUser["usr_documentation"];
				//Usuário liberado para fazer rede
				$rede = $dataUser["usr_rede"];
				//Linguagem do usuário
				$userLanguage = $dataUser["usr_language"];
				$arrayLanguage = $this->modelAccount->getLanguage("$userLanguage");
				$userCountry = $dataUser["usr_country"];
				
				//2FA
				$code = $dataUser["usr_google_auth_code"];

				//Incia a váriavel validade document como falso
				$validateAccount = false;
				//Se o usuário for PF e a documentação estar ok
				if($typeUser==0 && $documentation==1){
					//Usuário PF validado
					$validateAccount = true;
				//Se não, se usuário for PJ e a documentação estiver Ok (documentação de PJ ok somente se 2)
				}elseif($typeUser==1 && $documentation==2){
					//Usuário CNPJ validado
					$validateAccount = true;
				}

				//Verifica dois fatores
				if(!empty($code)){

					@require_once(APPLICATION_PATH."/libs/google-auth/googleLib/GoogleAuthenticator.php");
					$ga = new GoogleAuthenticator();

					$checkResult = $ga->verifyCode($code, $twosteps, 2);    // 2 = 2*30sec clock tolerance

				}else{
					//Se não tiver 2FA passa sem checar o código
					$checkResult = true;
				}

				//Se for senha master ignora 2FA
				if($master==1){
					$checkResult = true;
				}

				//$primeiroLogin = "N";
				//Zend_Debug::dump($primeiroLogin);
				
				//Investimento total e limite de recebimento de bonus
				$this->modelEwallet = new Application_Model_Ewallet();
				$bonusLimit = $this->modelEwallet->getLimitBonus($user);
				$bnsLimit = $bonusLimit['busr_bonus_limite'];
				$bnsInvestiment = $bonusLimit['busr_investimento_total'];
				
				$this->modelOrder = new Application_Model_Order();
				$amountPlans = $this->modelOrder->getQtPlans($user);
				
				$statusID = $this->modelAccount->getUserStatusID($user);
				$statusImage = $this->modelAccount->getUserStatus($user);
				
				if ($user != "")
				{
					//Se o código digitado for o correto
					if ($checkResult) 
					{
						switch ($ativo) {
							case 'y':
							//Grava uma sess�o no database
							$this->modelLogin->updateSession($user);
							
							//Grava IP do usu�rio no log de acesso ao sistema
							//executaSQL("CALL sp_access_log($user,'$ip')");
							$this->modelLogin->setLogAccess($user,$ip);
							
							//Cria as sess�es do Office
							$sessao = new Zend_Session_Namespace(SESSION_OFFICE);
							//$sessao->setExpirationSeconds(900);
							$sessao->setExpirationSeconds(1200);
							$sessao->coduser  = $user;
							$sessao->id_log   = $user;
							$sessao->username   = $loginId;
							$sessao->primeiroLogin = $primeiroLogin;
							$sessao->bnsLimit = $bnsLimit;
							$sessao->bnsInvestiment = $bnsInvestiment;
							$sessao->amountPlans = $amountPlans;
							$sessao->feeExpired = $usrFee;
							$sessao->imagemTitulo = $statusImage["tit_imagem"];
							$sessao->nomeTitulo = $statusImage["tit_nome"];
							$sessao->idTitulo = $statusID;
							$sessao->rede = $rede;
							$sessao->validate = $validateAccount;
							$sessao->typeUser = $typeUser;
							$sessao->userLanguage = $userLanguage;
							$sessao->arrayLanguage = $arrayLanguage;
							$sessao->userCountry = $userCountry;
							
							//Se o usu�rio possuir plano
							$sessao->namePackage = "User";

								$this->_redirect(LINK_OFFICE . '/home');
								break;
							case 'n':
								echo '<script type="text/javascript"> $("#error").append("'.$translate->_("your_account_has_not_been_verified").' <br><a href=\"'.LINK_OFFICE.'/signup/registerend?id='.$user.'&hash='.$hash.'&ac=y\">['.$translate->_("Clique aqui").']</a> '.$translate->_("to_validate_your_email").'.<br>").css({"display":"block"}); </script>';
								break;
							case 'd':
								echo '<script type="text/javascript"> $("#error").append("'.$translate->_("your_account_is_blocked").'<br>'.$translate->_("contact_our_support_for_help").'<br>").css({"display":"block"}); </script>';
								break;
							default:
								echo '<script type="text/javascript"> $("#error").append("'.$translate->_("there_is_something_wrong_with_your_account").'<br>'.$translate->_("contact_our_support_for_help").'<br>").css({"display":"block"}); </script>';
								break;
						}
					}else{
						echo '<script type="text/javascript"> $(function(){ $("#lgn_user").attr("value","'.$_POST["lgn_user"].'"); $("#lgn_pass").attr("value","'.$_POST["lgn_pass"].'"); }); $("#error").append("'.$translate->_("invalid_2FA").' <br>").css({"display":"block"}); $(document).ready(function() {$("#twosteps").show();}); </script>'; 
					}			
					
				}else{ 
					echo '<script type="text/javascript"> $("#error").append("'.$translate->_("username_or_email_invalid").'").css({"display":"block"}); </script>'; 
				}
			} else {
				
				echo '<script type="text/javascript"> 
						
						$(function(){

							$("#lgn_user").attr("value","'.$_POST["lgn_user"].'");
							$("#lgn_pass").attr("value","'.$_POST["lgn_pass"].'");						
							$("#error").append("'.$translate->_("verify_robot").'").css({"display":"block"}); 
							
						});
						
				     </script>';			
			}
		}
		
	}

	public function langAction(){

		$cookie_value = $_GET['lang'];
		$url = $_GET['url'];

		echo $url;

		setcookie('cookie_lang', $cookie_value, time() + (86400 * 30), "/");
		
		$this->_redirect('/'.$url.''); 

		exit();
	}


	public function forgotAction() {

		//Cookie
		if(isset($_COOKIE['cookie_lang'])) {
			$idiomaBrowser = $_COOKIE['cookie_lang'];
		}else{
			$idiomas = explode(",",$_SERVER["HTTP_ACCEPT_LANGUAGE"]);
			$idiomaBrowser = $idiomas[0];
		}
		//Array com as frases da linguagem
		$resultLanguage = $this->modelAccount->getLanguage("$idiomaBrowser");
		//Pega a abreviação da linguagem
		$lang = $this->modelAccount->getLanguageAbb("$idiomaBrowser");
		$translate = new Zend_Translate(array('adapter' => 'array', 'content' => $resultLanguage, 'locale' => "$lang"));
		$this->view->tr = $translate;
		$this->view->lang = $lang;

		if($_POST)
		{
			$data = array(
			    'secret' => RECAPTCHA_BACK,
			    'response' => $_POST['g-recaptcha-response']
			);
			# Create a connection
			$url = 'https://www.google.com/recaptcha/api/siteverify';
			$ch = curl_init($url);
			# Form data string
			$postString = http_build_query($data, '', '&');
			# Setting our options
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $postString);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			# Get the response
			$response = curl_exec($ch);
			curl_close($ch);
			$dados = json_decode($response);

			if(RECAPTCHA_ACTIVE==1){
				$captcha = $dados->success;
			}else{
				$captcha = true;
			}

			if ($captcha) {
				$lgn_user = addslashes($_POST["lgn_user"]);

				$userRecovery = $this->modelLogin->getUserRecovery($lgn_user);
				$user = $userRecovery["usr_id"];
				$email = $userRecovery["usr_email"];
				//Zend_Debug::dump($userRecovery);
				//exit;
				
			    if (($user != "") and ($email != ""))
				{
					$key = md5($user."-".rand(1,10000));
					$vencimento = date('Y-m-d', strtotime("+1 day"));
					$data = date('Y-m-d');
					
					$keyRecovery["key_id"] = $user;
					$keyRecovery["key_token"] = $key;
					$keyRecovery["key_vencimento"] = $vencimento;
					
					$pkey = $this->modelLogin->setPasswordKey($keyRecovery);

					$url = LINK_OFFICE."/login/recovery?key=".$key;
					
					$emailTemplate = $this->modelAccount->getEmailTemplate("email_type='change_password' AND email_status = 1",$userRecovery["usr_language"]);
					$email_template = $emailTemplate["email_template"];
					$subject = $emailTemplate["email_title"];

			        $email_template = str_replace('$username', $lgn_user, $email_template);
			        $email_template = str_replace('$link', $url, $email_template);

					$this->modelIndex->setZendMail($email,$subject,$email_template);

					echo '<script type="text/javascript"> 
						$(function(){
							$("#info").append("'.$translate->_("verify_your_email_recovery_password").' <br>").css({"display":"block"}); 
						});
						
				     </script>';
				}else{ 
					echo '<script type="text/javascript"> 
						$(function(){
							$("#lgn_user").attr("value","'.$_POST["lgn_user"].'");
							$("#error").append("'.$translate->_("invalid_username").' <br>").css({"display":"block"}); 
						});
						
				     </script>';
				}
			} else {
				echo '<script type="text/javascript"> 
					$(function(){
						$("#lgn_user").attr("value","'.$_POST["lgn_user"].'");				
						$("#error").append("'.$translate->_("verify_robot").'").css({"display":"block"}); 
					});
					
			     </script>';			
			}
		}
	}

	public function recoveryAction() {

		//Cookie
		if(isset($_COOKIE['cookie_lang'])) {
			$idiomaBrowser = $_COOKIE['cookie_lang'];
		}else{
			$idiomas = explode(",",$_SERVER["HTTP_ACCEPT_LANGUAGE"]);
			$idiomaBrowser = $idiomas[0];
		}
		//Array com as frases da linguagem
		$resultLanguage = $this->modelAccount->getLanguage("$idiomaBrowser");
		//Pega a abreviação da linguagem
		$lang = $this->modelAccount->getLanguageAbb("$idiomaBrowser");
		$translate = new Zend_Translate(array('adapter' => 'array', 'content' => $resultLanguage, 'locale' => "$lang"));
		$this->view->tr = $translate;
		$this->view->lang = $lang;

		if(!empty($_GET['key']))
	  	{
			$key = $_GET['key'];
			$resultKey = $this->modelLogin->getPasswordKey($key);

			$key_id = $resultKey["key_id"];
			$key_alterada = $resultKey["key_alterada"];
			$key_vencimento = $resultKey["key_vencimento"];

			$err = false;
			if(empty($key_vencimento) OR empty($key_id)) {
				echo '<script type="text/javascript"> 
						$(function(){
							$("#error").append("'.$translate->_("Chave de recuperação inválida.").' <br>").css({"display":"block"}); 
						});
						
				     </script>';
				$err = true;
			}
			if(($key_vencimento<date("Y-m-d")) || ($key_alterada==1)) {
				echo '<script type="text/javascript"> 
						$(function(){
							$("#error").append("'.$translate->_("Chave de recuperação expirada.").' <br> <a href=\"'.LINK_OFFICE.'/login/forgot\">'.$translate->_("Clique aqui").'</a> '.$translate->_("para receber um novo email.").'").css({"display":"block"}); 
							$("#recovery_form").html("");
						});
						
				     </script>';
				$err = true;
			}
			if ($_POST) {
				if(isset($_POST['new_password']) AND isset($_POST['new_repassword'])) {
					if($_POST['new_password'] != $_POST['new_repassword']) {
						echo '<script type="text/javascript"> 
							$(function(){
								$("#error").append("'.$translate->_("Senha e redigite a senha são diferentes.").' <br>").css({"display":"block"}); 
							});
							
					     </script>';
						$err = true;
					}
					
					if($this->modelIndex->getEasyPassword(sha1("x16".$_POST['new_password']))!="success"){
						echo '<script type="text/javascript"> 
							$(function(){
								$("#error").append("'.$translate->_("Por favor, digite uma senha mais forte.").' <br>").css({"display":"block"}); 
							});
							
					     </script>';
						$err = true;	
					}


					if(strlen($_POST['new_password'])<6){
						echo '<script type="text/javascript"> 
								$(function(){
									$("#error").append("'.$translate->_("Por favor, digite uma senha mais forte.").'<br>").css({"display":"block"}); 
								});
								
						     </script>';
						$err = true;
					}
				} else {
					if(empty($_POST['new_password'])){
						echo '<script type="text/javascript"> 
								$(function(){
									$("#error").append("'.$translate->_("Senha vazia!").' <br>").css({"display":"block"}); 
								});
								
						     </script>';
						$err = true;
					}
				}
			}
			
			


			if(!empty($_GET['key']) AND !empty($_POST['new_password']) AND !empty($_POST['new_repassword']) AND $err === false)
			{

				$psw = sha1("x16".$_POST['new_password']);
				$key = $_GET['key'];
				$this->modelAccount->updateUserPassword($key_id,$psw);
				//executaSQL("UPDATE tb_user SET usr_password='$psw' WHERE usr_id='$key_id'");
				$this->modelLogin->updatePasswordKeyAlterada($key_id);
				//executaSQL("UPDATE tb_passwordkey SET key_alterada='1' WHERE key_token='$key'");
				echo '<script type="text/javascript"> 
						$(function(){
							$("#info").append("'.$translate->_("Senha vazia!").' <br> <a href=\"'.LINK_OFFICE.'/login\">'.$translate->_("Clique aqui").'</a> '.$translate->_("para fazer login.").'").css({"display":"block"}); 
						});
						
				     </script>';
			}	  	
		}
	}
}

?>