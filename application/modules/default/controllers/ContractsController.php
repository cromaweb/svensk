<?php
class Default_ContractsController extends SYSTEM_Controllers_Office
{
    public function init() {
        parent::init();
		$this->modelIndex = new Application_Model_Index();
		$this->view->page_main = 'Contratos';
		$this->view->page_link = LINK_OFFICE."/contratos/";
		$this->view->subpages = $this->modelIndex->getSubPages("order");
    }
  
    public function indexAction()
    {
		$this->modelAccount = new Application_Model_Account();
		$this->modelTreeview = new Application_Model_Treeview();
		$this->modelOrder = new Application_Model_Order();

		$user = $this->view->User;	
		$this->view->user = $user;
		//Retorna o total de planos
		$this->view->amount = $this->modelOrder->getQtPlans($user);
		//Retorna o total de planos
		$this->view->plans = $this->modelOrder->getPlans($user);
		//Retorna o t�tulo do usu�rio
		$package = $this->modelAccount->getUserPackage($user);
		$pkg = $this->modelTreeview->getPackageName($package);
		$this->view->account = $pkg["prod_titulo"];

	}

	public function renewplanAction(){
		try
		{
			$this->model = new Application_Model_Contratos();

			$user = $this->view->User;
			$plan_id = addslashes($_POST["plan"]);
			$status = addslashes($_POST["status"]);

			//Pega os dados do plano
			$plan = $this->model->getPlanID($plan_id);

			$retorno = $plan["count"];

			//Se o plano existir e estiver no status de renovação
			if($plan["count"]==1){

				//Verifca se vai renovar ou sacar
				if($status=="renew"){
					//Renova o plano
					$retorno = $this->model->renewPlan($plan_id,$user,$plan["uplan_prod_id"]);

				}elseif($status=="withdraw"){
					//Devolve o valor do plano para o usuário
					$retorno = $this->model->withdrawPlan($plan_id,$user,$plan["uplan_pedido_id"]);

				}
			}

			$this->_helper->json->sendJson($retorno);
			
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error +2new");
			
		}
		
		exit();
	}
	
}

?>