<?php
class Default_HomeController extends SYSTEM_Controllers_Office
{
    public function init() {
        parent::init();
		$this->modelAccount = new Application_Model_Account();
		$this->modelIndex = new Application_Model_Index();
		$this->modelEwallet = new Application_Model_Ewallet();
		$this->modelOrder = new Application_Model_Order();
		$this->modelTransactions = new Application_Model_Transactions();
    }
  
    public function indexAction()
    {
		try
		{
			$user = $this->view->User;		
			$this->view->user = $user;	
			//Retorna a pontuação total do usuário
			//$this->view->points = $this->modelStatus->getPoints($user);
			//Retorna o título do usuário
			$this->view->status = $this->modelAccount->getUserStatus($user);
			
			//Saldo
			$balance = $this->modelEwallet->getBalance($user);
			//Saldo disponível
			$this->view->availableBalance = $balance["bsal_saldo_liberado"];
			//Saldo bloqueado
			$this->view->lockedBalance = $balance["bsal_saldo_bloqueado"];
			//Saldo a receber
			$this->view->balanceProcess = $balance["bsal_saldo_areceber"];
			//Wallet Earnings
			$this->view->balanceEarnings = $balance["bsal_saldo_rendimento"];
			//Wallet Comissions
			$this->view->balanceComissions = $balance["bsal_saldo_comissoes"];

			//Planos
			$this->view->plans = $this->modelOrder->getPlans($user);

			//Transações
			$this->view->transactions = $this->modelTransactions->getFinancialStatementLimit($user,5,1);
			
			#### Contratos
			//Retorna o total de planos
			//$this->view->amount_plans = $this->modelOrder->getQtPlans($user);
			
			//Investimento total e limite de recebimento de bonus
			//$bonusLimit = $this->modelEwallet->getLimitBonus($user);
			//$bnsLimit = $bonusLimit['busr_bonus_limite'];
			//$bnsInvestiment = $bonusLimit['busr_investimento_total'];
			
			
			//Cotação BTC/BRL
			$this->view->btc_brl = $this->modelIndex->getBRLBTC();

			//Cotação BTC/USD
			$this->view->btc_usd = $this->modelIndex->getUSDBTC();

			//Cotação

			//$cotacao = $this->modelIndex->getCoin();

			// $arr = json_decode($cotacao);
			// $this->view->usdbrl = $arr->USD;
			// $this->view->eurbrl = $arr->EUR;
			// $this->view->btcbrl = $arr->BTC;

		
		}catch(Exception $e){
			die($e->getMessage());
			//die("Error +++1");
			
		}	

	}

}

?>