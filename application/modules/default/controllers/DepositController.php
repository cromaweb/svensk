<?php
class Default_DepositController extends SYSTEM_Controllers_Office
{
    public function init() {
        parent::init();
        $this->model = new Application_Model_Deposit();
        $this->modelIndex = new Application_Model_Index();
		$this->_redirector = $this->_helper->getHelper('Redirector');
		$this->view->depositFee = 0.00;
		$this->view->minimumAmountDeposit = 100.00;
    }


    public function indexAction()
    {
		$this->modelAccount = new Application_Model_Account();
		$this->modelOrder = new Application_Model_Order();

		//Tradução
		$sessao = new Zend_Session_Namespace(SESSION_OFFICE);
		$arrayLanguage = $sessao->arrayLanguage;
		$userLanguage = $sessao->userLanguage;
	
		$translate = new Zend_Translate(array('adapter' => 'array', 'content' => $arrayLanguage, 'locale' => "$userLanguage"));

		try
		{
			$user = $this->view->User;
			
			if ($_POST["val"]==1 && empty($_POST["error"])) {

				//Se a conta estiver confirmada
				//if($this->modelAccount->getValidate($user)==1){

					//Verifica se o usuário tem um endereço BTC
					$countUserAddress = $this->model->getCountUserAddress($user);
					
					//Se não possuir um endereço BTC
					if($countUserAddress==0){
						
						//Pega um novo endereço para o usuário
						$newAddress = $this->model->setUserNewAddress($user);
						$this->model->setUserWallet($user,$newAddress);
						
					}

					//Pega o endereço BTC do usuário
					$address = $this->model->getUserAddress($user);
					
					//Recebe o valor digitado em dolar pelo cliente
					$dollar = str_replace('USDT ','',$_POST['amount']);
					$dollar = str_replace(',','',$dollar);
					$dollar = floatval($dollar);
					$dollar = round($dollar, 2);

					//Se o valor for maior ou igual ao valor mínimo
					if ($dollar >= $this->view->minimumAmountDeposit)
					{
						//Verifica se tem fatura abertas
						$openDeposit = $this->model->getOpenDeposit($user);

						//Se existir alguma fatura de depósito em aberto
						if($openDeposit["amount"]>0){
							//Cancela as faturas de depósito abertas
							$this->model->updateOpenDeposit($user);
						}

						//Adiciona o valor de taxa com o valor informado de depósito
						$dollar += $this->view->depositFee;

						//Converte o valor dólar para BTC
						$amountBTC = $this->modelIndex->getTradeUSDBTC($dollar);

						//Conversão BTC para USD no momento
						$amountRate = $this->modelIndex->getUSDBTC();
		
						//cadastrar na tabela transacoes btc
						$arrayData = array(
							"bco_user"				=> $user,
							"bco_type"				=> "D",
							"bco_dollar_amount"		=> $dollar,
							"bco_bitcoin_amount"	=> $amountBTC,
							"bco_dollar_fee"		=> $this->view->depositFee,
							"bco_rate"				=> $amountRate,
							"bco_account_to"		=> $address
						);
						//Seta o depósito na tabela
						$idBitcoinOperation = $this->model->setDeposit($arrayData);
		
						//$this->model->updateDeposit($idBitcoinOperation,$arrayUpdate);
		
						//$this->_redirector->setExit(true)->setGotoSimple("index","deposit");
						//$this->_redirect(LINK_OFFICE . "/deposit");

						//Refresh na págin com o retorn
						//$this->view->assign('error',"<script type='text/javascript'>window.location.href='deposit';</script>");
						$this->view->assign('meta'," <meta http-equiv='refresh' content='1'>");
					
		
					} else {
						$this->view->assign('error',"".$translate->_("O valor deve ser maior ou igual a USDT")." ".round($this->view->minimumAmountDeposit, 2));
					}
				
				// }else{
				// 	$this->view->assign('error',"".$translate->_("Sua conta não está verificada!")."");
				// }
			}
	
			$deposits = $this->model->getAllDeposits($user,1);
	
			foreach ($deposits as $key => $value) {
				switch ($value['bco_status']) {
					case 'P':
						$deposits[$key]['status'] = "".$translate->_("Pago")."";
						break;
					case 'D':
						$deposits[$key]['status'] = "Double Paid";
						break;
					case 'M':
						$deposits[$key]['status'] = "Partially Paid";
						break;
					case 'W':
						$datetime1 = new DateTime($value['bco_date_insert']);
						$datetime1->add(new DateInterval('PT60M'));
						$datetime2 = new DateTime();
						if ($datetime2 > $datetime1) {
							$this->model->updateDeposit($value['bco_id'],array("bco_status" => "C"));
							$deposits[$key]['bco_status'] = "C";
							$deposits[$key]['status'] = "".$translate->_("Cancelado")."";
						} else {
							$deposits[$key]['final_countdown'] = $datetime1->format('Y-m-d H:i:s');
							$deposits[$key]['status'] = "".$translate->_("Aguardando depósito")."";
						}
						break;
					case 'U':
						$datetime1 = new DateTime($value['bco_date_insert']);
						$datetime1->add(new DateInterval('PT60M'));
							$deposits[$key]['final_countdown'] = $datetime1->format('Y-m-d H:i:s');
							$deposits[$key]['status'] = "".$translate->_("Processando pagamento")."";
						//}
						break;
					case 'C':
						if($value['bco_limit']!=0 && $value['bco_status']!='P')
						{
							$deposits[$key]['status'] = "Bonus Limit";
						}
						elseif($value['bco_withdrawal_id']!=0 && $value['bco_status']!='P')
						{
							$deposits[$key]['status'] = "Fee | Withdrawal";
						}
						else
						{
							$deposits[$key]['status'] = "".$translate->_("Cancelado")."";
						}
						break;
				} 
			}
			
			if($_POST["error"]!=0){
				$this->modelOrder->setLog($_POST["codigomensaje"]."|".$_POST["error"].":".$_POST["mensaje"], $_POST["pago"], $user);
				$this->view->error = $error;
				$this->view->errorEuro = $_POST["error"];
				$this->view->msgerror = $_POST["mensaje"];
			}
			
			$this->view->assign('deposits',$deposits);
		
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error DC+1000");
			
		}
	}


	public function verifystatusAction()
	{
		try
		{
			$this->_helper->layout->disableLayout();
			$user = $this->view->User;	
			$invoiceNumber = $_POST['id'];
			$invoice = $this->model->getDeposit($invoiceNumber);
	
			if ($invoice['bco_user'] == $user) {
				switch ($invoice['bco_status']) {
					case 'W':
						$datetime1 = new DateTime($invoice['bco_date_insert']);
						$datetime1->add(new DateInterval('PT60M'));
						$datetime2 = new DateTime();
						if ($datetime2 > $datetime1) {
							$this->model->updateDeposit($invoiceNumber,array("bco_status" => "C"));
							$return = "Canceled";
						} else {
							$return = "waiting";
						}
						break;
					case 'P':
						$return = "Paid";
						break;
					case 'D':
						$return = "Double Paid";
						break;
					case 'M':
						$return = "Partially Paid";
						break;
					case 'U':
						$return = "Processing payment";
						break;
					case 'C':
						$return = "Canceled";
						break;
				}
			} else {
				$return = "error";
			}
	
			echo $return;
			exit;
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error DC+1001");
			
		}
	}

}

?>