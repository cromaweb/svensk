<?php
class Default_CustomersController extends SYSTEM_Controllers_Office
{
    public function init() {
        parent::init();
        $this->modelAccount = new Application_Model_Account();
		$this->modelIndex = new Application_Model_Index();
		$this->modelStatus = new Application_Model_Statusexecutive();
		$this->modelMyenrollees = new Application_Model_Myenrollees();
		
    }
  
    public function indexAction()
    {	
		
		$user = $this->view->User;

		$this->view->contract = $this->modelAccount->getUserContractSign($user);
		
		$usernameRede = $this->getRequest()->getParam('username');
		
		if(!empty($usernameRede))
		{
		    $userID = $this->modelAccount->getUserId($usernameRede);
		    $userID = $userID["usr_id"];
		    
		    if(!empty($userID))
		    {
		        $level = $this->modelAccount->getUserLevel($userID,$user);
		        
		        if(!empty($level)){
    		        $this->view->level = $level;
    		        $this->view->usernameRede = $usernameRede;
    		        $user = $userID;
		        }

		    }
		    
		}

		//Pega os dados do usuário
		$userDados = $this->modelAccount->getUserFind($user);

		//Verifica se usuário é diretor da empresa
		if($userDados["usr_director"]==1){
			//Se for diretor seta o $user para o usuário do top para listar todos os clientes e agentes
			$enrollees = $this->modelMyenrollees->getTotalEnrollees();
			$this->view->agents = $this->modelMyenrollees->getTotalAgents();
			$this->view->director = 1;
			//Rede
			//$this->view->totalDirects = $this->modelAccount->getUserUplineTotal(100110);
			//$this->view->totalUpline = $this->view->totalDirects;

		}else{
			//Pega os indicados diretos
			$enrollees = $this->modelMyenrollees->getMyEnrollees($user);
			//Rede
			//$this->view->totalDirects = $this->modelAccount->getUserTotalDirects($user);
			//$this->view->totalUpline = $this->modelAccount->getUserUplineTotal($user);
		}
		
        // Create a Paginator for the blog posts query
        $paginator = Zend_Paginator::factory($enrollees);
		
		//Number for page
		$paginator->setItemCountPerPage(30);
		
		// Read the current page number from the request. Default to 1 if no explicit page number is provided.
		$paginator->setCurrentPageNumber($this->_getParam('page', 1));
		
        // Assign the Paginator object to the view
        $this->view->paginator = $paginator;
		
        // Assign the number enrollees object to the view
		$this->view->enrollees = count($enrollees);
		
		$this->view->totalDirects = $this->view->enrollees;
		
		//$this->view->totalIndirects = $this->view->totalUpline - $this->view->totalDirects;
		
		//$status = $this->modelAccount->getUserStatusID($user);
		//$this->view->statusExecutive = $this->modelStatus->getTitleName($status);
	}
	
}

?>