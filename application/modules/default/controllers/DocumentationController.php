<?php
class Default_DocumentationController extends SYSTEM_Controllers_Office
{
    public function init() {
		parent::init();
		$this->modelAccount = new Application_Model_Account();
    }
  
    public function indexAction()
    {

    	$db = Zend_Registry::get('database');
 		$user = $this->view->User;

		$select = $db->select();
		$select->from('tb_documentacao')
		       ->where('doc_idUsuario = '.$user);
		$this->view->Documentacao = $db->fetchRow($select);	

		//Busca os dados do usuário
		$dadosUser = $this->modelAccount->getUserBasic($user);
		$this->view->DadosUser = $dadosUser;

		if($this->validate==0 &&  $dadosUser["usr_documentation"]!=0){
			$this->view->assign('validate',1);
			$sessao = new Zend_Session_Namespace(SESSION_OFFICE);
			$sessao->validate = 1;
		}

		if($dadosUser["usr_primeiro_login"]=="S"){
			$sessao = new Zend_Session_Namespace(SESSION_OFFICE);
			$sessao->primeiroLogin = 'S';
			$this->view->assign('primeiroLogin','S');
		}

 	}

 	public function sendAction()
 	{

 		function uploadFile($arquivo, $pasta, $tipos){

			//Tradução
			$sessao = new Zend_Session_Namespace(SESSION_OFFICE);
			$arrayLanguage = $sessao->arrayLanguage;
			$userLanguage = $sessao->userLanguage;
		
			$translate = new Zend_Translate(array('adapter' => 'array', 'content' => $arrayLanguage, 'locale' => "$userLanguage"));

		    if(isset($arquivo)){
		        $infos = explode(".", $arquivo["name"]);
		
		        $nome = md5(date('Y').$_SESSION['user'].date('md').rand(1,1000000000)).".";
		 
		        $tipoArquivo = $infos[count($infos) - 1];
		 
		        $tipoPermitido = false;
		        foreach($tipos as $tipo){
		            if(strtolower($tipoArquivo) == strtolower($tipo)){
		                $tipoPermitido = true;
		            }
		        }

		        if(!$tipoPermitido){
		            $retorno["erro"] = ''.$translate->_("Tipo inválido! Aceito somente jpg, png, gif or pdf.").'';
		        } else if ($arquivo['size'] > (1 * 3072 * 3072)) {
					$retorno["erro"] = ''.$translate->_("Tamanho de (3MB) foi excedido.").'';
				} else{
		            if(move_uploaded_file($arquivo['tmp_name'], $pasta . $nome . $tipoArquivo)){
		                $retorno["caminho"] = $pasta . $nome . $tipoArquivo;
		            }
		            else{
		                $retorno["erro"] = ''.$translate->_("Erro ao enviar. Por favor, tente novamente.").'';
		            }
		        }
		    }
		    else{
		        $retorno["erro"] = ''.$translate->_("Arquivo não encontrado.").'';
		    }
		    return $retorno;
		}
 		/* Importa o arquivo onde a função de upload está implementada */
		$user = $this->view->User;

		$arquivo = $_FILES['arquivo'];
		 
		/*Define os tipos de arquivos válidos (No nosso caso, só imagens)*/
		$tipos = array('jpg', 'png', 'gif', 'pdf');
		 
		/* Chama a função para enviar o arquivo */
		//$enviar = uploadFile($arquivo, getcwd().'/upload/documentation/', $tipos);
		$enviar = uploadFile($arquivo, '/home/'.FOLDER_OFFICE.'/documentation.'.SITE_DOMAIN.'/', $tipos);
		 
		$data['sucesso'] = false;
		 
		if ($enviar['erro']) {    
		    $data['msg'] = $enviar['erro'];
		}
		else {
			$tipo = $_POST['type'];
			
			$db = Zend_Registry::get('database'); 
			$exp1 = explode('/',$enviar['caminho']);
			$nomearquivo = end($exp1);
			switch ($tipo) {
				case "personal":
					$sql = "INSERT INTO tb_documentacao" 
		           ." (doc_idUsuario, doc_pessoal_arquivo, doc_pessoal_detalhes, doc_pessoal_status) " 
		           ." VALUES(".$user.",'".$nomearquivo."','','U') ON DUPLICATE KEY "
		           ." UPDATE doc_pessoal_arquivo = '".$nomearquivo."', doc_pessoal_detalhes = '', doc_pessoal_status = 'U'";
					break;

				case "address":
					$sql = "INSERT INTO tb_documentacao" 
		           ." (doc_idUsuario, doc_endereco_arquivo, doc_endereco_detalhes, doc_endereco_status) " 
		           ." VALUES(".$user.",'".$nomearquivo."','','U') ON DUPLICATE KEY "
		           ." UPDATE doc_endereco_arquivo = '".$nomearquivo."', doc_endereco_detalhes = '', doc_endereco_status = 'U'";
					break;

				case "backdocument":
					$sql = "INSERT INTO tb_documentacao" 
		           ." (doc_idUsuario, doc_pessoal_verso_arquivo, doc_pessoal_verso_detalhes, doc_pessoal_verso_status) " 
		           ." VALUES(".$user.",'".$nomearquivo."','','U') ON DUPLICATE KEY "
		           ." UPDATE doc_pessoal_verso_arquivo = '".$nomearquivo."', doc_pessoal_verso_detalhes = '', doc_pessoal_verso_status = 'U'";
				   break;
				   
				case "selfie":
					$sql = "INSERT INTO tb_documentacao" 
		           ." (doc_idUsuario, doc_selfie_arquivo, doc_selfie_detalhes, doc_selfie_status) " 
		           ." VALUES(".$user.",'".$nomearquivo."','','U') ON DUPLICATE KEY "
		           ." UPDATE doc_selfie_arquivo = '".$nomearquivo."', doc_selfie_detalhes = '', doc_selfie_status = 'U'";
					break;

				case "company":
					$sql = "INSERT INTO tb_documentacao" 
		           ." (doc_idUsuario, doc_company_arquivo, doc_company_detalhes, doc_company_status) " 
		           ." VALUES(".$user.",'".$nomearquivo."','','U') ON DUPLICATE KEY "
		           ." UPDATE doc_company_arquivo = '".$nomearquivo."', doc_company_detalhes = '', doc_company_status = 'U'";
					break;
		           
				default:
					$data['sucesso'] = false;
		    		$data['msg'] = $tipo;
		    		echo json_encode($data);
					exit;
					break;
			}
			
		    $db->query($sql);

		    $select = $db->select();
			$select->from('tb_documentacao')
			       ->where('doc_idUsuario = '.$user);
			$doc = $db->fetchRow($select);

			$dps = $doc["doc_pessoal_status"];
            $des = $doc["doc_endereco_status"];
			$dbs = $doc["doc_pessoal_verso_status"];
			$dss = $doc["doc_selfie_status"];
			$dcs = $doc["doc_company_status"];

            //Busca os dados do usuário
			$user_company = $this->modelAccount->getUserBasic($user);
			$company = $user_company["usr_type"];

			if($company==0){

				if (($dps == 'U' || $dps == 'V' || $dps == 'P') 
					&& ($des == 'U' || $des == 'V' || $des == 'P') 
					&& ($dbs == 'U' || $dbs == 'V' || $dbs == 'P')
					&& ($dss == 'U' || $dss == 'V' || $dss == 'P')) 
				{
					$data['last'] = true;
				} else {
					$data['last'] = false;
				}

			}else{

				if (($dps == 'U' || $dps == 'V' || $dps == 'P') 
					&& ($des == 'U' || $des == 'V' || $des == 'P') 
					&& ($dbs == 'U' || $dbs == 'V' || $dbs == 'P')
					&& ($dss == 'U' || $dss == 'V' || $dss == 'P')
					&& ($dcs == 'U' || $dcs == 'V' || $dcs == 'P')) 
				{
					$data['last'] = true;
				} else {
					$data['last'] = false;
				}
			}


		    $data['sucesso'] = true;
		    $exp2 = explode('/',$enviar['caminho']);
		    $data['msg'] = end($exp2);
		}
		 
		echo json_encode($data);
	    exit;
 	}

 	public function deleteAction()
 	{
 		$user = $this->view->User;

		$db = Zend_Registry::get('database'); 
		$select = $db->select();
		$select->from('tb_documentacao')
		       ->where('doc_idUsuario = '.$user);
		$doc = $db->fetchRow($select);

 		switch ($_POST['id']) {
			case "personal":
				if ($doc["doc_pessoal_status"] != 'P' && $doc["doc_pessoal_status"] != 'V') {
					//@unlink(getcwd().'/upload/documentation/'.$doc["doc_pessoal_arquivo"]);
					@unlink('/home/'.FOLDER_OFFICE.'/documentation.'.SITE_DOMAIN.'/'.$doc["doc_pessoal_arquivo"]);
					$data = array('doc_pessoal_arquivo' => "",
					      			'doc_pessoal_detalhes'  => "",
					      			'doc_pessoal_status' => "");
					      
					 $db->update('tb_documentacao', $data,'doc_idUsuario = '.$user);
					echo 1;
				} else {
					echo 3;
				}
				
				break;

			case "address":
				if ($doc["doc_endereco_status"] != 'P' && $doc["doc_endereco_status"] != 'V') {
					//@unlink(getcwd().'/upload/documentation/'.$doc["doc_endereco_arquivo"]);
					@unlink('/home/'.FOLDER_OFFICE.'/documentation.'.SITE_DOMAIN.'/'.$doc["doc_endereco_arquivo"]);
					$data = array('doc_endereco_arquivo' => "",
					      			'doc_endereco_detalhes'  => "",
					      			'doc_endereco_status' => "");
					      
					 $db->update('tb_documentacao', $data,'doc_idUsuario = '.$user);
					echo 1;
				} else {
					echo 3;
				}
				break;

			case "backdocument":
				if ($doc["doc_pessoal_verso_status"] != 'P' && $doc["doc_pessoal_verso_status"] != 'V') {
					//@unlink(getcwd().'/upload/documentation/'.$doc["doc_pessoal_verso_arquivo"]);
					@unlink('/home/'.FOLDER_OFFICE.'/documentation.'.SITE_DOMAIN.'/'.$doc["doc_pessoal_verso_arquivo"]);
					$data = array('doc_pessoal_verso_arquivo' => "",
					  			'doc_pessoal_verso_detalhes'  => "",
					  			'doc_pessoal_verso_status' => "");
					  
					$db->update('tb_documentacao', $data,'doc_idUsuario = '.$user);
					echo 1;
				} else {
					echo 3;
				}
			   break;
			   
			case "selfie":
				if ($doc["doc_selfie_status"] != 'P' && $doc["doc_selfie_status"] != 'V') {
					//@unlink(getcwd().'/upload/documentation/'.$doc["doc_selfie_arquivo"]);
					@unlink('/home/'.FOLDER_OFFICE.'/documentation.'.SITE_DOMAIN.'/'.$doc["doc_selfie_arquivo"]);
					$data = array('doc_selfie_arquivo' => "",
					  			'doc_selfie_detalhes'  => "",
					  			'doc_selfie_status' => "");
					  
					$db->update('tb_documentacao', $data,'doc_idUsuario = '.$user);
					echo 1;
				} else {
					echo 3;
				}
			   break;
			   
			case "company":
				if ($doc["doc_company_status"] != 'P' && $doc["doc_company_status"] != 'V') {
					//@unlink(getcwd().'/upload/documentation/'.$doc["doc_company_arquivo"]);
					@unlink('/home/'.FOLDER_OFFICE.'/documentation.'.SITE_DOMAIN.'/'.$doc["doc_company_arquivo"]);
					$data = array('doc_company_arquivo' => "",
					  			'doc_company_detalhes'  => "",
					  			'doc_company_status' => "");
					  
					$db->update('tb_documentacao', $data,'doc_idUsuario = '.$user);
					echo 1;
				} else {
					echo 3;
				}
	           break;
	           
			default:
				echo 2;
				exit;
				break;
		}

 		exit;
 	}

 	public function verificationAction()
 	{

		$this->modelIndex = new Application_Model_Index();

 		$user = $this->view->User;

		$db = Zend_Registry::get('database'); 

	    $select = $db->select();
		$select->from('tb_documentacao')
		       ->where('doc_idUsuario = '.$user);
		$doc = $db->fetchRow($select);

		$data = array();
		
		if ($doc["doc_pessoal_status"] == 'U') {
			$data['doc_pessoal_status'] = 'P';
		}
		if ($doc["doc_endereco_status"] == 'U') {
			$data['doc_endereco_status'] = 'P';
		}
		if ($doc["doc_pessoal_verso_status"] == 'U') {
			$data['doc_pessoal_verso_status'] = 'P';
		}
		if ($doc["doc_selfie_status"] == 'U') {
			$data['doc_selfie_status'] = 'P';
		}
		if ($doc["doc_company_status"] == 'U') {
			$data['doc_company_status'] = 'P';
		}
		if (!empty($data)) {
			$db->update('tb_documentacao', $data,'doc_idUsuario = '.$user);

            //Busca os dados do usuário
			$user_doc = $this->modelAccount->getUserBasic($user);
			$username = $user_doc["usr_login_id"];
			$email = $user_doc["usr_email"];
			$url = LINK_OFFICE . "/documentation";

			$emailTemplate = $this->modelAccount->getEmailTemplate("email_type='validate_docs' AND email_status = 1",$user_doc["usr_language"]);
			$email_template = $emailTemplate["email_template"];
			$subject = $emailTemplate["email_title"];
			
			$email_template = str_replace('$username', $username, $email_template);
			$email_template = str_replace('$link', $url, $email_template);

			$this->modelIndex->setZendMail($email,$subject,$email_template);

			$validador = VALIDATOR;
			//Envia email para o validador
			$emailTemplate = $this->modelAccount->getEmailTemplate("email_type='validate_docs_admin' AND email_status = 1",$user_doc["usr_language"]);
			$email_template = $emailTemplate["email_template"];
			$subject = $emailTemplate["email_title"];
			
			$email_template = str_replace('$username', $username, $email_template);

			$this->modelIndex->setZendMail($validador,$subject,$email_template);

			echo 1;
		} else {
			echo 2;
		}

 		exit;
	 }
	 
	 public function generatecontractAction()
	 {
		 $this->modelD4sign= new Application_Model_D4sign();
		 $this->modelIndex = new Application_Model_Index();
 
		 try
		 {			
			 $user = $this->view->User;
			 $act = addslashes($_POST["act"]);
 
				 $userDados = $this->modelAccount->getUserFind($user);
 
				 $contract 				= $userDados["usr_contract"];
 
				 //Se o status estiver para criar o contrato
				 if($contract==0){
 
					 //Dados da D4sign
					 $uri = "https://secure.d4sign.com.br/api/v1/";
					 $tokenAPI = "live_19ea54e87e8d1541e7f402157feea38165f886380524eaab586872be6ba93c69";
					 $cryptKey = "live_crypt_oojf9P5UKBFtCnuoySFhTNOxrsEs94ch";

					 if($userDados["usr_country"]=="BR"){

						//Cofre
						$safe = "8dcea126-1231-4c3f-8d74-2ed9fc3061a9";
						$template_id = "MTM1NTk=";
	
						//Gerar documento usando template versão Cliente
						$data = [];
						$data = array("name_document" => "WTC-CONTRATO-AGENTE-$user",
							"templates" => array( 
								$template_id => array(
									"RAZAOSOCIAL"=> $userDados["usr_company_name"],
									"TIPOEMPRESA"=> "",
									"CNPJ"=> $userDados['usr_cnpj'],
									"ENDERECO"=> $userDados["usr_address"].",".$userDados["usr_number"].",".$userDados["usr_address2"].", cidade ".$userDados["usr_city"].", estado ".$userDados["usr_state"].", CEP ".$userDados["usr_zip_code"],
									"nome"=> $userDados['usr_name'],
									"cargo"=> "socio",
									"cpf"=> $userDados["usr_cpf"],
									"data"=> date("d/m/Y")
								)
							)
						);

					}else{
						//Cofre
						$safe = "3dab8aac-55a2-49f1-9368-ba10cdeb3292";
						$template_id = "MTU3MzE=";
	
						//Gerar documento usando template versão Cliente
						$data = [];
						$data = array("name_document" => "DIGITAL-SERVICE-DISTRIBUTION-AGREEMENT-$user",
							"templates" => array( 
								$template_id => array(
									"NAME"=> $userDados['usr_name'],
									"COMPLETEADDRESS"=> $userDados["usr_address"].",".$userDados["usr_number"].",".$userDados["usr_address2"].", cidade ".$userDados["usr_city"].", estado ".$userDados["usr_state"].", CEP ".$userDados["usr_zip_code"],
									"NATIONALITY"=> $this->modelIndex->getNameCountry($userDados["usr_country"]),
									"DOCUMENTID"=> $userDados["usr_rg"],
									"DATEOFBIRTH"=> $userDados["usr_birth"],
									"date"=> date("d/m/Y")
								)
							)
						);
					}
					 $template = $this->modelD4sign->getTemplate($uri,$safe,$tokenAPI,$cryptKey,$data);
					 //$template = $this->modelD4sign->getTemplate($uri,"c181d1f9-ba0b-4cd6-abe5-a1ee1374a600",$tokenAPI,$cryptKey,$data);
					 $this->modelD4sign->setLog($user,$template);
					 $template = json_decode($template);
					 
					 //Se o documento foi gerado com o template
					 if($template->message=="success"){
 
						 //Set tag com ID do user
						 $data = [];
						 $data = array("tag" => $user);
						 $this->modelD4sign->setTag($uri,$template->uuid,$tokenAPI,$cryptKey,$data);
 
						 $data = [];
						 $data = array(
							 "signers" => array(
									 array(
									 "email"=> $userDados["usr_email"],
									 "act" => "4",
									 "foreign" => "0",
									 "certificadoicpbr" => "0",
									 "assinatura_presencial" => "0",
									 "docauth" => "0",
									 "docauthandselfie" => "0",
									 "embed_methodauth" => "email",
									 "embed_smsnumber" => "",
									 "upload_allow" => "0"
									 ),
									 array(
										 "email"=> "contratos@worldtradecorporation.io",
										 "act" => "4",
										 "foreign" => "0",
										 "certificadoicpbr" => "0",
										 "assinatura_presencial" => "0",
										 "docauth" => "0",
										 "docauthandselfie" => "0",
										 "embed_methodauth" => "email",
										 "embed_smsnumber" => "",
										 "upload_allow" => "0"
									 ),
									 array(
										 "email"=> "lourenco@worldtradecorporation.io",
										 "act" => "5",
										 "foreign" => "0",
										 "certificadoicpbr" => "0",
										 "assinatura_presencial" => "0",
										 "docauth" => "0",
										 "docauthandselfie" => "0",
										 "embed_methodauth" => "email",
										 "embed_smsnumber" => "",
										 "upload_allow" => "0"
									 ),
									 array(
										 "email"=> "inacio@worldtradecorporation.io",
										 "act" => "5",
										 "foreign" => "0",
										 "certificadoicpbr" => "0",
										 "assinatura_presencial" => "0",
										 "docauth" => "0",
										 "docauthandselfie" => "0",
										 "embed_methodauth" => "email",
										 "embed_smsnumber" => "",
										 "upload_allow" => "0"
										 )
								 )
							 );
						 //Set signatário
						 $signatory = $this->modelD4sign->setSignatory($uri,$template->uuid,$tokenAPI,$cryptKey,$data);
						 $signatory = json_decode($signatory,true);
						 //$this->modelD4sign->setLog($user,$signatory);
 
						 //Seta o webhook
						 $data = [];
						 $data = array(
							 "url"=> "https://dash.worldtradecorporation.io/routines/agentd4sign"
							 );
 
						 $this->modelD4sign->setWebhook($uri,$template->uuid,$tokenAPI,$cryptKey,$data);
 
						 //Se o signatário foi incluído
						 if($signatory["message"][0]["success"]=="1"){
 
							 $data = [];
							 $data = array(
								 "skip_email"=> "0",
								 "workflow" => "0"
								 );
							 $sendtosigner = $this->modelD4sign->sendDocument($uri,$template->uuid,$tokenAPI,$cryptKey,$data);
							 $this->modelD4sign->setLog($user,$sendtosigner);
							 $sendtosigner = json_decode($sendtosigner);
 
							 if($sendtosigner->message=="File sent to successfully signing"){
								 //Envia o ok e documento está pronto para assinar
								 $retorno = 1;
								 $this->modelD4sign->updateAgentContract($user,1,$template->uuid,$signatory["message"][0]["key_signer"]);
							 }else{
								 $retorno = "Erro ao enviar contrato ".$sendtosigner->message;
								 $this->modelD4sign->updateAgentContract($user,4,$template->uuid);
								 
							 }
 
						 }else{
							 $retorno = "Erro no signatário do contrato ".$signatory->message->success;
							 $this->modelD4sign->updateAgentContract($user,4,$template->uuid);
							 $retorno = $signatory;
						 }
 
					 }else{
						 $retorno = "Erro ao gerar contrato ".$template->message;
						 $this->modelD4sign->updateAgentContract($user,4,$template->uuid);
					 }
						 
 
				 }else{
					 $retorno = "Contrato já foi gerado ou assinado.";
				 }
 
			 //Logs
			 $this->modelD4sign->setLog($user,$retorno);
 
			 $this->_helper->json->sendJson($retorno);
 
			 //var_dump($signatory["message"][0]["success"]);
 
			 
 
		 }catch(Exception $e){
			 //die($e->getMessage());
			 die("Error +1cx");
			 
		 }
 
		 exit();
	 }
	
}
?>