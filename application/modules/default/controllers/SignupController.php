<?php
class Default_SignupController extends SYSTEM_Controllers_Office
{
    public function init() {
        parent::init();
        $this->_helper->layout->disableLayout();
		$this->model = new Application_Model_Signup();
		$this->modelAccount = new Application_Model_Account();
		$this->modelTreeview = new Application_Model_Treeview();
		$this->modelIndex = new Application_Model_Index();

		$this->_redirector = $this->_helper->getHelper('Redirector');
    }
  
    public function indexAction()
    {
		$this->_helper->layout->setLayout('signup');
		
		$sessionNamespace = new Zend_Session_Namespace(SESSION_BASE);
		$userSponsor = $sessionNamespace->usuario_indicador;
		
	  	$result = $this->modelAccount->getUserBasic(null,$userSponsor);

	  	
		//$result = $this->modelAccount->getUserId($userSponsor,null);
		//Se retornar resultado

		if(!empty($result)){
			if ($result['usr_active'] == 'd' || $result['usr_rede']==0 || $result['usr_director']==1 || $result['usr_contract']!=2) {
				unset($sessionNamespace->usuario_indicador);
				$this->_redirector->gotoUrl(LINK_OFFICE. '/signup/promoter');
			} else {

				//Cookie
				if(isset($_COOKIE['cookie_lang'])) {
					$idiomaBrowser = $_COOKIE['cookie_lang'];
				}else{
					$idiomas = explode(",",$_SERVER["HTTP_ACCEPT_LANGUAGE"]);
					$idiomaBrowser = $idiomas[0];
				}
				//Array com as frases da linguagem
				$resultLanguage = $this->modelAccount->getLanguage("$idiomaBrowser");
				//Pega a abreviação da linguagem
				$lang = $this->modelAccount->getLanguageAbb("$idiomaBrowser");
				$translate = new Zend_Translate(array('adapter' => 'array', 'content' => $resultLanguage, 'locale' => "$lang"));
				$this->view->tr = $translate;
				$this->view->lang = $lang;

				//--------------------------

				$this->view->assign('user',$userSponsor);

				$idSponsor = $result["usr_id"];
				$preferedLeg = $this->modelAccount->getUserLeg($idSponsor);
				$this->view->assign('userId',$idSponsor);
				$this->view->assign('name',$result["usr_name"]);
				$this->view->assign('lastName',$result["usr_last_name"]);
				$this->view->assign('preferedLeg',$preferedLeg);
				
				if($this->modelTreeview->getDadOrg($idSponsor)>0){
					//Pega a letra da posição através do ID do patrocinador e ID da Leg dele
					$position  = $this->model->getPreferedLeg($idSponsor,$preferedLeg);
					//Defini a posição através do retorno da letra da Leg
					if($position=="E"){
						$this->view->assign('position','left');
					}else{
						$this->view->assign('position','right');
					} 
				}
			}
			
		} else {
			$this->_redirector->gotoUrl(LINK_OFFICE. '/signup/promoter');
		}
		
		//---------------------Em caso de POST
		if ($_POST)
		{ 
	
		  $act = $_POST["act"];
		  //$idPatrocinador = $_POST["idSponsor"];
		  //$idPatrocinador = $idSponsor; //ID do patrocinador igual o ID da sessão
		  $email = addslashes($_POST["email"]);
		  $email = strtolower($email);
		  $email2 = addslashes($_POST["email2"]);
		  $email2 = strtolower($email2);
		  $country = addslashes($_POST["country"]);
		  $login = addslashes($_POST["login"]);
		  $login = strtolower($login);
		  $terms = addslashes($_POST["terms"]);
		  $password = $_POST["password"];
		  $repassword = $_POST["repassword"];

		  //Identifica a linguagem de acordo com o país
		  if($country == "BR"){
			  $language = "pt-br";
		  }

		  switch($country){
				case "BR":
					$language = "pt-br";
					break;
				case "KR":
					$language = "ko";
					break;
				case "RU":
					$language = "ru";
					break;
				default:
					$language = "en";
		  }
	
		  //session_start();
		  //ob_start(); 
		  $data = array(
			  'secret' => RECAPTCHA_BACK,
			  'response' => $_POST['g-recaptcha-response']
		  );
		  # Create a connection
		  $url = 'https://www.google.com/recaptcha/api/siteverify';
		  $ch = curl_init($url);
		  # Form data string
		  $postString = http_build_query($data, '', '&');
		  # Setting our options
		  curl_setopt($ch, CURLOPT_POST, 1);
		  curl_setopt($ch, CURLOPT_POSTFIELDS, $postString);
		  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		  # Get the response
		  $response = curl_exec($ch);
		  curl_close($ch);
		  $dados = json_decode($response);

		  if(RECAPTCHA_ACTIVE==1){
			$captcha = $dados->success;
		  }else{
			$captcha = true;
		  }
	
		if ($captcha) {
		  
			$erro=0;
				
			  //Verifica se o username/login já existe na base
			  if($this->modelAccount->getUserLoginExist($login)!=1){
				$mensagem_erro .= "".$translate->_('login_exist')."<br>";
				$erro++;  
			  }
	
			  //Verifica se o email já existe na base
			  if($this->modelAccount->getUserEmailExist($email)!=1){
				$mensagem_erro .= "".$translate->_('Email already used in another account')."<br>";
				$erro++;  
			  }
			  
			  if(strlen($login)<3){
				$mensagem_erro .= "".$translate->_('3_characters_minimum')."<br>";
				$erro++;
			  }
			  
			  if(strlen($login)>20){
				$mensagem_erro .= "".$translate->_('maximum_20_characters')."<br>";
				$erro++;
			  }
			  
			  if (preg_match('/^[a-za-zA-Z\d_]{4,28}$/i', $login)) { 
	
			  } else {
				$mensagem_erro .= "".$translate->_('invalid_username')."<br>";
				$erro++;
			  }
	
			  #//se não encontrar @
			  if(!preg_match('/@/i', $email) ){
				$mensagem_erro .= "".$translate->_('invalid_email')."<br>";
				$erro++; 
			  }
			   
			  #//verificar se e-mail é igual
			  if($email!=$email2){
				$mensagem_erro .= "".$translate->_('The email/retype email does not match')."<br>";
				$erro++;
			  }
			  
			  if(empty($country)){
				$mensagem_erro .= "".$translate->_('Empty Country')."<br>";
				$erro++; 
			  }
			   
			  #//verificar se campo nome foi setado
			  if(empty($password)){
				$mensagem_erro .= "".$translate->_('Empty password')."<br>";
				$erro++; 
			  }
			  
			  if(strlen($password)<6){
				$mensagem_erro .= "".$translate->_('The password must be at least 6 characters')."<br>";
				$erro++;
			  }
	
			  if($this->modelIndex->getEasyPassword(sha1("x16".$password))!="success"){
				$mensagem_erro .= "".$translate->_('Please, type a stronger password')."<br>";
				$erro++;
			  }
	
			  #//verificar se campo nome foi setado
			  
			  if($password!=$repassword){
				$mensagem_erro .= "".$translate->_('The password does not match')."<br>";
				$erro++;
			  }
			  
			  if($terms != "checked"){
				$mensagem_erro .= "".$translate->_('You have to agree to the terms')."<br>";
				$erro++;
			  }
	
			   
			  #---------- Caso não tenha nenhum erro nos dados enviados ---------------------------
			   if($erro==0){
				
				$date = date("Y-m-d H:i:s");
				
				//Cadastra usuário no banco de dados
				$request["usr_login_id"] = $login;
				$request["usr_email"] = $email;
				$request["usr_password"] = $password;
				$request["usr_invited_id"] = $idSponsor;
				$request["usr_active"] = "n";
				$request["usr_reg_date"] = $date;
				$request["usr_language"] = $language;
				$request["usr_country"] = $country;
				
				$resUser = $this->modelAccount->setUser($request);
				
				# Faz a consulta do ID válido
				$regConf = $this->modelAccount->getUserId($login,$email);
				$numConf = @count($regConf);
				$idUsr = $regConf["usr_id"];
				$hashUsr = $regConf["usr_hash"];
	
				if($numConf==1){
	
					//Insere ID do usuário na tabela controle de perna e os valores padrões
					$this->model->setControlePerna($idUsr);
	
					//Registra posição na arvore binária de acordo com as legs do patrocinador
					$reg = $this->model->getPositionDad($idSponsor,$idUsr,$position);
					$idPai = $reg[0];
					$perna = $reg[1];
					
					$orgazinacao["idUsr"] = $idUsr;
					$orgazinacao["idPai"] = $idPai;
					$orgazinacao["perna"] = $perna;
					$orgazinacao["idPatrocinador"] = $idSponsor;
					$orgazinacao["date"] = $date;
					
					$sqlOrg = $this->model->setOrganizacao($orgazinacao);
	
					if($sqlOrg){
						$url = LINK_OFFICE . "/signup/confirmation?a=$idUsr&b=$hashUsr";
					  
						$emailTemplate = $this->modelAccount->getEmailTemplate("email_type='confirmation' AND email_status = 1",$language);
						$email_template = $emailTemplate["email_template"];
						$subject = $emailTemplate["email_title"];
					
						$email_template = str_replace('$email', $email, $email_template);
						$email_template = str_replace('$link_confirmation', $url, $email_template);
				
						$this->modelIndex->setZendMail($email,$subject,$email_template);

						$this->_redirector->gotoUrl(LINK_OFFICE. '/signup/registerend?id='.$idUsr.'&hash='.$hashUsr);

					}else{

					  $this->view->assign("error", "".$translate->_('Error when complete the registration! Contact Technical Support')." (".EMAIL_SUPPORT.")");
					  $this->view->assign('country', $country);
					  $this->view->assign('email', $email);
					  $this->view->assign('email2', $email2);
					  $this->view->assign('login', $login);
					}
					
				}
			  //fecha();
				 
			   }else{
				//$retorno['status'] = "error";
				//$retorno['message'] = '';
				  $this->view->assign("error", $mensagem_erro);
				  $this->view->assign('country', $country);
				  $this->view->assign('email', $email);
				  $this->view->assign('email2', $email2);
				  $this->view->assign('login', $login);
	
			   }
	
			  
			/*} else { //validacao act != 1
			  $retorno['status'] = "error";
			  $retorno['message'] = "Data do not match!";
			}*/
			
		  } else {
			$this->view->assign("error", $translate->_('verify_robot'));
			$this->view->assign('country', $country);
			$this->view->assign('email', $email);
			$this->view->assign('email2', $email2);
			$this->view->assign('login', $login);
			//Check if you are not a robot
		  }
		  //header('Content-Type: application/json');
		  //$this->_helper->json->sendJson($retorno);
		  //echo json_encode($retorno);
		  //echo $retorno;
		  //exit;
		}
	
	}
	
	public function registerendAction()
    {   
		$this->_helper->layout->setLayout('signup');

		//Cookie
		if(isset($_COOKIE['cookie_lang'])) {
			$idiomaBrowser = $_COOKIE['cookie_lang'];
		}else{
			$idiomas = explode(",",$_SERVER["HTTP_ACCEPT_LANGUAGE"]);
			$idiomaBrowser = $idiomas[0];
		}
		//Array com as frases da linguagem
		$resultLanguage = $this->modelAccount->getLanguage("$idiomaBrowser");
		//Pega a abreviação da linguagem
		$lang = $this->modelAccount->getLanguageAbb("$idiomaBrowser");
		$translate = new Zend_Translate(array('adapter' => 'array', 'content' => $resultLanguage, 'locale' => "$lang"));
		$this->view->tr = $translate;
		$this->view->lang = $lang;

		//--------------------------

		$id = $_GET['id'];
		$hash = $_GET["hash"];
		
		$dados = $this->modelAccount->getUserBasic($id);
		$this->view->assign('login',$dados["usr_login_id"]);
		
		$sessionNamespace = new Zend_Session_Namespace(SESSION_BASE);
		unset($sessionNamespace->usuario_indicador);
		
		if($dados["usr_active"] == 'y' || $dados["usr_hash"]!=$hash) {
			$this->_redirector->gotoUrl(LINK_OFFICE.'/signup/confirmation?a=a');
		}
        
    }

	public function sendAction() {

		//Cookie
		if(isset($_COOKIE['cookie_lang'])) {
			$idiomaBrowser = $_COOKIE['cookie_lang'];
		}else{
			$idiomas = explode(",",$_SERVER["HTTP_ACCEPT_LANGUAGE"]);
			$idiomaBrowser = $idiomas[0];
		}
		//Array com as frases da linguagem
		$resultLanguage = $this->modelAccount->getLanguage("$idiomaBrowser");
		//Pega a abreviação da linguagem
		$lang = $this->modelAccount->getLanguageAbb("$idiomaBrowser");
		$translate = new Zend_Translate(array('adapter' => 'array', 'content' => $resultLanguage, 'locale' => "$lang"));
		
		
		$user = $_GET['user'];
		
		//Pega os dados básicos do usuário
		$dados = $this->modelAccount->getUserBasic($id,$user);

		//Se usuário ainda naõ ativou, reenvia o email
		if ($dados["usr_active"] == 'n') {

			$url = LINK_OFFICE . "/signup/confirmation?a=".$dados["usr_id"]."&b=".$dados["usr_hash"]."";
			
			$emailTemplate = $this->modelAccount->getEmailTemplate("email_type='confirmation' AND email_status = 1",$dados["usr_language"]);
			$email_template = $emailTemplate["email_template"];
			$subject = $emailTemplate["email_title"];
						
			$email_template = str_replace('$email', $dados["usr_email"], $email_template);
			$email_template = str_replace('$link_confirmation', $url, $email_template);
			
			$return = $this->modelIndex->setZendMail($dados["usr_email"],$subject,$email_template);

			if ($return = "success") {
				$retorno['status'] = "success";
				$retorno['message'] = "".$translate->_('Email sent with success!')."";
			} else {
				$retorno['status'] = "error";
				$retorno['message'] = "".$translate->_('We had troubles sending the email. Please try again.')."";
			}
		
		} else {
			$retorno['status'] = "redirect";
			$retorno['message'] = LINK_OFFICE.'/signup/confirmation?a=a';
		}

		echo json_encode($retorno);
		exit;
    }

    public function confirmationAction() 
    {
      $this->_helper->layout->setLayout('signup');
      if ($_GET['a'] == 'a') {
        $this->view->status = "confirmado";
      } else {
        $id = $_GET['a'];
        $hash = $_GET['b'];
        //$sql = geraSQL("SELECT usr_login_id, usr_email, usr_hash, usr_active FROM tb_user WHERE usr_id='$id'");
		$dados = $this->modelAccount->getUserBasic($id);
      
        if (@count($dados) > 0) {

          $this->view->user = $dados["usr_login_id"];
		  
          if ($hash == $dados["usr_hash"]) {
            if ($dados["usr_active"] == 'n') {
				
				$this->modelAccount->updateUserActive($id);

				$emailTemplate = $this->modelAccount->getEmailTemplate("email_type='welcome' AND email_status = 1",$dados["usr_language"]);
				$email_template = $emailTemplate["email_template"];
				$subject = $emailTemplate["email_title"];
		
              	$email_template = str_replace('$username', $dados["usr_login_id"], $email_template);
				
				$return = $this->modelIndex->setZendMail($dados["usr_email"],$subject,$email_template);
            }
            
            $this->view->status = "confirmado";
          } else {
            $this->view->status = "erro";
          }
        } else {
          $this->view->status = "nouser";
        }
      }
    }
	
	public function promoterAction()
    { 
		$this->_helper->layout->setLayout('signup');
		
		//Cookie
		if(isset($_COOKIE['cookie_lang'])) {
			$idiomaBrowser = $_COOKIE['cookie_lang'];
		}else{
			$idiomas = explode(",",$_SERVER["HTTP_ACCEPT_LANGUAGE"]);
			$idiomaBrowser = $idiomas[0];
		}
		//Array com as frases da linguagem
		$resultLanguage = $this->modelAccount->getLanguage("$idiomaBrowser");
		//Pega a abreviação da linguagem
		$lang = $this->modelAccount->getLanguageAbb("$idiomaBrowser");
		$translate = new Zend_Translate(array('adapter' => 'array', 'content' => $resultLanguage, 'locale' => "$lang"));
		$this->view->tr = $translate;
		$this->view->lang = $lang;

    	$sessionNamespace = new Zend_Session_Namespace(SESSION_BASE);
		$userSponsor = $sessionNamespace->usuario_indicador;
		$result = $this->modelAccount->getUserBasic(null,$userSponsor);
		if(empty($result)){
			if ($result['usr_active'] == 'd') {
    			unset($sessionNamespace->usuario_indicador);
    		}
    		if ($_POST) {
    			$sponsor = $_POST['sponsor'];
    			$result2 = $this->modelAccount->getUserBasic(null,$sponsor);
    			if(!empty($result2) && $result2['usr_active'] != 'd'){
    				$sessionNamespace->usuario_indicador = $sponsor;
    				$this->_redirector->gotoUrl(LINK_OFFICE. '/signup');
    			} else {
    				$this->view->assign("error", ''.$translate->_("invalid_username_agent").'');
					$this->view->assign('sponsor', $sponsor);
    			}
			}
    	} else {
    		$this->_redirector->gotoUrl(LINK_OFFICE. '/signup');
    	}
    }
	
	public function validateusernameAction(){

		//Recebe os elementos via GET
		$login = addslashes($_GET['login']);
	
		if($this->modelAccount->getUserLoginExist($login)!=1)//se retornar algum resultado
			//Se o login já existir você exibe false
			echo 'false';
		else
			//Se o login não existir você exibe true
			echo 'true';
		exit();
		
	}
	
	public function validateemailAction(){

		//Recebe os elementos via GET
		$email = addslashes($_GET['email']);
		
		if($this->modelAccount->getUserEmailExist($email)!=1)//se retornar algum resultado
			//Se o email já existir você exibe false
			echo 'false';
		else
			//Se o email não existir você exibe true
			echo 'true';
		exit();
		
	}

	public function langAction(){

		$cookie_value = $_GET['lang'];
		$url = $_GET['url'];

		echo $url;

		setcookie('cookie_lang', $cookie_value, time() + (86400 * 30), "/");
		
		$this->_redirect('/'.$url.''); 

		exit();
	}
	
    public function refAction()
    {
        //$this->_helper->layout->disableLayout();
        //echo "Olá";
        $this->modelAccount = new Application_Model_Account();
        $sessionNamespace = new Zend_Session_Namespace(SESSION_BASE);
        $sessionNamespace->first_page = "yes";

        //print_r(Zend_Controller_Front::getInstance()->getRequest()->getRequestUri(),1);
    	
        $usuario_indicador = substr(Zend_Controller_Front::getInstance()->getRequest()->getRequestUri(),1);
        $usuario_indicador = explode("signup/ref/",$usuario_indicador);
        $usuario_indicador = $usuario_indicador[1];
    	//Zend_Debug::dump($usuario_indicador);
    	//$controllers = array("index", "signup", "roi", "news", "datacenter", "coinapult", "partnercentral", "?our"); 

    	if (!empty($usuario_indicador) && $usuario_indicador != "favicon.ico") {
    		//$sessionNamespace = new Zend_Session_Namespace(SESSION_BASE);
    		//echo $usuario_indicador."<br>";
            $result = $this->modelAccount->getUserBasic(null,$usuario_indicador);
            //Zend_Debug::dump($result);
            if(!empty($result) && $result['usr_active'] != 'd'){
                $sessionNamespace->usuario_indicador = $usuario_indicador;
            } else {
                unset($sessionNamespace->usuario_indicador);
            }

			$this->_redirect(LINK_OFFICE."/signup");
    	}else{
    	    //echo "Erro";
    	    $this->_redirect(LINK_OFFICE . "/home");
    	}

        //print_r($_SESSION);
        exit;
		
	}	
	
}
