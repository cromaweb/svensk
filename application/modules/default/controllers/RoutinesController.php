<?php

class Default_RoutinesController extends SYSTEM_Controllers_Office
{
    
    protected $txs_confirmations = 3;
	
	function init() 
    { 
		parent::init();
        $this->_helper->layout->disableLayout();
    } 
	
    public function indexAction()
    {	
			//$this->_redirect(LINK_ADMIN);
		
	}
	
	public function paymonthlyinvoiceAction()
	{
		$this->modelOrder = new Application_Model_Order();
		$this->modelEwallet = new Application_Model_Ewallet();
		$this->modelAccount = new Application_Model_Account();
		
		//Cancela as faturas vencidas a mais de dois dias
		$this->modelOrder->setCancelOverdueInvoice(1);
		
		$invoices = $this->modelOrder->getMonthlyInvoices();
		//Zend_Debug::dump($invoices);
		
		foreach($invoices as $reg)
		{
			$id = $reg["fat_id"];
			$user = $reg["fat_idUsuario"];
			$produto = $reg["fat_produto"];
			echo "ID: $id | $user<br>";
			
			if($produto==1 || $produto==2)
			{
				echo "Account<br>";
				//list($package) = abreSQL("SELECT usr_package FROM tb_user WHERE usr_id='$user'");
				$package = $this->modelAccount->getUserPackage($user);
				echo "Pkg: $package<br>";
				if($package==1)
				{
					//Verifica a quantidade de registro com o produto PRO pago
					//list($countFat) = abreSQL("SELECT count(fat_id) FROM tb_fatura WHERE fat_produto=1 AND fat_status=2 AND fat_idUsuario='$user'");
					$countFat = $this->modelOrder->getAmountInvoicesPaidProductsUser($user,1);
					//Se for maior que zero, quita a fatura mensal
					if($countFat>0)
					{
						echo "Fat 1 | $id | $countFat<br>";
						$this->modelEwallet->payInvoice($user, 'Balance', $id);
					}
				}elseif($package==2)
				{
					//Verifica a quantidade de registro com o produto PRO pago
					//list($countFat) = abreSQL("SELECT count(fat_id) FROM tb_fatura WHERE fat_produto=2 AND fat_status=2 AND fat_idUsuario='$user'");
					$countFat = $this->modelOrder->getAmountInvoicesPaidProductsUser($user,2);
					//Se for maior que zero, quita a fatura mensal
					if($countFat>0)
					{
						echo "Fat 2 | $id | $countFat<br>";
						$this->modelEwallet->payInvoice($user, 'Balance', $id);
					}
				}
			}elseif($produto==4)
			{
				echo "Plan | $id<br>";
				$this->modelEwallet->payInvoice($user, 'Balance', $id);
				
			}
					
	
			echo "<br>Final<br>";
		}
		
		exit();
		
			
	}
	
	public function blockchainexplorerAction()
	{
	    $this->modelRoutines = new Application_Model_Routines();
	    $this->modelDeposit = new Application_Model_Deposit();
        $this->modelIndex = new Application_Model_Index();
        $this->modelEwallet = new Application_Model_Ewallet();
        $this->modelAccount = new Application_Model_Account();
        $this->modelBonus = new Application_Model_Bonus();
	    
        $USDBTC = $this->modelIndex->getUSDBTC();

        $server_security = "server.ethernize.com";
        
        if($_GET["val"]==$server_security){
                                
            $address = $this->modelRoutines->getUsedAddresses();

            foreach ($address as $key => $value) {
                
                $user = $value->wallet_address_user_id;
                $wallet_address_balance = $value->wallet_address_balance;

                $wallet_user = $this->modelDeposit->getUserWallet($user);
                $wallet_id = $wallet_user["wallet_id"];
                $wallet_amount = $wallet_user["wallet_amount"];

                // $wallet_address = $this->modelDeposit->getWalletAddress($value->wallet_address_address);
                // $wallet_address_id = $wallet_address["wallet_address_id"];
                // $wallet_address_balance = $wallet_address["wallet_address_balance"];

                echo " Address: ".$value->wallet_address_address." | Balance: ".$value->wallet_address_balance."<br>";
                
                $curl = curl_init();

                curl_setopt_array($curl, array(
                    CURLOPT_URL => "https://blockchain.info/q/getreceivedbyaddress/" . trim($value->wallet_address_address),
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_TIMEOUT => 30000,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "GET",
                    CURLOPT_HTTPHEADER => array(
                        // Set Here Your Requesred Headers
                        'Content-Type: application/json',
                    ),
                ));

                $response = curl_exec($curl);
                $err = curl_error($curl);
                curl_close($curl);

                if ($err) {
                    return 'Curl Error';
                }

                echo $response."<br>";

                $response = (int) $response; 
                //Se o saldo recebido for maior do que o saldo do usuário, ele faz a subtração
                if($response>$wallet_address_balance && $response!=404){

                    //Pega a quantidade de satoshis recebidos no endereço e diminui o saldo atual para ver o resultado
                    $balance = $response - $wallet_address_balance;
                    //Cria um hash com endereço e timestamp
                    $hash = trim($value->wallet_address_address)."|".time();

                    $wallet_transaction["wallet_transaction_wallet_id"] = $wallet_id;
                    $wallet_transaction["wallet_transaction_hash"] = $hash;
                    $wallet_transaction["wallet_transaction_from"] = $response;
                    $wallet_transaction["wallet_transaction_to"] = $wallet_address_balance;
                    $wallet_transaction["wallet_transaction_operation"] = 0;
                    $wallet_transaction["wallet_transaction_amount"] = $balance;

                    if($this->modelRoutines->setWalletTransaction($wallet_transaction)) {
                    echo "Trasaction saved. Updating wallet_address balance<br>";
                        //Pega o resultado final e divide pela quantidade de satoshis do BTC e soma ao saldo existente na carteira
                        $wallet_amount +=  $balance;
                        echo "Wallet:".$wallet_amount."<br>";
                        //Seta o saldo da Wallet_user e do wallet_address
                        $this->modelDeposit->setUserWalletBalance($wallet_id,$wallet_amount,$value->wallet_address_id,$response);

                        //Add credit
                        $check_hash_credit = $this->modelRoutines->getWalletTransaction($hash);
                        $transactionID = $check_hash_credit["wallet_transaction_id"];
                        echo "Transaction:".$transactionID."<br>";

                        $bitcoin = ($balance / 100000000);
                        //Verifica se tem fatura aberta com o mesmo tanto de BTC depositado
                        $valueDeposit = $this->modelDeposit->getDepositRoutine($user,$bitcoin);
                        echo "Value deposit:".$valueDeposit."<br>";
                        //Se o valor for diferente de zero
                        if($valueDeposit>0){
                            $creditValue = $valueDeposit;
                        }else{
                            $creditValue = $USDBTC * $bitcoin;
                        }
                        
                        $message = "Deposit BTC ".$transactionID;
                        $this->modelDeposit->addCredit($user,$creditValue,$message);


                        ## Envia email informando o depóstio

                        //Busca os dados do usuário
                        $user_doc = $this->modelAccount->getUserBasic($user);
                        $username = $user_doc["usr_login_id"];
                        $email = $user_doc["usr_email"];

                        //Para o cliente
                        $emailTemplate = $this->modelAccount->getEmailTemplate("email_type='deposit' AND email_status = 1",$user_doc["usr_language"]);
                        $email_template = $emailTemplate["email_template"];
                        $subject = $emailTemplate["email_title"];
                        
                        $email_template = str_replace('$username', $username, $email_template);
                        $email_template = str_replace('$bitcoin', $bitcoin, $email_template);
                        $email_template = str_replace('$usdt', $creditValue, $email_template);

                        $this->modelIndex->setZendMail($email,$subject,$email_template);

                        //Para o administrador

                        $email = EMAIL_ADMIN_DEPOSIT;

                        $emailTemplate = $this->modelAccount->getEmailTemplate("email_type='deposit_admin' AND email_status = 1");
                        $email_template = $emailTemplate["email_template"];
                        $subject = $emailTemplate["email_title"];
                        
                        $email_template = str_replace('$bitcoin', $bitcoin, $email_template);
                        $email_template = str_replace('$usdt', $creditValue, $email_template);

                        $this->modelIndex->setZendMail($email,$subject,$email_template);

                    } else {
                    echo 'Erro ao criar transação';
                    }

                }

            }

        }
        
		exit();
			
    }
	
    protected function blockHeigth() {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://blockchain.info/latestblock",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                // Set Here Your Requesred Headers
                'Content-Type: application/json',
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
            return $this->sendError('Curl Error', $err);
        } 

        return json_decode($response);
    }

    public function d4signAction() {
        $this->modelD4sign = new Application_Model_D4sign();
        $this->modelOrder = new Application_Model_Order();
        $this->modelAccount = new Application_Model_Account();
        $this->modelEwallet = new Application_Model_Ewallet();

        //Se tiver post
        if($_POST){

            $uuid = $_POST["uuid"];
            $type_post = $_POST["type_post"]; //4
            $message = $_POST["message"]; //4
            $email = $_POST["email"];

            //echo $uuid." | ".$type_post." | ".$message." | ".$email;

            // $retorno = array(
            //     "uuid" => $uuid,
            //     "type_post" => $type_post,
            //     "message" => $message);

            //Busca a fatura por ID do contrato
            $invoice = $this->modelOrder->getInvoiceContract($uuid);
            //Busca os dados do usuário da fatura
            $user = $this->modelAccount->getUserBasic($invoice["fat_idUsuario"]);

            $this->modelD4sign->setLog($user["usr_id"],"Webhook: $uuid | $type_post | $message | $email ");

            //Se o usuário via webhook for igual ao do usuário do contrato
            if($email==$user["usr_email"] && $invoice["fat_contrato"]==1 && $type_post=="4"){
                //Atualiza o contrato
                $this->modelD4sign->updateInvoiceContract($invoice["fat_id"],2,$uuid,$invoice["fat_key_signer"]);

                //Faz o processo de pagamento da fatura, caso houver saldo
                $invoiceID = $invoice["fat_id"];
                //Pega os dados da fatura
                $sqlVerify = $this->modelOrder->getInvoiceUser($invoice["fat_id"],$user["usr_id"]);
                //Valor da fatura
                $valueInvoice = $sqlVerify["fat_valor"];
                //Pega o saldo do usuário
                $balance = $this->modelEwallet->getBalance($user["usr_id"]);
                //Saldo da wallet de compra
                $balance = $balance["bsal_saldo_liberado"];
    
                //Se a fatura não exister ou já estar paga
                if (!$sqlVerify)
                {
                    $retorno = "Username ou fatura inválida / paga!<br>";
                
                //Se o saldo for menor que o valor da fatura, sem saldo suficiente
                }elseif($balance < $valueInvoice){
    
                    $retorno = "Não há saldo para pagar a fatura<br>";
    
                }else {
                    $paidInvoice = $this->modelEwallet->payInvoice($user["usr_id"], '1', $invoiceID, $p = "");
    
                    $retorno = $paidInvoice;
                    
                }

                //Grava log de pagamento da fatura
                $this->modelD4sign->setLog($user["usr_id"],"Invoice return: $invoiceID | $retorno ");
            }

        }
        exit();
    }

    public function agentd4signAction(){
        $this->modelD4sign = new Application_Model_D4sign();
        $this->modelOrder = new Application_Model_Order();
        $this->modelAccount = new Application_Model_Account();

        //Se tiver post
        if($_POST){

            $uuid = $_POST["uuid"];
            $type_post = $_POST["type_post"]; //4
            $message = $_POST["message"]; //4
            $email = $_POST["email"];

            //echo $uuid." | ".$type_post." | ".$message." | ".$email;

            // $retorno = array(
            //     "uuid" => $uuid,
            //     "type_post" => $type_post,
            //     "message" => $message);

            //Busca a fatura por ID do contrato
            $user = $this->modelAccount->getUserContract($uuid);

            $this->modelD4sign->setLog($user["usr_id"],"Webhook Agent: $uuid | $type_post | $message | $email ");

            //Se o usuário via webhook for igual ao do usuário do contrato
            if($email==$user["usr_email"] && $user["usr_contract"]==1 && $type_post=="4"){
                //Atualiza o contrato
                $this->modelD4sign->updateAgentContract($user["usr_id"],2,$uuid,$user["usr_key_signer"]);
            }

        }
        exit();
    }

    public function invaliddocumentAction()
    {

        //$this->_helper->layout->disableLayout();

        $this->modelIndex = new Application_Model_Index();
        $this->modelAccount = new Application_Model_Account();

        //Recebe os elementos via GET
        $user = addslashes($_GET['user']);
        $reason = addslashes($_GET['reason']);

        if(!empty($user))
        {

            //Busca os dados do usuário
            $user_doc = $this->modelAccount->getUserBasic($user);
            $username = $user_doc["usr_login_id"];
            $email = $user_doc["usr_email"];
            $url = LINK_OFFICE . "/documentation";

            //Se exister o username
            if(!empty($username)){

                $emailTemplate = $this->modelAccount->getEmailTemplate("email_type='validate_account' AND email_status = 1",$user_doc["usr_language"]);
                $email_template = $emailTemplate["email_template"];
                $subject = $emailTemplate["email_title"];
                
                $email_template = str_replace('$username', $username, $email_template);
                $email_template = str_replace('$link', $url, $email_template);
                $email_template = str_replace('$reason', $reason, $email_template);

                if($this->modelIndex->setZendMail($email,$subject,$email_template)){
                    echo "Ok";
                }else{
                    echo "Error";
                }

            }else{
                echo "Error 1";
            }
        }else{
            echo "Error 2";
        }
        
        exit();

    }

    public function approveddocumentAction()
    {

        //$this->_helper->layout->disableLayout();

        $this->modelIndex = new Application_Model_Index();
        $this->modelAccount = new Application_Model_Account();

        //Recebe os elementos via GET
        $user = addslashes($_GET['user']);

        if(!empty($user))
        {

            //Busca os dados do usuário
            $user_doc = $this->modelAccount->getUserBasic($user);
            $username = $user_doc["usr_login_id"];
            $email = $user_doc["usr_email"];
            $url = LINK_OFFICE . "/documentation";

            //Se exister o username
            if(!empty($username)){

                $emailTemplate = $this->modelAccount->getEmailTemplate("email_type='verified_account' AND email_status = 1",$user_doc["usr_language"]);
                $email_template = $emailTemplate["email_template"];
                $subject = $emailTemplate["email_title"];
                
                $email_template = str_replace('$username', $username, $email_template);
                $email_template = str_replace('$link', $url, $email_template);

                if($this->modelIndex->setZendMail($email,$subject,$email_template)){
                    echo "Ok";
                }else{
                    echo "Error";
                }

            }else{
                echo "Error 1";
            }
        }else{
            echo "Error 2";
        }
        
        exit();

    }
	
}