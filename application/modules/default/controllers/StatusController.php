<?php
class Default_StatusController extends SYSTEM_Controllers_Office
{
    public function init() {
        parent::init();
		$this->modelIndex = new Application_Model_Index();
		$this->model = new Application_Model_Statusexecutive();
		$this->modelAccount = new Application_Model_Account();
		$this->view->page_main = 'Status';
		$this->view->page_link = LINK_OFFICE."/status/";
		$this->view->subpages = $this->modelIndex->getSubPages("statusexecutive");
    }
  
    public function indexAction()
    {
		$user = $this->view->User;	
		$this->view->user = $user;
		//Retorna a pontua��o total do usu�rio
		$this->view->points = $this->model->getPoints($user);
		//Retorna o t�tulo do usu�rio
		$this->view->status = $this->modelAccount->getUserStatusID($user);

	}
	
}

?>