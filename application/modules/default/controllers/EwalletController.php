<?php
class Default_EwalletController extends SYSTEM_Controllers_Office
{
    public function init() {
        parent::init();
		$this->model = new Application_Model_Ewallet();
		$this->modelIndex = new Application_Model_Index();
		$this->modelWithdrawal = new Application_Model_Withdrawal();
		$this->modelAccount2 = new Application_Model_Account();
		$this->view->page_main = 'Carteira';
		$this->view->page_link = LINK_OFFICE."/ewallet/";
		//Pega a situa��o se o usu�rio pode transferir ou n�o valores
		$transfer = $this->model->getEwalletTransfer($this->view->User);
		$this->view->subpages = $this->modelIndex->getSubPages("ewallet",$transfer);

		$this->_redirector = $this->_helper->getHelper('Redirector');

		$package = $this->modelAccount2->getUserPackage($this->view->User);
	 	if (empty($package)) {
			$package = 0;
		}

	 	switch ($package) {
	 		case 5:
	 		case 0:
	 			//ACCOUNT MEMBER
	 			$this->view->accountTitle = "member";
	 			break;
	 		case 6:
	 			//ACCOUNT STARTER
	 			$this->view->accountTitle = "starter";
	 			break;
	 		case 1:
	 			//ACCOUNT PRO
	 			$this->view->accountTitle = "pro";
	 			break;
	 		case 2:
	 			//ACCOUNT PRO+
	 			$this->view->accountTitle = "proplus";
	 			break;
	 		
	 		default:
	 			die("Invalid package. Please contact us.");
	 			break;
	 	}
    }
  
    public function indexAction()
    {
		try
		{
			$user = $this->view->User;
			$this->view->user = $user;
		
			$val = $_POST["check"];
			$semana = $_POST["week"];
			$typeBonus = $_POST["typeBonus"];
			
			$accumulatedPoints = $this->model->getAccumulatedPoints($user);
			$pontosAcumuladosEsq = $accumulatedPoints["busr_pontos_acumulados_esquerda"];
			$pontosAcumuladosDir = $accumulatedPoints["busr_pontos_acumulados_direita"];
			
			$consolidatedBonus = $this->model->getConsolidatedBonus($user);
			$valor = $consolidatedBonus["bcons_valor_startfast"];
			$pontos = $consolidatedBonus["bcons_pontos_unilevel"];

			$this->view->pontos = $pontos;
			
			//list($availableBalance,$lockedBalance,$balanceProcess) = abreSQL("SELECT bsal_saldo_liberado,bsal_saldo_bloqueado,bsal_saldo_areceber FROM tb_financeiro_saldo WHERE bsal_idUsuario='$user'");
			$balance = $this->model->getBalance($user);
			$availableBalance = $balance["bsal_saldo_liberado"];
			$balanceProcess = $balance["bsal_saldo_areceber"];
			$balanceEarnings = $balance["bsal_saldo_rendimento"];
			$balanceComissions = $balance["bsal_saldo_comissoes"];
			$balanceBitwyn = $balance["bsal_saldo_bitwyn"];
				
			$this->view->availableBalance = $availableBalance;
			$this->view->balanceProcess = $balanceProcess;
			$this->view->balanceEarnings = $balanceEarnings;
			$this->view->balanceComissions = $balanceComissions;
			$this->view->balanceBitwyn = $balanceBitwyn;

			$this->view->rbma = $this->model->getRBMA();
			
			
			//Cotação BTC/BRL
			$this->view->btc_brl = $this->modelIndex->getBRLBTC();
			
			//Lista
			$ewallet_active = $this->model->getEwalletActive($user);
			$this->view->ewallet_active = $ewallet_active;
		
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error +3");
			
		}

	}
	
	//FUNCTION TRANSFER
    public function transferAction()
    {
		$this->modelAccount = new Application_Model_Account();
		$this->modelBonus = new Application_Model_Bonus();
		
		try
		{
			$user = $this->view->User;
			$this->view->user = $user;
			$this->view->openDeposit = $this->modelBonus->getOpenDeposit($user);
		
			$val = $_POST["check"];
			$semana = $_POST["week"];
			$typeBonus = $_POST["typeBonus"];
			
			
			//list($availableBalance,$lockedBalance,$balanceProcess) = abreSQL("SELECT bsal_saldo_liberado,bsal_saldo_bloqueado,bsal_saldo_areceber FROM tb_financeiro_saldo WHERE bsal_idUsuario='$user'");
			$balance = $this->model->getBalance($user);
			$availableBalance = $balance["bsal_saldo_liberado"];
			$balanceProcess = $balance["bsal_saldo_areceber"];
			$balanceEarnings = $balance["bsal_saldo_rendimento"];
			$balanceComissions = $balance["bsal_saldo_comissoes"];
				
			$this->view->availableBalance = $availableBalance;
			$this->view->balanceProcess = $balanceProcess;
			$this->view->balanceEarnings = $balanceEarnings;
			$this->view->balanceComissions = $balanceComissions;
			
			//Lista
			//list($ewallet_active) = abreSQL("select usr_ewallet_active from tb_user where usr_id = " . $user);
			$ewallet_active = $this->model->getEwalletActive($user);
			$this->view->ewallet_active = $ewallet_active;
			//Conta/package do usu�rio
			$package = $this->modelAccount->getUserPackage($user);
			$this->view->package = $package;
			//Conta transfer
			$transfer = $this->model->getEwalletTransfer($user);
			$this->view->transfer = $transfer;
		
		}catch(Exception $e){
			die($e->getMessage());
			//die("Error +3");
			
		}

	}

	public function transferreceiptAction()
    {
    	$user = $this->view->User;
    	print_r($this->model->getTransfers($user));
    	print_r($this->model->getTransferByHash("76a63dd9490c4514188e3e77b5dff48112312"));
    	print_r($this->model->getTransferById(1));
    	exit;
    }
	
	//FUNCTION TRANSFERCREDIT
	public function transfercreditAction(){
		
		$this->modelAccount = new Application_Model_Account();
		$this->modelOrder = new Application_Model_Order();
		
		$user = $this->view->User;
		$package = $this->modelAccount->getUserPackage($user);
		$transfer = $this->model->getEwalletTransfer($user);		
		//Se a Conta do usu�rio for de PRO+ pra cima, transfere
		//if($package>=2)
		//if($transfer=="s")
		//{

			$pin = $_POST["trans_pin"];
			$checkpin = $this->modelAccount->checkPin($user, $pin);
	
			if ($checkpin == 'valid') {
				$op = addslashes($_POST["act"]);
				$username = addslashes($_POST["username"]);
				$accountfrom = addslashes($_POST["accountfrom"]);

				$dollar = str_replace('$ ','',$_POST['transferValue']);
		    	$dollar = str_replace(',','',$dollar);
		    	$dollar = floatval($dollar);
		    	$value = round($dollar, 2);

				//$value = str_replace(',','',addslashes($_POST["transferValue"]));
				
				//list($usercredit) = abreSQL("SELECT usr_id FROM tb_user WHERE usr_login_id='$username'");
				$idUser = $this->modelAccount->getUserId($username);
				$usercredit = $idUser["usr_id"];
				//$return = transferCredit($codeOfc,$usercredit,$value);
				if (!empty($usercredit)) { 
		    		if ($accountfrom == "r" || $accountfrom == "c" || $accountfrom == "l" || $accountfrom == "b") {
	    				$balance = $this->model->getBalance($user);
	    				if ($balance['bsal_saldo_liberado'] >= 0 || $user == $usercredit) {
							if($accountfrom == "b"){
								$valueTransfers = $this->model->getTransfersByMonth($user,"b");
								$valueTransfers = $valueTransfers['sum_value'] + $value;
								$bonusLimit = $this->model->getLimitBonus($user);
								$valueInvestiment = $bonusLimit['busr_investimento_total'];
								
								if($valueTransfers<=$valueInvestiment){
									$return = $this->model->setTransferCredit($user,$usercredit,$value,$accountfrom);
								}else{
									$return = 3;
								}
								
							}else{
								$return = $this->model->setTransferCredit($user,$usercredit,$value,$accountfrom);
							}
								
							if($return==1){
								$retorno = "transferok";
							}elseif ($return == 2) {
								$retorno = "<p style='color:red'>Erro, não pode transferir para a conta de origem!</p>";
							}elseif ($return == 3) {
								$retorno = "<p style='color:red'>Erro, o limite de transferência mensal (Transferências: $ $valueTransfers > Investimento : $ $valueInvestiment) foi excedido!</p>";
							}elseif ($return == 4) {
								$retorno = "<p style='color:red'>Erro, o valor mínimo para a transferência são $ 10,00!</p>";
							} else {
								$retorno = "<p style='color:red'>Erro, sem saldo para transferir!</p>";
							}
						} else {
							$retorno = "<p style='color:red'>Sua carteira Saldo Disponível não pode ter saldo negativa. Você pode fazer transferências de Saldo Rendimentos ou da Saldo Comissões para tornar sua Carteira Especial positiva.</p>";
						}
			    	} else {
						$retorno = "<p style='color:red'>Carteira de envio inválida, por favor escolha entre carteira Disponível, Rendimentos ou Comissões.</p>";
					}
				} else {
					$retorno = "<p style='color:red'>Username não encontrado!</p>";
				}
			} else {
				$retorno = "<p style='color:red'>".$checkpin."</p>";
			} 
			//echo $retorno;
			$this->_helper->json->sendJson($retorno);
		//}
		exit();
	}
	
/*	//FUNCTION PAYMENTS
	public function paymentsAction()
	{
		try
		{
			$user = $this->view->User;
			$this->view->user = $user;
		
			$val = $_POST["check"];
			$typeBonus = $_POST["typeBonus"];
	
			$balance = $this->model->getBalance($user);
			$availableBalance = $balance["bsal_saldo_liberado"];
			$lockedBalance = $balance["bsal_saldo_bloqueado"];
			$balanceProcess = $balance["bsal_saldo_areceber"];
				
			$this->view->availableBalance = $availableBalance;
			$this->view->lockedBalance = $lockedBalance;
			$this->view->balanceProcess = $balanceProcess;
			
			//Lista
			//list($ewallet_active) = abreSQL("select usr_ewallet_active from tb_user where usr_id = " . $user);
			$ewallet_active = $this->model->getEwalletActive($user);
			$this->view->ewallet_active = $ewallet_active;
		
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error +4");
			
		}
	}*/


	
	//FUNCTION TRANSACTIONS
    public function transactionsAction()
    {	
		$this->modelTransactions = new Application_Model_Transactions();
		$this->modelAccount = new Application_Model_Account();
		
		$user = $this->view->User;	
		$registered = $this->modelAccount->getUserRegistered($user);
		$registered	= explode(" ", $registered);
		$this->view->registered = $registered[0];
		
		//list($totalLancamentos) = abreSQL("SELECT count(*) FROM tb_financeiro_extrato WHRE extf_idUsuario = " . $user);
		//$totalLancamentos = $this->model->getFinancialStatementAmount($user);
		

		if ($_POST) {
			$url = LINK_OFFICE . '/ewallet/transacoes';
			if (!empty($_POST['date'])) {
				$url .= '/date/'.$_POST['date'];
			}

			if (!empty($_POST['balance'])) {
				$url .= '/balance/'.$_POST['balance'];
			}

			if (!empty($_POST['order']) && $_POST['order'] >= 1 && $_POST['order'] <= 2) {
				$url .= '/order/'.$_POST['order'];
			}

			$this->_redirector->gotoUrl($url);
		}


		$search = $this->_getParam('date', 1);
		$this->view->search = $search;
		if($search == 1)
		{
			$monthYear = date("m-Y");	
		}else{
			$monthYear = $search;
		}

		$filterAccount = $this->_getParam('balance', 'all');
		$this->view->filterAccount = $filterAccount;
		switch ($filterAccount) {
		 	case 'available':
		 		$accountFrom = "l";
		 		break;
		 	case 'earnings':
		 		$accountFrom = "r";
		 		break;
		 	case 'comissions':
		 		$accountFrom = "c";
		 		break;
		 	case 'bitwyn':
		 		$accountFrom = "b";
		 		break;
		 	default:
		 		$accountFrom = "";
		 		break;
		}

		if (!empty($this->_getParam('order')) && $this->_getParam('order') >= 1 && $this->_getParam('order') <= 2) {
			$this->view->order = $this->_getParam('order');
		} else {
			$this->view->order = 1;
		}
		
		if ($this->view->order == 1) {
			$order = 'DESC';
		} else {
			$order = 'ASC';
		}
		//$this->view->extrato = geraSQL("select * from tb_financeiro_extrato  where extf_idUsuario = " . $user . $where . " order by extf_data ". $list);

		$extrato = $this->modelTransactions->getFinancialStatement($user,$monthYear,$order,$accountFrom);
		
        // Create a Paginator for the blog posts query
        $paginator = Zend_Paginator::factory($extrato);
		
		//Number for page
		$paginator->setItemCountPerPage(30);
		
		// Read the current page number from the request. Default to 1 if no explicit page number is provided.
		$paginator->setCurrentPageNumber($this->_getParam('page', 1));
		
        // Assign the Paginator object to the view
        $this->view->extrato = $paginator;

        $balance = $this->model->getBalance($user);

		$this->view->assign('availableBalance', $balance["bsal_saldo_liberado"]);
		$this->view->assign('balanceProcess', $balance["bsal_saldo_areceber"]);
		$this->view->assign('balanceEarnings', $balance["bsal_saldo_rendimento"]);
		$this->view->assign('balanceComissions', $balance["bsal_saldo_comissoes"]);
	}
	
	public function checkinvoiceAction(){

		try
		{
			$this->modelAccount = new Application_Model_Account();
			$this->modelOrder = new Application_Model_Order();
	
			$op = addslashes($_POST["act"]);
			$fatura = addslashes($_POST["invoice"]);
			$username = addslashes($_POST["username"]);
			
			//list($idusr) = abreSQL("SELECT usr_id FROM tb_user WHERE usr_login_id='$username'");		
			$idUser = $this->modelAccount->getUserId($username);
			$idUser = $idUser["usr_id"];
				
			if($op==1){
	
				/*list($fat_descricao,$fat_valor) = abreSQL("SELECT fat_descricao,fat_valor 
															FROM tb_fatura,tb_user
															WHERE usr_id = fat_idUsuario
															AND fat_id='$fatura' 
															AND usr_login_id='$username'
															AND fat_status='1'");*/
				$sqlVerify = $this->modelOrder->getInvoiceUser($fatura,$idUser);
				
				$fat_valor = $sqlVerify["fat_valor"];
				$fat_descricao = $sqlVerify["fat_descricao"];
				
				if(!empty($fat_valor))
				{
					//list($nome) = abreSQL("SELECT usr_name FROM tb_user WHERE usr_login_id='$username'");
					
					// meu array
					$informacoes = array('iduser'=>$idusr,'username'=>$username,'invoice'=>$fatura,'description'=>$fat_descricao,'price'=>'US$'.$fat_valor);
					echo json_encode($informacoes);
				  
				}else{
					$informacoes = array('err'=>'<p style="color:red">Username or Invoice invalid/paid!</p>');
					echo json_encode($informacoes) ;
				}
			
			}	
		
			exit();
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error +4");
			
		}
	}
	
	public function payinvoiceAction(){

		try
		{
			$this->modelAccount = new Application_Model_Account();
			$this->modelOrder = new Application_Model_Order();
	
			$user = $this->view->User;
			$pin = addslashes($_POST["payInv_pin"]);
			
			$checkpin = $this->modelAccount->checkPin($user, $pin);
			//$checkpin = checkPin($user, $pin);
			if ($checkpin == 'valid') {
				
				$pay = $_POST["pay"];
				$pay_username = addslashes($_POST["pay_username"]);
				$pay_invoice = addslashes($_POST["pay_invoice"]);
				$balance = $this->model->getBalance($user);
				$balance = $balance["bsal_saldo_liberado"];
				
				if ($pay == 1)
				{
					$idUser = $this->modelAccount->getUserId($pay_username);
					$idUser = $idUser["usr_id"];
					
					$sqlVerify = $this->modelOrder->getInvoiceUser($pay_invoice,$idUser);
	
					$valueInvoice = $sqlVerify["fat_valor"];
				
					if (!$sqlVerify)
					{
					  $retorno = "<p style='color:red'>Username or Invoice invalid/paid!</p>";
					} 
					elseif($balance < $valueInvoice){
					  $retorno = "<p style='color:red'>No balance to pay the invoice</p>";
					} else {
					  $paidInvoice = $this->model->payInvoice($user, '1', $pay_invoice);
					  //echo "<p style='color:red'>Paid invoice!</p>";
					  if($user != $idUser){
						$this->modelOrder->setLog("Indirect Payment Invoice ".$pay_invoice, $user, $idUser);
					  }
					  $retorno = $paidInvoice;
					}
				}
			} else {
				$retorno = "<p style='color:red'>".$checkpin."-".$pay_username."</p>";
			} 	
			$this->_helper->json->sendJson($retorno);		
			exit();
		
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error +4");
			
		}
	}
	
	public function testeAction()
	{
			$user = $this->view->User;
			$this->view->user = $user;
		Zend_Debug::dump($this->model->getEwalletTransfer($user));
		exit;
		
	}
}

?>