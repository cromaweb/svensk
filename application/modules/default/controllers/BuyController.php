<?php
class Default_BuyController extends SYSTEM_Controllers_Office
{
    public function init() {
        parent::init();
		$this->modelEwallet = new Application_Model_Ewallet();
		$this->modelAccount = new Application_Model_Account();
		$this->model = new Application_Model_Order();
		$this->modelIndex = new Application_Model_Index();
		$this->view->subpages = $this->modelIndex->getSubPages("order");
		
		$user = $this->view->User;
		
		$balance = $this->modelEwallet->getBalance($user);
		$this->view->availableBalance = $balance["bsal_saldo_liberado"];
		$this->view->balanceProcess = $balance["bsal_saldo_areceber"];
		$this->view->balanceEarnings = $balance["bsal_saldo_rendimento"];
		$this->view->balanceComissions = $balance["bsal_saldo_comissoes"];


    }
  
    public function indexAction()
    {	
		try
		{
			$user = $this->view->User;

			//Tradução
			$sessao = new Zend_Session_Namespace(SESSION_OFFICE);
			$arrayLanguage = $sessao->arrayLanguage;
			$userLanguage = $sessao->userLanguage;
		
			$translate = new Zend_Translate(array('adapter' => 'array', 'content' => $arrayLanguage, 'locale' => "$userLanguage"));
			
			$pedido			= $_POST["order"];
			$periodo		= $_POST["period_income"];
			$value			= (int)  $_POST["value"];
			$cancelar		= $_POST["cancel"];
			$cancIdFatura	= $_POST["cancIdFatura"];
			//$qtde_produto = (int)  $_POST["qtPacka"];
			$qtde_produto 	= 1;
			
			//Verifica na fatura aberta a quantidade de itens dos pedidos de planos
			//list($itens) = abreSQL("SELECT SUM(fat_itens) FROM tb_fatura WHERE fat_idUsuario='$user' AND fat_itens <> 0 AND fat_status=1");
			$itens = $this->model->getItensInvoicePlan($user);
			//Verifica se � um plano, se for plano n�o trava limite de pedidos
			//list($qtdePlanos) = abreSQL("SELECT count(*) FROM tb_user_plans WHERE uplan_user_id='$user' AND uplan_status=1");
			$qtdePlanos = $this->model->getQtPlans($user);
			
			$qtdePlanos = $qtdePlanos + $itens;
			$this->view->qtdePlanos = $qtdePlanos;
					
			if ($cancelar == 1){ $this->model->cancelOrder($user, $cancIdFatura); }
	
			//### PEDIDO - onde começa o pedido ###
			if ($pedido == 1) 
			{
			    
			    if($pedido == 1 && $periodo == 12)
			    {
			        $id_produto = 1;
			    }
			    
				if (isset($id_produto))
				 {
					 //Verifica se já possui algum pedido aberto
					$pedAberto = $this->model->getOpenPurchase($user,$id_produto);
					//Pega o tipo de conta/pacote selelcionado pelo usuário
					//list($existe, $ordemPedido,$prodEspecial) = abreSQL("SELECT count(prod_id) AS existe, prod_ordem, prod_especial FROM tb_product WHERE prod_id='$id_produto'");
					$product = $this->model->getProductBasic($id_produto);
					$existe = $product["existe"];
					//$prodValor = $product["prod_valor"];
					$ordemPedido = $product["prod_ordem"];
					$prodEspecial = $product["prod_especial"];
					//Quantidades de planos existentes junto com os solicitados
					$qtdePlanos = $qtdePlanos + $qtde_produto;

					//Pega os dados do usuário
					$userdata = $this->modelAccount->getUserFind($user);
					$validate = $userdata["usr_documentation"];
					
					//Se não encontrar nenhum pedido no banco, entra na condição//Verifica se á um plano, se for plano não trava limite de pedidos
					if ($pedAberto == 0 && $existe==1 && $prodEspecial==1 && $qtde_produto>0 && $value>=100 /*&& $validate!=0*/)
					{


						//Pega o tipo de conta/pacote que o usu�rio possui no seu atual cadastro
						//list($package) = abreSQL("SELECT usr_package FROM tb_user WHERE usr_id = '$user'; ");
						$package = $this->modelAccount->getUserPackage($user);
						
						$this->view->package = $package;
						
						//Pega a ordem do tipo de conta/pacote do usu�rio na tabela de produtos
						//list($ordemPkg) = abreSQL("SELECT prod_ordem FROM tb_product WHERE prod_id='$package'");
						$product = $this->model->getProductBasic($package);
						$ordemPkg = $product["prod_ordem"];
						$this->view->ordemPkg = $ordemPkg;
						
						//Se for um ####### PLANO ESPECIAL
						if($prodEspecial==1)
						{
								//Fun�ao que faz um novo pedido
								$orderPlan = $this->model->setNewOrderPlan($id_produto, $user, $qtde_produto, $value);
								if($orderPlan==1){
									$this->view->msgSucessPlan = "<script type='text/javascript'>alert('".$translate->_("pedido_feito")."');window.location.href='buy/invoice'</script>";
								}else{
									$this->view->msgPlan = "Erro no pedido!";	
								}
							
						}
						
					} else {
						if($pedAberto > 0){
							$this->view->msgPlan = ''.$translate->_("Existe pedidos abertos!").' <a href="/buy/invoice"> '.$translate->_(">> Clique aqui para visualizar sua fatura! <<").' </a>';
						}elseif($qtde_produto<=0){
							$this->view->msgPlan = ''.$translate->_("O número deve ser um número inteiro maior que 0!").'';
						}elseif($value<100){
						    $this->view->msgPlan = ''.$translate->_("O valor de locação deve ser maior ou igual a USDT 100!").'';
						} /*elseif($validate==0){
						    $this->view->msgPlan = ''.$translate->_("Sua conta não está verificada!").'';
						}*/
					}
				 } else {
					$this->view->msgPlan = ''.$translate->_("Por favor, selecione um contrato!").'';
				 }
			
			}
			//### PEDIDO - Onde termina o pedido ###

		}catch(Exception $e){
			//die($e->getMessage());
			die("Error +1");
			
		}
	}
	
    public function invoiceAction()
    {	
		try
		{			
			$user = $this->view->User;
			
			$balance = $this->modelEwallet->getBalance($user);
			$saldo_liberado = $balance["bsal_saldo_liberado"];

			$userdata = $this->modelAccount->getUserFind($user);

			$this->view->name 				= $userdata["usr_name"];
			$this->view->cpf    			= $userdata["usr_cpf"];
			$this->view->email 				= $userdata["usr_email"];
			$this->view->birth 				= implode('/',array_reverse(explode('-',$userdata["usr_birth"])));

			
			if (empty($saldo_liberado)) { $this->view->saldo_liberado = 0; }else{ $this->view->saldo_liberado = $saldo_liberado; }

			$cancelar     = $_POST["cancel"];
			$cancIdFatura = $_POST["cancIdFatura"];

			if ($cancelar == 1){ $this->model->cancelOrder($user, $cancIdFatura); }

			// $invoiceNotCanceled = $this->model->getInvoiceNotCanceled($user);
			// $this->view->invoiceNotCanceled = $invoiceNotCanceled;
			
			// if($invoiceNotCanceled>0)
			// {
				$this->view->invoice = $this->model->getInvoice($user);
			// }

		}catch(Exception $e){
			//die($e->getMessage());
			die("Error +1");
			
		}
	}

    public function generatecontractAction()
    {
		$this->modelD4sign= new Application_Model_D4sign();

		try
		{			
			$user = $this->view->User;
			$invoice = addslashes($_POST["invoice"]);

			$resultInvoice = $this->model->getInvoiceUser($invoice,$user);
			$invoiceContract = $resultInvoice["fat_contrato"];
			$invoiceValue = $resultInvoice["fat_valor"];

			$balance = $this->modelEwallet->getBalance($user);
			$saldo_liberado = $balance["bsal_saldo_liberado"];

			//Verifica se tem saldo para fazer o pagamento da fatura antes de gerar o contrato
			//Se o saldo disponível para compra for maior ou igual ao valor da fatura
			if (!$resultInvoice)
			{
				$retorno = "Username ou fatura inválida / paga!<br>";
			}
			elseif($saldo_liberado>=$invoiceValue)
			{

				$userDados = $this->modelAccount->getUserFind($user);

				//Se o status estiver para criar o contrato
				if($invoiceContract==0){

					//Dados da D4sign
					$uri = "https://secure.d4sign.com.br/api/v1/";
					$tokenAPI = "live_19ea54e87e8d1541e7f402157feea38165f886380524eaab586872be6ba93c69";
					$cryptKey = "live_crypt_oojf9P5UKBFtCnuoySFhTNOxrsEs94ch";

					if($userDados["usr_country"]=="BR"){

						//Cofre 
						$safe = "91323386-074c-46e5-a74f-a4f5f493fa1f";

						//Se for PF
						if($userDados["usr_type"]==0){
							//Template
							$template_id = "MTQwMjk=";
							//Gerar documento usando template versão Cliente
							$data = [];
							$data = array("name_document" => "WTC-OFICIAL-PROMOCIONAL-PF-USR-$user-[$invoice]",
								"templates" => array( 
									$template_id => array(
										"nome"=> $userDados["usr_name"],
										"rg"=> $userDados['usr_rg'],
										"cpf"=> $userDados["usr_cpf"],
										"endereco_completo"=> $userDados["usr_address"].",".$userDados["usr_number"].",".$userDados["usr_address2"].", cidade ".$userDados["usr_city"].", estado ".$userDados["usr_state"].", CEP ".$userDados["usr_zip_code"],
										"valor_locado"=> "$ ".number_format($invoiceValue, 2, '.', ','),
										"data_transferencia"=> date("d/m/Y"),
										"data_contrato"=> date("d/m/Y")
									)
								)
							);
						}else{ // Se não é PJ
							//Template
							$template_id = "MTQwMjg=";
							//Gerar documento usando template versão Cliente
							$data = [];
							$data = array("name_document" => "WTC-OFICIAL-PROMOCIONAL-PJ-USR-$user-[$invoice]",
								"templates" => array( 
									$template_id => array(
										"RAZAOSOCIAL"=> $userDados["usr_company_name"],
										"TIPOEMPRESA"=> "",
										"CNPJ"=> $userDados['usr_cnpj'],
										"ENDERECO"=> $userDados["usr_address"].",".$userDados["usr_number"].",".$userDados["usr_address2"].", cidade ".$userDados["usr_city"].", estado ".$userDados["usr_state"].", CEP ".$userDados["usr_zip_code"],
										"nome"=> $userDados['usr_name'],
										"cargo"=> "socio",
										"cpf"=> $userDados["usr_cpf"],
										"valor_locado"=> "$ ".number_format($invoiceValue, 2, '.', ','),
										"data_transferencia"=> date("d/m/Y"),
										"data_contrato"=> date("d/m/Y")
									)
								)
							);
						}

					}elseif($userDados["usr_country"]=="KR"){

						//Cofre 
						$safe = "bfc5cd65-1d03-4093-82c8-d32b286709c3";

						//Se for PF
						if($userDados["usr_type"]==0){
							//Template
							$template_id = "MTQ5NzU=";
							//Gerar documento usando template versão Cliente
							$data = [];
							$data = array("name_document" => "WTC-OFFICIAL-PROMOTIONAL-KOREA-USR-$user-[$invoice]",
								"templates" => array( 
									$template_id => array(
										"nome"=> $userDados['usr_name'],
										"rg"=> $userDados['usr_rg'],
										"rented_value"=> "USDT ".number_format($invoiceValue, 2, '.', ','),
										"transfer_date"=> date("d/m/Y"),
										"contract_date"=> date("d/m/Y")
									)
								)
							);
						}
					}else{
						//Cofre 
						$safe = "7d6ad2c5-fc5b-4dd8-a65e-be080e06fcec";

						//Se for PF
						if($userDados["usr_type"]==0){
							//Template
							$template_id = "MTU1NzI=";
							//Gerar documento usando template versão Cliente
							$data = [];
							$data = array("name_document" => "DIGITAL-ASSET-LEASE-AGREEMENT-LESSOR-USR-$user-[$invoice]",
								"templates" => array( 
									$template_id => array(
										"name"=> $userDados['usr_name'],
										"document"=> $userDados['usr_rg'],
										"full_address"=> $userDados['usr_rg'],
										"value_leased"=> "USDT ".number_format($invoiceValue, 2, '.', ','),
										"transfer_date"=> date("d/m/Y"),
										"date"=> date("d/m/Y")
									)
								)
							);
						}

					}


					// $uri = "http://demo.d4sign.com.br/api/v1/";
					// $tokenAPI = "live_793fd6e389f2d55f9c0066dca1817014098c27968b504a648c2a2746679aab9f";
					// $cryptKey = "live_crypt_EVtbZDxjds5oNTPdfOQhrGhStSMqfaUG";


					$template = $this->modelD4sign->getTemplate($uri,$safe,$tokenAPI,$cryptKey,$data);
					//$template = $this->modelD4sign->getTemplate($uri,"c181d1f9-ba0b-4cd6-abe5-a1ee1374a600",$tokenAPI,$cryptKey,$data);
					$this->modelD4sign->setLog($user,$template);
					$template = json_decode($template);
					
					//Se o documento foi gerado com o template
					if($template->message=="success"){

						//Set tag com ID do user
						$data = [];
						$data = array("tag" => $user);
						$this->modelD4sign->setTag($uri,$template->uuid,$tokenAPI,$cryptKey,$data);

						$data = [];
						$data = array(
							"signers" => array(
									array(
									"email"=> $userDados["usr_email"],
									"act" => "4",
									"foreign" => "0",
									"certificadoicpbr" => "0",
									"assinatura_presencial" => "0",
									"docauth" => "0",
									"docauthandselfie" => "0",
									"embed_methodauth" => "email",
									"embed_smsnumber" => "",
									"upload_allow" => "0"
									),
									array(
										"email"=> "contratos@worldtradecorporation.io",
										"act" => "4",
										"foreign" => "0",
										"certificadoicpbr" => "0",
										"assinatura_presencial" => "0",
										"docauth" => "0",
										"docauthandselfie" => "0",
										"embed_methodauth" => "email",
										"embed_smsnumber" => "",
										"upload_allow" => "0"
									),
									array(
										"email"=> "lourenco@worldtradecorporation.io",
										"act" => "5",
										"foreign" => "0",
										"certificadoicpbr" => "0",
										"assinatura_presencial" => "0",
										"docauth" => "0",
										"docauthandselfie" => "0",
										"embed_methodauth" => "email",
										"embed_smsnumber" => "",
										"upload_allow" => "0"
									),
									array(
										"email"=> "inacio@worldtradecorporation.io",
										"act" => "5",
										"foreign" => "0",
										"certificadoicpbr" => "0",
										"assinatura_presencial" => "0",
										"docauth" => "0",
										"docauthandselfie" => "0",
										"embed_methodauth" => "email",
										"embed_smsnumber" => "",
										"upload_allow" => "0"
										)
								)
							);
						//Set signatário
						$signatory = $this->modelD4sign->setSignatory($uri,$template->uuid,$tokenAPI,$cryptKey,$data);
						$signatory = json_decode($signatory,true);
						//$this->modelD4sign->setLog($user,$signatory);

						//Seta o webhook
						$data = [];
						$data = array(
							"url"=> LINK_OFFICE."/routines/d4sign"
							);

						$this->modelD4sign->setWebhook($uri,$template->uuid,$tokenAPI,$cryptKey,$data);

						//Se o signatário foi incluído
						if($signatory["message"][0]["success"]=="1"){

							$data = [];
							$data = array(
								"skip_email"=> "0",
								"workflow" => "0"
								);
							$sendtosigner = $this->modelD4sign->sendDocument($uri,$template->uuid,$tokenAPI,$cryptKey,$data);
							$this->modelD4sign->setLog($user,$sendtosigner);
							$sendtosigner = json_decode($sendtosigner);

							if($sendtosigner->message=="File sent to successfully signing"){
								//Envia o ok e documento está pronto para assinar
								$retorno = 1;
								$this->modelD4sign->updateInvoiceContract($invoice,1,$template->uuid,$signatory["message"][0]["key_signer"]);
							}else{
								$retorno = "Erro ao enviar contrato ".$sendtosigner->message;
								$this->modelD4sign->updateInvoiceContract($invoice,4,$template->uuid);
								
							}

						}else{
							$retorno = "Erro no signatário do contrato ".$signatory->message->success;
							$this->modelD4sign->updateInvoiceContract($invoice,4,$template->uuid);
							$retorno = $signatory;
						}

					}else{
						$retorno = "Erro ao gerar contrato ".$template->message;
						$this->modelD4sign->updateInvoiceContract($invoice,4,$template->uuid);
					}
						

				}else{
					$retorno = "Contrato já foi gerado ou assinado.";
				}

			}else{ // Se o saldo for insuficiente
				$retorno = "Saldo de compra insuficiente para gerar contrato.";
			}

			//Logs
			$this->modelD4sign->setLog($user,$retorno);

			$this->_helper->json->sendJson($retorno);

			//var_dump($signatory["message"][0]["success"]);

			

		}catch(Exception $e){
			//die($e->getMessage());
			die("Error +1cx");
			
		}

		exit();
	}

	public function payinvoiceAction(){
		try
		{
			//Tradução
			$sessao = new Zend_Session_Namespace(SESSION_OFFICE);
			$arrayLanguage = $sessao->arrayLanguage;
			$userLanguage = $sessao->userLanguage;
		
			$translate = new Zend_Translate(array('adapter' => 'array', 'content' => $arrayLanguage, 'locale' => "$userLanguage"));

			$user = $this->view->User;
			$pay_invoice = addslashes($_POST["pay_invoice"]);
			
				
			$sqlVerify = $this->model->getInvoiceUser($pay_invoice,$user);
			
			$valueInvoice = $sqlVerify["fat_valor"];
			
			$balance = $this->modelEwallet->getBalance($user);
			$balance = $balance["bsal_saldo_liberado"];

			//Pega os dados do usuário e do país
			$userdata = $this->modelAccount->getUserFind($user);
			$userCountry = $userdata["usr_country"];

			if (!$sqlVerify)
			{
				$retorno = ''.$translate->_("Username ou fatura inválida / paga!").'<br>';

			}elseif($sqlVerify["fat_contrato"]!=2 && $userCountry=="BR"){

				$retorno = ''.$translate->_("Assine seu contrato!").'<br>';

			}elseif($balance < $valueInvoice){

				$retorno = ''.$translate->_("Não há saldo para pagar a fatura").'<br>';

			}else {
				$paidInvoice = $this->modelEwallet->payInvoice($user, '1', $pay_invoice, $percWalletBitwyn);

				$retorno = $paidInvoice;
				
			}

			$this->_helper->json->sendJson($retorno);
			
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error +2");
			
		}
		
		exit();
	}

}

?>