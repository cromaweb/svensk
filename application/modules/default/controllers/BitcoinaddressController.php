<?php
class Default_BitcoinaddressController extends SYSTEM_Controllers_Office
{
    public function init() {
        parent::init();
        $this->model = new Application_Model_Bankaccount();
        $this->modelIndex = new Application_Model_Index();
        $this->_redirector = $this->_helper->getHelper('Redirector');
	 	$this->_flashmessenger = $this->_helper->getHelper('FlashMessenger');
    }
  
    public function indexAction(){	

    	$flashMessenger = $this->_flashmessenger;
	    if ($flashMessenger->hasMessages()) {

	        $messages = $flashMessenger->getMessages();
	        foreach ($messages as $message);
	        $this->view->assign('mess', $message);
	    }

	    $user = $this->view->User;	

	    $bankaccounts = $this->model->getAll($user);
	    $this->view->assign('bankaccounts',$bankaccounts);
		
	}

	public function newAction() {

	}
	
	public function saveAction() {

		$this->modelAccount = new Application_Model_Account();

		$user = $this->view->User;	
	
		$serializedData = $_POST["data"];
		$unserializedData = array();
		parse_str($serializedData,$unserializedData);
			
		$description = addslashes($unserializedData["description"]);
		$account     = addslashes($unserializedData["account"]);
		$code     = addslashes($unserializedData["cod2fa"]);

		if (empty($description) || empty($account)) {
        	$result['status'] = "error";
			$result['message'] = "Por favor, verifique os campos obrigatórios (*).";
        } else {

			$userData = $this->modelAccount->getUserBasic($user);
			//Code 2FA
			$hash = $userData["usr_google_auth_code"];

			@require_once(APPLICATION_PATH."/libs/google-auth/googleLib/GoogleAuthenticator.php");
			$ga = new GoogleAuthenticator();

			$checkResult = $ga->verifyCode($hash, $code, 2);    // 2 = 2*30sec clock tolerance

			//Se o código digitado for o correto
			if ($checkResult) 
			{

				$newBankAccount = $this->model->setBankaccount($user, $description, $account);

				$result['status'] = "success";
				$result['message'] = "Seu novo endereço de bitcoin foi adicionado.";
				$this->_flashmessenger->addMessage('Seu novo endereço de bitcoin foi adicionado.');

			}else{

				$result['status'] = "error";
				$result['message'] = "Token 2FA inválido!";
				
			}
        }
        echo json_encode($result);
		exit();
	}

	public function removeAction() {
		$user = $this->view->User;
		$id = $_GET['id'];
		$this->model->delete($user, $id);
		$this->_flashmessenger->addMessage('Seu endereço de bitcoin foi deletado.');
		$this->_redirector->gotoUrl(LINK_OFFICE. '/bitcoinaddress');
	}
}

?>