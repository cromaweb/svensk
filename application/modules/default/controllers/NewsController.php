<?php
class Default_NewsController extends SYSTEM_Controllers_Office
{
    public function init() {
        parent::init();
        $this->model = new Application_Model_News();
        $this->modelIndex = new Application_Model_Index();
        $this->view->page_main = 'News';
		$this->view->page_link = LINK_OFFICE."/news/";
        $this->view->subpages = $this->modelIndex->getSubPages("news");
        $this->_redirector = $this->_helper->getHelper('Redirector');
    }

    public function indexAction()
	{
		$news = $this->model->getAllNews();

		$paginator = Zend_Paginator::factory($news);
		
		//Number for page
		$paginator->setItemCountPerPage(5);
		
		// Read the current page number from the request. Default to 1 if no explicit page number is provided.
		$paginator->setCurrentPageNumber($this->_getParam('page', 1));
		
        // Assign the Paginator object to the view        
		$this->view->assign('news',$paginator);
	}

	public function viewAction()
	{

		$id = $this->getRequest()->getParam('id');
		if (empty($id) || !isset($id)) {
			$this->_redirector->gotoUrl(LINK_OFFICE. '/news');
			exit;
		}

		$news = $this->model->getNews($id);
		if (!empty($news[0])) {
			$this->view->assign('news', $news[0]);
		} else {
			$this->_redirector->gotoUrl(LINK_OFFICE. '/news');
			exit;
		}
	}

	
 
}