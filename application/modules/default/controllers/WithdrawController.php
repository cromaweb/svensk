<?php
class Default_WithdrawController extends SYSTEM_Controllers_Office
{
    public function init() {
        parent::init();
        $this->model = new Application_Model_Withdrawal();
        $this->modelBankaccount = new Application_Model_Bankaccount();
        $this->modelDeposit = new Application_Model_Deposit();
        $this->modelAccount = new Application_Model_Account();
        $this->modelEwallet = new Application_Model_Ewallet();
        $this->modelOrder = new Application_Model_Order();
		$this->modelIndex = new Application_Model_Index();
		//Pega a situação se o usuário pode transferir ou não valores
		$transfer = $this->modelEwallet->getEwalletTransfer($this->view->User);
		$this->view->subpages = $this->modelIndex->getSubPages("ewallet",$transfer);
		$this->_redirector = $this->_helper->getHelper('Redirector');
	 	$this->_flashmessenger = $this->_helper->getHelper('FlashMessenger');

	 	$this->view->withdrawalFee = 0.00;

		//$package = $this->modelAccount->getUserPackage($this->view->User);
		 
	 	//if (empty($package)) {
			$package = 0;
		//}

	 	switch ($package) {
	 		case 5:
	 		case 0:
	 			//ACCOUNT MEMBER
	 			$this->view->accountTitle = "member";
	 			$this->view->limiteSaqueRendimentos = 200000; // 0 US$
	 			$this->view->limiteSaqueComissoes = 200000; // 10 US$
	 			$this->view->taxaSaque = 0.00; //5%
	 			break;
	 		case 6:
	 			//ACCOUNT STARTER
	 			$this->view->accountTitle = "starter";
	 			$this->view->limiteSaqueRendimentosDia = 200000; // 10 US$ por dia
	 			$this->view->limiteSaqueComissoesDia = 200000; // 100 US$ por dia
	 			$this->view->taxaSaque = 0.00; //3.5%
	 			break;
	 		case 1:
	 			//ACCOUNT PRO
	 			$this->view->accountTitle = "pro";
	 			$this->view->limiteSaqueRendimentosDia = 200000; // 20 US$ por dia
	 			$this->view->limiteSaqueComissoesDia = 200000; // 250 US$ por dia
	 			$this->view->taxaSaque = 0.00; //1.5%
	 			break;
	 		case 2:
	 			//ACCOUNT PRO+
	 			$this->view->accountTitle = "proplus";
	 			$this->view->limiteSaqueRendimentosDia = 200000; // 50 US$ por dia
	 			$this->view->limiteSaqueComissoesDia = 200000; // 500 US$ por dia
	 			$this->view->taxaSaque = 0.00; //1%
	 			break;
	 		
	 		default:
	 			die("Invalid package. Please contact us.");
	 			break;
	 	}
    }
  
    public function indexAction()
    {	
		$this->modelBonus = new Application_Model_Bonus();
		
		try
		{
			
			$flashMessenger = $this->_flashmessenger;
			if ($flashMessenger->hasMessages()) {
	
				$messages = $flashMessenger->getMessages();
				foreach ($messages as $message);
				$this->view->assign('mess', $message);
			}
	
			$user = $this->view->User;
			$this->view->openDeposit = $this->modelBonus->getOpenDeposit($user);
			$this->view->ewallet_active = $this->modelEwallet->getEwalletActive($user);
			$this->view->withdrawal_active = $this->modelEwallet->getEwalletWithdrawal($user);

			$bankaccounts = $this->modelBankaccount->getAll($user);
			$this->view->assign('bankaccounts',$bankaccounts);

			//echo $this->modelIndex->getDiaUtil(5, date("m"), date("Y"));
	
			$autoriza_saque = false;
			if ($_POST) 
			{
				$diaUtil = $this->modelIndex->getDiaUtil(5, date("m"), date("Y"));

				//echo $_POST['amount'];
				if($this->modelAccount->getValidate($user) == 1)
				{

					$userData = $this->modelAccount->getUserBasic($user);
					//Code 2FA
					$hash = $userData["usr_google_auth_code"];

					//Verifica se tem 2FA cadastrado, é obrigatório para o saque
					if (!empty($hash)) 
					{
						if (empty($_POST['bankaccount']) || empty($_POST['amount']) || empty($_POST['pin']) || empty($_POST['accountfrom'])) {
							$this->view->assign('error',"Por favor, verifique os campos obrigatórios.");
						} else {
							//Datas disponíveis para saque
							if ((date('d') >= 01 && date('d')<=$diaUtil))
							{
								if ($_POST['accountfrom'] == "r" || $_POST['accountfrom'] == "c") {
									$bankaccount = $this->modelBankaccount->getBankaccount($_POST['bankaccount'],$user);
									if (!empty($bankaccount['account_number'])) {

										$dollar = str_replace('USDT ','',$_POST['amount']);
										$dollar = str_replace(',','',$dollar);
										$dollar = floatval($dollar);
										$dollar = round($dollar, 2);
										
										//Quantidade saques o usuário já fez
										$withdrawalsAll = $this->model->getWithdrawalsAll($user);
										$qtyWithdrawal = $withdrawalsAll["quantity"];
										$this->view->qtyWithdrawal = $qtyWithdrawal;
										//Quantidade mínima do saque
										$minWithdrawal =  $this->model->minWithdrawal($qtyWithdrawal);
										$this->view->minWithdrawal = $minWithdrawal;
			
										$this->view->withdrawalFee = round($dollar * $this->view->taxaSaque,2);
										
										if ($dollar >= $minWithdrawal) {
											
											$dollar = round($dollar + $this->view->withdrawalFee,2);
											if ($this->view->accountTitle == "member") {
												//so pode 1 saque com limite de 10 dolares pra comissao e 0 pra rendimento
												$withdrawalsAllTime = $this->model->getWithdrawalsAllTime($user,$_POST['accountfrom']);
			    				
												if ($_POST['accountfrom'] == "r") {
													$limiteGeral = $this->view->limiteSaqueRendimentos;
												} elseif ($_POST['accountfrom'] == "c") {
													$limiteGeral = $this->view->limiteSaqueComissoes;
												} else {
													$balance = $this->modelEwallet->getBalance($user);
													$limiteGeral = round($balance['bsal_saldo_bitfreedom'] * 0.005, 2);	
												}
		
												if ($limiteGeral >= $withdrawalsAllTime['sum_value'] + $dollar) {
													
													$autoriza_saque = true;
		
												} else {
													$this->view->assign('error',"Você não tem limite para essa transação. [1]");
												}
			
											} else {
												if ($_POST['accountfrom'] == "r") {
													$limiteDiario = $this->view->limiteSaqueRendimentosDia;
												} elseif ($_POST['accountfrom'] == "c") {
													$limiteDiario = $this->view->limiteSaqueComissoesDia;
												} else {
													$balance = $this->modelEwallet->getBalance($user);
													$limiteDiario = round($balance['bsal_saldo_bitfreedom'] * 0.005, 2);	
												}
			
												//valida saque diario
												$withdrawalsByDay = $this->model->getWithdrawalsByDay($user,$_POST['accountfrom']);
			
												if ($_POST['accountfrom'] == "f" ) {
													$transfersByDay = $this->modelEwallet->getCountTransfersByDay($user,$_POST['accountfrom']);
													//removendo validação de 1 saque por dia
													if (false) { 
													//if ($withdrawalsByDay['quantity'] > 0 || $transfersByDay['quantity'] > 0 ) {
														//$this->view->assign('error',"You are only allowed to make 1 transaction (either transfer or withdrawal) per day from your Wallet BitFreedom.");
													} elseif($limiteDiario < $dollar) {
														$this->view->assign('error',"Você não tem limite para essa transação. [2]");
													} else {
														$autoriza_saque = true;
													}
												} else {
													if ($limiteDiario >= ($withdrawalsByDay['sum_value'] + $dollar)) {
														$autoriza_saque = true;
													} else {
														$this->view->assign('error',"Você não tem limite para essa transação. [3]");
													}
												}
											}
										} else {
											$this->view->assign('error',"Valor mínimo para saque: USDT $minWithdrawal.");
										}	
									} else {
										$this->view->assign('error',"Carteira inválida, por favor, tente novamente.");
									}
								} else {
									$this->view->assign('error',"Saldo inválido, por favor, escolha Rendimentos ou Comissões.");
								}
							} else {
								$this->view->assign('error',"Os saques só estão disponíveis entre os dias 01 e 03 de cada mês.");
							}
						}
			
						if($autoriza_saque) 
						{
							
							$dollarMinusFee = $dollar - $this->view->withdrawalFee;
							$code = $_POST["pin"];
							// $checkpin = $this->modelAccount->checkPin($user, $pin);
							
							@require_once(APPLICATION_PATH."/libs/google-auth/googleLib/GoogleAuthenticator.php");
							$ga = new GoogleAuthenticator();
		
							$checkResult = $ga->verifyCode($hash, $code, 2);    // 2 = 2*30sec clock tolerance
		
							//Se o código digitado for o correto
							if ($checkResult) 
							{
								$currency = curl_init();
								curl_setopt($currency, CURLOPT_URL, "https://blockchain.info/tobtc?currency=BRL&value=".$dollarMinusFee);
								curl_setopt($currency, CURLOPT_RETURNTRANSFER, TRUE);
								curl_setopt($currency, CURLOPT_HEADER, FALSE);
								$r = curl_exec($currency);
								curl_close($currency);
			
								if ($r > 0) {
									$balance = $this->modelEwallet->getBalance($user);
									if ($balance['bsal_saldo_liberado'] >= 0) {
										if ($_POST['accountfrom'] == "r") {
											$availableBalance = $balance["bsal_saldo_rendimento"];
										} elseif($_POST['accountfrom'] == "c") {
											$availableBalance = $balance["bsal_saldo_comissoes"];
										}
			
										if ($availableBalance >= $dollar) {
											$this->model->setWithdrawal($user,$dollar,$_POST['accountfrom']);
			
											//cadastrar na tabela transacoes btc
											$arrayData = array(
												"bco_user" 			 => $user,
												"bco_type" 			 => "W",
												"bco_bitcoin_amount" => $r,
												"bco_dollar_amount"  => $dollar,
												"bco_account_from"	 => $_POST['accountfrom'],
												"bco_account_to"	 => $bankaccount['account_number'],
												"bco_status"		 => "U",
												"bco_dollar_fee" 	 => $this->view->withdrawalFee);
											
											$idBitcoinOperation = $this->modelDeposit->setDeposit($arrayData);
											$this->_flashmessenger->addMessage('Seu saque está sendo processado por nossa equipe.');
											$this->_redirector->setExit(true)->setGotoSimple("index","saque");
										} else {
											$this->view->assign('error',"Saldo insuficiente para a transação.");
										}
									} else {
										$this->view->assign('error',"Seu saldo disponível deve ser positivo.");
									}
								} else {
									$this->view->assign('error',"Encontramos um erro ao criar seu saque. Por favor, tente novamente.");
								}
							} else {
								$this->view->assign('error','Token 2FA inválido!');
							} 
						}
					}else{
						$this->view->assign('error',"Obrigatório o cadastramento de 2FA (Verificação em duas etapas). <a href='/account/authcode'>Clique aqui</a>");
					}

				}else{
					$this->view->assign('error',"Sua conta não está verificada.");
				}
				
				$this->view->assign('amount',$_POST['amount']);
				$this->view->assign('bankaccount',$_POST['bankaccount']);
				$this->view->assign('accountfrom',$_POST['accountfrom']);
	
				$dollarCalc = str_replace('USDT  ','',$_POST['amount']);
				$dollarCalc = str_replace(',','',$dollarCalc);
				$dollarCalc = floatval($dollarCalc);
				$dollarCalc = round($dollarCalc, 2);
				$returnFee = round($dollarCalc * $this->view->taxaSaque,2);
				$returnTotal = round($dollarCalc + $returnFee,2);
				$this->view->assign('returnFee',"USDT  " . number_format($returnFee, 2, '.', ','));
				$this->view->assign('returnTotal',"USDT  " . number_format($returnTotal, 2, ',', '.'));
				
			}
	
			$withdrawals = $this->model->getAll($user);
			$this->view->assign('withdrawals',$withdrawals);
	
	
			$balance = $this->modelEwallet->getBalance($user);
	
			$availableBalance = $balance["bsal_saldo_liberado"];
			$balanceProcess = $balance["bsal_saldo_areceber"];
			$balanceEarnings = $balance["bsal_saldo_rendimento"];
			$balanceComissions = $balance["bsal_saldo_comissoes"];
				
			$this->view->availableBalance = $availableBalance;
			$this->view->balanceProcess = $balanceProcess;
			$this->view->balanceEarnings = $balanceEarnings;
			$this->view->balanceComissions = $balanceComissions;
		
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error +++1");
			
		}
		
	}
	
	public function cancelAction()
	{		
		$user = $this->view->User;
		$withdrawalId = $_GET['id'];
		
		if (empty($withdrawalId))
		{
			die('no id');
		}
			
		$withdrawal = $this->model->getWithdrawal($user, $withdrawalId);
		if (!empty($withdrawal))
		{
			if ($withdrawal['bco_status'] == 'U')
			{
				$this->model->cancelWithdrawal($user,$withdrawal['bco_dollar_amount'],$withdrawal['bco_account_from']);
				$arrayUpdate = array(
					"bco_status" => 'C'
				); 	

				$this->model->updateWithdrawal($withdrawalId,$arrayUpdate);	

				$this->_flashmessenger->addMessage('Seu saque foi cancelado.');
				$this->_redirector->setExit(true)->setGotoSimple("index","saque");
			} else {
				$this->_redirector->setExit(true)->setGotoSimple("index","saque");
				
			}
		} else {
			$this->_redirector->setExit(true)->setGotoSimple("index","saque");
		}
		

	}


	public function sendAction() {
		/*require_once(APPLICATION_PATH."/libs/coinapult/coinapult.php");

		$apikey = 'c38351cb4bbcac8d05a99fcf0aabf7';
		$apisecret = 'cfdf456601ad9646053f457e6c2368f2b3106bc887702c240de086965b5f';
		$client = new coinapult($apikey, $apisecret);	
		//$address = $client->get_bitcoin_address();
		$result = $client->send(3.127688, '1Dh87eu1J2sBrVhZ9kiHNkQP4UuKLUAzyx', 'BTC', $extOID = 'testwithoutbalance', $callback="https://www.bitwyn.com/coinapult/callback");
		echo "ok";
		print_r($result);
*/
		//Array ( [completeTime] => [extOID] => testwithoutbalance [in] => Array ( [amount] => 0 [currency] => BTC [expected] => 3.127688 ) [out] => Array ( [amount] => 0 [currency] => BTC [expected] => 3.127688 ) [state] => pending [timestamp] => 1472435662 [transaction_id] => 57c395ce7e8d093c61b3b16e [type] => payment )
		exit;
	}
}

?>