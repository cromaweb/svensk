<?php
class Default_OverviewController extends SYSTEM_Controllers_Office
{
    public function init() {
        parent::init();
		$this->modelAccount = new Application_Model_Account();
		$this->modelBonus = new Application_Model_Bonus();
		$this->modelIndex = new Application_Model_Index();
		$this->modelStatus = new Application_Model_Statusexecutive();
		$this->modelEwallet = new Application_Model_Ewallet();
		//$this->modelOrder = new Application_Model_Order();
		$this->modelMyenrollees = new Application_Model_Myenrollees();
		$this->view->page_main = 'Resumo da conta';
		$this->view->page_link = LINK_OFFICE."/overview/";
		$this->view->subpages = $this->modelIndex->getSubPages("overview");
    }
  
    public function indexAction()
    {
		try
		{
			$user = $this->view->User;		
			$this->view->user = $user;	
			//Retorna a pontuação total do usuário
			//$this->view->points = $this->modelStatus->getPoints($user);
			//Retorna o título do usuário
			$this->view->status = $this->modelAccount->getUserStatus($user);
			
			//Saldo
			$balance = $this->modelEwallet->getBalance($user);
			//Saldo disponível
			$this->view->availableBalance = $balance["bsal_saldo_liberado"];
			//Saldo bloqueado
			$this->view->lockedBalance = $balance["bsal_saldo_bloqueado"];
			//Saldo a receber
			$this->view->balanceProcess = $balance["bsal_saldo_areceber"];
			//Wallet Earnings
			$this->view->balanceEarnings = $balance["bsal_saldo_rendimento"];
			//Wallet Comissions
			$this->view->balanceComissions = $balance["bsal_saldo_comissoes"];

			//Patrocinados
			$enrollees = $this->modelMyenrollees->getMyEnrollees($user);
			$this->view->enrollees = count($enrollees);
			
			#### Contratos
			//Retorna o total de planos
			//$this->view->amount_plans = $this->modelOrder->getQtPlans($user);
			
			//Investimento total e limite de recebimento de bonus
			//$bonusLimit = $this->modelEwallet->getLimitBonus($user);
			//$bnsLimit = $bonusLimit['busr_bonus_limite'];
			//$bnsInvestiment = $bonusLimit['busr_investimento_total'];
			
			//Retorna a conta do usuário
			//$package = $this->modelAccount->getUserPackage($user);
			//$pkg = $this->modelTreeview->getPackageName($package);
			//$this->view->account = $pkg["prod_titulo"];
			
			//Cotação BTC/BRL
			$this->view->btc_brl = $this->modelIndex->getBRLBTC();
			
			//Bonus
			$bonusValue = $this->modelBonus->getBonusValues($user);
			$this->view->startFast = $bonusValue["startfast"];
			$this->view->team = $bonusValue["team"];
			$this->view->sharing = $bonusValue["sharing"];
			
			$this->view->points = $this->modelBonus->getPointsVME($user);
			
			//Rede
			$this->view->totalDirects = $this->modelAccount->getUserTotalDirects($user);
			$this->view->totalUpline = $this->modelAccount->getUserUplineTotal($user);
			
			$this->view->totalIndirects = $this->view->totalUpline - $this->view->totalDirects;
			
			$status = $this->modelAccount->getUserStatusID($user);
			$this->view->statusExecutive = $this->modelStatus->getTitleName($status);

		
		}catch(Exception $e){
			die($e->getMessage());
			//die("Error +++1");
			
		}
		
	}
	
}

?>