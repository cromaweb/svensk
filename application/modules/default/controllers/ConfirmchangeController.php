<?php
class Default_ConfirmchangeController extends SYSTEM_Controllers_Office
{
    public function init() {
        parent::init();
		$this->_helper->layout->disableLayout();  
    }

    public function loginAction() {
		$this->_helper->layout->setLayout('signup');
		$this->modelAccount = new Application_Model_Account();
		
    	//@require_once(APPLICATION_PATH."/libs/spirit.php");

    	$usr		= intval($_GET['a']);
    	$old_login 	= $_GET['b'];
    	$new_login 	= $_GET['c'];
    	$key_user  	= $_GET['d'];

    	//$verifyLoginChange = abreSQL("select * from tb_user_login_change where user_id = '$usr' and confirmed = 'N'");
		$verifyLoginChange = $this->modelAccount->getUserLoginChange("user_id = '$usr' AND confirmed = 'N'");

    	if (!empty($verifyLoginChange)) {
    		if (md5($verifyLoginChange['old_login']) == $old_login && md5($verifyLoginChange['new_login']) == $new_login && md5($verifyLoginChange['key_user']) == $key_user) {
    			//executaSQL("update tb_user_login_change set confirmed = 'Y' where id = " . $verifyLoginChange['id']);
				$this->modelAccount->setConfirmedLoginChange($verifyLoginChange['id']);
    			//executaSQL("update tb_user set usr_login_id = '".$verifyLoginChange['new_login']."' where usr_id = " . $usr);
				$this->modelAccount->setLoginChange($usr,$verifyLoginChange['new_login']);
				//executaSQL("update tb_user_blocked set login = '".$verifyLoginChange['old_login']."' where login = '".$verifyLoginChange['new_login']."'");
				$this->modelAccount->setUserOldBlocked($verifyLoginChange['old_login'],$verifyLoginChange['new_login']);
				//executaSQL("insert into tb_log_altera_dados (lad_descricao,lad_idUsuario,lad_ip_acesso,lad_data) values ('Confirm change login from **".$verifyLoginChange['old_login']."** to **".$verifyLoginChange['new_login']."**', ".$usr.", '".$_SERVER['REMOTE_ADDR']."', NOW())");
				$requestLogAlteraDados['lad_descricao'] = "Confirm change login from **".$verifyLoginChange['old_login']."** to **".$verifyLoginChange['new_login']."**";
				$requestLogAlteraDados['lad_idUsuario'] = $usr;
				$requestLogAlteraDados['lad_ip_acesso'] = $_SERVER['REMOTE_ADDR'];
				$this->modelAccount->setLogAlteraDados($requestLogAlteraDados);
				
				$this->view->status = "success";
    		} else {
    			$this->view->status =  "error";
    		}
    	} else {
			$this->view->status =  "error";
		}
    }

    public function oldemailAction() {
		$this->_helper->layout->setLayout('signup');
		$this->modelAccount = new Application_Model_Account();

    	$usr      = intval($_GET['a']);
    	$old_email 	= $_GET['b'];
    	$new_email 	= $_GET['c'];
    	$key_old  	= $_GET['d'];

    	//$verifyEmailChange = abreSQL("select * from tb_user_email_change where user_id = '$usr' and confirmed_old = 'N'");
		$verifyEmailChange = $this->modelAccount->getUserEmailChange("user_id = '$usr' AND confirmed_old = 'N'");
    	if (!empty($verifyEmailChange)) {
    		if (md5($verifyEmailChange['old_email']) == $old_email && md5($verifyEmailChange['new_email']) == $new_email && md5($verifyEmailChange['key_old']) == $key_old) {
    			//executaSQL("update tb_user_email_change set confirmed_old = 'Y' where id = " . $verifyEmailChange['id']);
				$this->modelAccount->setConfirmedOldEmailChange($verifyEmailChange['id']);
				
				$requestLogAlteraDados['lad_descricao'] = "Confirm change email oldemail from **".$verifyEmailChange['old_email']."** to **".$verifyEmailChange['new_email']."**";
				$requestLogAlteraDados['lad_idUsuario'] = $usr;
				$requestLogAlteraDados['lad_ip_acesso'] = $_SERVER['REMOTE_ADDR'];
				$this->modelAccount->setLogAlteraDados($requestLogAlteraDados);
    			//executaSQL("insert into tb_log_altera_dados (lad_descricao,lad_idUsuario,lad_ip_acesso,lad_data) values ('Confirm change email oldemail from **".$verifyEmailChange['old_email']."** to **".$verifyEmailChange['new_email']."**', ".$usr.", '".$_SERVER['REMOTE_ADDR']."', NOW())");
    			if ($verifyEmailChange['confirmed_new'] == "Y") {
    				//executaSQL("update tb_user set usr_email = '".$verifyEmailChange['new_email']."' where usr_id = " . $usr);
					$this->modelAccount->setEmailChange($usr,$verifyEmailChange['new_email']);
					
					$requestLogAlteraDados['lad_descricao'] = "Confirm change email from **".$verifyEmailChange['old_email']."** to **".$verifyEmailChange['new_email']."**";
					$requestLogAlteraDados['lad_idUsuario'] = $usr;
					$requestLogAlteraDados['lad_ip_acesso'] = $_SERVER['REMOTE_ADDR'];
					$this->modelAccount->setLogAlteraDados($requestLogAlteraDados);
					//executaSQL("insert into tb_log_altera_dados (lad_descricao,lad_idUsuario,lad_ip_acesso,lad_data) values ('Confirm change email from **".$verifyEmailChange['old_email']."** to **".$verifyEmailChange['new_email']."**', ".$usr.", '".$_SERVER['REMOTE_ADDR']."', NOW())");
    			}
    			
				$this->view->status = "success";
    		} else {
    			$this->view->status =  "error";
    		}
    	} else {
			$this->view->status =  "error";
		}
	}

    public function newemailAction() {
		$this->_helper->layout->setLayout('signup');
		$this->modelAccount = new Application_Model_Account();

    	$usr      = intval($_GET['a']);
    	$old_email 	= $_GET['b'];
    	$new_email 	= $_GET['c'];
    	$key_new  	= $_GET['d'];

    	//$verifyEmailChange = abreSQL("select * from tb_user_email_change where user_id = '$usr' and confirmed_new = 'N'");
		$verifyEmailChange = $this->modelAccount->getUserEmailChange("user_id = '$usr' AND confirmed_new = 'N'");
		
    	if (!empty($verifyEmailChange)) {
    		if (md5($verifyEmailChange['old_email']) == $old_email && md5($verifyEmailChange['new_email']) == $new_email && md5($verifyEmailChange['key_new']) == $key_new) {
    			//executaSQL("update tb_user_email_change set confirmed_new = 'Y' where id = " . $verifyEmailChange['id']);
				$this->modelAccount->setConfirmedNewEmailChange($verifyEmailChange['id']);
				
				$requestLogAlteraDados['lad_descricao'] = "Confirm change email newemail from **".$verifyEmailChange['old_email']."** to **".$verifyEmailChange['new_email']."**";
				$requestLogAlteraDados['lad_idUsuario'] = $usr;
				$requestLogAlteraDados['lad_ip_acesso'] = $_SERVER['REMOTE_ADDR'];
				$this->modelAccount->setLogAlteraDados($requestLogAlteraDados);
    			//executaSQL("insert into tb_log_altera_dados (lad_descricao,lad_idUsuario,lad_ip_acesso,lad_data) values ('Confirm change email newemail from **".$verifyEmailChange['old_email']."** to **".$verifyEmailChange['new_email']."**', ".$usr.", '".$_SERVER['REMOTE_ADDR']."', NOW())");
    			if ($verifyEmailChange['confirmed_old'] == "Y") {
    				//executaSQL("update tb_user set usr_email = '".$verifyEmailChange['new_email']."' where usr_id = " . $usr);
					$this->modelAccount->setEmailChange($usr,$verifyEmailChange['new_email']);
					
					$requestLogAlteraDados['lad_descricao'] = "Confirm change email from **".$verifyEmailChange['old_email']."** to **".$verifyEmailChange['new_email']."**";
					$requestLogAlteraDados['lad_idUsuario'] = $usr;
					$requestLogAlteraDados['lad_ip_acesso'] = $_SERVER['REMOTE_ADDR'];
					$this->modelAccount->setLogAlteraDados($requestLogAlteraDados);
					//executaSQL("insert into tb_log_altera_dados (lad_descricao,lad_idUsuario,lad_ip_acesso,lad_data) values ('Confirm change email from **".$verifyEmailChange['old_email']."** to **".$verifyEmailChange['new_email']."**', ".$usr.", '".$_SERVER['REMOTE_ADDR']."', NOW())");
    			}
    			
				$this->view->status = "success";
    		} else {
    			$this->view->status =  "error";
    		}
    	} else {
			$this->view->status =  "error";
		}
    }
	
	public function pinAction() {
		$this->_helper->layout->setLayout('signup');
		$this->modelAccount = new Application_Model_Account();
		
        $usr      = intval($_GET['a']);
        $pin_key  = $_GET['b'];

        //$verifyPinChange = abreSQL("select usr_pin, usr_pin_key, usr_pin_key_expiration from tb_user where usr_id = '$usr'");
		$verifyPinChange = $this->modelAccount->getUserPIN($usr);

        if (empty($verifyPinChange['usr_pin']) && !empty($verifyPinChange['usr_pin_key'])) {
            if ($data_hoje < $verifyPinChange['usr_pin_key_expiration']) {          
                if (md5($verifyPinChange['usr_pin_key']) == $pin_key) {
                    $newpin = rand(1000,9999);
                    
                    //executaSQL("update tb_user set usr_pin = '".sha1("x16".$newpin)."', usr_pin_key = '', usr_pin_key_expiration = '' where usr_id = " . $usr);
					$this->modelAccount->setUserNewPIN($usr,$newpin);
					
					$requestLogAlteraDados['lad_descricao'] = "Confirm change PIN', ".$usr.", '".$_SERVER['REMOTE_ADDR']."";
					$requestLogAlteraDados['lad_idUsuario'] = $usr;
					$requestLogAlteraDados['lad_ip_acesso'] = $_SERVER['REMOTE_ADDR'];
					$this->modelAccount->setLogAlteraDados($requestLogAlteraDados);
                    //executaSQL("insert into tb_log_altera_dados (lad_descricao,lad_idUsuario,lad_ip_acesso,lad_data) values ('Confirm change pin', ".$usr.", '".$_SERVER['REMOTE_ADDR']."', NOW())");
                    
                    $this->view->newpin = $newpin;
                    $this->view->status = "success";
                } else {
                    $this->view->status =  "error";
                }
            } else {
                $this->view->status =  "expirated";
            }
        } else {
            $this->view->status =  "expirated";
        }
    }

}