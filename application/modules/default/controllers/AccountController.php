<?php
class Default_AccountController extends SYSTEM_Controllers_Office
{
    public function init() {
        parent::init();
		$this->model = new Application_Model_Account();
		$this->modelIndex = new Application_Model_Index();
		$this->_redirector = $this->_helper->getHelper('Redirector');
		$this->_flashmessenger = $this->_helper->getHelper('FlashMessenger');
    }
	
/*	public function __construct()
	{
	  $this->model = new Default_Model_Account();
	  
	}*/
  
    public function indexAction()
    {
		try
		{
			$flashMessenger = $this->_flashmessenger;
			if ($flashMessenger->hasMessages()) {
	
				$messages = $flashMessenger->getMessages();
				foreach ($messages as $message);
				$this->view->assign('mess', $message);
			}
			//$model = new Application_Model_Account();
			//@require_once(APPLICATION_PATH."/libs/spirit.php");
			
			$user = $this->view->User;	
			
			$userDados = $this->model->getUserFind($user);
	
			$this->view->name 				= $userDados["usr_name"];
			$this->view->surname 			= $userDados["usr_last_name"];
			$this->view->cpf    			= $userDados["usr_cpf"];
			$this->view->cnpj    			= $userDados["usr_cnpj"];
			$this->view->rg 				= $userDados['usr_rg'];
			$this->view->email 				= $userDados["usr_email"];
			$this->view->login 				= $userDados["usr_login_id"];
			$this->view->phone 				= $userDados["usr_phone"];
			$this->view->birth 				= implode('/',array_reverse(explode('-',$userDados["usr_birth"])));
			$this->view->address			= $userDados["usr_address"];
			$this->view->address2			= $userDados["usr_address2"];
			$this->view->number				= $userDados["usr_number"];
			$this->view->city				= $userDados["usr_city"];
			$this->view->state				= $userDados["usr_state"];
			$this->view->zip_code			= $userDados["usr_zip_code"];
			$this->view->type				= $userDados["usr_type"];
			$this->view->company_name		= $userDados["usr_company_name"];
			$this->view->opening_date		= implode('/',array_reverse(explode('-',$userDados["usr_opening_date"])));
			
			$genderType = $userDados["usr_gender"];
			
			if($genderType=='m') $gender = 'Masculino';
			if($genderType=='f') $gender = 'Feminino';
			
			$this->view->gender	= $gender;
			
			$code = $userDados["usr_country"];
			$country = $this->modelIndex->getNameCountry($code);
			
			$this->view->country  = $country;
	
			$verifyLoginChange = $this->model->getUserLoginChange("user_id = '$user' and confirmed = 'N'");
			if (!empty($verifyLoginChange)) {
				$this->view->changingLogin = true;
			} else {
				$this->view->changingLogin = false;
			}
	
			$verifyEmailChange = $this->model->getUserEmailChange("user_id = '$user' and (confirmed_old = 'N' or confirmed_new = 'N')");
			if (!empty($verifyEmailChange)) {
				$this->view->changingEmail = true;
			} else {
				$this->view->changingEmail = false;
			}
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error AC+1000");
			
		}
	}
	

	public function updateAction()
    {
		try
		{
			
			$user = $this->view->User;
			$this->view->coduser = $user;
			
			$userDados = $this->model->getUserFind($user);
			//$userDados = abreSQL("select * from tb_user usr where usr_id = '$user'");
	
			$this->view->name 				= $userDados["usr_name"];
			$this->view->cpf    			= $userDados["usr_cpf"];
			$this->view->cnpj    			= $userDados["usr_cnpj"];
			$this->view->rg 				= $userDados["usr_rg"];
			$this->view->email 				= $userDados["usr_email"];
			$this->view->login 				= $userDados["usr_login_id"];
			$this->view->phone 				= $userDados["usr_phone"];
			$this->view->birth 				= implode('/',array_reverse(explode('-',$userDados["usr_birth"])));
			$this->view->address			= $userDados["usr_address"];
			$this->view->number				= $userDados['usr_number'];
			$this->view->address2			= $userDados["usr_address2"];
			$this->view->district			= $userDados["usr_district"];
			$this->view->city				= $userDados["usr_city"];
			$this->view->state				= $userDados["usr_state"];
			$this->view->zip_code			= $userDados["usr_zip_code"];
			$this->view->type				= $userDados["usr_type"];
			$this->view->company_name		= $userDados["usr_company_name"];
			$this->view->opening_date		= implode('/',array_reverse(explode('-',$userDados["usr_opening_date"])));

			$code = $userDados["usr_country"];
			$country = $this->modelIndex->getNameCountry($code);
			$this->view->country  = $country;
	
			$verifyLoginChange = $this->model->getUserLoginChange("user_id = '$user' and confirmed = 'N'");

			if (!empty($verifyLoginChange)) {
				$this->view->changingLogin = true;
				$this->view->oldLogin = $verifyLoginChange['old_login'];
				$this->view->newLogin = $verifyLoginChange['new_login'];
			} else {
				$this->view->changingLogin = false;
			}
	
			$verifyEmailChange = $this->model->getUserEmailChange("user_id = '$user' and (confirmed_old = 'N' or confirmed_new = 'N')");
	
			if (!empty($verifyEmailChange)) {
				$this->view->changingEmail = true;
				$this->view->oldEmail = $verifyEmailChange['old_email'];
				$this->view->oldEmailConfirmed = $verifyEmailChange['confirmed_old'];
				$this->view->newEmail = $verifyEmailChange['new_email'];
				$this->view->newEmailConfirmed = $verifyEmailChange['confirmed_new'];
			} else {
				$this->view->changingEmail = false;
			}
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error AC+1001");
			
		}
	}

	public function saveAction(){
		try
		{
			//Tradução
			$sessao = new Zend_Session_Namespace(SESSION_OFFICE);
			$arrayLanguage = $sessao->arrayLanguage;
			$userLanguage = $sessao->userLanguage;
		
			$translate = new Zend_Translate(array('adapter' => 'array', 'content' => $arrayLanguage, 'locale' => "$userLanguage"));

			$user = $this->view->User;	
		
			$serializedData = $_POST["dados"];
			$unserializedData = array();
			parse_str($serializedData,$unserializedData);
			
			$type				= $unserializedData["type"];
			$name				= $unserializedData["name"];
			$cpf				= preg_replace( '/[^0-9]/is', '',$unserializedData["cpf"]);
			$cnpj    			= preg_replace('/[^0-9]/', '', (string) $unserializedData["cnpj"]);
			$rg 				= $unserializedData["rg"];
			$birth				= implode('-',array_reverse(explode('/',$unserializedData["dob"])));
			$phone				= $unserializedData["phone"];
			$address			= $unserializedData["address"];
			$address2			= $unserializedData["address2"];
			$number 			= $unserializedData["number"];
			$district 			= $unserializedData["district"];
			$city				= $unserializedData["city"];
			$state				= $unserializedData["state"];
			$zip_code			= $unserializedData["zip_code"];
			$login				= $unserializedData["login"];
			$email				= $unserializedData["email"];
			$company_name		= $unserializedData["company_name"];
			$opening_date		= implode('-',array_reverse(explode('/',$unserializedData["opening_date"])));
	
			$userDados = $this->model->getUserFind($user);
			$userCountry = $userDados["usr_country"];
			//$userDados 	= abreSQL("select * from tb_user usr where usr_id = '$user'");

			//Repassa a informação de primeiro login para a variavel $firstLogin
			$firstLogin = $userDados["usr_primeiro_login"];

			//Se for do Brasil
			if($userCountry == "BR"){
				//Se o usuário for PF
				if(empty($cpf)){
					$cpf = $userDados["usr_cpf"];
				}

				//Se for PJ e primeiro login, passa a informação de atualização do PJ somente
				if($userDados["usr_type"]==1 && $userDados["usr_primeiro_login"]=="S" && !empty($userDados["usr_name"])){
					$firstLogin = "C";
				}
			}

			//Se for fazer rede
			if($userDados["usr_rede"] == 1){
				//Se o usuário estiver habilitado como rede, ele deve ser PJ
				$type = "pj";
			}
			
			$oldEmail = $userDados["usr_email"];
			$oldLogin = $userDados["usr_login_id"];
	
			if (!$login) {
				$login = $oldLogin;
			}
	
			if (!$email) {
				$email = $oldEmail;
			}
	
			$erro = false;

			//Se for o primeiro login ativa a verificação de campos
			if($userDados["usr_primeiro_login"]=="S")
			{

				//Se for PF no DB e não for tipo PJ novo cadastro
				if(($userDados["usr_type"]==0 && $type=="pf") || ($userCountry != "BR")){

					$company_name = NULL;
					$cnpj = NULL;
					$opening_date = NULL;
					
				}

				//Inicia as variáveis
				$mensagem_erro = "";
				$campo = "";

				if (empty($name) && empty($userDados["usr_name"])){
					$erro = true;
					$campo .= "[".$translate->_("Nome")."],";
				}

				//Se for do Brasil
				if($userCountry == "BR"){
					if(empty($cpf) && empty($userDados["usr_cpf"])){
						$erro = true;
						$campo .= "[CPF],";
					}

					if(empty($number) && empty($userDados["usr_number"])){
						$erro = true;
						$campo .= "[".$translate->_("numero")."],";
					} 
				}

				if(empty($rg) && empty($userDados["usr_rg"])){
					$erro = true;
					$campo .= "[".$translate->_("ID")."],";
				}
				
				if(empty($birth) && empty($userDados["usr_birth"]) || (empty($birth) && ($userDados["usr_birth"]=="0000-00-00"))){
					$erro = true;
					$campo .= "[".$translate->_("data_de_nascimento")."],";
				}
				if(!empty($birth)){
					if(!($this->model->validateDate($birth, 'Y-m-d'))){
						$erro = true;
						$campo .= "[".$translate->_("data_de_nascimento")."]Y$birth,";
					}

					if($this->model->calculateAge($birth)<18){
						$erro = true;
						$campo .= "[".$translate->_("Deve ser maior de idade")."]$birth,";
					}
				}
				
				if(empty($address) && empty($userDados["usr_address"])){
					$erro = true;
					$campo .= "[".$translate->_("endereco")."],";
				} 
				
				if(empty($district) && empty($userDados["usr_district"]) && $userCountry != "KR"){
					$erro = true;
					$campo .= "[".$translate->_("district")."],";
				}
				
				if(empty($city) && empty($userDados["usr_city"]) && $userCountry != "KR"){
					$erro = true;
					$campo .= "[".$translate->_("cidade")."],";
				}
				
				if(empty($state) && empty($userDados["usr_state"]) && $userCountry != "KR"){
					$erro = true;
					$campo .= "[".$translate->_("uf")."],";
				}
				
				if(empty($zip_code) && empty($userDados["usr_zip_code"])){
					$erro = true;
					$campo .= "[".$translate->_("cep")."]";
				}

				//Se for do Brasil
				if($userCountry == "BR"){
					if(!($this->model->validaCPF($cpf))){
						$erro = true;
						$mensagem_erro .= "CPF inválido (*).<br>";
					}
				}//End BR

				//Se for do Brasil
				if($userCountry == "BR"){
					//Se for PJ
					if($userDados["usr_type"]==1 || $type=="pj"){
						
						if(empty($company_name) && empty($userDados["usr_company_name"])){
							$erro = true;
							$campo .= "[Razão Social],";
						} 
						
						if(empty($cnpj) && empty($userDados["usr_cnpj"])){
							$erro = true;
							$campo .= "[CNPJ],";
						} 
						
						if(empty($opening_date) && empty($userDados["usr_opening_date"]) || (empty($opening_date) && ($userDados["usr_opening_date"]=="0000-00-00"))){
							$erro = true;
							$campo .= "[Data de abertura],";
						}

						if(($opening_date=="00/00/0000")){
							$erro = true;
							$campo .= "[Data de abertura],";
						} 

						//Verifica se o CNPJ é válido
						if(!($this->model->validaCNPJ($cnpj))){
							$erro = true;
							$mensagem_erro .= "CNPJ inválido (*).<br>";
						}

						//Verifica se existe o CNPJ no banco
						if($this->model->verificaCNPJDB($cnpj)>0){
							$erro = true;
							$mensagem_erro .= "CNPJ já existe na base de dados (*).<br>";
						}					
					}

					//Se o tipo de DB é PF e não existe nome no banco (ou seja não é atualização de dados e sim um novo cadastro)
					if($userDados["usr_type"]!=1 && empty($userDados["usr_name"])){
						//Verifica se o CPF do novo cadastro não está cadastrado na base
						if($this->model->verificaCPFDB($cpf)>0){
							$erro = true;
							$mensagem_erro .= "CPF já existe na base de dados (*).<br>";
						}
					}

				} // End BR

				if($erro){
					$mensagem_erro .= "".$translate->_("Verifique os campos obrigatórios")." $campo (*).<br>";
				}


			}else{ // Se não for o primeiro login

				//Inicia as variáveis
				$mensagem_erro = "";
				$campo = "";
				
				if(empty($address)){
					$erro = true;
					$campo .= "[".$translate->_("endereco")."],";
				} 
				//Se for do Brasil
				if($userCountry == "BR"){
					if(empty($number)){
						$erro = true;
						$campo .= "[".$translate->_("numero")."],";
					}
				}
				
				if(empty($district) && $userCountry != "KR"){
					$erro = true;
					$campo .= "[".$translate->_("district")."],";
				}
				
				if(empty($city) && $userCountry != "KR"){
					$erro = true;
					$campo .= "[".$translate->_("cidade")."],";
				}
				
				if(empty($state) && $userCountry != "KR"){
					$erro = true;
					$campo .= "[".$translate->_("district")."],";
				}
				
				if(empty($zip_code)){
					$erro = true;
					$campo .= "[".$translate->_("cep")."]";
				}

				if($erro){
					$mensagem_erro .= "".$translate->_("Verifique os campos obrigatórios")." $campo (*).<br>";
				}
			}

			//Verificar se é maior de 18 anos
			// if($this->model->calcularIdade($unserializedData["dob"])<18){
			// 	$erro = true;
			// 	$mensagem_erro = "Deve ser maior de 18 anos (*).<br>";
			// }

			
			$erroEmail = false;
			if ($email != $oldEmail) {
				$verifyEmailChange = $this->model->getUserEmailChange("user_id = '$user' AND (confirmed_old = 'N' or confirmed_new = 'N')");
				//$verifyEmailChange = abreSQL("select * from tb_user_email_change where user_id = '$user' and (confirmed_old = 'N' or confirmed_new = 'N')");
	
				if (!empty($verifyEmailChange)) {
					$mensagem_erro .= "".$translate->_("Você já está mudando seu email.")."<br>";
					$erroEmail = true;
					$erro = true;
				}
				#//se não encontrar @
				if(!preg_match("/@/", $email) ){
				  $mensagem_erro .= "".$translate->_("Email inválido.")."<br>";
				  $erroEmail = true;
				  $erro = true;
				}
	
				if (!$erroEmail) {
					$s4 = $this->model->getUser("usr_id <> '$user' AND usr_email='$email'");
					//$s4 = geraSQL("SELECT usr_login_id FROM tb_user WHERE  usr_id <> '$user' and usr_email='$email'");
					$mnr4 = @count($s4);
	
					$s5 = $this->model->getUserEmailChangeBlocked($id = null,$email);
					//$s5 = geraSQL("SELECT id FROM tb_user_email_change WHERE  old_email='$email' or new_email='$email'");
					$mnr5 = @count($s5);
	
					if ($mnr4>0 || $mnr5 > 0) { 
					  $mensagem_erro .= "".$translate->_("Email não está disponível.")." $mnr4 | $mnr5<br>";
					  $erroEmail = true; 
					  $erro = true;
					}
				}
			}
	
			if (!$erro) {

				if (!$erroEmail && $email != $oldEmail) {
					$keyOldEmail = uniqid( rand( ), true );
					$keyNewEmail = uniqid( rand( ), true );
					
					$requestEmail['user_id'] = $user;
					$requestEmail['old_email'] = $oldEmail;
					$requestEmail['new_email'] = $email;
					$requestEmail['confirmed_old'] = 'N';
					$requestEmail['key_old'] = $keyOldEmail;
					$requestEmail['confirmed_new'] = 'N';
					$requestEmail['key_new'] = $keyNewEmail;
					$requestEmail['ip_acesso'] = $_SERVER['REMOTE_ADDR'];
					
					$this->model->setUserEmailChange($requestEmail);	
	
					//send email to $oldEmail
					$url = LINK_OFFICE . "/confirmchange/oldemail?a=".$user."&b=".md5($oldEmail)."&c=".md5($email)."&d=".md5($keyOldEmail)."";
	
					$emailTemplate = $this->model->getEmailTemplate("email_type='old_email' AND email_status = 1");
					$email_template = $emailTemplate["email_template"];
					$subject = $emailTemplate["email_title"];
					
					$email_template = str_replace('$username', $oldLogin, $email_template);
					$email_template = str_replace('$new_email', $email, $email_template);
					$email_template = str_replace('$link', $url, $email_template);
	
					$this->modelIndex->setZendMail($oldEmail,$subject,$email_template);
	
					//send email to $email
					$url = LINK_OFFICE . "/confirmchange/newemail?a=".$user."&b=".md5($oldEmail)."&c=".md5($email)."&d=".md5($keyNewEmail)."";
	
					
					$emailTemplate = $this->model->getEmailTemplate("email_type='new_email' AND email_status = 1");
					$email_template = $emailTemplate["email_template"];
					$subject = $emailTemplate["email_title"];
					
					$email_template = str_replace('$username', $oldLogin, $email_template);
					$email_template = str_replace('$new_email', $email, $email_template);
					$email_template = str_replace('$link', $url, $email_template);
	
					$this->modelIndex->setZendMail($email,$subject,$email_template);
	
					$requestLogAlteraDados = array();
					$requestLogAlteraDados['lad_descricao'] = "Start change email from **$oldLogin** to **$login**";
					$requestLogAlteraDados['lad_idUsuario'] = $user;
					$requestLogAlteraDados['lad_ip_acesso'] = $_SERVER['REMOTE_ADDR'];
					
					$this->model->setLogAlteraDados($requestLogAlteraDados);
	
				}
	
	
				$db = Zend_Registry::get('database'); 
				$select = $db->select();
				$select->from('tb_documentacao')
					   ->where('doc_idUsuario = '.$user);
				$doc = $db->fetchRow($select);
	
				if ($address != $userDados['usr_address'] || $address2 != $userDados['usr_address2'] || $city != $userDados['usr_city'] || $state != $userDados['usr_state'] || $zip_code != $userDados['usr_zip_code']) {
					if ($doc["doc_endereco_status"] == 'P' || $doc["doc_endereco_status"] == "V") {
						$data = array('doc_endereco_status' => "U");
						$db->update('tb_documentacao', $data,'doc_idUsuario = '.$user);
					}
				}

				//Se for PJ tipo igual  a 1 
				if($type=="pj"){
					$codeType = 1;
				//Se não, é PF, tipo igual a 0
				}else{
					$codeType = 0;
				}
	
				$requestUser['usr_id'] = $user;
				$requestUser['usr_type'] = $codeType; 
				$requestUser['usr_name'] = $name; 
				$requestUser['usr_cpf'] = $cpf;
				$requestUser['usr_cnpj'] = $cnpj; 
				$requestUser['usr_rg'] = $rg;
				$requestUser['usr_birth'] = $birth; 
				$requestUser['usr_phone'] = $phone; 
				$requestUser['usr_address'] = $address; 
				$requestUser['usr_address2'] = $address2;
				$requestUser['usr_number'] = $number;
				$requestUser['usr_district'] = $district;
				$requestUser['usr_city'] = $city; 
				$requestUser['usr_state'] = $state; 
				$requestUser['usr_zip_code'] = $zip_code;
				$requestUser["usr_company_name"] = $company_name;
				$requestUser["usr_opening_date"] = $opening_date;

				$this->model->updateUserAccount($requestUser,$firstLogin);
				
				$requestLogAlteraDados = array();
				$requestLogAlteraDados['lad_descricao'] = "Update data account :".$requestUser;
				$requestLogAlteraDados['lad_idUsuario'] = $user;
				$requestLogAlteraDados['lad_ip_acesso'] = $_SERVER['REMOTE_ADDR'];
				
				$this->model->setLogAlteraDados($requestLogAlteraDados);
				
				$this->view->assign('primeiroLogin','N');
				$sessao = new Zend_Session_Namespace(SESSION_OFFICE);
				$sessao->primeiroLogin = 'N';
	
				$result['status'] = "success";
				$result['message'] = "Dados atualizados! Se você alterou seu endereço, lembre-se de enviar o arquivo para validar esses dados na Documentação.";
				$this->_flashmessenger->addMessage('Dados atualizados! Se você alterou seu e-mail ou login, confirme as alterações em seu e-mail.');
			} else {
				$result['status'] = "error";
				$result['message'] = $mensagem_erro;
			}
			echo json_encode($result);
			exit();
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error AC+1002");
			
		}
		
	}

	public function getconfirmusernamechangeAction() {
		try
		{
			//@require_once(APPLICATION_PATH."/libs/spirit.php");
			//@require_once(APPLICATION_PATH."/libs/cripto.php");	
	
			$this->_helper->layout->disableLayout();  
			$user = $this->view->User;		
			
			$verifyLoginChange = $this->model->getUserLoginChange("user_id = '$user' and confirmed = 'N'");
			//$verifyLoginChange = abreSQL("select * from tb_user_login_change where user_id = '$user' and confirmed = 'N'");
			
			$email = $this->model->getUserEmail($user);
			//list($email) =  abreSQL("select usr_email from tb_user where usr_id = '$user'");
			if (!empty($verifyLoginChange) && !empty($email)) {
				//send email to $oldEmail
				$url = LINK_OFFICE . "/confirmchange/login?a=".$user."&b=".md5($verifyLoginChange['old_login'])."&c=".md5($verifyLoginChange['new_login'])."&d=".md5($verifyLoginChange['key_user'])."";
				
				$emailTemplate = $this->model->getEmailTemplate("email_type='change_username' AND email_status = 1");
				$email_template = $emailTemplate["email_template"];
				$subject = $emailTemplate["email_title"];
				//list($email_template, $subject) =  abreSQL("SELECT email_template, email_title FROM tb_email_template WHERE email_type='change_username' AND email_status = 1");
				$email_template = str_replace('$username', $verifyLoginChange['old_login'], $email_template);
				$email_template = str_replace('$new_username', $verifyLoginChange['new_login'], $email_template);
				$email_template = str_replace('$link', $url, $email_template);
	
				if ($this->modelIndex->setZendMail($email,$subject,$email_template)=="success") {
					$retorno['status'] = "success";
					$retorno['message'] = "Email sent with success!";
				} else {
					$retorno['status'] = "error";
					$retorno['message'] = "We had troubles sending the email. Please try again.";
				}
	  
			}
	
			if (empty($retorno['status'])) {
				$retorno['status'] = "error";
				$retorno['message'] = "Something went wrong. Please, try again.";
			}
	
			echo json_encode($retorno);
			exit;
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error AC+1003");
			
		}
	}

	public function cancelusernamechangeAction() {
		try
		{
			//@require_once(APPLICATION_PATH."/libs/spirit.php");
			//@require_once(APPLICATION_PATH."/libs/cripto.php");	
	
			$this->_helper->layout->disableLayout();  
			$user = $this->view->User;
			
			$verifyLoginChange = $this->model->getUserLoginChange("user_id = '$user' AND confirmed = 'N'");
			//$verifyLoginChange = abreSQL("select * from tb_user_login_change where user_id = '$user' and confirmed = 'N'");
			if (!empty($verifyLoginChange)) {
				
				$this->model->deleteUserLoginChange($verifyLoginChange['id']);
				//executaSQL("delete from tb_user_login_change where id = ".$verifyLoginChange['id']);
				$this->model->deleteUserBlocked($verifyLoginChange['new_login']);
				//executaSQL("delete from tb_user_blocked where login='".$verifyLoginChange['new_login']."'");
				
				$requestLogAlteraDados = array();
				$requestLogAlteraDados['lad_descricao'] = "Cancel change login from **".$verifyLoginChange['old_login']."** to **".$verifyLoginChange['new_login']."**";
				$requestLogAlteraDados['lad_idUsuario'] = $user;
				$requestLogAlteraDados['lad_ip_acesso'] = $_SERVER['REMOTE_ADDR'];
				
				$this->model->setLogAlteraDados($requestLogAlteraDados);
				//executaSQL("insert into tb_log_altera_dados (lad_descricao,lad_idUsuario,lad_ip_acesso,lad_data) values ('Cancel change login from **".$verifyLoginChange['old_login']."** to **".$verifyLoginChange['new_login']."**', ".$user.", '".$_SERVER['REMOTE_ADDR']."', NOW())");
			}
	
			$this->_redirect(LINK_OFFICE . '/account/update');
			exit;
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error AC+1004");
			
		}
	}

	public function getconfirmoldemailchangeAction() {
		try
		{
			//@require_once(APPLICATION_PATH."/libs/spirit.php");
			//@require_once(APPLICATION_PATH."/libs/cripto.php");	
	
			$this->_helper->layout->disableLayout();  
			$user = $this->view->User;		
			
			$verifyEmailChange = $this->model->getUserEmailChange("user_id = '$user' AND confirmed_old = 'N'");
			//$verifyEmailChange = abreSQL("select * from tb_user_email_change where user_id = '$user' and confirmed_old = 'N'");
			$user_login = $this->model->getUserLogin($user);
			//list($user_login) =  abreSQL("select usr_login_id from tb_user where usr_id = '$user'");
			if (!empty($verifyEmailChange)) {
				//send email to $oldEmail
				$url = LINK_OFFICE . "/confirmchange/oldemail?a=".$user."&b=".md5($verifyEmailChange['old_email'])."&c=".md5($verifyEmailChange['new_email'])."&d=".md5($verifyEmailChange['key_old'])."";
				
				$emailTemplate = $this->model->getEmailTemplate("email_type='old_email' AND email_status = 1");
				$email_template = $emailTemplate["email_template"];
				$subject = $emailTemplate["email_title"];
				//list($email_template, $subject) =  abreSQL("SELECT email_template, email_title FROM tb_email_template WHERE email_type='old_email' AND email_status = 1");
				$email_template = str_replace('$username', $user_login, $email_template);
				$email_template = str_replace('$new_email', $verifyEmailChange['new_email'], $email_template);
				$email_template = str_replace('$link', $url, $email_template);
	
				if ($this->modelIndex->setZendMail($verifyEmailChange['old_email'],$subject,$email_template)=="success") {
					$retorno['status'] = "success";
					$retorno['message'] = "Email sent with success!";
				} else {
					$retorno['status'] = "error";
					$retorno['message'] = "We had troubles sending the email. Please try again.";
				}
	
			}
			if (empty($retorno['status'])) {
				$retorno['status'] = "error";
				$retorno['message'] = "Something went wrong. Please, try again.";
			}
	
			echo json_encode($retorno);
			exit;
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error AC+1005");
			
		}
	}

	public function getconfirmnewemailchangeAction() {
		try
		{
			//@require_once(APPLICATION_PATH."/libs/spirit.php");
			//@require_once(APPLICATION_PATH."/libs/cripto.php");	
	
			$this->_helper->layout->disableLayout();  
			$user = $this->view->User;		
			
			$verifyEmailChange = $this->model->getUserEmailChange("user_id = '$user' AND confirmed_old = 'N'");
			//$verifyEmailChange = abreSQL("select * from tb_user_email_change where user_id = '$user' and confirmed_new = 'N'");
			$user_login = $this->model->getUserLogin($user);
			//list($user_login) =  abreSQL("select usr_login_id from tb_user where usr_id = '$user'");
			if (!empty($verifyEmailChange)) {
				$url = LINK_OFFICE . "/confirmchange/newemail?a=".$user."&b=".md5($verifyEmailChange['old_email'])."&c=".md5($verifyEmailChange['new_email'])."&d=".md5($verifyEmailChange['key_new'])."";
				
				$emailTemplate = $this->model->getEmailTemplate("email_type='new_email' AND email_status = 1");
				$email_template = $emailTemplate["email_template"];
				$subject = $emailTemplate["email_title"];
				//list($email_template, $subject) =  abreSQL("SELECT email_template, email_title FROM tb_email_template WHERE email_type='new_email' AND email_status = 1");
				$email_template = str_replace('$username', $user_login, $email_template);
				$email_template = str_replace('$new_email', $verifyEmailChange['new_email'], $email_template);
				$email_template = str_replace('$link', $url, $email_template);
				
				if ($this->modelIndex->setZendMail($verifyEmailChange['new_email'],$subject,$email_template)=="success") {
					$retorno['status'] = "success";
					$retorno['message'] = "Email sent with success!";
				} else {
					$retorno['status'] = "error";
					$retorno['message'] = "We had troubles sending the email. Please try again.";
				}

			}
			if (empty($retorno['status'])) {
				$retorno['status'] = "error";
				$retorno['message'] = "Something went wrong. Please, try again.";
			}
			echo json_encode($retorno);
			exit;	
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error AC+1006");
			
		}
	}

	public function cancelemailchangeAction() {
		try
		{
			//@require_once(APPLICATION_PATH."/libs/spirit.php");
			//@require_once(APPLICATION_PATH."/libs/cripto.php");	
	
			$this->_helper->layout->disableLayout();  
			$user = $this->view->User;		
			
			$verifyEmailChange = $this->model->getUserEmailChange("user_id = '$user' AND (confirmed_old = 'N' or confirmed_new = 'N')");
			//$verifyEmailChange = abreSQL("select * from tb_user_email_change where user_id = '$user' and (confirmed_old = 'N' or confirmed_new = 'N')");
			if (!empty($verifyEmailChange)) {
				$this->model->deleteUserEmailChange($verifyEmailChange['id']);
				//executaSQL("delete from tb_user_email_change where id = ".$verifyEmailChange['id']);
				
				$requestLogAlteraDados = array();
				$requestLogAlteraDados['lad_descricao'] = "Cancel change email from **".$verifyEmailChange['old_email']."** to **".$verifyEmailChange['new_email']."**";
				$requestLogAlteraDados['lad_idUsuario'] = $user;
				$requestLogAlteraDados['lad_ip_acesso'] = $_SERVER['REMOTE_ADDR'];
				
				$this->model->setLogAlteraDados($requestLogAlteraDados);
				//executaSQL("insert into tb_log_altera_dados (lad_descricao,lad_idUsuario,lad_ip_acesso,lad_data) values ('Cancel change email from **".$verifyEmailChange['old_email']."** to **".$verifyEmailChange['new_email']."**', ".$user.", '".$_SERVER['REMOTE_ADDR']."', NOW())");
			}
	
			$this->_redirect(LINK_OFFICE . '/account/update');
			exit;
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error AC+1007");
			
		}
	}

	public function languageAction()
    {
		try
		{
			
			$user = $this->view->User;
	
			if ($_POST) {
				$serializedData = $_POST["dados"];
				$unserializedData = array();
				parse_str($serializedData,$unserializedData);
			
				$language = $unserializedData['language_preference'];
	
				$this->model->updateUserLanguage($user,$language);

				$arrayLanguage = $this->model->getLanguage("$language");

				$this->view->assign('userLanguage',"$language");
				$this->view->assign('arrayLanguage',"$arrayLanguage");
				$sessao = new Zend_Session_Namespace(SESSION_OFFICE);
				$sessao->userLanguage = $language;
				$sessao->arrayLanguage = $arrayLanguage;

				echo "update";
			
				exit();
			}
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error AC+1008");
			
		}
	}
	
	public function linkdeindicacaoAction()
    {

	}

	public function authcodeAction()
    {
		try
		{
			$user = $this->view->User;

			//Puxa informação do usuário
			$userData = $this->model->getUserBasic($user);
			//Code 2FA
			$authcode = $userData["usr_google_auth_code"];
			$email = $userData["usr_email"];

			$this->view->authcode = $authcode;

			if (empty($authcode))
			{
				@require_once(APPLICATION_PATH."/libs/google-auth/googleLib/GoogleAuthenticator.php");
				$ga = new GoogleAuthenticator();

				$secret = $ga->createSecret();

				$this->view->secret = $secret;

				$this->view->qrCodeUrl = $ga->getQRCodeGoogleUrl($email, $secret);
			}

			if ($_POST) {

				if(empty($authcode)){

					$code = $_POST["code"];
					$hash = $_POST["hash"];

					@require_once(APPLICATION_PATH."/libs/google-auth/googleLib/GoogleAuthenticator.php");
					$ga = new GoogleAuthenticator();

					$checkResult = $ga->verifyCode($hash, $code, 2);    // 2 = 2*30sec clock tolerance

					//Se o código digitado for o correto
					if ($checkResult) 
					{
						$this->model->updateUser2FA($user,$hash);

						$retorno['status'] = "success";
						$retorno['message'] = "2FA cadastrado com sucesso!";

					} 
					else 
					{
						$retorno['status'] = "error";
						$retorno['message'] = "Código incorreto, tente novamente.";
					}

					$requestLogAlteraDados = array();
					$requestLogAlteraDados['lad_descricao'] = "2FA Register: ".$action;
					$requestLogAlteraDados['lad_idUsuario'] = $user;
					$requestLogAlteraDados['lad_ip_acesso'] = $_SERVER['REMOTE_ADDR'];
					
					$this->model->setLogAlteraDados($requestLogAlteraDados);

				}else{

					$retorno['status'] = "error";
					$retorno['message'] = "Código já cadastrado.";
				}

				$this->_helper->json->sendJson($retorno);

			}

			

			
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error AC+1009");
			
		}
	}
	
	public function disableauthcodeAction()
    {
		try
		{
			if ($_POST) {

				$user = $this->view->User;

				//Puxa informação do usuário
				$userData = $this->model->getUserBasic($user);
				//Code 2FA
				$authcode = $userData["usr_google_auth_code"];

				if(!empty($authcode)){

					@require_once(APPLICATION_PATH."/libs/google-auth/googleLib/GoogleAuthenticator.php");
					$ga = new GoogleAuthenticator();

					$code = $_POST["code"];
					
					$checkResult = $ga->verifyCode($authcode, $code, 2);    // 2 = 2*30sec clock tolerance

					//Se o código digitado for o correto
					if ($checkResult) 
					{
						$this->model->updateUser2FA($user,"");

						$retorno['status'] = "success";
						$retorno['message'] = "2FA desabilitado com sucesso!";

					} 
					else 
					{
						$retorno['status'] = "error";
						$retorno['message'] = "Código incorreto, tente novamente.";
					}

					$requestLogAlteraDados = array();
					$requestLogAlteraDados['lad_descricao'] = "2FA Disable: ".$action;
					$requestLogAlteraDados['lad_idUsuario'] = $user;
					$requestLogAlteraDados['lad_ip_acesso'] = $_SERVER['REMOTE_ADDR'];
					
					$this->model->setLogAlteraDados($requestLogAlteraDados);

				}else{

					$retorno['status'] = "error";
					$retorno['message'] = "Problema na desabilitação do 2FA.";
				}

				$this->_helper->json->sendJson($retorno);

			}
			
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error AC+1009");
			
		}
    }

	public function pinAction()
    {
		try
		{
			if ($_POST) {
	
				$user = $this->view->User;		
				
				$email = $this->model->getUserEmail($user);
				$username = $this->model->getUserLogin($user);

				$keyPin = uniqid( rand( ), true );
	
				$data_hoje = date('Y-m-d H:i:s');
				$date = new DateTime($data_hoje);   	
				$date->add(new DateInterval('P1D'));
				$expiration_date = $date->format('Y-m-d H:i:s');
				
				$this->model->updateUserPin($user,$keyPin,$expiration_date);
				
				$requestLogAlteraDados = array();
				$requestLogAlteraDados['lad_descricao'] = "Start in change";
				$requestLogAlteraDados['lad_idUsuario'] = $user;
				$requestLogAlteraDados['lad_ip_acesso'] = $_SERVER['REMOTE_ADDR'];
				
				$this->model->setLogAlteraDados($requestLogAlteraDados);
	
				$url = LINK_OFFICE . "/confirmchange/pin?a=".$user."&b=".md5($keyPin);
	
				$emailTemplate = $this->model->getEmailTemplate("email_type='security_pin' AND email_status = 1");
				$email_template = $emailTemplate["email_template"];
				$subject = $emailTemplate["email_title"];
				
				$email_template = str_replace('$username', $username, $email_template);
				$email_template = str_replace('$link', $url, $email_template);
	
				if ($this->modelIndex->setZendMail($email,$subject,$email_template)=="success") {
					$retorno['status'] = "success";
					$retorno['message'] = "Your new pin has been sent to your email.";
				} else {
					$retorno['status'] = "error";
					$retorno['message'] = "We had troubles sending your new pin. Please try again or contact our support.";
				}
	
				//echo json_encode($retorno);
				$this->_helper->json->sendJson($retorno);
				//exit;
			} 
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error AC+1009");
			
		}
    }
	
	public function passwordAction()
	{
		try
		{
			if ($_POST) {
				@require_once(APPLICATION_PATH."/libs/cripto.php");	
			
				$user = $this->view->User;
		
				$serializedData = $_POST["action"];
				$unserializedData = array();
				parse_str($serializedData,$unserializedData);
				
				$lenghtPassword = $unserializedData["new_password"];
				$currentPassword = sha1("x16".$unserializedData["current_password"]);
				$newPassword	= sha1("x16".$unserializedData["new_password"]);
				$easyPassword = $this->modelIndex->getEasyPassword($newPassword);
				
				$password = $this->model->getUserPassword($user);
				//$rowVer = abreSQL("select * from tb_user where usr_id = '$user'");
				if($password != $currentPassword)
				{
					$retorno['status'] = "incorret";
					
				}elseif(strlen($lenghtPassword)<6 || strlen($lenghtPassword)>15)
				{
					$retorno['status'] = "lenght";
					
				}elseif($password == $newPassword)
				{
					$retorno['status'] = "equal";
					
				}elseif($easyPassword == "error")
				{
					$retorno['status'] = "invalid";
					
				}
				else
				{
					$this->model->updateUserPassword($user,$newPassword);
					//executaSQL("update tb_user set usr_password = '$novasenha' where usr_id = '$user'");
					
					$requestLogAlteraDados = array();
					$requestLogAlteraDados['lad_descricao'] = "Update password";
					$requestLogAlteraDados['lad_idUsuario'] = $user;
					$requestLogAlteraDados['lad_ip_acesso'] = $_SERVER['REMOTE_ADDR'];
					
					$this->model->setLogAlteraDados($requestLogAlteraDados);
					//executaSQL("insert into tb_log_altera_dados (lad_descricao,lad_idUsuario,lad_ip_acesso,lad_data) values ('Update password', ".$user.", '".$_SERVER['REMOTE_ADDR']."', NOW())");
					$retorno['status'] = "success";
					//$this->_redirect(LINK_OFFICE . '/logout');
					Zend_Session::destroy();
				}
				$this->_helper->json->sendJson($retorno);
				//exit();
				
			}
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error AC+1010");
			
		}
		
	}
}
?>