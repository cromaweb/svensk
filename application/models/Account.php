<?php

class Application_Model_Account
{

	public function __construct()
	{
		/*$this->db_TbUser = new Application_Model_DbTable_TbUser();
	  $this->db_TbUserEmailChange = new Application_Model_DbTable_TbUserEmailChange();
	  $this->db_TbUserLoginChange = new Application_Model_DbTable_TbUserLoginChange();
	  $this->db_TbUserBlocked = new Application_Model_DbTable_TbUserBlocked();
	  $this->db_TbLogAlteraDados = new Application_Model_DbTable_TbLogAlteraDados();
	  $this->db_TbEmailTemplate = new Application_Model_DbTable_TbEmailTemplate();*/
	}

	//TB_USER BEGIN ###########
	public function getUser($where = null, $order = null, $limit = null)
	{

		$this->db_TbUser = new Application_Model_DbTable_TbUser();

		try {
			$select = $this->db_TbUser->select()->from($this->db_TbUser)->order($order)->limit($limit);
			if (!is_null($where)) {
				$select->where($where);
			}
			return $this->db_TbUser->fetchAll($select)->toArray();
		} catch (Exception $e) {
			die("Error 100");
		}
	}

	public function getUserBasic($id = null, $login = null)
	{
		$this->db_TbUser = new Application_Model_DbTable_TbUser();
		try {

			if (!is_null($id) || !is_null($login)) {
				$select = $this->db_TbUser->select()
					->from($this->db_TbUser);
				if (!is_null($id)) {
					$select->where("usr_id = $id");
				} else {
					$select->where("usr_login_id = '$login'");
				}
				return $this->db_TbUser->fetchRow($select);
			}
		} catch (Exception $e) {
			die("Error 101");
		}
	}

	public function getUserFind($id)
	{

		$this->db_TbUser = new Application_Model_DbTable_TbUser();

		try {

			$arr = $this->db_TbUser->find($id)->toArray();
			return $arr[0];
		} catch (Exception $e) {
			die("Error 102");
		}
	}

	public function setUser(array $request)
	{
		$this->db_TbUser = new Application_Model_DbTable_TbUser();

		try {

			$dados = array(
				'usr_login_id' => $request['usr_login_id'],
				'usr_email' => $request['usr_email'],
				'usr_password' => sha1("x16" . $request['usr_password']),
				'usr_invited_id' => $request['usr_invited_id'],
				'usr_active' => $request['usr_active'],
				'usr_reg_date' => new Zend_Db_Expr('NOW()'),
				'usr_hash' => sha1($request['usr_login_id'] . $request['usr_reg_date']),
				'usr_language' => $request['usr_language'],
				'usr_country' => $request['usr_country']
			);
			return $this->db_TbUser->insert($dados);
		} catch (Exception $e) {
			//die("Error 103");
			die($e->getMessage());
		}
	}

	public function updateUser(array $request)
	{
		$this->db_TbUser = new Application_Model_DbTable_TbUser();

		try {

			$dados = array(
				'usr_login_id' => $request['usr_login_id'],
				'usr_name' => $request['usr_name'],
				'usr_email' => $request['usr_email'],
				'usr_password' => $request['usr_password'],
				'usr_invited_id' => $request['usr_invited_id'],
				'usr_package' => $request['usr_package'],
				'usr_active' => $request['usr_active'],
				'usr_phone' => $request['usr_phone'],
				'usr_tax_id' => $request['usr_tax_id'],
				'usr_birth' => $request['usr_birth'],
				'usr_address' => $request['usr_address'],
				'usr_address2' => $request['usr_address2'],
				'usr_city' => $request['usr_city'],
				'usr_state' => $request['usr_state'],
				'usr_zip_code' => $request['usr_zip_code'],
				'usr_country' => $request['usr_country']
			);
			$where = $this->db_TbUser->getAdapter()->quoteInto("usr_id = ?", $request['usr_id']);
			$this->db_TbUser->update($dados, $where);
		} catch (Exception $e) {
			die("Error 104");
		}
	}

	public function updateUserAccount(array $request, $firstLogin = 'N')
	{
		$this->db_TbUser = new Application_Model_DbTable_TbUser();

		try {

			if ($firstLogin == 'S') {
				$dados = array(
					'usr_type' => $request['usr_type'],
					'usr_name' => $request['usr_name'],
					'usr_cpf' => $request['usr_cpf'],
					'usr_cnpj' => $request['usr_cnpj'],
					'usr_rg' => $request['usr_rg'],
					'usr_birth' => $request['usr_birth'],
					'usr_phone' => $request['usr_phone'],
					'usr_address' => $request['usr_address'],
					'usr_number' => $request['usr_number'],
					'usr_address2' => $request['usr_address2'],
					'usr_district' => $request['usr_district'],
					'usr_city' => $request['usr_city'],
					'usr_state' => $request['usr_state'],
					'usr_zip_code' => $request['usr_zip_code'],
					'usr_company_name' => $request['usr_company_name'],
					'usr_opening_date' => $request['usr_opening_date'],
					'usr_primeiro_login' => 'N'
				);
			} elseif ($firstLogin == 'C') {
				$dados = array(
					'usr_cnpj' => $request['usr_cnpj'],
					'usr_phone' => $request['usr_phone'],
					'usr_address' => $request['usr_address'],
					'usr_number' => $request['usr_number'],
					'usr_address2' => $request['usr_address2'],
					'usr_district' => $request['usr_district'],
					'usr_city' => $request['usr_city'],
					'usr_state' => $request['usr_state'],
					'usr_zip_code' => $request['usr_zip_code'],
					'usr_company_name' => $request['usr_company_name'],
					'usr_opening_date' => $request['usr_opening_date'],
					'usr_primeiro_login' => 'N'
				);
			} else {
				$dados = array(
					'usr_phone' => $request['usr_phone'],
					'usr_address' => $request['usr_address'],
					'usr_number' => $request['usr_number'],
					'usr_address2' => $request['usr_address2'],
					'usr_district' => $request['usr_district'],
					'usr_city' => $request['usr_city'],
					'usr_state' => $request['usr_state'],
					'usr_zip_code' => $request['usr_zip_code'],
					'usr_primeiro_login' => 'N'
				);
			}

			$where = $this->db_TbUser->getAdapter()->quoteInto("usr_id = ?", $request['usr_id']);
			return $this->db_TbUser->update($dados, $where);
		} catch (Exception $e) {
			die("Error 105");
		}
	}

	public function getUserId($login = null, $email = null)
	{
		$this->db_TbUser = new Application_Model_DbTable_TbUser();

		try {
			if (!is_null($login) || !is_null($email)) {
				$select = $this->db_TbUser->select()
					->from($this->db_TbUser, array('usr_id', 'usr_hash'));

				if (!is_null($login)) {
					$select->where("usr_login_id = '$login'");
				} else {
					$select->where("usr_email = '$email'");
				}
				return $this->db_TbUser->fetchRow($select);
			}
		} catch (Exception $e) {
			die("Error 106");
		}
	}

	public function getUserEmail($id = null, $login = null, $email = null)
	{
		$this->db_TbUser = new Application_Model_DbTable_TbUser();

		try {

			if (!is_null($id) || !is_null($login) || !is_null($email)) {
				$select = $this->db_TbUser->select()
					->from($this->db_TbUser, 'usr_email');
				if (!is_null($id)) {
					$select->where("usr_id = $id");
				} elseif (!is_null($login)) {
					$select->where("usr_login_id = '$login'");
				} else {
					$select->where("usr_email = '$email'");
				}

				$return = $this->db_TbUser->fetchRow($select);
				return $return["usr_email"];
			}
		} catch (Exception $e) {
			die("Error 107");
		}
	}

	public function getUserEmailExist($email)
	{
		$this->db_TbUser = new Application_Model_DbTable_TbUser();

		try {

			if (!is_null($email)) {

				$result1 = @count($this->getUserEmail($id = null, $login = null, $email));

				$result2 = @count($this->getUserEmailChangeBlocked($id = null, $email));

				if ($result1 == 0 && $result2 == 0) {
					return 1;
				}
			}
		} catch (Exception $e) {
			die("Error 108");
		}
	}

	public function getUserLogin($id = null, $login = null)
	{
		$this->db_TbUser = new Application_Model_DbTable_TbUser();

		try {

			if (!is_null($id) || !is_null($login)) {
				$select = $this->db_TbUser->select()
					->from($this->db_TbUser, 'usr_login_id');
				if (!is_null($id)) {
					$select->where("usr_id = $id");
				} else {
					$select->where("usr_login_id = '$login'");
				}

				$return = $this->db_TbUser->fetchRow($select);
				return $return["usr_login_id"];
			}
		} catch (Exception $e) {
			die("Error 109");
		}
	}

	public function getUserLoginExist($login)
	{

		try {
			if (!is_null($login)) {

				$result1 = @count($this->getUserLogin(null, $login));
				$result2 = @count($this->getUserBlocked($login));

				if ($result1 == 0 && $result2 == 0) {
					return 1;
				}
			}
		} catch (Exception $e) {
			die("Error 110");
		}
	}

	public function getUserLevel($user, $sponsor)
	{
		$this->db = new Application_Model_DbTable_TbUnilevel();

		try {
			$select = $this->db->select()
				->from($this->db, array('unilevel_level'))
				->where("unilevel_user = ?", $user)
				->where("unilevel_antecessor = ?", $sponsor);

			$return = $this->db->fetchRow($select);
			return $return["unilevel_level"];
		} catch (Exception $e) {
			die($e->getMessage());
			//die("Error 106++");

		}
	}

	public function getValidate($user)
	{
		$this->db = new Application_Model_DbTable_TbUser();

		try {
			$select = $this->db->select()
				->from($this->db, array('usr_type', 'usr_documentation'))
				->where("usr_id = ?", $user);

			$return = $this->db->fetchRow($select);
			//Inicia a variavel com 0
			$validate = 0;
			//Verifica se é válido
			if ($return["usr_type"] == 0 && $return["usr_documentation"] == 1) {
				$validate = 1;
			} elseif ($return["usr_type"] == 1 && $return["usr_documentation"] == 2) {
				$validate = 1;
			}
			//retorna a validade
			return $validate;
		} catch (Exception $e) {
			die($e->getMessage());
			//die("Error 106++");

		}
	}
	//Se é PJ ou PF
	public function getUserType($user)
	{
		$this->db = new Application_Model_DbTable_TbUser();

		try {
			$select = $this->db->select()
				->from($this->db, array('usr_type'))
				->where("usr_id = ?", $user);

			$return = $this->db->fetchRow($select);
			//retorna a validade
			return $return["usr_type"];
		} catch (Exception $e) {
			die($e->getMessage());
			//die("Error 106++");

		}
	}

	//Pegar usuário por contrato ID
	public function getUserContract($contract)
	{
		$this->db = new Application_Model_DbTable_TbUser();

		try {
			$select = $this->db->select()
				->from($this->db)
				->where("usr_contract_id = ?", $contract);

			return $this->db->fetchRow($select);
		} catch (Exception $e) {
			die($e->getMessage());
			//die("Error 106++");

		}
	}

	public function setLoginChange($idUser, $username)
	{
		$this->db = new Application_Model_DbTable_TbUser();

		try {
			$dados = array(
				'usr_login_id' => $username
			);
			$where = $this->db->getAdapter()->quoteInto("usr_id = ?", $idUser);
			$this->db->update($dados, $where);
		} catch (Exception $e) {
			die("Error 111");
		}
	}

	public function setEmailChange($idUser, $email)
	{
		$this->db = new Application_Model_DbTable_TbUser();

		try {
			$dados = array(
				'usr_email' => $email
			);
			$where = $this->db->getAdapter()->quoteInto("usr_id = ?", $idUser);
			$this->db->update($dados, $where);
		} catch (Exception $e) {
			die("Error 112");
		}
	}

	public function getUserPackage($id)
	{
		$this->db_TbUser = new Application_Model_DbTable_TbUser();

		try {

			if (!is_null($id)) {
				$select = $this->db_TbUser->select()
					->from($this->db_TbUser, 'usr_package')
					->where("usr_id = $id");
				$return = $this->db_TbUser->fetchRow($select);
				return $return["usr_package"];
			}
		} catch (Exception $e) {
			die("Error 113");
		}
	}

	public function getUserStatus($id)
	{
		try {
			if (!is_null($id)) {
				$db = Zend_Db_Table_Abstract::getDefaultAdapter();
				$select = $db->select()
					->from('tb_user', array('usr_titulo'))
					->join('tb_bns_titulo', 'tit_id=usr_titulo', array('tit_nome', 'tit_imagem'))
					->where("usr_id = ?", $id);

				return $db->fetchRow($select);
				//return $return["tit_imagem"];

			}
		} catch (Exception $e) {
			die("Error 240");
		}
	}

	public function getUserStatusID($user)
	{
		try {
			if (!is_null($user)) {
				$db = Zend_Db_Table_Abstract::getDefaultAdapter();
				$select = $db->select()
					->from('tb_user', array('usr_titulo'))
					->where("usr_id = ?", $user);

				$return = $db->fetchRow($select);
				return $return["usr_titulo"];
			}
		} catch (Exception $e) {
			die("Error 240+1");
		}
	}

	public function setUserStatusExecutive($user, $status)
	{
		$this->db = new Application_Model_DbTable_TbUser();

		try {
			$data['usr_titulo'] = $status;
			$where = $this->db->getAdapter()->quoteInto("usr_id = ?", $user);
			$this->db->update($data, $where);
		} catch (Exception $e) {
			die("Error 156+5");
		}
	}

	public function getUserLeg($id)
	{
		$this->db_TbUser = new Application_Model_DbTable_TbUser();

		try {
			$select = $this->db_TbUser->select()
				->from($this->db_TbUser, 'usr_prefered_leg')
				->where("usr_id = $id");
			$return = $this->db_TbUser->fetchRow($select);
			return $return["usr_prefered_leg"];
		} catch (Exception $e) {
			die("Error 114");
		}
	}

	public function getUserRegistered($id)
	{
		$this->db_TbUser = new Application_Model_DbTable_TbUser();

		try {
			$select = $this->db_TbUser->select()
				->from($this->db_TbUser, 'usr_reg_date')
				->where("usr_id = $id");
			$return = $this->db_TbUser->fetchRow($select);
			return $return["usr_reg_date"];
		} catch (Exception $e) {
			die("Error 115");
		}
	}

	public function getUserContractSign($id)
	{
		$this->db_TbUser = new Application_Model_DbTable_TbUser();

		try {
			$select = $this->db_TbUser->select()
				->from($this->db_TbUser, 'usr_contract')
				->where("usr_id = $id");
			$return = $this->db_TbUser->fetchRow($select);
			return $return["usr_contract"];
		} catch (Exception $e) {
			die("Error 115");
		}
	}

	public function getUserPassword($id)
	{
		$this->db_TbUser = new Application_Model_DbTable_TbUser();

		try {
			$select = $this->db_TbUser->select()
				->from($this->db_TbUser, 'usr_password')
				->where("usr_id = $id");
			$return = $this->db_TbUser->fetchRow($select);
			return $return["usr_password"];
		} catch (Exception $e) {
			die("Error 116");
		}
	}

	public function updateUserLeg($user, $leg)
	{
		$this->db_TbUser = new Application_Model_DbTable_TbUser();

		try {
			$dados = array(
				'usr_prefered_leg' => $leg
			);
			$where = $this->db_TbUser->getAdapter()->quoteInto("usr_id = ?", $user);
			$this->db_TbUser->update($dados, $where);
		} catch (Exception $e) {
			die("Error 117");
		}
	}

	public function updateUserLanguage($user, $language)
	{
		$this->db_TbUser = new Application_Model_DbTable_TbUser();

		try {
			$dados = array(
				'usr_language' => $language
			);
			$where = $this->db_TbUser->getAdapter()->quoteInto("usr_id = ?", $user);
			$this->db_TbUser->update($dados, $where);
		} catch (Exception $e) {
			die("Error 117+");
		}
	}

	public function getLanguage($language)
	{
		$this->db = new Application_Model_DbTable_TbLanguage();

		try {

			//Puxa a abreviação da linguagem
			$lang = $this->getLanguageAbb($language);

			$select = $this->db->select()
				->from($this->db, array("lang_phrase","lang_$lang"));

			$resultLanguage = $this->db->fetchAll($select)->toArray();
		
			foreach($resultLanguage as $rowLanguage)
			{
				$lang_phrase = $rowLanguage["lang_phrase"];
				$lang_language = $rowLanguage["lang_$lang"];
				$arrayLanguage["$lang_phrase"] = $lang_language;
	
			}

			//Um array com as palavras do idioma
			return $arrayLanguage;

		} catch (Exception $e) {
			die($e->getMessage());
			//die("Error 120xx");
		}
	}

	public function getLanguageAbb($language)
	{
		//Se condições linguagem
		if($language == 'pt' || $language == 'pt-BR' || $language == 'pt_BR' ||$language == 'pt-br'){
			$lang = 'pt-br';
		}elseif($language == 'ko'){
			$lang = 'ko';
		}else{
			$lang = 'en';
		}

		return $lang;
	}

	public function updateUserPassword($user, $password)
	{
		$this->db_TbUser = new Application_Model_DbTable_TbUser();

		try {
			$dados = array(
				'usr_password' => $password
			);
			$where = $this->db_TbUser->getAdapter()->quoteInto("usr_id = ?", $user);
			$this->db_TbUser->update($dados, $where);
		} catch (Exception $e) {
			die("Error 118");
		}
	}

	public function updateUser2FA($user, $secret)
	{
		$this->db_TbUser = new Application_Model_DbTable_TbUser();

		try {
			$dados = array(
				'usr_google_auth_code' => $secret
			);
			$where = $this->db_TbUser->getAdapter()->quoteInto("usr_id = ?", $user);
			$this->db_TbUser->update($dados, $where);
		} catch (Exception $e) {
			die("Error 119xx");
		}
	}

	public function updateUserPin($user, $keyPin, $expiration_date)
	{
		$this->db_TbUser = new Application_Model_DbTable_TbUser();

		try {
			$dados = array(
				'usr_pin' => '',
				'usr_pin_key' => $keyPin,
				'usr_pin_key_expiration' => $expiration_date,
				'usr_pin_errors' => '0'
			);
			$where = $this->db_TbUser->getAdapter()->quoteInto("usr_id = ?", $user);
			$this->db_TbUser->update($dados, $where);
		} catch (Exception $e) {
			die("Error 119");
		}
	}


	public function getUserPIN($id)
	{
		$this->db = new Application_Model_DbTable_TbUser();

		try {
			$select = $this->db->select()
				->from($this->db, array('usr_pin', 'usr_pin_key', 'usr_pin_key_expiration'))
				->where("usr_id = ?", $id);

			return $this->db->fetchRow($select);
		} catch (Exception $e) {
			die("Error 120");
		}
	}

	public function setUserNewPIN($idUser, $newpin)
	{
		$this->db = new Application_Model_DbTable_TbUser();

		try {
			$dados = array(
				'usr_pin' => sha1("x16" . $newpin),
				'usr_pin_key' => '',
				'usr_pin_key_expiration' => ''
			);

			$where = $this->db->getAdapter()->quoteInto("usr_id = ?", $idUser);
			$this->db->update($dados, $where);
		} catch (Exception $e) {
			die("Error 121");
		}
	}

	public function getSponsor($id = null, $login = null, $email = null)
	{
		$this->db_TbUser = new Application_Model_DbTable_TbUser();

		try {

			$select = $this->db_TbUser->select()
				->from($this->db_TbUser, 'usr_invited_id');
			if (!is_null($id)) {
				$select->where("usr_id = $id");
			} elseif (!is_null($login)) {
				$select->where("usr_login_id = '$login'");
			} else {
				$select->where("usr_email = '$email'");
			}

			$return = $this->db_TbUser->fetchRow($select);

			return $return["usr_invited_id"];
		} catch (Exception $e) {
			die("Error 122");
		}
	}

	public function getUserLimitTransfer($idUser)
	{
		$this->db_TbUser = new Application_Model_DbTable_TbUser();

		try {
			$select = $this->db_TbUser->select()
				->from($this->db_TbUser, array('usr_limit_transfer_day'))
				->where("usr_id = '$idUser'");

			$return = $this->db_TbUser->fetchRow($select);
			return $return["usr_limit_transfer_day"];
		} catch (Exception $e) {
			//die($e->getMessage());
			die("Error 123");
		}
	}

	public function updateUserActive($user)
	{
		$this->db_TbUser = new Application_Model_DbTable_TbUser();

		try {

			$dados = array(
				'usr_active' => 'y'
			);
			$where = $this->db_TbUser->getAdapter()->quoteInto("usr_id = ?", $user);
			$this->db_TbUser->update($dados, $where);
		} catch (Exception $e) {
			die("Error 124");
		}
	}

	public function deleteUser($id)
	{
		$this->db_TbUser = new Application_Model_DbTable_TbUser();

		try {
			$dados = array(
				'usr_ewallet_active' => 'N',
				'usr_acesso_liberado' => 'N'
			);
			$where = $this->db_TbUser->getAdapter()->quoteInto("usr_id = ?", $id);
			$this->db_TbUser->update($dados, $where);
		} catch (Exception $e) {
			die("Error 125");
		}
	}


	//TB_EMAIL_TEMPLATE BEGIN
	public function getEmailTemplate($where = null, $language = null, $order = null, $limit = null)
	{
		$this->db_TbEmailTemplate = new Application_Model_DbTable_TbEmailTemplate();

		if($language=="pt-br" || is_null($language)){
			$language = "pt";
		}

		try {

			$select = $this->db_TbEmailTemplate->select()
						->from($this->db_TbEmailTemplate)
						->where("email_language = ?",$language)
						->order($order)
						->limit($limit);
			
			if (!is_null($where)) {
				$select->where($where);
			}



			//Coloca resultado em uma variável
			return $this->db_TbEmailTemplate->fetchRow($select);
		} catch (Exception $e) {
			die("Error 126");
		}
	}

	//TB_USER_LOGIN_CHANGE 
	public function getUserLoginChange($where = null, $order = null, $limit = null)
	{
		$this->db_TbUserLoginChange = new Application_Model_DbTable_TbUserLoginChange();

		try {

			$select = $this->db_TbUserLoginChange->select()->from($this->db_TbUserLoginChange)->order($order)->limit($limit);
			if (!is_null($where)) {
				$select->where($where);
			}
			return $this->db_TbUserLoginChange->fetchRow($select);
		} catch (Exception $e) {
			die("Error 127");
		}
	}

	public function getUserLoginChangeFind($id)
	{
		$this->db_TbUserLoginChange = new Application_Model_DbTable_TbUserLoginChange();

		try {
			$arr = $this->db_TbUserLoginChange->find($id);
			return $arr[0];
		} catch (Exception $e) {
			die("Error 128");
		}
	}

	public function setUserLoginChange(array $request)
	{
		$this->db_TbUserLoginChange = new Application_Model_DbTable_TbUserLoginChange();

		try {

			$dados = array(
				'user_id' => $request['user_id'],
				'old_login' => $request['old_login'],
				'new_login' => $request['new_login'],
				'confirmed' => $request['confirmed'],
				'key_user' => $request['key_user'],
				'ip_acesso' => $request['ip_acesso'],
				'data' => new Zend_Db_Expr('NOW()')
			);
			return $this->db_TbUserLoginChange->insert($dados);
		} catch (Exception $e) {
			die("Error 129");
		}
	}

	public function setConfirmedLoginChange($id)
	{
		$this->db_TbUserLoginChange = new Application_Model_DbTable_TbUserLoginChange();

		try {
			$dados = array(
				'confirmed' => 'Y'
			);
			$where = $this->db_TbUserLoginChange->getAdapter()->quoteInto("id = ?", $id);
			$this->db_TbUserLoginChange->update($dados, $where);
		} catch (Exception $e) {
			die("Error 130");
		}
	}

	public function deleteUserLoginChange($id)
	{
		$this->db_TbUserLoginChange = new Application_Model_DbTable_TbUserLoginChange();

		try {

			$where = $this->db_TbUserLoginChange->getAdapter()->quoteInto("id = ?", $id);
			$this->db_TbUserLoginChange->delete($where);
		} catch (Exception $e) {
			die("Error 131");
		}
	}

	//TB_USER_EMAIL_CHANGE BEGIN ############
	public function getUserEmailChange($where = null, $order = null, $limit = null)
	{
		$this->db_TbUserEmailChange = new Application_Model_DbTable_TbUserEmailChange();

		try {
			$select = $this->db_TbUserEmailChange->select()->from($this->db_TbUserEmailChange)->order($order)->limit($limit);
			if (!is_null($where)) {
				$select->where($where);
			}
			return $this->db_TbUserEmailChange->fetchRow($select);
		} catch (Exception $e) {
			die("Error 132");
		}
	}

	//TB_USER_EMAIL_CHANGE BEGIN ############
	public function getUserEmailChangeBlocked($id = null, $email = null)
	{
		$this->db_TbUserEmailChange = new Application_Model_DbTable_TbUserEmailChange();

		try {
			if (!is_null($id) || !is_null($email)) {
				$select = $this->db_TbUserEmailChange->select()
					->from($this->db_TbUserEmailChange);
				if (!is_null($id)) {
					$select->where("user_id = $id");
				} else {
					$select->where("new_email = '$email' AND (confirmed_old='N' OR confirmed_new='N')");
				}
				return $this->db_TbUserEmailChange->fetchRow($select);
			}
		} catch (Exception $e) {
			die("Error 133");
		}
	}

	public function getUserEmailChangeFind($id)
	{
		$this->db_TbUserEmailChange = new Application_Model_DbTable_TbUserEmailChange();

		try {
			$arr = $this->db_TbUserEmailChange->find($id)->toArray();
			return $arr[0];
		} catch (Exception $e) {
			die("Error 134");
		}
	}

	public function setUserEmailChange(array $request)
	{
		$this->db_TbUserEmailChange = new Application_Model_DbTable_TbUserEmailChange();

		try {
			$dados = array(
				'user_id' => $request['user_id'],
				'old_email' => $request['old_email'],
				'new_email' => $request['new_email'],
				'confirmed_old' => $request['confirmed_old'],
				'key_old' => $request['key_old'],
				'confirmed_new' => $request['confirmed_new'],
				'key_new' => $request['key_new'],
				'ip_acesso' => $request['ip_acesso'],
				'data' => new Zend_Db_Expr('NOW()')
			);
			return $this->db_TbUserEmailChange->insert($dados);
		} catch (Exception $e) {
			die("Error 135");
		}
	}

	public function updateUserEmailChange(array $request)
	{
		$this->db_TbUserEmailChange = new Application_Model_DbTable_TbUserEmailChange();

		try {

			$dados = array(
				'old_email' => $request['old_email'],
				'new_email' => $request['new_email'],
				'confirmed_old' => $request['confirmed_old'],
				'key_old' => $request['key_old'],
				'confirmed_new' => $request['confirmed_new'],
				'key_new' => $request['key_new'],
				'ip_acesso' => $request['ip_acesso'],
				'data' => new Zend_Db_Expr('NOW()')
			);
			$where = $this->db_TbUserEmailChange->getAdapter()->quoteInto("user_id = ?", $request['user_id']);
			$this->db_TbUserEmailChange->update($dados, $where);
		} catch (Exception $e) {
			die("Error 136");
		}
	}

	public function setConfirmedOldEmailChange($id)
	{
		$this->db = new Application_Model_DbTable_TbUserEmailChange();

		try {
			$dados = array(
				'confirmed_old' => 'Y'
			);
			$where = $this->db->getAdapter()->quoteInto("id = ?", $id);
			$this->db->update($dados, $where);
		} catch (Exception $e) {
			die("Error 137");
		}
	}

	public function setConfirmedNewEmailChange($id)
	{
		$this->db = new Application_Model_DbTable_TbUserEmailChange();

		try {
			$dados = array(
				'confirmed_new' => 'Y'
			);
			$where = $this->db->getAdapter()->quoteInto("id = ?", $id);
			$this->db->update($dados, $where);
		} catch (Exception $e) {
			die("Error 138");
		}
	}

	public function deleteUserEmailChange($id)
	{
		$this->db_TbUserEmailChange = new Application_Model_DbTable_TbUserEmailChange();

		try {
			$where = $this->db_TbUserEmailChange->getAdapter()->quoteInto("id = ?", $id);
			$this->db_TbUserEmailChange->delete($where);
		} catch (Exception $e) {
			die("Error 139");
		}
	}


	//TB_USER_BLOCKED BEGIN ############
	public function getUserBlocked($login)
	{
		$this->db_TbUserBlocked = new Application_Model_DbTable_TbUserBlocked();

		try {

			if (!is_null($login)) {
				$select = $this->db_TbUserBlocked->select()
					->from($this->db_TbUserBlocked)
					->where("login = '$login'");

				return $this->db_TbUserBlocked->fetchRow($select);
			}
		} catch (Exception $e) {
			die("Error 140");
		}
	}

	public function setUserBlocked($login)
	{
		$this->db_TbUserBlocked = new Application_Model_DbTable_TbUserBlocked();

		try {

			$dados = array(
				'login' => "$login"
			);
			return $this->db_TbUserBlocked->insert($dados);
		} catch (Exception $e) {
			die("Error 141");
		}
	}

	public function setUserOldBlocked($loginOld, $loginNew)
	{
		$this->db = new Application_Model_DbTable_TbUserBlocked();

		try {
			$dados = array(
				'login' => $loginOld
			);

			$where = $this->db->getAdapter()->quoteInto("login = ?", $loginNew);
			$this->db->update($dados, $where);
		} catch (Exception $e) {
			die("Error 142");
		}
	}

	public function deleteUserBlocked($login)
	{
		$this->db_TbUserBlocked = new Application_Model_DbTable_TbUserBlocked();

		try {
			$where = $this->db_TbUserBlocked->getAdapter()->quoteInto("login = ?", $login);
			$this->db_TbUserBlocked->delete($where);
		} catch (Exception $e) {
			die("Error 143");
		}
	}

	//TB_LOG_ALTERA_DADOS BEGIN ############
	public function setLogAlteraDados(array $request)
	{
		$this->db_TbLogAlteraDados = new Application_Model_DbTable_TbLogAlteraDados();

		try {

			$dados = array(
				'lad_descricao' => $request['lad_descricao'],
				'lad_idUsuario' => $request['lad_idUsuario'],
				'lad_ip_acesso' => $request['lad_ip_acesso'],
				'lad_data' => new Zend_Db_Expr('NOW()')
			);
			return $this->db_TbLogAlteraDados->insert($dados);
		} catch (Exception $e) {
			die("Error 144");
		}
	}

	//CHECK PIN #############
	public function checkPin($idusr, $pin)
	{
		$this->db_TbUser = new Application_Model_DbTable_TbUser();

		try {

			$select = $this->db_TbUser->select()
				->from($this->db_TbUser, array('usr_id', 'usr_pin', 'usr_pin_errors'))
				->where("usr_id = ?", $idusr);

			$verify = $this->db_TbUser->fetchRow($select);

			if (empty($pin)) {
				return "PIN é obrigatório. Obter um novo PIN na Minha Conta -> PIN de Segurança";
			}

			if (empty($verify['usr_pin'])) {
				return "Você não tem um PIN válido. Obter um novo PIN na Minha Conta -> PIN de Segurança";
			}

			if ($verify['usr_pin_errors'] == 2) {
				return "Seu PIN foi bloqueado. Obter um novo PIN na Minha Conta -> PIN de Segurança";
			}

			if ($verify['usr_pin'] != sha1("x16" . $pin)) {
				$newPinErrors = $verify['usr_pin_errors'] + 1;
				//executaSQL("UPDATE tb_user SET usr_pin_errors = '$newPinErrors' WHERE usr_id = '$idusr'");
				$dados = array(
					'usr_pin_errors' => $newPinErrors
				);
				$where = $this->db_TbUser->getAdapter()->quoteInto("usr_id = ?", $idusr);
				$this->db_TbUser->update($dados, $where);

				if ($verify['usr_pin_errors'] == 1) {
					return "Seu PIN foi bloqueado. Obter um novo PIN na Minha Conta -> PIN de Segurança";
				} else {
					return "PIN inválido. Se você errar novamente, ele será bloqueado e você terá que obter um novo PIN em Minha Conta -> PIN de segurança";
				}
			} else {
				return "valid";
			}
		} catch (Exception $e) {
			//die($e->getMessage());
			die("Error 145");
		}
	}

	public function getUserTotalDirects($user)
	{
		$this->db_TbUser = new Application_Model_DbTable_TbUser();

		try {
			$select = $this->db_TbUser->select()
				->from($this->db_TbUser, array('amount' => 'COUNT(*)'))
				->where("usr_invited_id = ?", $user)
				->where('usr_active <> ?', "d");

			$return = $this->db_TbUser->fetchRow($select);
			return $return["amount"];
		} catch (Exception $e) {
			die("Error 106");
		}
	}

	public function getUserUplineTotal($user)
	{
		$this->db = new Application_Model_DbTable_TbUnilevel();

		try {
			$select = $this->db->select()
				->from($this->db, array('amount' => 'COUNT(*)'))
				->where("unilevel_antecessor = ?", $user);

			$return = $this->db->fetchRow($select);
			return $return["amount"];
		} catch (Exception $e) {
			die("Error 106");
		}
	}

	public function verificaCPFDB($cpf)
	{
		$this->db_TbUser = new Application_Model_DbTable_TbUser();

		try {
			if (!is_null($cpf)) {
				$select = $this->db_TbUser->select()
					->from($this->db_TbUser, array('amount' => 'COUNT(*)'))
					->where("usr_cpf = ?", $cpf)
					->where('usr_active <> ?', "d");

				$return = $this->db_TbUser->fetchRow($select);
				return $return["amount"];
			} else {
				return 0;
			}
		} catch (Exception $e) {
			die($e->getMessage());
			die("Error 106+");
		}
	}

	public function verificaCNPJDB($cnpj)
	{
		$this->db_TbUser = new Application_Model_DbTable_TbUser();

		try {
			if (!is_null($cnpj)) {
				$select = $this->db_TbUser->select()
					->from($this->db_TbUser, array('amount' => 'COUNT(*)'))
					->where("usr_cnpj = ?", $cnpj)
					->where('usr_active <> ?', "d");

				$return = $this->db_TbUser->fetchRow($select);
				return $return["amount"];
			} else {
				return 0;
			}
		} catch (Exception $e) {
			die($e->getMessage());
			die("Error 106++");
		}
	}

	public function calculateAge($date){

		$time = strtotime($date);

		if($time === false){
			return '';
		}

		$year_diff = '';
		$date = date('Y-m-d', $time);
		list($year,$month,$day) = explode('-',$date);
		$year_diff = date('Y') - $year;
		$month_diff = date('m') - $month;
		$day_diff = date('d') - $day;
		if ($day_diff < 0 || $month_diff < 0) $year_diff--;

		return $year_diff;
	}

	public function validateDate($date, $format = NULL)
	{
		$d = DateTime::createFromFormat($format, $date);
		return $d && $d->format($format) == $date;
	}

	// public function idade($data){

	// 	// separando yyyy, mm, ddd
	// 	list($ano, $mes, $dia) = explode('-', $data);
	
	// 	// data atual
	// 	$hoje = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
	// 	// Descobre a unix timestamp da data de nascimento do fulano
	// 	$nascimento = mktime( 0, 0, 0, $mes, $dia, $ano);
	
	// 	// cálculo
	// 	$idade = floor((((($hoje - $nascimento) / 60) / 60) / 24) / 365.25);
	// }

	public function validaCNPJ($cnpj)
	{
		$cnpj = preg_replace('/[^0-9]/', '', (string) $cnpj);

		// Valida tamanho
		if (strlen($cnpj) != 14)
			return false;

		// Verifica se todos os digitos são iguais
		if (preg_match('/(\d)\1{13}/', $cnpj))
			return false;

		// Valida primeiro dígito verificador
		for ($i = 0, $j = 5, $soma = 0; $i < 12; $i++) {
			$soma += $cnpj[$i] * $j;
			$j = ($j == 2) ? 9 : $j - 1;
		}

		$resto = $soma % 11;

		if ($cnpj[12] != ($resto < 2 ? 0 : 11 - $resto))
			return false;

		// Valida segundo dígito verificador
		for ($i = 0, $j = 6, $soma = 0; $i < 13; $i++) {
			$soma += $cnpj[$i] * $j;
			$j = ($j == 2) ? 9 : $j - 1;
		}

		$resto = $soma % 11;

		return $cnpj[13] == ($resto < 2 ? 0 : 11 - $resto);
	}

	function validaCPF($cpf)
	{

		// Extrai somente os números
		$cpf = preg_replace('/[^0-9]/is', '', $cpf);

		// Verifica se foi informado todos os digitos corretamente
		if (strlen($cpf) != 11) {
			return false;
		}

		// Verifica se foi informada uma sequência de digitos repetidos. Ex: 111.111.111-11
		if (preg_match('/(\d)\1{10}/', $cpf)) {
			return false;
		}

		// Faz o calculo para validar o CPF
		for ($t = 9; $t < 11; $t++) {
			for ($d = 0, $c = 0; $c < $t; $c++) {
				$d += $cpf{
				$c} * (($t + 1) - $c);
			}
			$d = ((10 * $d) % 11) % 10;
			if ($cpf{
			$c} != $d) {
				return false;
			}
		}
		return true;
	}
}
