<?php

class Application_Model_Bonus
{

   public function getBonusAmount($id)
   {
	   $this->db = new Application_Model_DbTable_TbBnsConsolidado();
		try
		{
	
			$select = $this->db->select()
									->from($this->db,array('amount' => 'COUNT(*)'))
									->where("bcons_idUsuario = ?",$id);
			
			$return = $this->db->fetchRow($select);
			return $return["amount"];
			
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error 150");
			
		}
   }
   
   public function getBonusLimit($idUser)
   {
	   $this->db = new Application_Model_DbTable_TbBnsUsuario();
		try
		{
	
			$select = $this->db->select()
									->from($this->db,array('amount' => 'COUNT(*)'))
									->where("(busr_investimento_total = 0 OR busr_investimento_total IS NULL)")
									->where("(busr_bonus_limite = 0 OR busr_bonus_limite IS NULL)")
									->where("busr_idUsuario = ?",$idUser);
			
			$return = $this->db->fetchRow($select);
			return $return["amount"];
			
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error 150+1");
			
		}
   }
   
   public function getOpenDepositLimit($idUser)
   {
	   $this->db = new Application_Model_DbTable_TbBnsUsuario();
	   
		try
		{
	
			$select = $this->db->select()
									->from($this->db,array('busr_status'))
									->where("busr_idUsuario = ?",$idUser);
									
			$return = $this->db->fetchRow($select);			

			return $return["busr_status"];
			
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error 150+3");
			
		}
   }
   
   public function getOpenDepositFee($idUser,$withdrawalId = 0)
   {
	   $this->db = new Application_Model_DbTable_TbBitcoinOperations();
	   
		try
		{
	
			$select = $this->db->select()
									->from($this->db,array('amount' => 'COUNT(*)'))
									->where("bco_withdrawal_id <> 0")
									->where("bco_status <> 'P' AND bco_status <> 'U'")
									->where("bco_user = ?",$idUser);
									
			if($withdrawalId!=0)
			{
			  $select->where("bco_withdrawal_id <> $withdrawalId");
			}
									
			$return = $this->db->fetchRow($select);
			
			if($return["amount"]<>0){
				return 2;
			}else{
				return 0;
			}
			
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error 150+4");
			
		}
   }
   
   public function getOpenDeposit($idUser)
   {   
	   
		try
		{
	
			$fee = $this->getOpenDepositFee($idUser);
			
			$limit = $this->getOpenDepositLimit($idUser);
									
			return $fee+$limit;
									

			
		}catch(Exception $e){
			die($e->getMessage());
			die("Error 150+2");
			
		}
   }
   
   public function getBonusConsolidated($user,$date,$order)
   {
	   $this->db = new Application_Model_DbTable_TbBnsConsolidado();
	   
	   $date = explode("-",$date);
	   $month = $date[0];
	   $year = $date[1];
	   
		try{
	
			$select = $this->db->select()
									->from($this->db)
									->where("bcons_idUsuario = ?",$user)
									->where('YEAR(bcons_data) = ?', $year)
									->where('MONTH(bcons_data) = ?', $month)
									->order("bcons_data $order");
			
			return $this->db->fetchAll($select);
			
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error 151");
			
		}
   }
   
   public function getBonusValues($user)
   {
	   $this->db = new Application_Model_DbTable_TbBnsConsolidado();
	   
		try{
	
			$select = $this->db->select()
									->from($this->db,array('sum(bcons_valor_startfast) as startfast','sum(bcons_valor_team) as team','sum(bcons_valor_sharing_bonus) as sharing'))
									->where("bcons_idUsuario = ?",$user);
									
			return $this->db->fetchRow($select);
			
			
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error 151+sum");
			
		}
   }
   
   public function getPointsVME($user)
   {
	   $this->db = new Application_Model_DbTable_TbBns();
	   
		try{
	
			$select = $this->db->select()
									->from($this->db,array('sum(bon_pontos) as pontos'))
									->where("bon_idUpline = ?",$user);
									
			$return = $this->db->fetchRow($select);
			return $return["pontos"];
			
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error 151+sum");
			
		}
   }
   
   public function getBonusBinary($user,$date,$order,$type)
   {
	   
	   $date = explode("/",$date);
	   $day = $date[0];
	   $month = $date[1];
	   $year = $date[2];
		
	   
		try{
	
			$db = Zend_Db_Table_Abstract::getDefaultAdapter();
			$select = $db->select()
									->from('tb_bns',array('bon_data','bon_tipo','bon_perna','bon_pontos'))
									->join('tb_user','bon_idUsuario=usr_id',array('usr_login_id'))
									->join('tb_bns_compra','bon_idCompra=comp_id',array())
									->join('tb_product','comp_idProduto=prod_id',array('prod_titulo'));
									
									if($type=="start")
									{
										$select->where('bon_tipo = 4');
									}
									else
									{
										$select->where("(bon_tipo = 2 OR bon_tipo = 3)");
									}
									
									$select->where("bon_idUpline = ?",$user)
									->where('YEAR(bon_data) = ?', $year)
									->where('MONTH(bon_data) = ?', $month)
									->where('DAY(bon_data) = ?', $day);

									$select->order("bon_data $order");
			
			return $db->fetchAll($select);
			
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error 152");
			
		}
   }
   
   public function getBonus($user,$start_date,$end_date,$order,$type)
   {

        /*$data = DateTime::createFromFormat('d/m/Y', $start_date);
        if(!($data && $data->format('d/m/Y') === $start_date)){
           $start_date = date("d/m/Y");
		}
        
        $data2 = DateTime::createFromFormat('d/m/Y', $end_date);
        if(!($data2 && $data2->format('d/m/Y') === $end_date)){
           $end_date = date("d/m/Y");
		}*/
		
		$start_date = explode("/",$start_date);
		$start_date = $start_date[2]."-".$start_date[1]."-".$start_date[0];
	   
        $end_date = explode("/",$end_date);
		$end_date = $end_date[2]."-".$end_date[1]."-".$end_date[0];
		
	   
		try{
	
			$db = Zend_Db_Table_Abstract::getDefaultAdapter();
			$select = $db->select()
									->from('tb_bns',array('bon_data','bon_tipo','bon_valor'))
									->join('tb_user','bon_idUsuario=usr_id',array('usr_login_id'))
									->where("bon_idUpline = ?",$user)
									->where("DATE(bon_data) >= ?",  $start_date)
									->where("DATE(bon_data) <= ?",  $end_date);
									if(!empty($type)){
									    $select->where('bon_tipo = ?',$type);
									}else{
									    $select->where('bon_tipo <> 4');
									}
									$select->order("bon_data $order");
			//$sql = $select->__toString();
            //echo "$sql\n";
			return $db->fetchAll($select);
			
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error 152++");
			
		}
   }
   
    public function getBonusExecutiveCheck($user,$date,$order,$type)
   {
	   
	   $date = explode("/",$date);
	   $day = $date[0];
	   $month = $date[1];
	   $year = $date[2];
		
	   
		try{
	
			$db = Zend_Db_Table_Abstract::getDefaultAdapter();
			$select = $db->select()
									->from('tb_bns',array('bon_data','bon_tipo','bon_perna','bon_pontos'))
									->where("bon_tipo = 5")
									->where("bon_idUpline = ?",$user)
									->where('YEAR(bon_data) = ?', $year)
									->where('MONTH(bon_data) = ?', $month)
									->where('DAY(bon_data) = ?', $day);
									
									
			$select->order("bon_data $order");
			
			return $db->fetchAll($select);
			
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error 152");
			
		}
   }
   
    public function getBonusExecutive($user,$date,$order,$type)
   {
	   
	   $date = explode("/",$date);
	   $day = $date[0];
	   $month = $date[1];
	   $year = $date[2];
		
	   
		try{
	
			$db = Zend_Db_Table_Abstract::getDefaultAdapter();
			$select = $db->select()
									->from('tb_bns',array('bon_data','bon_tipo','bon_perna','bon_pontos'))
									->where("bon_tipo = 6")
									->where("bon_idUpline = ?",$user)
									->where('YEAR(bon_data) = ?', $year)
									->where('MONTH(bon_data) = ?', $month)
									->where('DAY(bon_data) = ?', $day);
									
									
			$select->order("bon_data $order");
			
			return $db->fetchAll($select);
			
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error 152");
			
		}
   }
   
	public function getVipProgram($id = null, $btc = null,$qualification = null)
	{
		$this->db = new Application_Model_DbTable_TbBnsVipProgram();
		
		try
		{		
			$select = $this->db->select()
									->from($this->db,array('vip_id','vip_nome','vip_porcentagem'));
										
			if(!is_null($btc) && !is_null($qualification)){
				$select->where("vip_valor <= '$btc' AND vip_id>'$qualification'");
										
			}elseif(!is_null($id)){
				$select->where("vip_id = $id");
			}
				
			return $this->db->fetchRow($select);
			
		}catch(Exception $e){
			die("Error 156+4");
			
		}
	}
   
	public function getUserVipProgram($user)
	{
		$this->db = new Application_Model_DbTable_TbUser();
		
	   try
	   {
			if(!is_null($user)){
				
				$select = $this->db->select()
										->from($this->db,array('usr_qualification'))
										->where("usr_id = ?", $user);
				
				$return = $this->db->fetchRow($select);
				return $return["usr_qualification"];
			
			}
			
		}catch(Exception $e){
			die("Error 156+3");
			
		}
	}
	
	public function setUserVipProgram($user,$qualification)
	{
		$this->db = new Application_Model_DbTable_TbUser();
		
	   try
	   {
			$data['usr_qualification'] = $qualification;
			$where = $this->db->getAdapter()->quoteInto("usr_id = ?", $user);
			$this->db->update($data, $where);
			
		}catch(Exception $e){
			die("Error 156+5");
			
		}
	}
   
	public function getBnsUserVipProgram($user)
	{
		$this->db = new Application_Model_DbTable_TbBnsUsuario();
		
	   try
	   {
			if(!is_null($user)){
				
				$select = $this->db->select()
										->from($this->db,array('busr_idUsuario','busr_vip_program'))
										->where("busr_idUsuario = ?", $user);
				
				return $this->db->fetchRow($select);
			
			}
			
		}catch(Exception $e){
			die("Error 156+2");
			
		}
	}
   
	public function setBnsUserVipProgram($user,$btc)
	{
		$this->db = new Application_Model_DbTable_TbBnsUsuario();
		
	   try
	   {
			if(!is_null($btc)){
				$amountBTC = $this->getBnsUserVipProgram($user);
				
				if(empty($amountBTC["busr_idUsuario"])){
					
					$amount = $btc;
					
					$dados = array(
					'busr_idUsuario' => $user,
					'busr_vip_program' => $amount
					);
					return $this->db->insert($dados);
					
				}else{
					$amount = $amountBTC["busr_vip_program"] + $btc;
					$data['busr_vip_program'] = $amount;
					$where = $this->db->getAdapter()->quoteInto("busr_idUsuario = ?", $user);
					$this->db->update($data, $where);
				}
				
				//Pega a qualifica��o Vip Program do usu�rio
				/*$qualification = $this->getUserVipProgram($user);
				//Pega a ID do Vip Program com refer�ncia na quantidade de BTC
				$vipProgram = $this->getVipProgram(NULL, $amount);
				
				//Se o ID do Vip Pragram for maior que a qualification do usu�rio, atualiza
				if($vipProgram["vip_id"]>$qualification){
					//Set na tb_user a nova qualifica��o do Vip Program
					$this->setUserVipProgram($user,$vipProgram["vip_id"]);
					
				}*/
				
			}
			
		}catch(Exception $e){
			die("Error 156+1");
			
		}
	}
	
}

