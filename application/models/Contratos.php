<?php

class Application_Model_Contratos
{

    public function getPlanID($id)
    {
        $this->db_TbUserPlans = new Application_Model_DbTable_TbUserPlans();
        
         try{
             if(!is_null($id)){
     
               $select = $this->db_TbUserPlans->select()
                                         ->from($this->db_TbUserPlans,array('count' => 'COUNT(*)','uplan_prod_id','uplan_pedido_id'))
                                         ->where("uplan_id = ?",$id)
                                         ->where("uplan_status = 2");
               
               return $this->db_TbUserPlans->fetchRow($select);

             }
         }catch(Exception $e){
             die("Error 189++");
             
         }
    }

    public function getTotalValuePlanUser($id)
    {
        $this->db_TbUserPlans = new Application_Model_DbTable_TbUserPlans();
        
         try{
             if(!is_null($id)){
     
               $select = $this->db_TbUserPlans->select()
                                         ->from($this->db_TbUserPlans,array('sum' => 'SUM(uplan_value)'))
                                         ->where("uplan_user_id = ?",$id)
                                         ->where("uplan_status = 1");
               
               return $this->db_TbUserPlans->fetchRow($select);

             }
         }catch(Exception $e){
             die("Error 189++");
             
         }
    }

	public function renewPlan($id,$user,$prod)
	{
        $this->db_TbUserPlans = new Application_Model_DbTable_TbUserPlans();
        $this->modelEwallet = new Application_Model_Ewallet();
        $this->modelOrder = new Application_Model_Order();
	  
		try
		{
            //Produto atual que será o anterior
            $prod_ant = $prod;
            //Seta o produto 1 por padrão // 6 meses - saque mensal - 2,5%
            $prod = 1;
            //Pega informações do produto
            $product = $this->modelOrder->getProductBasic($prod);
            $prod_validade_dias = $product["prod_validade_dias"];

            //A data de hoje
            $currentDate = date("d-m-Y");
            //Data final do plano
            $dateEnd = $this->modelEwallet->SomaMeses($currentDate,$prod_validade_dias/30);

			/** faz o update do plano */
			$dados = array(
             'uplan_prod_id' => $prod,
             'uplan_day_start' => date("Y-m-d"),
             'uplan_day_end' => $dateEnd,
             'uplan_data_rendimento' => '0000-00-00',
             'uplan_data_liberado' => '0000-00-00',
             'uplan_status' => '1'
			);
			$where = $this->db_TbUserPlans->getAdapter()->quoteInto("uplan_id = ?", $id);
			$this->db_TbUserPlans->update($dados, $where);
			
            $this->modelOrder->setLog('Renew plan '.$id.'Prod Ant:'.$prod_ant, 0, $user);
            
            return "ok_renew";
		
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error 198+++");
			
		}
		
    }

    //Finaliza o produto e devolve o valor investido para o cliente
	public function withdrawPlan($id,$user,$order)
	{
        $this->db_TbUserPlans = new Application_Model_DbTable_TbUserPlans();
        $this->modelEwallet = new Application_Model_Ewallet();
        $this->modelOrder = new Application_Model_Order();
	  
		try
		{

			/** faz o update do plano */
			$dados = array(
             'uplan_status' => '3'
			);
			$where = $this->db_TbUserPlans->getAdapter()->quoteInto("uplan_id = ?", $id);
            $this->db_TbUserPlans->update($dados, $where);

            //Busca a fatura pelo número do pedido
            $orderInvoice = $this->modelOrder->getInvoiceOrder($order);
            //Pega o valor da fatura paga no pedido do plano
            $value = $orderInvoice["fat_valor"];

            //Devolve o valor para o usuários na 
            $this->modelEwallet->addCredit($user, 0, $value, "Estorno contrato [$id]", "r");
			
            $this->modelOrder->setLog('Withdraw plan '.$id, $value, $user);
            
            return "ok_withdraw";
		
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error 198---");
			
		}
		
	}

}

