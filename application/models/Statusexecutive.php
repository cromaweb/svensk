<?php

class Application_Model_Statusexecutive
{
   
   public function getPoints($user)
   {
	   $this->db = new Application_Model_DbTable_TbBnsUsuario();
	   
		try{
	
			$select = $this->db->select()
									->from($this->db,array('busr_pontos_esq_red','busr_pontos_dir_red','busr_pontos_esq_blue','busr_pontos_dir_blue','busr_pontos_esq_green','busr_pontos_dir_green'))
									->where("busr_idUsuario = ?",$user);
			
			return $this->db->fetchRow($select);
			
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error 239");
			
		}
   }
   
	public function getTitleName($id)
	{
	    $this->db = new Application_Model_DbTable_TbBnsTitulo();
	    
		try
		{
			$select = $this->db->select()
									->from($this->db,'tit_nome')
									->where("tit_id = $id");
			$return = $this->db->fetchRow($select);
			return $return["tit_nome"];
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error 222");
			
		}
	}
  
}

