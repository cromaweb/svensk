<?php

class Application_Model_Deposit
{
	public function __construct()
	{
	  $this->db_TbWallets = new Application_Model_DbTable_TbWallets();
	  $this->db_TbWalletAddresses = new Application_Model_DbTable_TbWalletAddresses();
	  $this->db_TbBitcoinOperations = new Application_Model_DbTable_TbBitcoinOperations();
	}

	public function getAllDeposits($user,$limit)
   {
	   
	   try
	   {
			$select = $this->db_TbBitcoinOperations->select()
									->from($this->db_TbBitcoinOperations)
									->where("bco_user = $user AND bco_type = 'D' AND (bco_date_insert > DATE_SUB(NOW(), INTERVAL 1 DAY) OR ((bco_limit<>0 OR bco_withdrawal_id<>0) AND bco_status<>'P' AND bco_status<>'C') )")
							->order("bco_id desc")
							->limit($limit);
			return $this->db_TbBitcoinOperations->fetchAll($select)->toArray();
		}catch(Exception $e){
			die($e->getMessage());
			die("Error 153");
			
		}
   }
	
    public function setDeposit(array $data)
    {
       try
       {
    		$data['bco_date_insert'] = date("Y-m-d H:i:s");
    		$data['bco_date_update'] = $data['bco_date_insert'];
    		return $this->db_TbBitcoinOperations->insert($data);
    	}catch(Exception $e){
    		die("Error 154");
    		
    	}
    }
    
   public function getDeposit($id)
   {
	   try
	   {
			$select = $this->db_TbBitcoinOperations->select()
						 ->from($this->db_TbBitcoinOperations)
						 ->where("bco_id = $id");
			return $this->db_TbBitcoinOperations->fetchRow($select)->toArray();
		}catch(Exception $e){
			die("Error 155");
			
		}
   }
   
   public function getDepositTransaction($transaction)
   {
	   try
	   {
		   if(!is_null($transaction)){
				$select = $this->db_TbBitcoinOperations->select()
							 ->from($this->db_TbBitcoinOperations)
							 ->where("bco_transaction_id = ?",$transaction);
				return $this->db_TbBitcoinOperations->fetchRow($select)->toArray();
		   }
		}catch(Exception $e){
			die($e->getMessage());
			die("Error 155+1");
			
		}
   }

   public function getDepositRoutine($id,$value)
   {
	   try
	   {
			$select = $this->db_TbBitcoinOperations->select()
						 ->from($this->db_TbBitcoinOperations)
						 ->where("bco_user = ?",$id)
						 ->where("bco_type = 'D'")
						 ->where("bco_status = 'W'")
						 ->where("bco_bitcoin_amount = ?",$value);

			$return = $this->db_TbBitcoinOperations->fetchRow($select);

			//Se o valor em dolar no campo for maior que zero
			if($return["bco_dollar_amount"]>0){
				//Atualiza campo confirmando o depósito
				$data['bco_status'] = 'P';
				$where = $this->db_TbBitcoinOperations->getAdapter()->quoteInto("bco_id = ?", $return["bco_id"]);
				$this->db_TbBitcoinOperations->update($data, $where);
				//Retira o valor da taxa
				$value = $return["bco_dollar_amount"] - $return["bco_dollar_fee"];
				//Retorna o valor em dolar
				return $value;

			}else{
				return 0;
			}
			

		}catch(Exception $e){
			die("Error 155");
			
		}
   }

	public function updateDeposit($id, array $data)
	{
	   try
	   {
			if(!is_null($id)){	
				$data['bco_date_update'] = date("Y-m-d H:i:s");
				$where = $this->db_TbBitcoinOperations->getAdapter()->quoteInto("bco_id = ?", $id);
				$this->db_TbBitcoinOperations->update($data, $where);
			}
		}catch(Exception $e){
			die("Error 156");
			
		}
	}

	public function updateOpenDeposit($user)
	{
	   try
	   {	
			$data['bco_status'] = "C";
			$where = array();
			$where[] = $this->db_TbBitcoinOperations->getAdapter()->quoteInto("bco_user = ?", $user);
			$where[] = $this->db_TbBitcoinOperations->getAdapter()->quoteInto("bco_status = ?", 'W');
			$this->db_TbBitcoinOperations->update($data, $where);
		}catch(Exception $e){
			die("Error 156 ++");
			
		}
	}
	
   public function getUserAddress($id)
   {
	   try
	   {
			$select = $this->db_TbWalletAddresses->select()
						 ->from($this->db_TbWalletAddresses,array('wallet_address_address'))
						 ->where("wallet_address_user_id = ?",$id)
						 ->where("wallet_address_active = 1")
						 ->limit(1);
			$return = $this->db_TbWalletAddresses->fetchRow($select);
			return $return["wallet_address_address"];
			
		}catch(Exception $e){
			die($e->getMessage());
			//die("Error 155+1");
			
		}
   }

   public function getCountUserAddress($id)
   {
	   try
	   {
			$select = $this->db_TbWalletAddresses->select()
						 ->from($this->db_TbWalletAddresses,array('amount'=>'COUNT(*)'))
						 ->where("wallet_address_user_id = ?",$id);
			$return = $this->db_TbWalletAddresses->fetchRow($select);
			return $return["amount"];
			
		}catch(Exception $e){
			//die("Error 155");
			die($e->getMessage());
			
		}
   }
   
   public function getNewAddress()
   {
	   try
	   {
			$select = $this->db_TbWalletAddresses->select()
						 ->from($this->db_TbWalletAddresses,array('wallet_address_address'))
						 ->where("wallet_address_user_id = 0")
						 ->where("wallet_address_active = 1")
						 ->limit(1);
			$return = $this->db_TbWalletAddresses->fetchRow($select);
			return $return["wallet_address_address"];
			
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error 155+1u");
			
		}
   }
   
	public function getUserWallet($id)
	{
	   try
	   {
			    
			$select = $this->db_TbWallets->select()
						 ->from($this->db_TbWallets,array('wallet_id','wallet_amount','wallet_address'))
						 ->where("wallet_user_id = ?",$id);
						 
			return $this->db_TbWallets->fetchRow($select);
			

		}catch(Exception $e){
			die("Error 156e $user");
			//die($e->getMessage());
			
		}
	}

	public function getWalletAddress($address)
	{
	   try
	   {
			    
			$select = $this->db_TbWalletAddresses->select()
						 ->from($this->db_TbWalletAddresses,array('wallet_address_id','wallet_address_user_id','wallet_address_balance'))
						 ->where("wallet_address_address = ?",$address);
						 
			return $this->db_TbWalletAddresses->fetchRow($select);
			

		}catch(Exception $e){
			die("Error 156f");
			//die($e->getMessage());
			
		}
	}

	public function setUserNewAddress($id)
	{
	   try
	   {
			if(!is_null($id)){
			    
			    $address = $this->getNewAddress();
				
				$where = $this->db_TbWalletAddresses->getAdapter()->quoteInto("wallet_address_address = ?", $address);
				$this->db_TbWalletAddresses->update(array("wallet_address_user_id" => $id,"created_at" => date("Y-m-d H:i:s"), "updated_at" => date("Y-m-d H:i:s")), $where);
				return $address;

			}
		}catch(Exception $e){
			die("Error 156c");
			//die($e->getMessage());
			
		}
	}
	
	public function setUserWallet($id,$address)
	{
	   try
	   {
			    
			$select = $this->db_TbWallets->select()
						 ->from($this->db_TbWallets,array('exists'=>'COUNT(*)'))
						 ->where("wallet_user_id = ?",$id);
			$return = $this->db_TbWallets->fetchRow($select);
			
			if($return["exists"]==0)
			{
    			$array =  array("wallet_user_id" => $id,
    							   "wallet_address" => $address,
    							   "updated_at" => date("Y-m-d H:i:s"));
    			
    			$this->db_TbWallets->insert($array);
			}
			else
			{
    			$where = $this->db_TbWallets->getAdapter()->quoteInto("wallet_user_id = ?", $id);
    			$this->db_TbWallets->update(array("wallet_user_id" => $id,"wallet_address" => $address ,"updated_at" => date("Y-m-d H:i:s")), $where);
			}
			


		}catch(Exception $e){
			die("Error 156x");
			//die($e->getMessage());
			
		}
	}
	
	public function setUserWalletBalance($id,$amount,$idAddress,$balance)
	{
	   try
	   {

			$where = $this->db_TbWallets->getAdapter()->quoteInto("wallet_id = ?", $id);
			$this->db_TbWallets->update(array("wallet_amount" => $amount,"updated_at" => date("Y-m-d H:i:s")), $where);
			
			$where = $this->db_TbWalletAddresses->getAdapter()->quoteInto("wallet_address_id = ?", $idAddress);
			return $this->db_TbWalletAddresses->update(array("wallet_address_balance" => $balance,"updated_at" => date("Y-m-d H:i:s")), $where);

		}catch(Exception $e){
			die("Error 156+1");
			//die($e->getMessage());
			
		}
	}

	public function getOpenDeposit($idUser)
	{
		$this->db = new Application_Model_DbTable_TbBitcoinOperations();
		
		 try
		 {
	 
			 $select = $this->db->select()
									 ->from($this->db,array('amount' => 'COUNT(*)'))
									 ->where("bco_type = 'D'")
									 ->where("bco_status = 'W'")
									 ->where("bco_user = ?",$idUser);
									 
									 
			 return $this->db->fetchRow($select);
			 
		 }catch(Exception $e){
			 //die($e->getMessage());
			 die("Error 150+4");
			 
		 }
	}
	
   public function addCredit($clientId,$creditValue,$message)
   {
		$this->db_TbAlteraLog = new Application_Model_DbTable_TbAlteraLog();
		$this->db_TbFinanceiroSaldo = new Application_Model_DbTable_TbFinanceiroSaldo();
		$this->db_TbFinanceiroExtrato = new Application_Model_DbTable_TbFinanceiroExtrato();

	   try
	   {
			$dia_hoje = date("Y-m-d H:i:s");
			$checkBalance = $this->db_TbFinanceiroSaldo->find($clientId);
			
			if(count($checkBalance) == 0){
				//Atualiza o saldo disponível 
				 $this->db_TbFinanceiroSaldo->insert(array("bsal_idUsuario" => $clientId, "bsal_saldo_liberado" => $creditValue));
				 $saldo_atual = $creditValue;
				 $saldo_anterior = 0;
			
			}else{
				 $select = $this->db_TbFinanceiroSaldo->select()
								->from($this->db_TbFinanceiroSaldo)
								->where("bsal_idUsuario = ?",$clientId);
				
				 $return = $this->db_TbFinanceiroSaldo->fetchRow($select)->toArray();
				 $saldo_anterior = $return['bsal_saldo_liberado'];
				 $saldo_atual = $return['bsal_saldo_liberado'] + $creditValue;
				
				 //Verifica o saldo do usuário
				 $where = $this->db_TbFinanceiroSaldo->getAdapter()->quoteInto("bsal_idUsuario = ?", $clientId);
				 $this->db_TbFinanceiroSaldo->update(array("bsal_saldo_liberado" => $saldo_atual), $where);
			
			}
			
			$arrExtrato =  array("extf_idUsuario" => $clientId,
							   "extf_valor" => $creditValue,
							   "extf_descricao" => $message,
							   "extf_conta" => "l",
							   "extf_saldo_anterior" => $saldo_anterior,
							   "extf_saldo_atual" => $saldo_atual,
							   "extf_tipo" => 'c',
							   "extf_data" => $dia_hoje);
			
			$this->db_TbFinanceiroExtrato->insert($arrExtrato);
			
			$this->db_TbAlteraLog->insert(array("logalt_tipo" => $message, 
											  "logalt_valor" => $creditValue,
											  "logalt_saldo_anterior" => $saldo_anterior,
											  "logalt_usuario" => $clientId,
											  "logalt_data" => $dia_hoje));
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error 158");
			
		}

   }

   public function setLog($text)
   {
	   try
	   {
			$this->db_TbBitcoinLog->insert(array("text" => $text));
	  
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error 159");
			
		}
   }

}

