<?php

class Application_Model_Signup
{
	public function __construct()
	{
		
	  $this->db_TbControlePerna = new Application_Model_DbTable_TbControlePerna();
	  $this->db_TbOrganizacao = new Application_Model_DbTable_TbOrganizacao();
	  $this->db_TbLogPerna = new Application_Model_DbTable_TbLogPerna();
	  $this->modelTreeview = new Application_Model_Treeview();
	  
	}
	
   public function getControlePerna($id)
   {
		try
		{
				if(!is_null($id)){
				  $select = $this->db_TbControlePerna->select()
											->from($this->db_TbControlePerna)
											->where("cper_idUsuario = $id");
				  return $this->db_TbControlePerna->fetchRow($select);
				}
		}catch(Exception $e){
			die("Error 209");
			
		}
   }
	
   public function setControlePerna($id)
   {
		try
		{
			if(!is_null($id)){
			  $dados = array(
				'cper_idUsuario' => $id
			  );
			  return $this->db_TbControlePerna->insert($dados);
			}
		}catch(Exception $e){
			die("Error 210");
			
		}
   }
   
   public function updateControlePerna(array $request,$leg = null)
   {
		try
		{
			if(!is_null($request) && !is_null($leg))
			{
				if($leg=="E"){
					$dados = array(
					'cper_ultimoIdPernaEsquerda' => $request["idPernaEsquerda"],
					'cper_qtdPernaEsquerda' => $request["qtdPernaEsquerda"]
					);
				}else{
					$dados = array(
					'cper_ultimoIdPernaDireita' => $request["idPernaDireita"],
					'cper_qtdPernaDireita' => $request["qtdPernaDireita"]
					);
				}
				//return $this->db_TbControlePerna->insert($dados);
				$where = $this->db_TbControlePerna->getAdapter()->quoteInto("cper_idUsuario = ?", $request['id']);
				$this->db_TbControlePerna->update($dados, $where);
			}
		}catch(Exception $e){
			die("Error 211");
			
		}
   }
   
   public function setLogPerna(array $request)
   {
		try
		{
			if(!is_null($request))
			{
			  $dados = array(
				'logp_idPai' => $request["idPai"],
				'logp_idUsuario' => $request["idUltimo"],
				'logp_perna' => $request["perna"],
				'logp_data' => new Zend_Db_Expr('NOW()')
				
			  );
			  return $this->db_TbLogPerna->insert($dados);
			}
		}catch(Exception $e){
			die("Error 212");
			
		}
   }
   
   public function getPreferedLeg($id,$preferedLeg)
   {
		try
		{
			if ($preferedLeg==1)
			{
			
				$pernaD = $this->modelTreeview->getCountLegsRight($id);
				$pernaE = $this->modelTreeview->getCountLegsLeft($id);
			
				if ($pernaE > $pernaD)
				{
					$posicao = "D";
				}
				else
				{
					$posicao = "E";
				}
			
			}
			
			if ($preferedLeg==2)
			{
				$posicao = "D";
			}
			
			if ($preferedLeg==3)
			{
				$posicao = "E";
			}
			
			return $posicao;
		}catch(Exception $e){
			die("Error 213");
			
		}
	   
   }
   
   public function setOrganizacao(array $request)
   {
		try
		{
			if(!is_null($request)){
			  $dados = array(
				'org_idUsuario' => $request['idUsr'],
				'org_pai' => $request['idPai'],
				'org_perna' => $request['perna'],
				'org_patrocinador' => $request['idPatrocinador'],
				'org_idSub' => '1',
				'org_data' => $request['date']
			  );
			  return $this->db_TbOrganizacao->insert($dados);
			}
		}catch(Exception $e){
			die("Error 214");
			
		}
   }
   
	###############--------------------------------------###############
	public function getLastIdLeg($id)
	{
		try
		{
			if(!empty($id)){
		
			$regPaiPerna = $this->getControlePerna($id);
		
			$ultimoIdPernaEsquerda = $regPaiPerna["cper_ultimoIdPernaEsquerda"];
			$ultimoIdPernaDireita = $regPaiPerna["cper_ultimoIdPernaDireita"];
			$qtdPernaEsquerda = $regPaiPerna["cper_qtdPernaEsquerda"];
			$qtdPernaDireita = $regPaiPerna["cper_qtdPernaDireita"];
		
			//Verifica se o ID é igual a 0, se for confere se existe filho
			$ultIdFilhoEsq = $ultimoIdPernaEsquerda;
			
			if($ultimoIdPernaEsquerda==0){
		
				$ultimoIdPernaEsquerda = $this->modelTreeview->getChildOrg($id,'E');
				
				if(count($ultimoIdPernaEsquerda)>0){
		
					++$qtdPernaEsquerda;
					
					//Log Pernas
					$log["idPai"] = $id;
					$log["idUltimo"] = $ultimoIdPernaEsquerda;
					$log["perna"] = 'E';
	
					$this->setLogPerna($log);
				}
			}
		
			//Verifica se o ID é igual a 0, se for confere se existe filho
			$ultIdFilhoDir = $ultimoIdPernaDireita;
		
			if($ultimoIdPernaDireita==0){
		
				$ultimoIdPernaDireita = $this->modelTreeview->getChildOrg($id,'D');
	
				if(count($ultimoIdPernaDireita)>0){
		
					++$qtdPernaDireita;
					
					//Log Pernas
					$log = array();
					$log["idPai"] = $id;
					$log["idUltimo"] = $ultimoIdPernaDireita;
					$log["perna"] = 'D';
					
					$this->setLogPerna($log);
		
				}
		
			}
		
			//Atualizar dados da Perna Esquerda Patrocinador
			$contEsq = 0;
			$ultIdPernaEsquerda = $ultimoIdPernaEsquerda;
		
			while($contEsq==0)
			{
				
				$verificaPernaEsquerda = $this->modelTreeview->getChildOrg($ultimoIdPernaEsquerda,'E');
	
				if(count($verificaPernaEsquerda)>0){
					
					$ultimoIdPernaEsquerda = $verificaPernaEsquerda;
					++$qtdPernaEsquerda;
					
					//Log Pernas
					$log = array();
					$log["idPai"] = $id;
					$log["idUltimo"] = $ultimoIdPernaEsquerda;
					$log["perna"] = 'E';
		
					$this->setLogPerna($log);
		
				}else{
		
					$contEsq = 1;
		
				}
		
			}
			
			$contDir = 0;
			$ultIdPernaDireita = $ultimoIdPernaDireita;
		
			//Atualizar dados da Perna Esquerda Patrocinador
			while($contDir==0)
			{
		
				$verificaPernaDireita = $this->modelTreeview->getChildOrg($ultimoIdPernaDireita,'D');
		
				if(count($verificaPernaDireita)>0){
					$ultimoIdPernaDireita = $verificaPernaDireita;
					++$qtdPernaDireita;
		
					//Log Pernas
					$log = array();
					$log["idPai"] = $id;
					$log["idUltimo"] = $ultimoIdPernaDireita;
					$log["perna"] = 'D';
		
				}else{
					$contDir = 1;
				}
			}
	
			# Atualiza ID da perna esquerda se o ultimo for diferente
			if(($ultIdPernaEsquerda!=$ultimoIdPernaEsquerda) || ($ultIdFilhoEsq!=$ultimoIdPernaEsquerda)){
				
				$request = array();
				$request["id"] = $id;
				$request["idPernaEsquerda"] = $ultimoIdPernaEsquerda;
				$request["qtdPernaEsquerda"] = $qtdPernaEsquerda;
	
				$this->updateControlePerna($request,"E");	
		
			}
		
			# Atualiza ID da perna direita se o ultimo for diferente
		
			if(($ultIdPernaDireita!=$ultimoIdPernaDireita) || ($ultIdFilhoDir!=$ultimoIdPernaDireita)){
				
				$request = array();
				$request["id"] = $id;
				$request["idPernaDireita"] = $ultimoIdPernaDireita;
				$request["qtdPernaDireita"] = $qtdPernaDireita;
				
				$this->updateControlePerna($request,"D");	
		
			}
		
			return array($ultimoIdPernaEsquerda,$ultimoIdPernaDireita,$qtdPernaEsquerda,$qtdPernaDireita);
		
			}
		
		}catch(Exception $e){
			die("Error 215");
			
		}
	
	}
	
	###############--------------------------------------###############
	public function getPositionDad($idSponsor,$idUsr,$position)
	{
		try
		{
			$lastIdLeg = $this->getLastIdLeg($idSponsor);
			$ultimoIdPernaEsquerda = $lastIdLeg[0];
			$ultimoIdPernaDireita = $lastIdLeg[1];
			$qtdPernaEsquerda = $lastIdLeg[2];
			$qtdPernaDireita = $lastIdLeg[3];
			
			switch($position){
			
			  //Perna Esquerda
			  case "E":
			
				  if($ultimoIdPernaEsquerda==0){
					  
					  ++$qtdPernaEsquerda;
			
					  $idPai = $idSponsor;
					  $perna = "E";
					  
						$request = array();
						$request["id"] = $idSponsor;
						$request["idPernaEsquerda"] = $idUsr;
						$request["qtdPernaEsquerda"] = $qtdPernaEsquerda;
						
						$this->updateControlePerna($request,"E");
			
					  /*$sql = mysql_query("UPDATE tb_controle_perna SET 
									  cper_qtdPernaEsquerda=$qtdPernaEsquerda,
									  cper_ultimoIdPernaEsquerda=$idUsr 
									  WHERE cper_idUsuario=$idSponsor")or die("Erro +6");*/
			
				  }else{
			
					  ++$qtdPernaEsquerda;
			
					  $idPai = $ultimoIdPernaEsquerda;
					  $perna = "E";
					  
						$request = array();
						$request["id"] = $idSponsor;
						$request["idPernaEsquerda"] = $idUsr;
						$request["qtdPernaEsquerda"] = $qtdPernaEsquerda;
						
						$this->updateControlePerna($request,"E");
			
					  /*$sql = mysql_query("UPDATE tb_controle_perna SET 
									  cper_qtdPernaEsquerda=$qtdPernaEsquerda,
									  cper_ultimoIdPernaEsquerda=$idUsr 
									  WHERE cper_idUsuario=$idSponsor")or die("Erro +7");*/
			
				  }
				  break;
			
			  //Perna Direita
			  case "D":
			
				  if($ultimoIdPernaDireita==0){
			
					  ++$qtdPernaDireita;
			
					  $idPai = $idSponsor;
					  $perna = "D";
					  
						$request = array();
						$request["id"] = $idSponsor;
						$request["idPernaDireita"] = $idUsr;
						$request["qtdPernaDireita"] = $qtdPernaDireita;
						
						$this->updateControlePerna($request,"D");
			
					  /*$sql = mysql_query("UPDATE tb_controle_perna SET 
									  cper_qtdPernaDireita=$qtdPernaDireita,
									  cper_ultimoIdPernaDireita=$idUsr 
									  WHERE cper_idUsuario=$idSponsor")or die("Erro +8");*/
			
				  }else{
			
					  ++$qtdPernaDireita;
			
					  $idPai = $ultimoIdPernaDireita;
					  $perna = "D";
					  
						$request = array();
						$request["id"] = $idSponsor;
						$request["idPernaDireita"] = $idUsr;
						$request["qtdPernaDireita"] = $qtdPernaDireita;
						
						$this->updateControlePerna($request,"D");
			
					  /*$sql = mysql_query("UPDATE tb_controle_perna SET 
									  cper_qtdPernaDireita=$qtdPernaDireita,
									  cper_ultimoIdPernaDireita=$idUsr 
									  WHERE cper_idUsuario=$idSponsor")or die("Erro +9");*/
			
				  }
				  break;
			
			  //Menor perna
			  default:
			
				  if($ultimoIdPernaEsquerda==0 && $ultimoIdPernaDireita==0){
			
					  ++$qtdPernaEsquerda;
			
					  $idPai = $idSponsor;
					  $perna = "E";
					  
						$request = array();
						$request["id"] = $idSponsor;
						$request["idPernaEsquerda"] = $idUsr;
						$request["qtdPernaEsquerda"] = $qtdPernaEsquerda;
						
						$this->updateControlePerna($request,"E");
			
					  /*$sql = mysql_query("UPDATE tb_controle_perna SET 
									  cper_qtdPernaEsquerda=$qtdPernaEsquerda,
									  cper_ultimoIdPernaEsquerda=$idUsr 
									  WHERE cper_idUsuario=$idSponsor")or die(mysql_error());*/
			
				  }elseif($ultimoIdPernaEsquerda!=0 && $ultimoIdPernaDireita==0){
			
					  ++$qtdPernaDireita;
			
					  $idPai = $idSponsor;
					  $perna = "D";
					  
						$request = array();
						$request["id"] = $idSponsor;
						$request["idPernaDireita"] = $idUsr;
						$request["qtdPernaDireita"] = $qtdPernaDireita;
						
						$this->updateControlePerna($request,"D");
			
					  /*$sql = mysql_query("UPDATE tb_controle_perna SET 
									  cper_qtdPernaDireita=$qtdPernaDireita,
									  cper_ultimoIdPernaDireita=$idUsr 
									  WHERE cper_idUsuario=$idSponsor")or die(mysql_error());*/
			
				  }elseif($ultimoIdPernaEsquerda==0 && $ultimoIdPernaDireita!=0){
			
					  ++$qtdPernaEsquerda;
			
					  $idPai = $idSponsor;
					  $perna = "E";
	
						$request = array();
						$request["id"] = $idSponsor;
						$request["idPernaEsquerda"] = $idUsr;
						$request["qtdPernaEsquerda"] = $qtdPernaEsquerda;
						
						$this->updateControlePerna($request,"E");
						
					  /*$sql = mysql_query("UPDATE tb_controle_perna SET 
									  cper_qtdPernaEsquerda=$qtdPernaEsquerda,
									  cper_ultimoIdPernaEsquerda=$idUsr 
									  WHERE cper_idUsuario=$idSponsor")or die(mysql_error());*/
			
				  }elseif($qtdPernaEsquerda==$qtdPernaDireita){
			
					  ++$qtdPernaEsquerda;
			
					  $idPai = $ultimoIdPernaEsquerda;
					  $perna = "E";
					  
						$request = array();
						$request["id"] = $idSponsor;
						$request["idPernaEsquerda"] = $idUsr;
						$request["qtdPernaEsquerda"] = $qtdPernaEsquerda;
						
						$this->updateControlePerna($request,"E");
			
					  /*$sql = mysql_query("UPDATE tb_controle_perna SET 
									  cper_qtdPernaEsquerda=$qtdPernaEsquerda,
									  cper_ultimoIdPernaEsquerda=$idUsr 
									  WHERE cper_idUsuario=$idSponsor")or die(mysql_error());*/
			
				  }elseif($qtdPernaEsquerda>$qtdPernaDireita){
			
					  ++$qtdPernaDireita;
			
					  $idPai = $ultimoIdPernaDireita;
					  $perna = "D";
					  
						$request = array();
						$request["id"] = $idSponsor;
						$request["idPernaDireita"] = $idUsr;
						$request["qtdPernaDireita"] = $qtdPernaDireita;
						
						$this->updateControlePerna($request,"D");
			
					  /*$sql = mysql_query("UPDATE tb_controle_perna SET 
									  cper_qtdPernaDireita=$qtdPernaDireita,
									  cper_ultimoIdPernaDireita=$idUsr 
									  WHERE cper_idUsuario=$idSponsor")or die(mysql_error());*/
			
				  }elseif($qtdPernaEsquerda<$qtdPernaDireita){
			
					  ++$qtdPernaEsquerda;
			
					  $idPai = $ultimoIdPernaEsquerda;
					  $perna = "E";
	
						$request = array();
						$request["id"] = $idSponsor;
						$request["idPernaEsquerda"] = $idUsr;
						$request["qtdPernaEsquerda"] = $qtdPernaEsquerda;
						
						$this->updateControlePerna($request,"E");
	
					  /*$sql = mysql_query("UPDATE tb_controle_perna SET 
									  cper_qtdPernaEsquerda=$qtdPernaEsquerda,
									  cper_ultimoIdPernaEsquerda=$idUsr 
									  WHERE cper_idUsuario=$idSponsor")or die(mysql_error());*/
			
				  }
				  break;
				  //return array($idPai,$perna);
			
			}// Final Switch
					
			return array($idPai,$perna);
			
		}catch(Exception $e){
		
			die("Error 216");
			
		}
		
	}//Final function getPositionDad

}