<?php

class Application_Model_Order
{
	public function __construct()
	{
	  /*$this->db_TbAlteraLog = new Application_Model_DbTable_TbAlteraLog();
	  $this->db_TbFatura = new Application_Model_DbTable_TbFatura();
	  $this->db_TbUserPlans = new Application_Model_DbTable_TbUserPlans();
	  $this->db_TbProduct = new Application_Model_DbTable_TbProduct();
	  $this->db_TbPedido = new Application_Model_DbTable_TbPedido();
	  $this->modelEwallet = new Application_Model_Ewallet();*/
	}

   public function getItensInvoice($id)
   {
	   $this->db_TbFatura = new Application_Model_DbTable_TbFatura();
	   
		try{
			if(!is_null($id)){
	
			  $select = $this->db_TbFatura->select()
										->from($this->db_TbFatura,array('itens' => 'SUM(fat_itens)'))
										->where("fat_idUsuario = ?",$id)
										->where("fat_itens <> 0")
										->where("fat_status = 1");
			  
			  $return = $this->db_TbFatura->fetchRow($select);
			  return $return["itens"];
			}
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error 187");
			
		}
   }
   
   public function getItensInvoicePlan($id)
   {
	   $this->db_TbFatura = new Application_Model_DbTable_TbFatura();
	   
		try{
			if(!is_null($id)){
	
			  $select = $this->db_TbFatura->select()
										->from($this->db_TbFatura,array('itens' => 'SUM(fat_itens)'))
										->where("fat_idUsuario = ?",$id)
										->where("fat_itens <> 0")
										->where("fat_produto = 3")
										->where("fat_status = 1");
			  
			  $return = $this->db_TbFatura->fetchRow($select);
			  return $return["itens"];
			}
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error 188");
			
		}
   }

   public function getQtPlans($id)
   {
	   $this->db_TbUserPlans = new Application_Model_DbTable_TbUserPlans();
	   
		try{
			if(!is_null($id)){
	
			  $select = $this->db_TbUserPlans->select()
										->from($this->db_TbUserPlans,array('count' => 'sum(uplan_amount)'))
										->where("uplan_user_id = ?",$id)
										->where("uplan_status = 1");
			  
			  $return = $this->db_TbUserPlans->fetchRow($select);
			  return $return["count"];
			}
		}catch(Exception $e){
			die("Error 189");
			
		}
   }
   
   public function getPlans($id)
   {   
		try{
			if(!is_null($id)){
				$db = Zend_Db_Table_Abstract::getDefaultAdapter();
				$select = $db->select()
										->from('tb_user_plans',array('uplan_id','uplan_value','uplan_day_start','uplan_day_end','uplan_saldo_acumulado','uplan_status'))
										->join('tb_product','uplan_prod_id=prod_id',array('prod_titulo'))
										->where("uplan_user_id = ?",$id);
										// ->where("uplan_status = 1");
			  
				return $db->fetchAll($select);
			}
		}catch(Exception $e){
			die("Error 241");
			
		}
   }
   
   	public function getStatusCotas($cotas)
	{
	   if($cotas>=20 && $cotas<60){
	       $result = 1;
	       //Bronze
	   }elseif($cotas>=60 && $cotas<120){
	       $result = 2;
	       //Bronze
	   }elseif($cotas>=120 && $cotas<200){
	       $result = 3;
	       //Bronze
	   }elseif($cotas>=200){
	       $result = 4;
	       //Bronze
	   }
	   
	   return $result;
	}
	
   public function getProductBasic($id)
   {
	   $this->db_TbProduct = new Application_Model_DbTable_TbProduct();
	   
		try{
			if(!is_null($id)){
	
			  $select = $this->db_TbProduct->select()
										->from($this->db_TbProduct,array('existe' => 'count(prod_id)','prod_titulo','prod_ordem','prod_valor','prod_validade_dias','prod_especial'))
										->where("prod_id = ?",$id)
										->where("prod_status = 1");
			  
			  return $this->db_TbProduct->fetchRow($select);
			}
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error 190");
			
		}
   }
   
	/** Function verificaPedidoAberto($id_usuario)
	*/
	
	public function getOpenPurchase($id_usuario,$id_produto)
	{
		$this->db_TbFatura = new Application_Model_DbTable_TbFatura();
		
		try{
			//Pega o tipo de conta/pacote selelcionado pelo usu�rio
			//list($prodEspecial) = abreSQL("SELECT prod_especial FROM tb_product WHERE prod_id='$id_produto'");
			$product = $this->getProductBasic($id_produto);
			$prodEspecial = $product["prod_especial"];
			
			//$sqlTotalPedido = "SELECT fat_id FROM tb_fatura WHERE fat_idUsuario = '$id_usuario' AND fat_status = '1'";
			//$sql = geraSQL($sqlTotalPedido);
			$select = $this->db_TbFatura->select()
									->from($this->db_TbFatura,array('fat_id'))
									->where("fat_idUsuario = ?",$id_usuario)
									->where("fat_status = 1");
			
			$result = $this->db_TbFatura->fetchAll($select)->toArray();
			
			$TotalFatAberto = 0;
			
			foreach($result as $row){
				
				$idfatura = $row["fat_id"];
				
				$productInvoice = $this->getProductInvoice($idfatura);				
				$prod_especial = $productInvoice["prod_especial"];
				$prod_id = $productInvoice["prod_id"];
				$prod_ordem = $productInvoice["prod_ordem"];
				
				if($prod_especial==$prodEspecial && $prod_id!=0)
				{
					$TotalFatAberto = 1;
					
				}elseif($prod_especial==$prodEspecial && $prod_id!=0)
				{
					$TotalFatAberto = 1;
				}
			}
			return $TotalFatAberto;
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error 191");
			
		}
	}
	
	/** Function: buscaProdutoFatura($idfatura) 
		Parameters:	$idfatura: ID da fatura
		Action: Busca um produto e usuario por ID da fatura
		Return: retorna uma lista com status do Start Click, Id do produto e ID do usuario
		To use: list($prod_start, $prod_id, $iduser) = buscaProdutoFatura($idfatura)
		Table: tb_product,tb_item,tb_pedido,tb_fatura
	*/
	
	public function getProductInvoice($idfatura)
	{
		try{
			$db = Zend_Db_Table_Abstract::getDefaultAdapter();
			$select = $db->select()
										->from('tb_product',array('prod_id','prod_titulo','prod_valor','prod_ordem','prod_max','prod_especial','prod_imagem','prod_validade_dias'))
										->join('tb_item','prod_id=item_produto',array())
										->join('tb_pedido','item_pedido=ped_id',array('ped_id'))
										->join('tb_fatura','ped_id=fat_pedido',array('fat_valor','fat_itens'))
										->where("fat_id = ?",$idfatura);
										
			//$sql = "SELECT prod_especial,prod_id,prod_ordem,fat_idUsuario FROM tb_product INNER JOIN tb_item INNER JOIN tb_pedido INNER JOIN tb_fatura ON prod_id=item_produto AND item_pedido=ped_id AND ped_id=fat_pedido AND fat_id='$idfatura'";
			return $db->fetchRow($select);
			//$stmt = $db->query($sql);
			//return $stmt->fetchAll();
			
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error 192");
			
		}
	}

	/** Function: newOrderPlan($id_produto, $id_usuario)
		Parameters: $id_produto: id do produto
					$id_usuario: id do usuario
		Action: Gera novo pedido, Item e Fatura do pedido
	*/
	public function setNewOrderPlan($id_produto, $id_usuario, $qtde, $valor)
	{
		if(empty($qtde))
			$qtde = 1;
		
		try
		{
			$db = Zend_Db_Table_Abstract::getDefaultAdapter();
			$return = $db->query("CALL sp_new_order_plan($id_produto,$id_usuario,$qtde,$valor)");
			
			if($return)
				return 1;
			else
				return 0;
		}catch(Exception $e){
			die($e->getMessage());
			//die("Error 193");
			
		}

	}
	
	public function setNewOrder($id_produto, $id_usuario)
	{
		try
		{
			$db = Zend_Db_Table_Abstract::getDefaultAdapter();
			$return = $db->query("CALL sp_new_order($id_produto,$id_usuario)");
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error 194");
			
		}
	}
	
	public function setNewOrderFee($id_produto, $id_usuario, $pedido, $qtdeItens)
	{
		try
		{
			$db = Zend_Db_Table_Abstract::getDefaultAdapter();
			$return = $db->query("CALL sp_new_order_fee($id_produto,$id_usuario,$pedido,$qtdeItens,now())");
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error 195");
			
		}
	}
	
   public function getAllProductAccount()
   {
	   $this->db_TbProduct = new Application_Model_DbTable_TbProduct();
	   
		try{

			$select = $this->db_TbProduct->select()
									->from($this->db_TbProduct)
									->where("prod_especial = 0")
									->where("prod_status = 1")
									->order("prod_valor");
			
			return $this->db_TbProduct->fetchAll($select)->toArray();
			  
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error 196");
			
		}
   }
   
   public function getAllProductPlan()
   {
	   $this->db_TbProduct = new Application_Model_DbTable_TbProduct();
	   
		try
		{
			
			$select = $this->db_TbProduct->select()
									->from($this->db_TbProduct)
									->where("prod_especial = 1")
									->where("prod_status = 1")
									->order("prod_valor");
			
			return $this->db_TbProduct->fetchAll($select)->toArray();

		}catch(Exception $e){
			//die($e->getMessage());
			die("Error 197");
			
		}
   }
   
	/**
	  Function: cancelarPedido($payuser, $fatuser, $invoice)
	  Parameters: $payuser: id do usuario que est� cancelando o pedido
				  $fatuser: login do usuario devedor da fatura
				  $invoice: Numero da fatura
	  Action: Efetua o cancelamento do pedido
	  Table: tb_fatura, tb_boleto, tb_pedido
	*/
	
	public function cancelOrder($user, $invoice)
	{
	  $this->db_TbFatura = new Application_Model_DbTable_TbFatura();
	  $this->db_TbPedido = new Application_Model_DbTable_TbPedido();
	  
		try
		{
		
			$id_pedido = $this->getOrderInvoice($invoice);
			$value = $this->getInvoiceValue($invoice);
			
			/** Cancela o pedido */
			$dados = array(
			 'ped_status' => '0'
			);
			$where = $this->db_TbPedido->getAdapter()->quoteInto("ped_id = ?", $id_pedido);
			$this->db_TbPedido->update($dados, $where);
			//executaSQL("UPDATE tb_pedido SET ped_status='0' WHERE ped_id = '$id_pedido'");
			
			/** Cancela a fatura */
			$dados = array(
			 'fat_status' => '0'
			);
			$where = $this->db_TbFatura->getAdapter()->quoteInto("fat_id = ?", $invoice);
			$this->db_TbFatura->update($dados, $where);
			//executaSQL("UPDATE tb_fatura SET fat_status='0' WHERE fat_id = '$invoice'");
			
			$this->setLog('Cancel order '.$invoice, $value, $user);
		
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error 198");
			
		}
		
	}
	
	/** Function: getOrderInvoice($invoice)
		Parameters:	$idfatura: ID da fatura
		Action: Busca um pedido por ID da fatura
		Return: id da fatura
		Table: tb_fatura
	*/
	
	public function getOrderInvoice($invoice)
	{
		$this->db_TbFatura = new Application_Model_DbTable_TbFatura();
	  
		try{

			$select = $this->db_TbFatura->select()
									->from($this->db_TbFatura,array('fat_pedido'))
									->where("fat_id = ?",$invoice);
			
			$return = $this->db_TbFatura->fetchRow($select);
			return $return["fat_pedido"];
			  
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error 199");
			
		}
	}

	public function getInvoiceOrder($order)
	{
		$this->db_TbFatura = new Application_Model_DbTable_TbFatura();
	  
		try{

			$select = $this->db_TbFatura->select()
									->from($this->db_TbFatura)
									->where("fat_pedido = ?",$order);
			
			return $this->db_TbFatura->fetchRow($select);
			  
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error 199+");
			
		}
	}

	public function getInvoiceContract($document)
	{
		$this->db_TbFatura = new Application_Model_DbTable_TbFatura();
	  
		try{

			$select = $this->db_TbFatura->select()
									->from($this->db_TbFatura)
									->where("fat_contrato_id = ?",$document);
			
			return $this->db_TbFatura->fetchRow($select);
			  
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error 199+");
			
		}
	}
	
	/**
		Function: getInvoiceValue($invoice)
		Parameters: $invoice: N�mero da fatura
		Action: Retorna o valor da fatura
		Return: fat_valor
		Table: tb_fatura
	*/
	public function getInvoiceValue($invoice)
	{
		$this->db_TbFatura = new Application_Model_DbTable_TbFatura();
	  
		try{

			$select = $this->db_TbFatura->select()
									->from($this->db_TbFatura,array('fat_valor'))
									->where("fat_id = ?",$invoice);
			
			$return = $this->db_TbFatura->fetchRow($select);
			return $return["fat_valor"];
			  
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error 200");
			
		}
	}
					
	public function setLog($desclog, $vallog, $usrlog)
	{
		
	  $this->db_TbAlteraLog = new Application_Model_DbTable_TbAlteraLog();
	  $this->modelEwallet = new Application_Model_Ewallet();
	  
		//executaSQL("INSERT into tb_alteraLog (logalt_tipo, logalt_valor, logalt_saldo_anterior, logalt_usuario, logalt_data) values ('$desclog', '$vallog',(select bsal_saldo_liberado from tb_financeiro_saldo where bsal_idUsuario = " . $usrlog."), '$usrlog', '$date')");
		try{
			$balance = $this->modelEwallet->getBalance($usrlog);
			$saldo_liberado = $balance["bsal_saldo_liberado"];
			
			$dados = array(
			'logalt_tipo' => $desclog,
			'logalt_valor' => $vallog,
			'logalt_saldo_anterior' => $saldo_liberado,
			'logalt_usuario' => $usrlog,
			'logalt_data' => new Zend_Db_Expr('NOW()')
			);
			return $this->db_TbAlteraLog->insert($dados);
	  
			  
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error 201");
			
		}
	}
	
	public function getInvoiceNotCanceled($user)
	{
		$this->db_TbFatura = new Application_Model_DbTable_TbFatura();
		
		try{
			$select = $this->db_TbFatura->select()
									->from($this->db_TbFatura,array('count' => 'COUNT(*)'))
									->where("fat_status <> '0'")
									->where("fat_idUsuario = ?",$user);
			
			$return = $this->db_TbFatura->fetchRow($select);
			return $return["count"];
			  
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error 202");
			
		}
	}
	
	public function getInvoice($user)
	{
		$this->db_TbFatura = new Application_Model_DbTable_TbFatura();
		
		try{

			$select = $this->db_TbFatura->select()
									->from($this->db_TbFatura)
									->where("fat_idUsuario = ?",$user)
									->order("fat_id DESC");
			
			return $this->db_TbFatura->fetchAll($select)->toArray();
			  
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error 203");
			
		}
	}
	
	public function getInvoiceUser($invoice,$user)
	{
		$this->db_TbFatura = new Application_Model_DbTable_TbFatura();
		
		try{

			$select = $this->db_TbFatura->select()
									->from($this->db_TbFatura,array('fat_descricao','fat_valor','fat_limite','fat_contrato'))
									->where("fat_idUsuario = ?",$user)
									->where("fat_status = 1")
									->where("fat_id = ?",$invoice);
			
			return $this->db_TbFatura->fetchRow($select);
			  
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error 204");
			
		}
	}
	
	public function getCheckUserInvoice($invoice)
	{
		$this->db_TbFatura = new Application_Model_DbTable_TbFatura();
		
		try{

			$select = $this->db_TbFatura->select()
									->from($this->db_TbFatura,array('fat_idUsuario'))
									->where("fat_status = 1")
									->where("fat_id = ?",$invoice);
			
			$return = $this->db_TbFatura->fetchRow($select);
			return $return["fat_idUsuario"];
			  
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error 204");
			
		}
	}
	
	public function getInvoiceUserLimit($user)
	{
		$this->db_TbFatura = new Application_Model_DbTable_TbFatura();
		
		try{

			$select = $this->db_TbFatura->select()
									->from($this->db_TbFatura,array('amount' => 'COUNT(fat_id)'))
									->where("fat_status = 1")
									->where("fat_limite = 1")
									->where("fat_idUsuario = ?",$user);
			
			$return = $this->db_TbFatura->fetchRow($select);
			return $return["amount"];
			  
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error 204+1");
			
		}
	}
	
	public function getInvoiceFeeUser($user)
	{
		$this->db_TbFatura = new Application_Model_DbTable_TbFatura();
		
		try{

			$select = $this->db_TbFatura->select()
									->from($this->db_TbFatura,array('fat_id','fat_dataVencimento'))
									->where("fat_idUsuario = ?",$user)
									->where("fat_status = 1")
									->where("fat_produto = 4");
			
			return $this->db_TbFatura->fetchRow($select);
			  
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error 205");
			
		}
	}
	
	public function getMonthlyInvoices()
	{
		$this->db_TbFatura = new Application_Model_DbTable_TbFatura();
		
		try{

			$select = $this->db_TbFatura->select()
									->from($this->db_TbFatura,array('fat_id','fat_produto','fat_idUsuario'))
									->where("(fat_produto=1 OR fat_produto=2 OR fat_produto=4)")
									->where("fat_status=1")
									->order("fat_id ASC")
									->limit(200);
			
			return $this->db_TbFatura->fetchAll($select)->toArray();
			  
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error 206");
			
		}
	}
	
	public function getAmountInvoicesPaidProductsUser($user,$product)
	{
		$this->db_TbFatura = new Application_Model_DbTable_TbFatura();
		
		try{

			$select = $this->db_TbFatura->select()
									->from($this->db_TbFatura,array('amount' => 'COUNT(fat_id)'))
									->where("fat_produto= ?",$product)
									->where("fat_status=2")
									->where("fat_idUsuario=?",$user);
			
			$row = $this->db_TbFatura->fetchRow($select);
			return $row["amount"];
			  
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error 207");
			
		}
	}
	
	/** Function: setCancelOverdueInvoice() 
		Parameters:	$user: ID do usu�rio
		Action: Cancela as faturas j� vencidas
		Return: true or false
		Table: tb_fatura
	*/
	public function setCancelOverdueInvoice($id)
	{
	  $this->db_TbFatura = new Application_Model_DbTable_TbFatura();
	  $this->db_TbPedido = new Application_Model_DbTable_TbPedido();
	  
		try
		{
			
			/** Cancela o pedido */
			$dados = array(
			 'ped_status' => '0'
			);
			$where = $this->db_TbPedido->getAdapter()->quoteInto("ped_data <= DATE_ADD(CURDATE(),INTERVAL -2 DAY) AND ped_status=?",$id);
			$this->db_TbPedido->update($dados, $where);
			//executaSQL("UPDATE tb_pedido SET ped_status='0' WHERE ped_id = '$id_pedido'");
			
			/** Cancela a fatura */
			$dados = array(
			 'fat_status' => '0'
			);
			$where = $this->db_TbFatura->getAdapter()->quoteInto("fat_dataVencimento <= DATE_ADD(CURDATE(),INTERVAL -2 DAY) AND fat_status=?",$id);
			$this->db_TbFatura->update($dados, $where);
			//executaSQL("UPDATE tb_fatura SET fat_status='0' WHERE fat_id = '$invoice'");
		
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error 208");
			
		}
	}	
	
}