<?php

class Application_Model_Withdrawal
{
	public function __construct()
	{
	  $this->db_TbBitcoinOperations = new Application_Model_DbTable_TbBitcoinOperations();
	  
	}

	
	public function getAll($user)
   	{
		try
		{
			/*$select = $this->db_TbBitcoinOperations->select();
			$select->where("bco_user = $user AND bco_type = 'W'");
			$select->order("bco_id desc");
			return $this->db_TbBitcoinOperations->fetchAll($select)->toArray();*/
			$db = Zend_Db_Table_Abstract::getDefaultAdapter();
			$array = $db->query("SELECT bt1.*, bt2.bco_id fee_id, bt2.bco_status fee_status, bt2.bco_date_insert fee_insert
									FROM tb_bitcoin_operations bt1 
									LEFT JOIN (SELECT * FROM (SELECT bco_id, bco_status, bco_withdrawal_id, bco_date_insert 
																FROM tb_bitcoin_operations 
																WHERE bco_withdrawal_id <> 0 AND bco_user = $user 
																ORDER BY bco_id DESC) a 
												GROUP BY bco_withdrawal_id) bt2 
										ON bt1.bco_id = bt2.bco_withdrawal_id
									WHERE bt1.bco_type = 'W' AND bt1.bco_user = $user 
									GROUP BY bt1.bco_id
									ORDER BY bt1.bco_id DESC");
			return $array;


		}catch(Exception $e){
			die($e->getMessage());
			die("Error 230");
			
		}
   	}

   	public function getWithdrawal($user, $id)
   	{
		try
		{
			$select = $this->db_TbBitcoinOperations->select();
			$select->where("bco_id = " . $id . " AND bco_user = " . $user . " AND bco_type = 'W'");
			$result = $this->db_TbBitcoinOperations->fetchAll($select)->toArray();
			if (!empty($result)) {
				return $result[0];
			} else {
				return array();
			}
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error 231");
			
		}
   	}

   	public function setWithdrawal($idUser,$value,$accountFrom) 
	{
		
		try
		{
			$this->modelEwallet = new Application_Model_Ewallet();

			$this->modelEwallet->addDebit($idUser,0,$value,"Saque",$accountFrom);
			
			$db = Zend_Db_Table_Abstract::getDefaultAdapter();
			
			$sql =  "UPDATE tb_financeiro_saldo SET bsal_saldo_areceber = (bsal_saldo_areceber + ".$value.") WHERE bsal_idUsuario = $idUser;";				
			$db->query($sql);
		
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error 232");
			
		}
	}

	public function updateWithdrawal($id,$data) 
	{
		try
		{
			if(!is_null($id)){	
				$data['bco_date_update'] = date("Y-m-d H:i:s");
				$where = $this->db_TbBitcoinOperations->getAdapter()->quoteInto("bco_id = ?", $id);
				$this->db_TbBitcoinOperations->update($data, $where);
			}
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error 233");
			
		}
	}

	public function cancelWithdrawal($idUser,$value, $accountFrom = null)
	{
		try
		{
			$this->modelEwallet = new Application_Model_Ewallet();
			$this->modelEwallet->addCredit($idUser,0,$value,"Saque cancelado",$accountFrom);
			
			$db = Zend_Db_Table_Abstract::getDefaultAdapter();
			$sql =  "UPDATE tb_financeiro_saldo SET bsal_saldo_areceber = (bsal_saldo_areceber - ".$value.") WHERE bsal_idUsuario = $idUser;";				
			$db->query($sql);
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error 234");
			
		}
	}

	public function getWithdrawalsAll($user)
	{
		try
		{
			$select = $this->db_TbBitcoinOperations->select();
			$select->from($this->db_TbBitcoinOperations, array("count(*) as quantity", "SUM(bco_dollar_amount) as sum_value"));
			$select->where("bco_user = " . $user . " AND bco_type = 'W' AND bco_status <>  'C'");
			$result = $this->db_TbBitcoinOperations->fetchRow($select);
			
			if (empty($result)) {
				$result['quantity'] = 0;
				$result['sum_value'] = 0;
			}
			
			return $result;
			
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error 235+1");
			
		}
	}

	public function getWithdrawalsAllTime($user,$accountFrom)
	{
		try
		{
			$select = $this->db_TbBitcoinOperations->select();
			$select->from($this->db_TbBitcoinOperations, array("count(*) as quantity", "SUM(bco_dollar_amount) as sum_value"));
			$select->where("bco_user = " . $user . " AND bco_type = 'W' AND bco_account_from = '".$accountFrom."' AND bco_status <>  'C'");
			$result = $this->db_TbBitcoinOperations->fetchRow($select);
			if (empty($result)) {
				$result['quantity'] = 0;
				$result['sum_value'] = 0;
			}
			return $result;
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error 235");
			
		}
	}

	public function getWithdrawalsByDay($user,$accountFrom)
	{
		try
		{
			$select = $this->db_TbBitcoinOperations->select();
			$select->from($this->db_TbBitcoinOperations, array("count(*) as quantity", "SUM(bco_dollar_amount) as sum_value"));
			$select->where("bco_user = " . $user . " AND bco_type = 'W' AND bco_account_from = '".$accountFrom."' AND DATE_FORMAT(bco_date_insert,  '%Y-%m-%d') =  '".date("Y-m-d")."' AND bco_status <>  'C'");
			$select->group("DATE_FORMAT(bco_date_insert,  '%Y-%m-%d')");
			$result = $this->db_TbBitcoinOperations->fetchRow($select);
			if (empty($result)) {
				$result['quantity'] = 0;
				$result['sum_value'] = 0;
			}
			return $result;
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error 235");
			
		}
	}

	/*public function getWithdrawalsByMonth($user)
	{
		try
		{
			$select = $this->db_TbBitcoinOperations->select();
			$select->from($this->db_TbBitcoinOperations, array("count(*) as quantity", "SUM(bco_dollar_amount) as sum_value"));
			$select->where("bco_user = " . $user . " AND bco_type = 'W' AND DATE_FORMAT(bco_date_insert,  '%Y-%m') =  '".date("Y-m")."' AND bco_status <>  'C'");
			$select->group("DATE_FORMAT(bco_date_insert,  '%Y-%m')");
			$result = $this->db_TbBitcoinOperations->fetchRow($select);
			if (empty($result)) {
				$result['quantity'] = 0;
				$result['sum_value'] = 0;
			}
			return $result;
		
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error 236");
			
		}
      	
	}

	public function getWithdrawalsByYear($user)
	{
		try
		{
			$select = $this->db_TbBitcoinOperations->select();
			$select->from($this->db_TbBitcoinOperations, array("count(*) as quantity", "SUM(bco_dollar_amount) as sum_value"));
			$select->where("bco_user = " . $user . " AND bco_type = 'W' AND DATE_FORMAT(bco_date_insert,  '%Y') =  '".date("Y")."' AND bco_status <>  'C'");
			$select->group("DATE_FORMAT(bco_date_insert,  '%Y')");
			$result = $this->db_TbBitcoinOperations->fetchRow($select);
			if (empty($result)) {
				$result['quantity'] = 0;
				$result['sum_value'] = 0;
			}
			return $result;
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error 237");
			
		}
		
	}*/

	public function getProductInfo($id)
   	{
		try
		{
			$this->db_TbProduct = new Application_Model_DbTable_TbProduct();
			
			if ($id > 0) {
			
				$select = $this->db_TbProduct->select()
									->from($this->db_TbProduct,array('prod_limit_transfer_day','prod_qtd_transfer_month','prod_limit_transfer_month','prod_limit_transfer_year'))
									->where("prod_id = ?",$id)
									->where("prod_status = 1");
			
				return $this->db_TbProduct->fetchRow($select);
			} else {
				$select = $this->db_TbProduct->select()
									->from($this->db_TbProduct,array('prod_limit_transfer_day','prod_qtd_transfer_month','prod_limit_transfer_month','prod_limit_transfer_year'))
									->where("prod_especial = 3");
			
				return $this->db_TbProduct->fetchRow($select);
			}
			
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error 238");
			
		}
		
	}
	
	public function minWithdrawal($amount){
		
		switch ($amount) {
			case 0:
				//Primeiro saque
				$withdrawal = 10.00;
				break;
			case 1:
				//Segundo saque
				//$withdrawal = 10.00;
				$withdrawal = 10.00;
				break;
			case 2:
				//Terceiro saque
				//$withdrawal = 100.00;
				$withdrawal = 10.00;
				break;
			
			default:
				//Quarto ou mais saque
				$withdrawal = 10.00;
				break;
		}
		
		return $withdrawal;
		
	}

}

