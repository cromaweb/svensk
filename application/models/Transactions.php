<?php

class Application_Model_Transactions
{

   public function getFinancialStatementAmount($id)
   {
	   $this->db = new Application_Model_DbTable_TbFinanceiroExtrato();
		try{
	
			$select = $this->db->select()
									->from($this->db,array('amount' => 'COUNT(*)'))
									->where("extf_idUsuario = ?",$id);
			
			$return = $this->db->fetchRow($select);
			return $return["amount"];
			
		}catch(Exception $e){
			die("Error 217");
			
		}
   }
   
   public function getFinancialStatementId($id,$numBegin,$numEnd)
   {
	   $this->db = new Application_Model_DbTable_TbFinanceiroExtrato();
		try{
	
			$select = $this->db->select()
									->from($this->db,array('extf_id'))
									->where("extf_idUsuario = ?",$id)
									->order("extf_data ASC")
									->limit($numBegin,$numEnd);
			
			$return = $this->db->fetchRow($select);
			return $return["extf_id"];
			
		}catch(Exception $e){
			die("Error 218");
			
		}
   }

   public function getFinancialStatementLimit($id,$numBegin,$numEnd)
   {
	   $this->db = new Application_Model_DbTable_TbFinanceiroExtrato();
		try{
	
			$select = $this->db->select()
									->where("extf_idUsuario = ?",$id)
									->order("extf_data DESC")
									->limit($numBegin,$numEnd);
			
			return $this->db->fetchAll($select)->toArray();
			
		}catch(Exception $e){
			die("Error 218");
			
		}
   }
   
   public function getFinancialStatement($user,$date,$order,$accountFrom = null)
   {
	   $this->db = new Application_Model_DbTable_TbFinanceiroExtrato();
	   
	   $date = explode("-",$date);
	   $month = $date[0];
	   $year = $date[1];
	   
		try
		{
			$select = $this->db->select();
			$select->where("extf_idUsuario = ?",$user);
			$select->where('YEAR(extf_data) = ?', $year);
			$select->where('MONTH(extf_data) = ?', $month);
			if ($accountFrom == "l" || $accountFrom == "r" || $accountFrom == "c") {
				$select->where("extf_conta = ?",$accountFrom);
			}
			$select->order("extf_data $order");
			
			return $this->db->fetchAll($select)->toArray();
			
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error 219");
			
		}
   }

}

