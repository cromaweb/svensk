<?php

class Application_Model_Ewallet
{
	
	public function __construct()
	{
	  
	}
	
   public function getBalance($id)
   {
	   $this->db_TbFinanceiroSaldo = new Application_Model_DbTable_TbFinanceiroSaldo();
		try{
			if(!is_null($id)){
	
			  $select = $this->db_TbFinanceiroSaldo->select()
										->from($this->db_TbFinanceiroSaldo,array('bsal_saldo_liberado','bsal_saldo_bloqueado','bsal_saldo_areceber','bsal_saldo_rendimento','bsal_saldo_comissoes','bsal_saldo_bitwyn','bsal_saldo_deposit'))
										->where("bsal_idUsuario = ?",$id);
			  
			  return $this->db_TbFinanceiroSaldo->fetchRow($select);
			}
		}catch(Exception $e){
			die("Error 160");
			
		}
   }
   
   public function getRBMA()
   {
	   $this->db_TbBnsRbma = new Application_Model_DbTable_TbBnsRbma();
		try{
			  $select = $this->db_TbBnsRbma->select()
										->from($this->db_TbBnsRbma,array('rbma_valor_real'))
										->where("rbma_id = 1");
			  
			  $return = $this->db_TbBnsRbma->fetchRow($select);
			  return $return["rbma_valor_real"];

		}catch(Exception $e){
			die("Error 242");
			
		}
   }

   
   public function getConvertUSDBTC($value)
   {

		//Convers�o saldo dispon�vel em BTC
        $currency = curl_init();
        curl_setopt($currency, CURLOPT_URL, "https://blockchain.info/tobtc?currency=USD&value=$value");
        curl_setopt($currency, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($currency, CURLOPT_HEADER, FALSE);
        $r = curl_exec($currency);

        curl_close($currency);
		
        if (empty($r) || $r=="Invalid Numerical Value") {
            $r = 0.00;
        }
		
		return $r;
   }
   
	/**
		Function: getFormOfPayment($id)
		Parameters: $invoice: N�mero da fatura
		Action: Retorna o valor da fatura
		Return: fat_valor
		Table: tb_fatura
	*/
	public function getFormOfPayment($id)
	{
		$this->db_TbMeiodepagamento = new Application_Model_DbTable_TbMeiodepagamento();
		try
		{
			if(!is_null($id)){
	
			  $select = $this->db_TbMeiodepagamento->select()
										->from($this->db_TbMeiodepagamento,array('mdp_nome'))
										->where("mdp_id = ?",$id);
			  
			  $return = $this->db_TbMeiodepagamento->fetchRow($select);
			  return $return["mdp_nome"];
			  
			}

		}catch(Exception $e){
			die("Error 161");
			
		}
	}
	
	/**
		Function: confirmPaymentInvoice($idInvoice,$descPayment,$formPayment)
		Parameters: $invoice: N�mero da fatura
		Action: Retorna o valor da fatura
		Return: fat_valor
		Table: tb_fatura
	*/
	public function setPaymentInvoice($idInvoice,$descPayment,$formPayment)
	{
		$this->db_TbFatura = new Application_Model_DbTable_TbFatura();
		try
		{
			$dados = array(
			 'fat_status' => '2',
			 'fat_pagseguro' => $descPayment,
			 'fat_formaPagamento' => $formPayment,
			 'fat_dataPagamento' => new Zend_Db_Expr('NOW()')
			);
			$where = $this->db_TbFatura->getAdapter()->quoteInto("fat_id = ?", $idInvoice);
			return $this->db_TbFatura->update($dados, $where);
		
		}catch(Exception $e){
			die("Error 162");
			
		}
	}
	
	/**
		Function: setOrder($idOrder)
		Parameters: $invoice: N�mero da fatura
		Action: Retorna o valor da fatura
		Return: fat_valor
		Table: tb_fatura
	*/
	public function setOrder($idOrder)
	{
		$this->db_TbPedido = new Application_Model_DbTable_TbPedido();
		try
		{
			$dados = array(
			 'ped_pago' => '1',
			 'ped_dataPagamento' => new Zend_Db_Expr('NOW()')
			);
			$where = $this->db_TbPedido->getAdapter()->quoteInto("ped_id = ?", $idOrder);
			return $this->db_TbPedido->update($dados, $where);
		
		}catch(Exception $e){
			die("Error 163");
			
		}
	}
	
	//FORMATA COMO TIMESTAMP
	public function dataToTimestamp($data){
		
	   $ano = substr($data, 6,4);
	   $mes = substr($data, 3,2);
	   $dia = substr($data, 0,2);
		return mktime(0, 0, 0, $mes, $dia, $ano); 
		
	}
	
	//SOMA MESES
    function SomaMeses($data,$meses){
    	//$data = "20-10-2014"; 
    	return date('Y-m-d', strtotime("+$meses months",strtotime($data))); 
    }
	
	//SOMA DIAS
    function SomaDias($data,$dias){
    	//$data = "20-10-2014"; 
    	return date('Y-m-d', strtotime("+$dias days",strtotime($data))); 
    }
	
	//SOMA 01 DIA
	public function Soma1dia($data){
		
	   $ano = substr($data, 6,4);
	   $mes = substr($data, 3,2);
	   $dia = substr($data, 0,2);
		return date("d/m/Y", mktime(0, 0, 0, $mes, $dia+1, $ano));
		
	}
	
	//SOMA DIAS �TEIS
	public function SomaDiasUteis($xDataInicial,$xSomarDias)
	{
		try
		{
			
		for($ii=1; $ii<=$xSomarDias; $ii++){
		  
		  $xDataInicial=$this->Soma1dia($xDataInicial); //SOMA DIA NORMAL
		  
		  //VERIFICANDO SE EH DIA DE TRABALHO
		  if(date("w", $this->dataToTimestamp($xDataInicial))=="0"){
			 //SE DIA FOR DOMINGO OU FERIADO, SOMA +1
			 $xDataInicial=$this->Soma1dia($xDataInicial);
			
		  }else if(date("w", $this->dataToTimestamp($xDataInicial))=="6"){
			 //SE DIA FOR SABADO, SOMA +2
			 $xDataInicial=$this->Soma1dia($xDataInicial);
			 $xDataInicial=$this->Soma1dia($xDataInicial);
			
		  }
		}
	   
		$xDataInicial = substr($xDataInicial,6,4) . "-" .substr($xDataInicial,3,2) . "-" .substr($xDataInicial,0,2);
	   
		return $xDataInicial;
		
		}catch(Exception $e){
			die("Error 164");
			
		}
	}
	
	/**
		Function: setNewPlanUserDB($idUser,$product,$order,$max,$dateStart,$dateEnd)
		Parameters: $invoice: N�mero da fatura
		Action: Retorna o valor da fatura
		Return: fat_valor
		Table: tb_fatura
	*/
	public function setNewPlanUser($idUser,$product,$order,$value,$max,$quantityOfItems,$dateEnd)
	{
		$this->db_TbUserPlans = new Application_Model_DbTable_TbUserPlans();
		try{			
			$dados = array(
			'uplan_user_id' => $idUser,
			'uplan_prod_id' => $product,
			'uplan_pedido_id' => $order,
			'uplan_value' => $value,
			'uplan_max' => $max,
			'uplan_amount' => $quantityOfItems,
			'uplan_day_start' => new Zend_Db_Expr('NOW()'),
			'uplan_day_end' => $dateEnd
			);
			return $this->db_TbUserPlans->insert($dados);
			  
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error 165");
			
		}
		
	}
	
	/**
		Function: setUpdatePackageUserDB($idUser,$order,$product)
		Parameters: $invoice: N�mero da fatura
		Action: Retorna o valor da fatura
		Return: fat_valor
		Table: tb_alteraLog
	*/
	public function setUpdatePackageUser($idUser,$order,$product)
	{
		$this->db_TbUser = new Application_Model_DbTable_TbUser();
		$this->setOrder($order);
		
		
		try
		{
			$dados = array(
			 'usr_start' => '1',
			 'usr_package' => $product
			);
			$where = $this->db_TbUser->getAdapter()->quoteInto("usr_id = ?", $idUser);
			return $this->db_TbUser->update($dados, $where);
		
		}catch(Exception $e){
			die("Error 166");
			
		}
	}
	
	/**
		Function: setBonusPurchase($idUser,$sponsor,$invoice,$product,$usrPackage,$typePurchase,$usrActive)
		Parameters: $invoice: N�mero da fatura
		Action: Retorna o valor da fatura
		Return: fat_valor
		Table: tb_bns_compra
	*/
	public function setBonusPurchase($idUser,$sponsor,$invoice,$product,$usrPackage,$typePurchase,$usrActive)
	{
		$this->db_TbBnsCompra = new Application_Model_DbTable_TbBnsCompra();
		
		try{			
			$dados = array(
			'comp_idUsuario' => $idUser,
			'comp_patrocinador' => $sponsor,
			'comp_idFatura' => $invoice,
			'comp_idProduto' => $product,
			'comp_idProdutoAnterior' => $usrPackage,
			'comp_tipoCompra' => $typePurchase,
			'comp_data' => new Zend_Db_Expr('NOW()'),
			'comp_ativo' => $usrActive
			);
			return $this->db_TbBnsCompra->insert($dados);
			  
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error 167");
			
		}
	}
	
	/**
		Function: setBonusPurchase($idUser,$sponsor,$invoice,$product,$usrPackage,$typePurchase,$usrActive)
		Parameters: $invoice: N�mero da fatura
		Action: Retorna o valor da fatura
		Return: fat_valor
		Table: tb_bns_compra
	*/
	public function setFinancialStatement($idUser,$invoice = 0,$description = '',$previousBalance,$currentBalance,$type,$value, $accountFrom)
	{
		$this->db_TbFinanceiroExtrato = new Application_Model_DbTable_TbFinanceiroExtrato();
		//Se alguns dos valores for NULL, incrementa o 0
		if(is_null($previousBalance)){ $previousBalance = 0; }
		if(is_null($currentBalance)){ $currentBalance = 0; }
		if(is_null($value)){ $value = 0; }

		if($accountFrom == "r") {
			$conta ="r";
		} elseif ($accountFrom == "c") {
			$conta = "c";
		} elseif ($accountFrom == "f") {
			$conta = "f";
		} elseif ($accountFrom == "b") {
			$conta = "b";
		} else {
			$conta = "l";
		}
		
		try{			
			$dados = array(
			'extf_idUsuario' => $idUser,
			'extf_idCompra' => $invoice,
			'extf_descricao' => $description,
			'extf_conta' => $conta,
			'extf_saldo_anterior' => $previousBalance,
			'extf_saldo_atual' => $currentBalance,
			'extf_tipo' => $type,
			'extf_valor' => $value,
			'extf_data' => new Zend_Db_Expr('NOW()')
			);
			return $this->db_TbFinanceiroExtrato->insert($dados);
			  
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error 168");
			
		}
	}
	
	/**
		Function: setBonusPurchase($idUser,$sponsor,$invoice,$product,$usrPackage,$typePurchase,$usrActive)
		Parameters: $invoice: N�mero da fatura
		Action: Retorna o valor da fatura
		Return: fat_valor
		Table: tb_bns_compra
	*/
	public function setDebitBalance($idUser,$value,$accountFrom = null)
	{

		try{			
			 $db = Zend_Db_Table_Abstract::getDefaultAdapter();

			if($accountFrom == "r") {
				$sql =  "INSERT INTO tb_financeiro_saldo (bsal_idUsuario, bsal_saldo_rendimento) 
			 			VALUES (".$idUser.",'".$value."') 
						ON DUPLICATE KEY 
						UPDATE bsal_saldo_rendimento = (bsal_saldo_rendimento - ".$value.");";
			} elseif ($accountFrom == "c") {
				$sql =  "INSERT INTO tb_financeiro_saldo (bsal_idUsuario, bsal_saldo_comissoes) 
			 			VALUES (".$idUser.",'".$value."') 
						ON DUPLICATE KEY 
						UPDATE bsal_saldo_comissoes = (bsal_saldo_comissoes - ".$value.");";
			} elseif ($accountFrom == "f") {
				$sql =  "INSERT INTO tb_financeiro_saldo (bsal_idUsuario, bsal_saldo_bitfreedom) 
			 			VALUES (".$idUser.",'".$value."') 
						ON DUPLICATE KEY 
						UPDATE bsal_saldo_bitfreedom = (bsal_saldo_bitfreedom - ".$value.");";
			} elseif ($accountFrom == "b") {
				$sql =  "INSERT INTO tb_financeiro_saldo (bsal_idUsuario, bsal_saldo_bitwyn) 
			 			VALUES (".$idUser.",'".$value."') 
						ON DUPLICATE KEY 
						UPDATE bsal_saldo_bitwyn = (bsal_saldo_bitwyn - ".$value.");";
			} else {
				$sql =  "INSERT INTO tb_financeiro_saldo (bsal_idUsuario, bsal_saldo_liberado) 
			 			VALUES (".$idUser.",'".$value."') 
						ON DUPLICATE KEY 
						UPDATE bsal_saldo_liberado = (bsal_saldo_liberado - ".$value.");";
			}
			 
							
			return $db->query($sql);
			  
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error 169");
			
		}
	}
	
	/**
		Function: setBonusPurchase($idUser,$sponsor,$invoice,$product,$usrPackage,$typePurchase,$usrActive)
		Parameters: $invoice: N�mero da fatura
		Action: Retorna o valor da fatura
		Return: fat_valor
		Table: tb_bns_compra
	*/
	public function setCreditBalance($idUser,$value, $accountFrom = null)
	{
		
		try{			
			 $db = Zend_Db_Table_Abstract::getDefaultAdapter();
			 if($accountFrom == "r") {
				$sql =  "INSERT INTO tb_financeiro_saldo (bsal_idUsuario, bsal_saldo_rendimento) 
			 			VALUES (".$idUser.",'".$value."') 
						ON DUPLICATE KEY 
						UPDATE bsal_saldo_rendimento = (bsal_saldo_rendimento + ".$value.");";
			} elseif ($accountFrom == "c") {
				$sql =  "INSERT INTO tb_financeiro_saldo (bsal_idUsuario, bsal_saldo_comissoes) 
			 			VALUES (".$idUser.",'".$value."') 
						ON DUPLICATE KEY 
						UPDATE bsal_saldo_comissoes = (bsal_saldo_comissoes + ".$value.");";
			} elseif ($accountFrom == "f") {
				$sql =  "INSERT INTO tb_financeiro_saldo (bsal_idUsuario, bsal_saldo_bitfreedom) 
			 			VALUES (".$idUser.",'".$value."') 
						ON DUPLICATE KEY 
						UPDATE bsal_saldo_bitfreedom = (bsal_saldo_bitfreedom + ".$value.");";
			} elseif ($accountFrom == "b") {
				$sql =  "INSERT INTO tb_financeiro_saldo (bsal_idUsuario, bsal_saldo_bitwyn) 
			 			VALUES (".$idUser.",'".$value."') 
						ON DUPLICATE KEY 
						UPDATE bsal_saldo_bitwyn = (bsal_saldo_bitwyn + ".$value.");";
			} else {
				$sql =  "INSERT INTO tb_financeiro_saldo (bsal_idUsuario, bsal_saldo_liberado) 
			 			VALUES (".$idUser.",'".$value."') 
						ON DUPLICATE KEY
						UPDATE bsal_saldo_liberado = (bsal_saldo_liberado + ".$value.");";
			}
			 
							
			return $db->query($sql);
			  
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error 170");
			
		}
	}
	
	/** 
		Function: addDebit($debuser, $admdebuser, $value, $description)
		Parameters: $debuser:  ID do usu�rio que o d�bito ser� efetuado
					$admdebuser: ID do administrador ou usu�rio que efetuou o d�bito
					$value: Valor do d�bito
					$description: Descri��o do d�bito
		Action: Adiciona d�bito na conta 
		Table: tb_account
	*/	
	public function addDebit($idUser, $invoice, $value, $description, $accountFrom = null)
	{
		$this->modelOrder = new Application_Model_Order();
		
		try{
			//$debit = "-".$value;
			$balance = $this->getBalance($idUser);

			if($accountFrom == "r") {
				$previousBalance = $balance["bsal_saldo_rendimento"];
			} elseif ($accountFrom == "c") {
				$previousBalance = $balance["bsal_saldo_comissoes"];
			} elseif ($accountFrom == "f") {
				$previousBalance = $balance["bsal_saldo_bitfreedom"];
			} elseif ($accountFrom == "b") {
				$previousBalance = $balance["bsal_saldo_bitwyn"];
			} else {
				$previousBalance = $balance["bsal_saldo_liberado"];
			}
			
			$currentBalance = $previousBalance - $value;
			$this->setFinancialStatement($idUser,$invoice,$description,$previousBalance,$currentBalance,'d',-$value, $accountFrom);

			$this->setDebitBalance($idUser,$value,$accountFrom);
	
			//geraLog($description, $value, $idUser);
			$this->modelOrder->setLog($description . " - CONTA: " . $accountFrom, $value, $idUser);
		
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error 171");
			
		}
	}


	/** 
		Function: addCredit($idUser, $value, $description)
		Parameters: $debuser:  ID do usu�rio que o d�bito ser� efetuado
					$admdebuser: ID do administrador ou usu�rio que efetuou o d�bito
					$value: Valor do d�bito
					$description: Descri��o do d�bito
		Action: Adiciona d�bito na conta 
		Table: tb_account
	*/	
	public function addCredit($idUser, $usertransfer, $value, $description, $accountFrom = null)
	{
		
		$this->modelOrder = new Application_Model_Order();
		try{
			$balance = $this->getBalance($idUser);

			if($accountFrom == "r") {
				$previousBalance = $balance["bsal_saldo_rendimento"];
			} elseif ($accountFrom == "c") {
				$previousBalance = $balance["bsal_saldo_comissoes"];
			} elseif ($accountFrom == "f") {
				$previousBalance = $balance["bsal_saldo_bitfreedom"];
			} elseif ($accountFrom == "b") {
				$previousBalance = $balance["bsal_saldo_bitwyn"];
			} else {
				$previousBalance = $balance["bsal_saldo_liberado"];
			}

			$currentBalance = $previousBalance + $value;
			$this->setFinancialStatement($idUser,$usertransfer,$description,$previousBalance,$currentBalance,'c',$value, $accountFrom);
			$this->setCreditBalance($idUser,$value, $accountFrom);
	
			//geraLog($description, $value, $idUser);
			$this->modelOrder->setLog($description . " - CONTA: " . $accountFrom, $value, $idUser);
		
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error 172");
			
		}
	}

	/**
		Function addTransfer
		Parameters: $user_from, $user_to, $value
		Table: tb_transfers
	*/
	public function addTransfer($user_from, $user_to, $value) {
		$this->db_TbTransfer = new Application_Model_DbTable_TbTransfers();
		$date = new Zend_Db_Expr('NOW()');
		$hash = md5($user_from . $user_to . $date . rand(1,999));
		try {
			$dados = array(
				'transfer_user_from' => $user_from,
				'transfer_user_to' => $user_to,
				'transfer_value' => $value,
				'transfer_hash_key' => $hash,
				'transfer_date' => $date
			);
			return $this->db_TbTransfer->insert($dados);
		} catch (Exception $e) {
			//die($e->getMessage());
		}
	}

	/**
	  Function: payInvoice($payuser, $fatuser, $invoice)
	  Parameters: $payuser: id do usuario que est� efetuando a baixa
				  $fatuser:    login do usuario devedor da fatura
				  $invoice: Numero da fatura
	  Action: Efetua a baixa na fatura
	  Table: tb_fatura
	*/
	public function payInvoice($idUser, $formPayment, $invoice, $percBitwyn)
	{

		if (empty($percBitwyn)) {
			$percBitwyn = 0;
		}
		if ($percBitwyn > 50) {
			$percBitwyn = 50;
		}

		$this->modelAccount = new Application_Model_Account();
		$this->modelOrder = new Application_Model_Order();
		$this->modelTreeview = new Application_Model_Treeview();

		
		try{
		//Caso seja baixa automatica, lembraar se o valor do produto � aquele que veio no arquivo de retorno no banco, caso seja diferente, ignorar.
		$fatUser = $this->modelOrder->getCheckUserInvoice($invoice);
		
		//Se o pagador for diferente do dono da fatura
		if($fatUser!=$idUser){
			$idUserDif = $idUser;
			$idUser = $fatUser;
		}
		//$sponsor = getSponsorDB($idUser);
		//list($sponsor,$usrPackage) = selectListManyFieldsDB("usr_invited_id,usr_package","tb_user","usr_id",$idUser);
		$sponsor = $this->modelAccount->getSponsor($idUser);
		$usrPackage = $this->modelAccount->getUserPackage($idUser);
		$packageSequence = $this->modelOrder->getProductBasic($usrPackage);
		$usrPackageSequence = $packageSequence["prod_ordem"];
		$descForm = $this->getFormOfPayment($formPayment);
		$reg = $this->modelOrder->getProductInvoice($invoice);
		$num = count($reg);

		//variaveis do $reg
		$plan = $reg["prod_especial"];
		$prod_validade_dias = $reg["prod_validade_dias"];
		if (empty($prod_validade_dias)) {
			$prod_validade_dias = 350;
		}
		$product = $reg["prod_id"];
		$namePkg = $reg["prod_titulo"];
		$imagePkg = $reg["prod_imagem"];
		$prodSequence = $reg["prod_ordem"];
		$max = $reg["prod_max"];
		$order = $reg["ped_id"];
		$value = $reg["fat_valor"];
		$quantityOfItems = $reg["fat_itens"];
		
		//Verifica se os uplines do usu�rio foram mapeados antes de come�ar o processo de pontua��o que depende de saber quais s�o os uplines.
		//$numcp = getNumLegsUplinesDB($idUser);
		$numcp = $this->modelTreeview->getCheckLeg($idUser,$sponsor);
	
		//Se tiver 1 fatura e o n�mero de uplines existir
		if($num>0 && $numcp>0){
		
		//Confirma pagamento da fatura, descri��o meio de pagamento, ID de pagamento
		$sql = $this->setPaymentInvoice($invoice,$descForm,$formPayment);
		
		//Coonfirma pedido
		$sqlPed = $this->setOrder($order);
		
    		if($sql){
    			
    			if($plan==1){
    				
    				//Se for um plano
    				//$currentDate = date("d/m/Y");
    				$currentDate = date("d-m-Y");
    				//$dateEnd = $this->SomaDiasUteis($currentDate,$prod_validade_dias);
    				$dateEnd = $this->SomaMeses($currentDate,$prod_validade_dias/30);
    				
    				//Insere um novo plano para o usu�rio
    				$sqlPurchase = $this->setNewPlanUser($idUser,$product,$order,$value,$max,$quantityOfItems,$dateEnd);
    				$purchase = 1;
    				
    						
    			}elseif($plan==2){
    				
    				$sqlPurchase = true;
    				$purchase = 3;
    				
    			}elseif($plan==0){
    				
    				//Se for uma conta
    				if((($usrPackage!=0) || (!empty($usrPackage))) && $prodSequence>$usrPackageSequence){
    					//setAlteraLogDB($idUser,"upgradepack",$usrPackage);
    					$this->modelOrder->setLog("upgradepack", $usrPackage, $idUser);
    				}
    				
    				if($prodSequence>$usrPackageSequence){
    					$sessao = new Zend_Session_Namespace(SESSION_OFFICE);
    					$sessao->namePackage = $namePkg;
    					$sessao->imagePackage = $imagePkg;
    					
    					$sqlPurchase = $this->setUpdatePackageUser($idUser,$order,$product);
    					//Permite inser��o de pontua��o de compras 0 n�o outro n�mero sim
    					$purchase = 0;
    				}else{
    					$sqlPurchase = true;
    					$purchase = 0;
    				}
    			}    		  
    
    		}
	

		}else{
		  $msg = "Erro payment!";
		}
		//Confere se foi inserido o pedido na tb_bns_compra
		if($sqlPurchase){
			
			if($purchase!=0){
				//Entrada do bonus da compra liquidada
				$this->setBonusPurchase($idUser,$sponsor,$invoice,$product,$usrPackage,$purchase,$usrActive = 'S');
			}
			if(!empty($idUserDif))
			{
				if ($percBitwyn > 0) {
					$valueWalletBitwyn = round($value * $percBitwyn / 100, 2);
					$this->addDebit($idUserDif, $invoice, $valueWalletBitwyn, 'Pagamento Fatura '.$invoice,'b');
					$valueWalletBytc =  $value - $valueWalletBitwyn;
					$this->addDebit($idUserDif, $invoice, $valueWalletBytc, 'Pagamento Fatura '.$invoice,'l');
				} else {
					$this->addDebit($idUserDif, $invoice, $value, 'Pagamento Fatura '.$invoice);
				}
				
			}else{
				if ($percBitwyn > 0) {
					$valueWalletBitwyn = round($value * $percBitwyn / 100, 2);
					$this->addDebit($idUser, $invoice, $valueWalletBitwyn, 'Pagamento Fatura '.$invoice,'b');
					$valueWalletBytc =  $value - $valueWalletBitwyn;
					$this->addDebit($idUser, $invoice, $valueWalletBytc, 'Pagamento Fatura '.$invoice,'l');
				} else {
					$this->addDebit($idUser, $invoice, $value, 'Pagamento Fatura '.$invoice);
				}
			}
			
		  $msg = "paid";
		}else{
		  $msg = "Erro ao confirmar fatura!";
		}
		
		return $msg;
		
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error 173");
			
		}
	}
	
   public function getLimitBonus($idUser)
   {
		$this->db = new Application_Model_DbTable_TbBnsUsuario();
		
		try
		{
			if(!is_null($idUser)){
				$select = $this->db->select()
										->from($this->db,array('busr_idUsuario','busr_investimento_total', 'busr_bonus_limite'))
										->where("busr_idUsuario = ?", $idUser);
			  
				return $this->db->fetchRow($select);
				
			}
		  
		}catch(Exception $e){
			die($e->getMessage());
			//die("Error 138+1");
			
		}
   }
	
	public function setLimitBonus($idUser, $value)
	{
		$this->db = new Application_Model_DbTable_TbBnsUsuario();
		
		try
		{
		    $numLimit = 3;
		    
			$return = $this->getLimitBonus($idUser);
			
			if ($return['busr_bonus_limite'] == 0) {
				$new_bonus_limit = $return['busr_investimento_total'] + $value;
				$new_bonus_limit = $new_bonus_limit * $numLimit;
			} elseif($return['busr_bonus_limite'] > 0) {
				$new_bonus_limit = $value * $numLimit;
				$new_bonus_limit = $new_bonus_limit + $return['busr_bonus_limite'];
			} else {
				$new_bonus_limit = $value * $numLimit;
			}
			
			if ($return['busr_idUsuario'] == 0) {
				
				$dados = array(
					'busr_idUsuario' => $idUser,
					'busr_bonus_limite' => round($new_bonus_limit,2),
					'busr_investimento_total' => round($return['busr_investimento_total'] + $value,2),
					'busr_status' => 1
				);
				
				$this->db->insert($dados);
				
				return 1;
				
			}else{
				
				$dados = array(
					'busr_bonus_limite' => round($new_bonus_limit,2),
					'busr_investimento_total' => round($return['busr_investimento_total'] + $value,2),
					'busr_status' => 1
				);
				
				$where = $this->db->getAdapter()->quoteInto("busr_idUsuario = ?", $idUser);
				$this->db->update($dados, $where);
				
				return 1;
			}
		  
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error 138");
			
		}
	}
	
	/** 
		Function: getAccumulatedPoints($idUser)
		Parameters: $debuser:  ID do usu�rio que o d�bito ser� efetuado
					$admdebuser: ID do administrador ou usu�rio que efetuou o d�bito
					$value: Valor do d�bito
					$description: Descri��o do d�bito
		Action: Adiciona d�bito na conta 
		Table: tb_account
	*/	
	public function getAccumulatedPoints($idUser)
	{
		
		$this->db = new Application_Model_DbTable_TbBnsUsuario();
		
		try
		{
		  $select = $this->db->select()
									->from($this->db,array('busr_pontos_acumulados_esquerda','busr_pontos_acumulados_direita'))
									->where("busr_idUsuario = ?",$idUser);
		  
		  return $this->db->fetchRow($select);

		}catch(Exception $e){
			die($e->getMessage());
			die("Error 174");
			
		}
	}
	
	/** 
		Function: getConsolidatedBonus($idUser)
		Parameters: $debuser:  ID do usu�rio que o d�bito ser� efetuado
					$admdebuser: ID do administrador ou usu�rio que efetuou o d�bito
					$value: Valor do d�bito
					$description: Descri��o do d�bito
		Action: Adiciona d�bito na conta 
		Table: tb_account
	*/	
	public function getConsolidatedBonus($idUser)
	{
		
		$this->db = new Application_Model_DbTable_TbBnsConsolidado();
		
		try
		{
		  $select = $this->db->select()
									->from($this->db,array('bcons_valor_startfast', 'bcons_pontos_unilevel'))
									->where("DATE(bcons_data)=DATE(now())")
									->where("bcons_idUsuario = ?",$idUser);
		  
		  return $this->db->fetchRow($select);

		}catch(Exception $e){
			die("Error 175");
			
		}
	}
	
	/** 
		Function: getConsolidatedBonus($idUser)
		Parameters: $debuser:  ID do usuário que o débito será efetuado
					$admdebuser: ID do administrador ou usuário que efetuou o débito
					$value: Valor do d�bito
					$description: Descri��o do d�bito
		Action: Adiciona d�bito na conta 
		Table: tb_account
	*/	
	public function getEwalletActive($idUser)
	{
		$this->db = new Application_Model_DbTable_TbUser();

		try
		{
		  $select = $this->db->select()
									->from($this->db,array('usr_ewallet_active'))
									->where("usr_id = ?",$idUser);
		  
		  $return = $this->db->fetchRow($select);
		  return $return["usr_ewallet_active"];

		}catch(Exception $e){
			die("Error 176");
			
		}
	}
	
	/** 
		Function: getEwalletTransfer($idUser)
		Parameters: $debuser:  ID do usu�rio que o d�bito ser� efetuado
					$admdebuser: ID do administrador ou usu�rio que efetuou o d�bito
					$value: Valor do d�bito
					$description: Descri��o do d�bito
		Action: Adiciona d�bito na conta 
		Table: tb_account
	*/	
	public function getEwalletTransfer($idUser)
	{
		$this->db = new Application_Model_DbTable_TbUser();

		try
		{
		  $select = $this->db->select()
									->from($this->db,array('usr_transfer'))
									->where("usr_id = ?",$idUser);
		  
		  $return = $this->db->fetchRow($select);
		  return $return["usr_transfer"];

		}catch(Exception $e){
			die($e->getMessage());
			die("Error 176+1");
			
		}
	}
	
	
	/** 
		Function: getEwalletWithdrawal($idUser)
		Parameters: $debuser:  ID do usu�rio que o d�bito ser� efetuado
					$admdebuser: ID do administrador ou usu�rio que efetuou o d�bito
					$value: Valor do d�bito
					$description: Descri��o do d�bito
		Action: Adiciona d�bito na conta 
		Table: tb_account
	*/	
	public function getEwalletWithdrawal($idUser)
	{
		$this->db = new Application_Model_DbTable_TbUser();

		try
		{
		  $select = $this->db->select()
									->from($this->db,array('usr_withdrawal'))
									->where("usr_id = ?",$idUser);
		  
		  $return = $this->db->fetchRow($select);
		  return $return["usr_withdrawal"];

		}catch(Exception $e){
			die($e->getMessage());
			die("Error 176+2");
			
		}
	}
	
	/** 
		Function: getConsolidatedBonus($idUser)
		Parameters: $debuser:  ID do usu�rio que o d�bito ser� efetuado
					$admdebuser: ID do administrador ou usu�rio que efetuou o d�bito
					$value: Valor do d�bito
					$description: Descri��o do d�bito
		Action: Adiciona d�bito na conta 
		Table: tb_account
	*/	
	public function getSumFinancialStatement($idUser)
	{
		$this->db = new Application_Model_DbTable_TbFinanceiroExtrato();

		try
		{
		  $select = $this->db->select()
									->from($this->db,array('amount' => 'sum(extf_valor)'))
									->where("extf_idUsuario = ?",$idUser)
									->where("extf_descricao LIKE 'Transfer Debit%'")
									->where("extf_data >= '" . date("Y-m-d") . " 00:00:00'");
		  
		  $return = $this->db->fetchRow($select);
		  return $return["amount"];

		}catch(Exception $e){
			die("Error 177");
			
		}
	}
	
	/**
	  Function: setTransferCredit($usertransfer,$usercredit,$value)
	  Parameters: $usertransfer: id do remetente da transfer�ncia
				  $usercredit: id do destinat�rio da transfer�ncia
				  $value = valor da transfer�ncia
	  Action: Efetua a transfer�ncia de valores do saldo de um usu�rio para um outro
	  Table: tb_user,tb_financeiro_extrato, tb_financeiro_saldo
	*/
	public function setTransferCredit($usertransfer,$usercredit,$value, $accountFrom = NULL)
	{
		$this->db_TbUser = new Application_Model_DbTable_TbUser();
		$this->modelAccount = new Application_Model_Account();
		
		try
		{
			//if ($usertransfer != $usercredit) {
				$value = str_replace(",","",$value);
				if ($value >= 10) {
					//verifica o valor limite de transferencias diarias do usuario
					//list($limit) = abreSQL("SELECT usr_limit_transfer_day FROM tb_user WHERE usr_id='$usertransfer'");
					//$limit = $this->modelAccount->getUserLimitTransfer($usertransfer);
					//verifica o valor de transferencias feitas pelo usuario no dia
					//list($v) = abreSQL("SELECT sum(extf_valor) FROM tb_financeiro_extrato WHERE extf_descricao LIKE 'Transfer Debit%' and extf_idUsuario = " . $usertransfer . " and extf_data >= '" . date("Y-m-d") . " 00:00:00'");
					//$v = $this->getSumFinancialStatement($usertransfer);
					
					//if ((($v * (-1) + $value) ) <= $limit) {
						//Verifica o saldo dispon�vel do usu�rio
						
						/*list($balance) = abreSQL("SELECT bsal_saldo_liberado 
							FROM tb_financeiro_saldo WHERE bsal_idUsuario='$usertransfer'");*/
						//$balance = getBalanceReleasedDB($usertransfer);
						$balance = $this->getBalance($usertransfer);

						if($accountFrom == "r") {
							$balance = $balance["bsal_saldo_rendimento"];
						} elseif ($accountFrom == "c") {
							$balance = $balance["bsal_saldo_comissoes"];
						} elseif ($accountFrom == "f") {
							$balance = $balance["bsal_saldo_bitfreedom"];
						} elseif ($accountFrom == "b") {
							$balance = $balance["bsal_saldo_bitwyn"];
						} else {
							$balance = $balance["bsal_saldo_liberado"];
						}

						
						$fee = round($value * 0,2);
						$dollar = round($value + $fee,2);

						//Verifica se o usu�rio do cr�dito existe na tb_user
						//$usrCreditExist = getCountUserDB($usercredit);
						$usernameDebit = $this->modelAccount->getUserLogin($usertransfer);
						$usernameCredit = $this->modelAccount->getUserLogin($usercredit);
						
						if($dollar<=$balance && count($usernameCredit)!=0){
							
							//addDebit($usertransfer, $usercredit, $value, "Transfer Debit $debit | $usercredit");
							$this->addTransfer($usertransfer, $usercredit, $value);
							$this->addDebit($usertransfer, $usercredit, $value, "Transfer Debit | $usernameCredit",$accountFrom);
							if ($fee > 0) {
								$this->addDebit($usertransfer, $usercredit, $fee, "Transfer Debit Fee | $usernameCredit",$accountFrom);
							}
							//addCredit($usercredit, $value, "Transfer Credit | $usertransfer");
							if($accountFrom == "b") {
								$this->addCredit($usercredit, $usertransfer, $value, "Transfer Credit | $usernameDebit",$accountFrom);
							}else{
								$this->addCredit($usercredit, $usertransfer, $value, "Transfer Credit | $usernameDebit");	
							}
							return 1;
			
						}else{
							return 0;
						}
					//} else {
					//	return 3;
					//}
				} else {
					return 4;
				}
			//} else {
			//	return 2;
			//}
		}catch(Exception $e){
			die("Error 178");
			
		}
	}

	public function getTransfersAllTime($user)
	{
		$this->db_TbFinanceiroExtrato = new Application_Model_DbTable_TbFinanceiroExtrato();
		try
		{
			$select = $this->db_TbFinanceiroExtrato->select();
			$select->from($this->db_TbFinanceiroExtrato, array("count(*) as quantity", "SUM(extf_valor) as sum_value"));
			$select->where("extf_idUsuario = '" . $user . "' AND extf_tipo = 'd' AND extf_descricao LIKE 'Transfer Debit  | %'");
			$result = $this->db_TbFinanceiroExtrato->fetchRow($select);

			if (empty($result)) {
				$result['quantity'] = 0;
				$result['sum_value'] = 0;

			} else {
				$result['sum_value'] = $result['sum_value'] * (-1);
			}
			return $result;
		
		}catch(Exception $e){
			//die($e->getMessage());
			//die("Error 236");
		}
      	
	}

	public function getCountTransfersByDay($user, $accountFrom)
	{
		$this->db_TbFinanceiroExtrato = new Application_Model_DbTable_TbFinanceiroExtrato();
		try
		{
			$db = Zend_Db_Table_Abstract::getDefaultAdapter();
   			$result = $db->query("SELECT count(*) AS quantity, SUM(extf_valor) AS sum_value FROM tb_financeiro_extrato WHERE (extf_idUsuario = '" . $user . "' AND extf_tipo = 'd' AND extf_conta = '".$accountFrom."' AND extf_descricao LIKE 'Transfer Debit%' AND DATE_FORMAT(extf_data, '%Y-%m-%d') = '".date("Y-m-d")."') GROUP BY DATE_FORMAT(extf_data, '%Y-%m-%d')")
   							->fetch();
			if (empty($result)) {
				$result['quantity'] = 0;
				$result['sum_value'] = 0;
			} else {
				$result['sum_value'] = $result['sum_value'] * (-1);
			}
			return $result;
		}catch(Exception $e){
			//die($e->getMessage());
			//die("Error 235");
		}
	}
	/*public function getTransfersByDay($user)
	{
		$this->db_TbFinanceiroExtrato = new Application_Model_DbTable_TbFinanceiroExtrato();
		try
		{
			/*$select = $this->db_TbFinanceiroExtrato->select();
			$select->from($this->db_TbFinanceiroExtrato, array("count(*) as quantity", "SUM(extf_valor) as sum_value"));
			$select->where("extf_idUsuario = '" . $user . "' AND extf_tipo = 'd' AND extf_descricao LIKE 'Transfer Debit  | %' AND DATE_FORMAT(extf_data, '%Y-%m-%d') =  '".date("Y-m-d")."'");
			$select->group("DATE_FORMAT(extf_data,  '%Y-%m-%d')");	

			$teste = $select->__toString();
			
			$result = $this->db_TbFinanceiroExtrato->fetchRow($select);* /

			$db = Zend_Db_Table_Abstract::getDefaultAdapter();
   			$result = $db->query("SELECT count(*) AS quantity, SUM(extf_valor) AS sum_value FROM tb_financeiro_extrato WHERE (extf_idUsuario = '" . $user . "' AND extf_tipo = 'd' AND extf_descricao LIKE 'Transfer Debit | %' AND DATE_FORMAT(extf_data, '%Y-%m-%d') = '".date("Y-m-d")."') GROUP BY DATE_FORMAT(extf_data, '%Y-%m-%d')")
   							->fetch();

			if (empty($result)) {
				$result['quantity'] = 0;
				$result['sum_value'] = 0;
			} else {
				$result['sum_value'] = $result['sum_value'] * (-1);
			}

			return $result;
		}catch(Exception $e){
			//die($e->getMessage());
			//die("Error 235");
		}
	}

	public function getTransfersByMonth($user)
	{
		$this->db_TbFinanceiroExtrato = new Application_Model_DbTable_TbFinanceiroExtrato();
		try
		{
			$select = $this->db_TbFinanceiroExtrato->select();
			$select->from($this->db_TbFinanceiroExtrato, array("count(*) as quantity", "SUM(extf_valor) as sum_value"));
			$select->where("extf_idUsuario = '" . $user . "' AND extf_tipo = 'd' AND extf_descricao LIKE 'Transfer Debit  | %' AND DATE_FORMAT(extf_data, '%Y-%m') =  '".date("Y-m")."'");
			$select->group("DATE_FORMAT(extf_data,  '%Y-%m')");

			$result = $this->db_TbFinanceiroExtrato->fetchRow($select);

			if (empty($result)) {
				$result['quantity'] = 0;
				$result['sum_value'] = 0;

			} else {
				$result['sum_value'] = $result['sum_value'] * (-1);
			}
			return $result;
		
		}catch(Exception $e){
			//die($e->getMessage());
			//die("Error 236");
		}
      	
	}
	public function getTransfersByYear($user)
	{
		$this->db_TbFinanceiroExtrato = new Application_Model_DbTable_TbFinanceiroExtrato();
		try
		{
			$select = $this->db_TbFinanceiroExtrato->select();
			$select->from($this->db_TbFinanceiroExtrato, array("count(*) as quantity", "SUM(extf_valor) as sum_value"));
			$select->where("extf_idUsuario = '" . $user . "' AND extf_tipo = 'd' AND extf_descricao LIKE 'Transfer Debit  | %' AND DATE_FORMAT(extf_data, '%Y') =  '".date("Y")."'");
			$select->group("DATE_FORMAT(extf_data,  '%Y')");

			$result = $this->db_TbFinanceiroExtrato->fetchRow($select);

			if (empty($result)) {
				$result['quantity'] = 0;
				$result['sum_value'] = 0;
			} else {
				$result['sum_value'] = $result['sum_value'] * (-1);
			}
			return $result;
		}catch(Exception $e){
			//die($e->getMessage());
			//die("Error 237");
		}
		
	}*/

	public function getTransfers($user) {
		$this->db_TbTransfer = new Application_Model_DbTable_TbTransfers();
		$select = $this->db_TbTransfer->select()
										->where("transfer_user_from = $user OR transfer_user_to = $user")
										->order("transfer_date");
		return $this->db_TbTransfer->fetchAll($select)->toArray();
	}

	public function getTransferByHash($hash) {
		$this->db_TbTransfer = new Application_Model_DbTable_TbTransfers();
		$select = $this->db_TbTransfer->select()
									->where("transfer_hash_key = '".$hash."'")
									->order("transfer_date");
		return $this->db_TbTransfer->fetchRow($select);
	}

	public function getTransferById($id) {
		$this->db_TbTransfer = new Application_Model_DbTable_TbTransfers();
		$select = $this->db_TbTransfer->select()
										->where("transfer_id = '".$id."'")
										->order("transfer_date");
		return $this->db_TbTransfer->fetchRow($select);
	}
	
	public function getUserDeposit($user)
	{
		$this->db = new Application_Model_DbTable_TbFinanceiroSaldo();
		
	   try
	   {
			if(!is_null($user)){
				
				$select = $this->db->select()
										->from($this->db,array('bsal_idUsuario','bsal_saldo_deposit'))
										->where("bsal_idUsuario = ?", $user);
				
				return $this->db->fetchRow($select);
			
			}
			
		}catch(Exception $e){
			die("Error 178+3");
			
		}
	}
	
	public function setUserDeposit($user,$value)
	{
		$this->db = new Application_Model_DbTable_TbFinanceiroSaldo();
		
	   try
	   {
			if(!is_null($value)){
				
				$amountDeposit = $this->getUserDeposit($user);
				
				if(empty($amountDeposit["bsal_idUsuario"])){
					
					$dados = array(
					'bsal_idUsuario' => $user,
					'bsal_saldo_deposit' => $value
					);
					return $this->db->insert($dados);
					
				}else{
					$amount = $amountDeposit["bsal_saldo_deposit"] + $value;
					$data['bsal_saldo_deposit'] = $amount;
					$where = $this->db->getAdapter()->quoteInto("bsal_idUsuario = ?", $user);
					$this->db->update($data, $where);
				}
				
			}
			
		}catch(Exception $e){
			die("Error 178+4");
			
		}
	}

}
