<?php

class Application_Model_Login
{
	
	public function __construct()
	{
		$this->db_TbLogAcesso = new Application_Model_DbTable_TbLogAcesso();
		$this->db_TbUser = new Application_Model_DbTable_TbUser();
		$this->db_TbPasswordkey = new Application_Model_DbTable_TbPasswordkey();
		$this->db_Session = new Application_Model_DbTable_Session();
		
	  
	}
	
	public function getAuthentication($username = null,$password = null,$master = null)
	{
		if(!is_null($username) && !is_null($password)){
			try
			{
			    if($master==1)
			    {
        			$select = $this->db_TbUser->select()
        										->from($this->db_TbUser)
        										->where("(usr_login_id = '$username')")
        										->ORwhere("(usr_email = '$username')");
			    }
			    else
			    {
        			$select = $this->db_TbUser->select()
        										->from($this->db_TbUser)
        										->where("(usr_login_id = '$username' AND usr_password ='$password')")
        										->ORwhere("(usr_email = '$username' AND usr_password ='$password')");
			    }
			}catch(Exception $e){
				die("Error 179");
			}
	
		  return $this->db_TbUser->fetchRow($select);
	
		}
		
	}
	
	public function getPassMaster()
	{
	    $this->db_TbConfig = new Application_Model_DbTable_TbConfig();

        try{
        $select = $this->db_TbConfig->select()
        							->from($this->db_TbConfig,array('config_pass_master'))
        							->where("config_id = 1");
        }catch(Exception $e){
        	die("Error 180+pass");
        }
	
        $return = $this->db_TbConfig->fetchRow($select);
        return $return["config_pass_master"];
		
	}
   
	public function getUserRecovery($username = null)
	{
		if(!is_null($username)){
			try{
			$select = $this->db_TbUser->select()
										->from($this->db_TbUser,array('usr_id','usr_email','usr_language'))
										->where("usr_login_id = ?",$username)
										->ORwhere("usr_email = ?",$username);
			}catch(Exception $e){
				die("Error 180");
			}
	
		  return $this->db_TbUser->fetchRow($select);
	
		}
		
	}
   
	public function setLogAccess($user,$ip)
	{
		try{
			
			$dados = array(
				'usr_id' => $user,
				'ace_ip' => $ip,
				'ace_datahora' => new Zend_Db_Expr('NOW()')
			);
			return $this->db_TbLogAcesso->insert($dados);

		}catch(Exception $e){
			die("Error 181");
			
		}
		
	}

	public function getPasswordKey($key)
	{
		if(!is_null($key)){
			try
			{
				
				$select = $this->db_TbPasswordkey->select()
											->from($this->db_TbPasswordkey,array('key_vencimento', 'key_alterada', 'key_id'))
											->where("key_token = ?",$key);
			}catch(Exception $e){
				die("Error 182");
			}
	
		  return $this->db_TbPasswordkey->fetchRow($select);
	
		}
		
	}

	public function setPasswordKey(array $request)
	{
		try{
			$dados = array(
				'key_id' => $request["key_id"],
				'key_token' => $request["key_token"],
				'key_vencimento' => $request["key_vencimento"],
				'key_alterada' => '0',
				'key_data' => new Zend_Db_Expr('NOW()')
			);
			return $this->db_TbPasswordkey->insert($dados);

		}catch(Exception $e){
			die("Error 183");
			
		}
		
	}
		
	public function updatePasswordKeyAlterada($user)
	{
		try{
			$dados = array(
				'key_alterada' => '1'
			);
			
			$where = $this->db_TbPasswordkey->getAdapter()->quoteInto("key_id = ?", $user);
			return $this->db_TbPasswordkey->update($dados,$where);
			
		}catch(Exception $e){
			die("Error 184");
			
		}
		
	}

	public function updateSession($user)
	{
		
		try
		{
			$select = $this->db_Session->select()
										->from($this->db_Session,array('id'))
										->where("data LIKE ?",'%'.$user.'%');
										
			$return = $this->db_Session->fetchRow($select);
			$id = $return["id"];
			
			if (!empty($id))
			{
				$dados = array(
					'data' => ''
				);
				$where = $this->db_Session->getAdapter()->quoteInto("id = ?", $id);
				return $this->db_Session->update($dados,$where);
			}

		}catch(Exception $e){
			die("Error 185");
			
		}
		
	}

}

