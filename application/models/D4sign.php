<?php

class Application_Model_D4sign
{
	public function __construct()
	{

	}

   public function getTemplate($uri,$id_template,$tokenAPI,$cryptKey,$data)
   {
        $url = $uri.'documents/'.$id_template.'/makedocumentbytemplate?tokenAPI='.$tokenAPI.'&cryptKey='.$cryptKey;
        
        $postdata = json_encode($data);
        
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        $result = curl_exec($ch);
        curl_close($ch);

        return $result;
   }

   public function setTag($uri,$id_document,$tokenAPI,$cryptKey,$data)
   {
        $url = $uri.'tags/'.$id_document.'/add?tokenAPI='.$tokenAPI.'&cryptKey='.$cryptKey;
        
        $postdata = json_encode($data);
        
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        $result = curl_exec($ch);
        curl_close($ch);

        return $result;
   }

   public function setSignatory($uri,$id_document,$tokenAPI,$cryptKey,$data)
   {
        $url = $uri.'documents/'.$id_document.'/createlist?tokenAPI='.$tokenAPI.'&cryptKey='.$cryptKey;
        
        $postdata = json_encode($data);
        
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        $result = curl_exec($ch);
        curl_close($ch);

        return $result;
   }

   public function setWebhook($uri,$id_document,$tokenAPI,$cryptKey,$data)
   {
        $url = $uri.'documents/'.$id_document.'/webhooks?tokenAPI='.$tokenAPI.'&cryptKey='.$cryptKey;
        
        $postdata = json_encode($data);
        
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        $result = curl_exec($ch);
        curl_close($ch);

        return $result;
   }

   public function sendDocument($uri,$id_document,$tokenAPI,$cryptKey,$data)
   {
        $url = $uri.'documents/'.$id_document.'/sendtosigner?tokenAPI='.$tokenAPI.'&cryptKey='.$cryptKey;
        
        $postdata = json_encode($data);
        
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        $result = curl_exec($ch);
        curl_close($ch);

        return $result;
   }

	public function updateInvoiceContract($invoice,$status,$uid = NULL,$key_signer = NULL)
	{
		$this->db_TbFatura = new Application_Model_DbTable_TbFatura();
		
		try{
               
			$dados = array('fat_contrato' => $status,'fat_contrato_id' => $uid,'fat_key_signer' => $key_signer);
               $where = $this->db_TbFatura->getAdapter()->quoteInto("fat_id=?",$invoice);
               $this->db_TbFatura->update($dados, $where);
			  
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error 20x3");
			
		}
     }

	public function updateAgentContract($user,$status,$uid = NULL,$key_signer = NULL)
	{
		$this->db = new Application_Model_DbTable_TbUser();
		
		try{
               
			$dados = array('usr_contract' => $status,'usr_contract_id' => $uid,'usr_key_signer' => $key_signer);
               $where = $this->db->getAdapter()->quoteInto("usr_id=?",$user);
               $this->db->update($dados, $where);

               $retorno = "Status: ".$status." | Uid: ".$uid." | Key: ".$key_signer;
               $this->setLogAgent($user,$retorno);
			  
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error 20x3");
			
		}
     }

	public function setLog($user,$retorno)
	{
		$this->modelAccount = new Application_Model_Account();
		
		try{
               
               $requestLogAlteraDados = array();
               $requestLogAlteraDados['lad_descricao'] = "Generate contract return: '" . $retorno ."'";
               $requestLogAlteraDados['lad_idUsuario'] = $user;
               $requestLogAlteraDados['lad_ip_acesso'] = $_SERVER['REMOTE_ADDR'];
               
               $this->modelAccount->setLogAlteraDados($requestLogAlteraDados);
			  
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error 20x4");
			
		}
     }

	public function setLogAgent($user,$retorno)
	{
		$this->modelAccount = new Application_Model_Account();
		
		try{
               
               $requestLogAlteraDados = array();
               $requestLogAlteraDados['lad_descricao'] = "Agent contract: '" . $retorno ."'";
               $requestLogAlteraDados['lad_idUsuario'] = $user;
               $requestLogAlteraDados['lad_ip_acesso'] = $_SERVER['REMOTE_ADDR'];
               
               $this->modelAccount->setLogAlteraDados($requestLogAlteraDados);
			  
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error 20x4");
			
		}
     }
     

	
}