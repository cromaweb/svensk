<?php

class Application_Model_Treeview
{
	public function __construct()
	{
		
	  $this->db_TbProduct = new Application_Model_DbTable_TbProduct();
	  $this->db_TbBnsTitulo = new Application_Model_DbTable_TbBnsTitulo();
	  $this->db_TbOrganizacao = new Application_Model_DbTable_TbOrganizacao();
	  $this->db_TbContpernaAws = new Application_Model_DbTable_TbContpernaAws();
	  $this->db_TbUser = new Application_Model_DbTable_TbUser();
	  $this->modelAccount = new Application_Model_Account();
	  
	}
	
	public function getPackageName($id)
	{
		try
		{
			if(!is_null($id)){
				$select = $this->db_TbProduct->select()
										->from($this->db_TbProduct,array('prod_titulo','prod_imagem'))
										->where("prod_id = $id");
				return $this->db_TbProduct->fetchRow($select);
			}
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error 220");
			
		}
	}
	
	public function getPackageImage($id)
	{
		try
		{
			if(!is_null($id)){
			  $select = $this->db_TbProduct->select()
										->from($this->db_TbProduct,'prod_imagem')
										->where("prod_id = $id");
			  $return = $this->db_TbProduct->fetchRow($select);
			  return $return["prod_imagem"];
			}
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error 221");
			
		}
	}
	
	public function getTitleName($id)
	{
		try
		{
			$select = $this->db_TbBnsTitulo->select()
									->from($this->db_TbBnsTitulo,'tit_nome')
									->where("tit_id = $id");
			$return = $this->db_TbBnsTitulo->fetchRow($select);
			return $return["tit_nome"];
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error 222");
			
		}
	}
	
	public function getChildOrg($dad,$leg = null)
	{
		try
		{
			$select = $this->db_TbOrganizacao->select()
									->from($this->db_TbOrganizacao,'org_idUsuario');
			if(!is_null($leg)){
			 $select->where("org_pai = '$dad' AND org_perna = '$leg'");
			}else{
			 $select->where("org_pai = $dad");
			}
			
			$return = $this->db_TbOrganizacao->fetchRow($select);
			return $return["org_idUsuario"];
	  
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error 223");
			
		}
	}
	
	public function getChildrenOrg($dad)
	{
		try
		{
			$select = $this->db_TbOrganizacao->select()
									->from($this->db_TbOrganizacao,'org_idUsuario')
									->where("org_pai = $dad");
			
			return $this->db_TbOrganizacao->fetchAll($select);
	  
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error 224");
			
		}
	  
	}
	
	public function getDadOrg($user)
	{
		try
		{
			$select = $this->db_TbOrganizacao->select()
									->from($this->db_TbOrganizacao,'org_pai')
									->where("org_idUsuario = $user");
			
			$return = $this->db_TbOrganizacao->fetchRow($select);
			return $return["org_pai"];
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error 225");
			
		}
	}
	
	public function getCheckLeg($id = null,$dad = null)
	{
		if(!is_null($id) && !is_null($dad))
		{
			try
			{
				$select = $this->db_TbContpernaAws->select()
										->from($this->db_TbContpernaAws,array('amount'=>'COUNT(*)'))
										->where("contp_idUsuario='$id' AND contp_idAntecessor = '$dad'");
				$return = $this->db_TbContpernaAws->fetchAll($select);
				return $return["amount"];
				
			}catch(Exception $e){
				//die($e->getMessage());
				die("Error 226");
				
			}
		}
	}
	
	public function getCountLegsRight($id = null)
	{
		if(!is_null($id))
		{
			try{
			  $select = $this->db_TbContpernaAws->select()
										->from($this->db_TbContpernaAws,array('amount'=>'COUNT(*)'))
										->where("contp_idAntecessor = '$id' AND contp_perna = 'D'");
			  $return = $this->db_TbContpernaAws->fetchRow($select);
			  
			  return $return["amount"];
			  
			}catch(Exception $e){
				//die($e->getMessage());
				die("Error 227");
			}
		}
	}
	
	public function getCountLegsLeft($id = null)
	{
		if(!is_null($id))
		{
			try
			{
				$select = $this->db_TbContpernaAws->select()
										->from($this->db_TbContpernaAws,array('amount'=>'COUNT(*)'))
										->where("contp_idAntecessor = '$id' AND contp_perna = 'E'");
				$return = $this->db_TbContpernaAws->fetchRow($select);
				
				return $return["amount"];
				
			}catch(Exception $e){
				//die($e->getMessage());
				die("Error 228");
			}
		}
	}
	
	public function getSearch($id = null,$dad = null,$name = null,$login = null)
	{
		if(!is_null($id) || !is_null($dad) || !is_null($name) || !is_null($login))
		{
			try
			{
			  	 $db = Zend_Db_Table_Abstract::getDefaultAdapter();
				 $sql =  "SELECT usr_id,usr_login_id,usr_name,usr_last_name FROM tb_contperna_aws 
								INNER JOIN tb_user 
								ON contp_idUsuario=usr_id ";
				 					
				if(!is_null($id) && !is_null($dad)){
					$sql .= "AND contp_idUsuario='$id'
							AND contp_idAntecessor='$dad'";
				}elseif(!is_null($dad) && !is_null($name)){
					$sql .= "AND contp_idAntecessor='$dad' AND (usr_name LIKE '$name%' OR usr_last_name LIKE '$name%' OR usr_login_id LIKE '$name%')";
				}elseif(!is_null($dad) && !is_null($login)){
					$sql .= "AND usr_login_id='$login' AND contp_idAntecessor='$dad'";
				}
				
				$stmt = $db->query($sql);
				return $stmt->fetchAll();
			  
			}catch(Exception $e){
				//die($e->getMessage());
				die("Error 229");
			}
		}
	}
}

