<?php

class Application_Model_Myenrollees
{
	public function __construct()
	{
	  //$this->db_TbOrganizacao = new Application_Model_DbTable_TbOrganizacao();
	  //$this->db_TbUser = new Application_Model_DbTable_TbUser();
	  //$this->db = Zend_Db_Table_Abstract::getDefaultAdapter();
	  
	}
	
	public function getMyEnrollees($id)
	{
	   if(!is_null($id)){

		$db = Zend_Db_Table_Abstract::getDefaultAdapter();
		$select = $db->select();
		$select->from('tb_user', array('usr_login_id', 'usr_name', 'usr_email', 'usr_phone','usr_reg_date','usr_rede','sum' => '(SELECT sum(uplan_value) FROM tb_user_plans WHERE uplan_user_id=usr_id AND uplan_status=1)' ));
		$select->where('usr_active <> ?',"d");
		$select->where('usr_invited_id = ?',$id);
		
		$select->order('usr_reg_date DESC');
		
		$result = $db->fetchAll($select);
		//$result = $select->__toString();
	
	   }
	   return $result;
	}

	public function getTotalEnrollees()
	{

		try{
			$db = Zend_Db_Table_Abstract::getDefaultAdapter();
			$select = $db->select();
			$select->from('tb_user', array('usr_login_id', 'usr_name', 'usr_email', 'usr_phone','usr_reg_date','usr_rede','sum' => '(SELECT sum(uplan_value) FROM tb_user_plans WHERE uplan_user_id=usr_id AND uplan_status=1)' ));
			$select->where('usr_active <> ?',"d");
			
			$select->order('usr_reg_date DESC');
			
			$result = $db->fetchAll($select);
			//$result = $select->__toString();
			
		
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error 186 ");
		}

	   return $result;
	}

	public function getTotalAgents()
	{

		try{

			$db = Zend_Db_Table_Abstract::getDefaultAdapter();
			$select = $db->select();
			$select->from('tb_user', array('amount' => 'COUNT(usr_id)'));
			$select->where('usr_active <> ?',"d");
			$select->where('usr_rede = ?',1);
			
			$return = $db->fetchRow($select);
			return $return["amount"];
			
		
		}catch(Exception $e){
			//die($e->getMessage());
			die("Error 186++++ ");
		}

	}

}

