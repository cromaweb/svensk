<?php

class Application_Model_DbTable_TbOrganizacao extends Zend_Db_Table_Abstract
{

    protected $_name = 'tb_organizacao';
	protected $_primary = 'org_idUsuario';
	
    /**
    * Reference map
    */
    protected $_referenceMap = array
    (
        array(
            'refTableClass' => 'Application_Model_DbTable_TbUser',
            'refColumns' => 'usr_id',
            'columns' => 'org_idUsuario',
        )
    );

}

