<?php

class Application_Model_DbTable_TbContpernaAws extends Zend_Db_Table_Abstract
{

    protected $_name = 'tb_contperna_aws';
	protected $_primary = 'contp_id';
	
    /**
    * Reference map
    */
    protected $_referenceMap = array
    (
        array(
            'refTableClass' => 'Application_Model_DbTable_TbUser',
            'refColumns' => 'usr_id',
            'columns' => 'contp_idUsuario',
        )
    );


}

