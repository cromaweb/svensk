<?php

class Application_Model_Routines
{
	public function __construct()
	{
	  $this->db_TbWalletAddresses = new Application_Model_DbTable_TbWalletAddresses();
	  $this->db_TbWalletTransactions = new Application_Model_DbTable_TbWalletTransactions();
	  
	}
	
	public function getUsedAddresses()
	{
	   
	   try{

			$select = $this->db_TbWalletAddresses->select();
			$select->from($this->db_TbWalletAddresses, array('wallet_address_id', 'wallet_address_address', 'wallet_address_user_id', 'wallet_address_balance'));
			$select->where('wallet_address_user_id<>0');
			$result = $this->db_TbWalletAddresses->fetchAll($select);
		
		}catch(Exception $e){
			//die($e->getMessage());
			die("Routines - Error 186");
		}

	   return $result;
	}
	
	public function getWalletTransaction($hash)
	{
	   try
	   {
			    
			$select = $this->db_TbWalletTransactions->select()
						 ->from($this->db_TbWalletTransactions,array('wallet_transaction_id'))
						 ->where("wallet_transaction_hash = ?",$hash);
						 
			return $this->db_TbWalletTransactions->fetchRow($select);
			

		}catch(Exception $e){
			die("Error 156");
			//die($e->getMessage());
			
		}
	}
	
   public function setWalletTransaction(array $request)
   {

	   try
	   {

	  $dados = array(
		'wallet_transaction_wallet_id' => $request['wallet_transaction_wallet_id'],
		'wallet_transaction_hash' => $request['wallet_transaction_hash'],
		'wallet_transaction_from' => $request['wallet_transaction_from'],
		'wallet_transaction_to' => $request['wallet_transaction_to'],
		'wallet_transaction_operation' => $request['wallet_transaction_operation'],
		'wallet_transaction_amount' => $request['wallet_transaction_amount'],
		'created_at' => new Zend_Db_Expr('NOW()'),
		'updated_at' => new Zend_Db_Expr('NOW()')
      );
      return $this->db_TbWalletTransactions->insert($dados);
	  
		}catch(Exception $e){
			//die("Error 103");
			die($e->getMessage());
		}
   }

}

