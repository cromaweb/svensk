<?php

class Application_Model_News
{
	public function __construct()
	{
	  $this->db_TbNews = new Application_Model_DbTable_TbNews();
     $this->db_TbNewsCategory = new Application_Model_DbTable_TbNewsCategory();
	}

	public function getAllNews()
   {
		$select = $this->db_TbNews->select()
							->where("news_status = 'A'")
                     ->order("news_date desc");

		return $this->db_TbNews->fetchAll($select)->toArray();
   }

   public function getNews($id)
   {
      $select = $this->db_TbNews->select()
                     ->where("news_status = 'A' AND news_id = " . $id)
                     ->order("news_date desc");

      return $this->db_TbNews->fetchAll($select)->toArray();
   }

	

}

