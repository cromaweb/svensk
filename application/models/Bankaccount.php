<?php

class Application_Model_Bankaccount
{
	public function __construct()
	{
	  	$this->db_bank_account = new Application_Model_DbTable_TbUserBankaccount();
	}

	public function getAll($user)
	{
	   try
	   {
			$select = $this->db_bank_account->select();
			$select->where("user_id = " . $user . " AND deleted = 0");
			return $this->db_bank_account->fetchAll($select)->toArray();
		}catch(Exception $e){
			die("Error 146");
			
		}
	}

   public function getBankaccount($id,$user)
   {
	   try
	   {
		  $select = $this->db_bank_account->select();
		  $select->where("id = " . $id . " AND user_id = " . $user . " AND deleted = 0");
		  return $this->db_bank_account->fetchRow($select);
		}catch(Exception $e){
			die("Error 147");
			
		}
	  
   }

   public function setBankaccount($user, $description, $account)
   {
	   try
	   {
			$data = array(
			 'user_id' => $user,
			 'description' => $description,
			 'account_number' => $account
			);
			return $this->db_bank_account->insert($data);
		}catch(Exception $e){
			die("Error 148");
			
		}
   }

   public function delete($user, $id) 
   {
	   try
	   {
			$dados = array("deleted" => 1);
			$where = $this->db_bank_account->getAdapter()->quoteInto("id = ? and user_id = ".$user."", $id);
			$this->db_bank_account->update($dados, $where);
		}catch(Exception $e){
			die("Error 149");
			
		}
   }

}

