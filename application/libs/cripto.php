<?php 
	function cript($str, $shift)
	{
		$char = range('a', 'z');
		$nume = range(0, 9);
		
		$flip1 = array_flip($char);
		$flip2 = array_flip($nume);
		
		for ($i = 0; $i < strlen($str); $i++)
		{
			if (in_array(strtolower($str[$i]), $char))
			{
				$ord = $flip1[strtolower($str{$i})];
				
				$ord = ($ord + $shift) % 26;
				
				if ($ord < 0) $ord += 26;
				
				$str{$i} = ($str{$i} == strtolower($str{$i})) ? $char[$ord] : strtoupper($char[$ord]);
			}
			else
			{
				if (in_array($str[$i], $nume))
				{
				$ord = $flip2[strtolower($str{$i})];
				
				$ord = ($ord + $shift) % 10;
				
				if ($ord < 0) $ord += 10;
				
				$str{$i} = ($str{$i} == $str{$i}) ? $nume[$ord] : $nume[$ord];
				}
			}
		}
		return $str;
	}
?> 